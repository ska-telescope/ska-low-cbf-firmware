----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 08/14/2023 10:14:24 PM
-- Module Name: ct1_tb - Behavioral
-- Description: 
--  Standalone testbench for PST corner turn 2 + beamformer
-- 
----------------------------------------------------------------------------------

library IEEE, correlator_lib, ct_lib, BF_lib, common_lib, filterbanks_lib, pst_lib, tbmodels_lib, PSR_Packetiser_lib, technology_lib;
use IEEE.STD_LOGIC_1164.ALL;
Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;
use IEEE.std_logic_textio.all;
USE ct_lib.ct2_reg_pkg.ALL;
USE common_lib.common_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;
USE technology_lib.tech_mac_100g_pkg.ALL;
--library DSP_top_lib;
--use DSP_top_lib.DSP_top_pkg.all;

entity pst_ct2_beamformer_tb is
    generic(
        
--        -----------------------------------------------------------------------------------------------
--        -- test 4 (4 stations, 2 coarse channels)
--        -----------------------------------------------------------------------------------------------
--        -- Number of virtual channels to use
--        g_STATIONS : integer := 4;
--        g_COARSE_CHANNELS : integer := 2;
--        g_FRAME_GAP : integer := 1000;
--        -- 
--        g_FRAME_COUNT_START : std_logic_vector(39 downto 0) := x"00000010FF"; -- x"00000000104E";
--        -- register configuration
--        g_POLY_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001000";  -- only bits 36:0 are used.
--        g_POLY_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
--        g_POLY_BUF0_OFFSET_NS : std_logic_vector(31 downto 0)            := x"02625A00";
        
--        g_POLY_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001100";
--        g_POLY_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000200";
--        g_POLY_BUF1_OFFSET_NS : std_logic_vector(31 downto 0)            := x"00000432";
        
--        g_JONES_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000000000";
--        g_JONES_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00001100";
--        g_JONES_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001050";
--        g_JONES_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
        
--        g_POLYNOMIAL_INIT_FILENAME : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test4_poly.txt";
--        g_JONES_INIT_FILENAME      : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test4_jones.txt";
--        g_BF_OUT_FILENAME          : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test4_pst_packets.txt";

--        g_PST_BEAMS : integer := 2; -- Generic configures number of beamformers instantiated
--        g_BEAMS_ENABLED : std_logic_vector(31 downto 0) := x"00000002"; -- register setting in CT2
--        g_SCALE_FACTOR : std_logic_vector(31 downto 0) := x"00000000"   -- Single precision floating point scale factor to use in the beamformer, 0 = firmware calculated.
--        -----------------------------------------------------------------------------------------------

        -----------------------------------------------------------------------------------------------
        -- test 8 (8 stations, 1 coarse channel)
        -----------------------------------------------------------------------------------------------
--        -- Number of virtual channels to use
--        g_STATIONS : integer := 8;
--        g_COARSE_CHANNELS : integer := 1;
--        g_FRAME_GAP : integer := 1000;
--        -- 
--        g_FRAME_COUNT_START : std_logic_vector(39 downto 0) := x"00000010FF"; -- x"00000000104E";
--        -- register configuration
--        g_POLY_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001000";  -- only bits 36:0 are used.
--        g_POLY_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
--        g_POLY_BUF0_OFFSET_NS : std_logic_vector(31 downto 0)            := x"02625A00";
        
--        g_POLY_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001100";
--        g_POLY_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000200";
--        g_POLY_BUF1_OFFSET_NS : std_logic_vector(31 downto 0)            := x"00000432";
        
--        g_JONES_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000000000";
--        g_JONES_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00001100";
--        g_JONES_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001050";
--        g_JONES_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
        
--        g_POLYNOMIAL_INIT_FILENAME : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test8_poly.txt";
--        g_JONES_INIT_FILENAME      : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test8_jones.txt";
--        g_BF_OUT_FILENAME          : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test8_pst_packets.txt";

--        g_PST_BEAMS : integer := 2; -- Generic configures number of beamformers instantiated
--        g_BEAMS_ENABLED : std_logic_vector(31 downto 0) := x"00000002"; -- register setting in CT2
--        g_SCALE_FACTOR : std_logic_vector(31 downto 0) := x"00000000"   -- Single precision floating point scale factor to use in the beamformer, 0 = firmware calculated.
        -----------------------------------------------------------------------------------------------

        -----------------------------------------------------------------------------------------------
        -- test 8 (8 stations, 1 coarse channel)
        -----------------------------------------------------------------------------------------------
        -- Number of virtual channels to use
--        g_STATIONS : integer := 8;
--        g_COARSE_CHANNELS : integer := 1;
--        g_FRAME_GAP : integer := 1000;
--        -- 
--        g_FRAME_COUNT_START : std_logic_vector(39 downto 0) := x"00000010FF"; -- x"00000000104E";
--        -- register configuration
--        g_POLY_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001000";  -- only bits 36:0 are used.
--        g_POLY_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
--        g_POLY_BUF0_OFFSET_NS : std_logic_vector(31 downto 0)            := x"02625A00";
        
--        g_POLY_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001100";
--        g_POLY_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000200";
--        g_POLY_BUF1_OFFSET_NS : std_logic_vector(31 downto 0)            := x"00000432";
        
--        g_JONES_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000000000";
--        g_JONES_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00001100";
--        g_JONES_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001050";
--        g_JONES_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
        
--        g_POLYNOMIAL_INIT_FILENAME : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test8_poly.txt";
--        g_JONES_INIT_FILENAME      : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test8_jones.txt";
--        g_BF_OUT_FILENAME          : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test8_pst_packets.txt";

--        g_PST_BEAMS : integer := 2; -- Generic configures number of beamformers instantiated
--        g_BEAMS_ENABLED : std_logic_vector(31 downto 0) := x"00000001"; -- register setting in CT2
--        g_SCALE_FACTOR : std_logic_vector(31 downto 0) := x"00000000"   -- Single precision floating point scale factor to use in the beamformer, 0 = firmware calculated.
        -----------------------------------------------------------------------------------------------
        g_STATIONS : integer := 4;
        g_COARSE_CHANNELS : integer := 132;
        g_FRAME_GAP : integer := 1000;
        -- 
        g_FRAME_COUNT_START : std_logic_vector(39 downto 0) := x"00000010FF"; -- x"00000000104E";
        -- register configuration
        g_POLY_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001000";  -- only bits 36:0 are used.
        g_POLY_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
        g_POLY_BUF0_OFFSET_NS : std_logic_vector(31 downto 0)            := x"02625A00";
        
        g_POLY_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001100";
        g_POLY_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000200";
        g_POLY_BUF1_OFFSET_NS : std_logic_vector(31 downto 0)            := x"00000432";
        
        g_JONES_BUF0_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000000000";
        g_JONES_BUF0_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00001100";
        g_JONES_BUF1_FRAME_START : std_logic_vector(63 downto 0)  := x"0000000000001050";
        g_JONES_BUF1_VALID_DURATION : std_logic_vector(31 downto 0)       := x"00000100";
        
        g_POLYNOMIAL_INIT_FILENAME : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test8_poly.txt";
        g_JONES_INIT_FILENAME      : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test8_jones.txt";
        g_BF_OUT_FILENAME          : string := "/home/hum089/projects/perentie/ska-low-cbf-fw-pst/designs/pst/src/test_ct2_bf/test8_4stations_pst_packets.txt";

        g_PST_BEAMS : integer := 2; -- Generic configures number of beamformers instantiated
        g_BEAMS_ENABLED : std_logic_vector(31 downto 0) := x"00000002"; -- register setting in CT2
        g_SCALE_FACTOR : std_logic_vector(31 downto 0) := x"00000000"   -- Single precision floating point scale factor to use in the beamformer, 0 = firmware calculated.

        
    );
end pst_ct2_beamformer_tb;

architecture Behavioral of pst_ct2_beamformer_tb is

    function get_axi_size(AXI_DATA_WIDTH : integer) return std_logic_vector is
    begin
        if AXI_DATA_WIDTH = 8 then
            return "000";
        elsif AXI_DATA_WIDTH = 16 then
            return "001";
        elsif AXI_DATA_WIDTH = 32 then
            return "010";
        elsif AXI_DATA_WIDTH = 64 then
            return "011";
        elsif AXI_DATA_WIDTH = 128 then
            return "100";
        elsif AXI_DATA_WIDTH = 256 then
            return "101";
        elsif AXI_DATA_WIDTH = 512 then
            return "110";    -- size of 6 indicates 64 bytes in each beat (i.e. 512 bit wide bus) -- out std_logic_vector(2 downto 0);
        elsif AXI_DATA_WIDTH = 1024 then
            return "111";
        else
            assert FALSE report "Bad AXI data width" severity failure;
            return "000";
        end if;
    end get_axi_size;
    constant M02_DATA_WIDTH : integer := 512;
    -- c_minGapData is a bit more than 0x2000 used in CT2, total clocks per CT2 frame needs to be this * beams * coarse channels.
    -- It counts against the 450MHz filterbank clock vs the 400 MHz beamformer clock, so needs to be larger than the
    -- beamformer gap by a factor of 450/400
    constant c_minGapData  : integer  := 9450;  
    
    constant c_VIRTUAL_CHANNELS : integer := g_STATIONS * g_COARSE_CHANNELS;
    
    signal m02_axi_awvalid : std_logic;
    signal m02_axi_awready : std_logic;
    signal m02_axi_awaddr : std_logic_vector(31 downto 0);
    signal m02_axi_awid : std_logic_vector(0 downto 0);
    signal m02_axi_awlen : std_logic_vector(7 downto 0);
    signal m02_axi_awsize : std_logic_vector(2 downto 0);
    signal m02_axi_awburst : std_logic_vector(1 downto 0);
    signal m02_axi_awlock :  std_logic_vector(1 downto 0);
    signal m02_axi_awcache :  std_logic_vector(3 downto 0);
    signal m02_axi_awprot :  std_logic_vector(2 downto 0);
    signal m02_axi_awqos :  std_logic_vector(3 downto 0);
    signal m02_axi_awregion :  std_logic_vector(3 downto 0);
    signal m02_axi_wready :  std_logic;
    signal m02_axi_wstrb :  std_logic_vector(63 downto 0);
    signal m02_axi_bvalid : std_logic;
    signal m02_axi_bready :  std_logic;
    signal m02_axi_bresp :  std_logic_vector(1 downto 0);
    signal m02_axi_bid :  std_logic_vector(0 downto 0);
    signal m02_axi_arvalid :  std_logic;
    signal m02_axi_arready :  std_logic;
    signal m02_axi_araddr :  std_logic_vector(31 downto 0);
    signal m02_axi_arid :  std_logic_vector(0 downto 0);
    signal m02_axi_arlen :  std_logic_vector(7 downto 0);
    signal m02_axi_arsize :  std_logic_vector(2 downto 0);
    signal m02_axi_arburst : std_logic_vector(1 downto 0);
    signal m02_axi_arlock :  std_logic_vector(1 downto 0);
    signal m02_axi_arcache :  std_logic_vector(3 downto 0);
    signal m02_axi_arprot :  std_logic_Vector(2 downto 0);
    signal m02_axi_arqos :  std_logic_vector(3 downto 0);
    signal m02_axi_arregion :  std_logic_vector(3 downto 0);
    signal m02_axi_rvalid :  std_logic;
    signal m02_axi_rready :  std_logic;
    signal m02_axi_rdata :  std_logic_vector((M02_DATA_WIDTH-1) downto 0);
    signal m02_axi_rlast :  std_logic;
    signal m02_axi_rid :  std_logic_vector(0 downto 0);
    signal m02_axi_rresp :  std_logic_vector(1 downto 0);

    signal clk300, clk300_rst, data_rst : std_logic := '0';
    signal clk400 : std_logic := '0';
    signal clk450 : std_logic := '0';
    
    signal axi_lite_mosi : t_axi4_lite_mosi;
    signal axi_lite_miso : t_axi4_lite_miso;
    signal axi_full_mosi : t_axi4_full_mosi;
    signal axi_full_miso : t_axi4_full_miso;
    signal axi_full_bf_mosi : t_axi4_full_mosi;
    signal axi_full_bf_miso : t_axi4_full_miso;
    
    signal pkt_packet_count : integer;
    signal pkt_virtual_channel : integer;
    signal pkt_wait_count : integer := 0;
    signal pkt_valid : std_logic := '0';
    signal absolute_sample : std_logic_vector(52 downto 0);
    
    signal write_HBM_to_disk : std_logic;
    signal hbm_dump_addr : integer;
    signal hbm_dump_filename, init_fname : string(1 to 9) := "dummy.txt";
    signal init_mem : std_logic;
    signal rst_n : std_logic;
    
    signal FD_virtualChannel : t_slv_16_arr(2 downto 0); -- 3 virtual channels, one for each of the PST data streams.
    signal FD_headerValid : std_logic_vector(2 downto 0);
    
    signal FD_frameCount : std_logic_vector(36 downto 0);
    
    type fb_pkt_fsm_type is (idle, send_pkt, inter_packet_gap, update_counts, pkt_gap, pkt_SOF, pkt_SOF_wait);
    signal fb_pkt_fsm : fb_pkt_fsm_type := idle;
    signal pkt_frame_count : std_logic_vector(36 downto 0);
    signal pkt_fine_channel : integer;
    
    signal CT_sofFinal : std_logic := '0';
    signal FD_data : t_slv_64_arr(2 downto 0);
    signal FD_dataValid : std_logic := '0';
    signal totalStations, totalCoarse, totalChannels : std_logic_vector(11 downto 0);
    signal m02_axi_wdata : std_logic_vector(511 downto 0);
    signal m02_axi_wlast, m02_axi_wvalid : std_logic;
    signal BFdataIn : std_logic_vector(191 downto 0);
    signal BFflagged : std_logic_vector(2 downto 0);
    signal BFFine : std_logic_vector(7 downto 0);
    signal BFCoarse : std_logic_vector(9 downto 0);
    signal BFFirstStation, BFLastStation : std_logic;
    signal BFTimeStep : std_logic_vector(4 downto 0);
    signal BFVirtualChannel : std_logic_vector(9 downto 0);
    signal BFPacketCount : std_logic_vector(39 downto 0);
    signal BFValidIn : std_logic;
    signal BF_phase_virtualChannel : std_logic_vector(9 downto 0);
    signal BF_phase_timeStep : std_logic_vector(7 downto 0);
    signal BF_phase_beam : std_logic_vector(3 downto 0);
    signal BF_phase : std_logic_vector(23 downto 0);
    signal BF_phase_step : std_logic_vector(23 downto 0);
    signal BF_phase_valid, BF_phase_clear : std_logic;
    signal BFJonesBuffer : std_logic;
    signal BFJones_status : std_logic_vector(1 downto 0);
    signal BFpoly_ok : std_logic_vector(1 downto 0);
    signal BFBeamsEnabled : std_logic_vector(7 downto 0);
    signal BF_scale_exp_frac : std_logic_vector(7 downto 0);
    signal dataMisMatch, dataMisMatchBFclk : std_logic;
    
    signal beamData : std_logic_vector(63 downto 0);
    signal beamPacketCount : std_logic_vector(39 downto 0);
    signal beamBeam : std_logic_vector(7 downto 0);
    signal beamFreqIndex : std_logic_vector(10 downto 0);
    signal beamValid : std_logic;
    signal beamJonesStatus : std_logic_vector(1 downto 0);
    signal beamPoly_ok : std_logic_vector(1 downto 0);
    signal bdbg_ILA_trigger : std_logic;
    
    signal axi_lite_done : std_logic := '0';
    signal ct2_axi_full_done : std_logic := '0';
    signal bf_axi_full_done : std_logic := '0';
    signal frame_gap_count, frame_gap : integer;
    signal BFPacketOdd : std_logic := '0';
    signal bad_polynomials : std_logic;
    
    signal clock_322     : std_logic := '0';
    signal clock_322_rst : std_logic := '1';
    signal clock_300_rst : std_logic := '1';
    signal clock_400_rst : std_logic := '1';
    signal testCount_322 : integer   := 0;
    signal testCount_300 : integer   := 0;
    signal power_up_rst_clock_322   : std_logic_vector(31 downto 0) := (others => '1');
    signal power_up_rst_clock_300   : std_logic_vector(31 downto 0) := (others => '1');
    signal power_up_rst_clock_400   : std_logic_vector(31 downto 0) := (others => '1');
    signal o_data_to_transmit      : t_lbus_sosi;
    signal i_data_to_transmit_ctl  : t_lbus_siso;
    
    signal packetiser_ctrl          : packetiser_stream_ctrl;
    signal packetiser_config        : packetiser_config_in;
    
    signal beamformer_to_packetiser_data    :  packetiser_stream_in; 
    signal beamformer_to_packetiser_data_2  :  packetiser_stream_in;
    signal beamformer_to_packetiser_data_3  :  packetiser_stream_in;
     
    signal packet_stream_stats              :  t_packetiser_stats(2 downto 0);
    
    signal packetiser_stream_1_host_bus_in  : packetiser_config_in;
    signal packetiser_stream_2_host_bus_in  : packetiser_config_in;
    signal packetiser_stream_3_host_bus_in  : packetiser_config_in;
      
    signal packetiser_host_bus_out          : packetiser_config_out;  
    signal packetiser_host_bus_out_2        : packetiser_config_out;
    signal packetiser_host_bus_out_3        : packetiser_config_out;  
    
    signal packetiser_host_bus_ctrl         :  packetiser_stream_ctrl;
    signal packetiser_host_bus_ctrl_2       :  packetiser_stream_ctrl;
    signal packetiser_host_bus_ctrl_3       :  packetiser_stream_ctrl;
    signal i_tx_axis_tready : std_logic;
    
    -- AXI to CMAC interface to be implemented
    signal o_tx_axis_tdata          : STD_LOGIC_VECTOR(511 downto 0);
    signal o_tx_axis_tkeep          : STD_LOGIC_VECTOR(63 downto 0);
    signal o_tx_axis_tvalid         : STD_LOGIC;
    signal o_tx_axis_tlast          : STD_LOGIC;
    signal o_tx_axis_tuser          : STD_LOGIC;
    
    signal pkt_word : std_logic_vector(15 downto 0) := x"0000";
    
    signal tdata_del : std_logic_vector(511 downto 0);
    signal d_samples : t_slv_32_arr(15 downto 0);
    signal pkt_data_word : std_logic_vector(15 downto 0);
    signal d_samples_pol : std_logic;
    signal d_samples_fine : std_logic_vector(13 downto 0);
    
    signal count1, count2, count3, count4, count5 : std_logic_vector(15 downto 0) := x"0000";
    
    signal beam_output_stream_1 : t_bf_output_stream;
    signal beam_output_stream_2 : t_bf_output_stream;

    
begin
    
    clk300 <= not clk300 after 1.666 ns;
    clock_322 <= not clock_322 after 3.11 ns;
    clk400 <= not clk400 after 1.25 ns;
    clk450 <= not clk450 after 1.111 ns;
    
    rst_n <= not clk300_rst;
    
    -------------------------------------------------------------------------------
    -- Programming of the corner turn 2 axi-lite registers :
    --    - jones_buffer0_valid_frame_low, jones_buffer0_valid_frame_high, jones_buffer0_valid_duration
    --    - jones_buffer1_valid_frame_low, jones_buffer1_valid_frame_high, jones_buffer1_valid_duration
    --    - poly_buffer0_info_valid, poly_buffer0_valid_frame_low, poly_buffer0_valid_frame_high, poly_buffer0_valid_duration, poly_buffer0_offset_ns
    --    - poly_buffer1_info_valid, poly_buffer1_valid_frame_low, poly_buffer1_valid_frame_high, poly_buffer1_valid_duration, poly_buffer1_offset_ns
    --    - BeamsEnabled
    
    --
    process
        file RegCmdfile: TEXT;
        variable RegLine_in : Line;
        variable regData, regAddr : std_logic_vector(31 downto 0);
        variable RegGood : boolean;
        variable c : character;
    begin
        clk300_rst <= '1';
        axi_lite_done <= '0';
        -- axi_lite for writes to corner turn 2
        axi_lite_mosi.awaddr <= (others => '0');
        axi_lite_mosi.awprot <= "000";
        axi_lite_mosi.awvalid <= '0';
        axi_lite_mosi.wdata <= (others => '0');
        axi_lite_mosi.wstrb <= "1111";
        axi_lite_mosi.wvalid <= '0';
        axi_lite_mosi.bready <= '0';
        axi_lite_mosi.araddr <= (others => '0');
        axi_lite_mosi.arprot <= "000";
        axi_lite_mosi.arvalid <= '0';
        axi_lite_mosi.rready <= '0';
        
        wait until rising_edge(clk300);
        wait until rising_edge(clk300);
        wait for 100 ns;
        wait until rising_edge(clk300);
        clk300_rst <= '0';
        wait until rising_edge(clk300);
        wait until rising_edge(clk300);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer0_valid_frame_low_address.address, true, g_JONES_BUF0_FRAME_START(31 downto 0));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer0_valid_frame_high_address.address, true, g_JONES_BUF0_FRAME_START(63 downto 32));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer0_valid_duration_address.address, true, g_JONES_BUF0_VALID_DURATION);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer1_valid_frame_low_address.address, true, g_JONES_BUF1_FRAME_START(31 downto 0));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer1_valid_frame_high_address.address, true, g_JONES_BUF1_FRAME_START(63 downto 32));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_jones_buffer1_valid_duration_address.address, true, g_JONES_BUF1_VALID_DURATION);
        wait until rising_edge(clk300);
        
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer0_valid_frame_low_address.address, true, g_POLY_BUF0_FRAME_START(31 downto 0));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer0_valid_frame_high_address.address, true, g_POLY_BUF0_FRAME_START(63 downto 32));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer0_valid_duration_address.address, true, g_POLY_BUF0_VALID_DURATION);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer0_offset_ns_address.address, true, g_POLY_BUF0_OFFSET_NS);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer0_info_valid_address.address, true, x"00000001");
        wait until rising_edge(clk300);
        
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer1_valid_frame_low_address.address, true, g_POLY_BUF1_FRAME_START(31 downto 0));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer1_valid_frame_high_address.address, true, g_POLY_BUF1_FRAME_START(63 downto 32));
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer1_valid_duration_address.address, true, g_POLY_BUF1_VALID_DURATION);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer1_offset_ns_address.address, true, g_POLY_BUF1_OFFSET_NS);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_poly_buffer1_info_valid_address.address, true, x"00000001");
        wait until rising_edge(clk300);
        
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_BeamsEnabled_address.address, true, g_BEAMS_ENABLED);
        wait until rising_edge(clk300);
        axi_lite_transaction(clk300, axi_lite_miso, axi_lite_mosi, c_statctrl_scaleFactor_address.address, true, g_SCALE_FACTOR);
        wait until rising_edge(clk300);
        
        axi_lite_done <= '1';
        wait;
    end process;
    
    --------------------------------------------------------------------
    -- Programming of the corner turn 2 axi-full registers
    --  - Holds the polynomial configuration
    --
    process
        file polyConfigfile: TEXT;
        variable PolyLine_in : Line;
        variable polyData, polyAddr : std_logic_vector(31 downto 0);
        variable polyGood : boolean;
        variable c2 : character;
    begin
        ct2_axi_full_done <= '0';
        axi_full_mosi.awaddr <= (others => '0');
        axi_full_mosi.awvalid <= '0';
        -- Always write bursts of 24 bytes = 6 x 4-byte words.
        -- Corresponds to one full polynomial.
        axi_full_mosi.awlen <= "00000101";
        axi_full_mosi.wvalid <= '0';
        axi_full_mosi.wdata <= (others => '0');
        axi_full_mosi.wlast <= '0';
        axi_full_mosi.bready <= '1';
        axi_full_mosi.arvalid <= '0';
        axi_full_mosi.awid <= x"00";
        axi_full_mosi.awprot <= "000";
        axi_full_mosi.awsize <= "010";   -- "010" = 32 bit wide data bus
        axi_full_mosi.awburst <= "01";   -- "01" indicates incrementing addresses for each beat in the burst. 
        axi_full_mosi.awcache <= "0011"; -- "0011" = bufferable transaction
        axi_full_mosi.awuser <= "0000";
        axi_full_mosi.awlock <= '0';
        axi_full_mosi.wid <= "00000000";
        axi_full_mosi.arid <= "00000000";
        axi_full_mosi.araddr <= (others => '0');
        axi_full_mosi.arprot <= "000";
        axi_full_mosi.arlen <= "00000101";
        axi_full_mosi.arsize <= "010";
        axi_full_mosi.arburst <= "01";
        axi_full_mosi.arcache <= "0011";
        axi_full_mosi.aruser <= "0000";
        axi_full_mosi.arlock <= '0';
        axi_full_mosi.rready <= '1';
        axi_full_mosi.awregion <= "0000";
        axi_full_mosi.arregion <= "0000";
        axi_full_mosi.arqos <= "0000";
        axi_full_mosi.awqos <= "0000";
        axi_full_mosi.wstrb <= (others => '1');
        
        wait until rising_edge(clk300);
        wait for 100 ns;
        wait until rising_edge(clk300);
        
        -- write configuration data to memory over the axi full interface.
        FILE_OPEN(polyConfigfile, g_POLYNOMIAL_INIT_FILENAME, READ_MODE);
        
        while (not endfile(polyConfigfile)) loop
            -- Get data for 1 source (64 bytes = 16 x 4byte words)
            readline(polyConfigFile, polyLine_in);
            -- drop "[" character that indicates the address
            read(polyLine_in,c2,polyGood);
            -- get the address
            hread(polyLine_in,polyAddr,polyGood);
            axi_full_mosi.awaddr(31 downto 0) <= polyAddr;
            axi_full_mosi.awvalid <= '1';
            wait until (rising_edge(clk300) and axi_full_miso.awready='1');
            wait for 1 ps;
            axi_full_mosi.awvalid <= '0';
            for i in 0 to 5 loop
                readline(polyConfigFile, polyLine_in);
                hread(polyLine_in,polyData,polyGood);
                axi_full_mosi.wvalid <= '1';
                axi_full_mosi.wdata(31 downto 0) <= polyData;
                if i=5 then
                    axi_full_mosi.wlast <= '1';
                else
                    axi_full_mosi.wlast <= '0';
                end if;
                wait until (rising_edge(clk300) and axi_full_miso.wready = '1');
                wait for 1 ps;
            end loop;
            axi_full_mosi.wvalid <= '0';
            wait until rising_edge(clk300);
            wait until rising_edge(clk300);
        end loop;
        wait until rising_edge(clk300);
        wait for 100 ns;
        wait until rising_edge(clk300);
        
        ---------------------------------------------------------------
        -- read back some data 
        ---------------------------------------------------------------
        axi_full_mosi.arlen <= "00000111";
        axi_full_mosi.araddr(31 downto 0) <= (others => '0');
        axi_full_mosi.arvalid <= '1';
        wait until (rising_edge(clk300) and axi_full_miso.arready='1');
        wait for 1 ps;
        axi_full_mosi.arvalid <= '0';
        
        wait until (rising_edge(clk300) and axi_full_miso.rvalid = '1');
        wait for 200 ns;
        wait until rising_edge(clk300);

        ---------------------------------------------------------------
        axi_full_mosi.arlen <= "00000111";
        axi_full_mosi.araddr(31 downto 0) <= x"00000030";
        axi_full_mosi.arvalid <= '1';
        wait until (rising_edge(clk300) and axi_full_miso.arready='1');
        wait for 1 ps;
        axi_full_mosi.arvalid <= '0';
        wait until (rising_edge(clk300) and axi_full_miso.rvalid = '1');
        wait for 200 ns;
        wait until rising_edge(clk300);        

        ---------------------------------------------------------------
        axi_full_mosi.arlen <= "00001111";
        axi_full_mosi.araddr(31 downto 0) <= x"00060000";
        axi_full_mosi.arvalid <= '1';
        wait until (rising_edge(clk300) and axi_full_miso.arready='1');
        wait for 1 ps;
        axi_full_mosi.arvalid <= '0';
        wait until (rising_edge(clk300) and axi_full_miso.rvalid = '1');
        wait for 200 ns;
        wait until rising_edge(clk300);         
        ---------------------------------------------------------------
        
        wait until rising_edge(clk300);
        ct2_axi_full_done <= '1';
        wait;
    end process;
    
    --------------------------------------------------------------------
    -- Programming of the Beamformer axi-full registers
    --  - Holds the jones matrices  
    process
        file JonesCmdfile: TEXT;
        variable RegLine_in : Line;
        variable regData, regAddr : std_logic_vector(31 downto 0);
        variable RegGood : boolean;
        variable c : character;
    begin
        bf_axi_full_done <= '0';
        axi_full_bf_mosi.awaddr <= (others => '0');
        axi_full_bf_mosi.awvalid <= '0';
        -- Always write bursts of 64 bytes = 16 x 4-byte words
        axi_full_bf_mosi.awlen <= "00001111";
        axi_full_bf_mosi.wvalid <= '0';
        axi_full_bf_mosi.wdata <= (others => '0');
        axi_full_bf_mosi.wlast <= '0';
        axi_full_bf_mosi.bready <= '1';
        axi_full_bf_mosi.arvalid <= '0';
        axi_full_bf_mosi.awid <= x"00";
        axi_full_bf_mosi.awprot <= "000";
        axi_full_bf_mosi.awsize <= "010";   -- "010" = 32 bit wide data bus
        axi_full_bf_mosi.awburst <= "01";   -- "01" indicates incrementing addresses for each beat in the burst. 
        axi_full_bf_mosi.awcache <= "0011"; -- "0011" = bufferable transaction
        axi_full_bf_mosi.awuser <= "0000";
        axi_full_bf_mosi.awlock <= '0';
        axi_full_bf_mosi.wid <= "00000000";
        axi_full_bf_mosi.arid <= "00000000";
        axi_full_bf_mosi.araddr <= (others => '0');
        axi_full_bf_mosi.arprot <= "000";
        axi_full_bf_mosi.arlen <= "00010011";
        axi_full_bf_mosi.arsize <= "000";
        axi_full_bf_mosi.arburst <= "01";
        axi_full_bf_mosi.arcache <= "0011";
        axi_full_bf_mosi.aruser <= "0000";
        axi_full_bf_mosi.arlock <= '0';
        axi_full_bf_mosi.rready <= '1';
        axi_full_bf_mosi.awregion <= "0000";
        axi_full_bf_mosi.arregion <= "0000";
        axi_full_bf_mosi.arqos <= "0000";
        axi_full_bf_mosi.awqos <= "0000";
        axi_full_bf_mosi.wstrb <= (others => '1');
        
        wait until rising_edge(clk300);
        wait for 100 ns;
        wait until rising_edge(clk300);
        
        -- write configuration data to memory over the axi full interface.
        FILE_OPEN(JonesCmdfile, g_JONES_INIT_FILENAME, READ_MODE);
        
        while (not endfile(JonesCmdfile)) loop
            -- Get data for 1 source (64 bytes = 16 x 4byte words)
            readline(JonesCmdfile, regLine_in);
            -- drop "[" character that indicates the address
            read(regLine_in,c,regGood);
            -- get the address
            hread(regLine_in,regAddr,regGood);
            axi_full_bf_mosi.awaddr(31 downto 0) <= regAddr;
            axi_full_bf_mosi.awvalid <= '1';
            wait until (rising_edge(clk300) and axi_full_bf_miso.awready='1');
            wait for 1 ps;
            axi_full_bf_mosi.awvalid <= '0';
            for i in 0 to 15 loop
                readline(JonesCmdfile, regLine_in);
                hread(regLine_in,regData,regGood);
                axi_full_bf_mosi.wvalid <= '1';
                axi_full_bf_mosi.wdata(31 downto 0) <= regData;
                if i=15 then
                    axi_full_bf_mosi.wlast <= '1';
                else
                    axi_full_bf_mosi.wlast <= '0';
                end if;
                wait until (rising_edge(clk300) and axi_full_bf_miso.wready = '1');
                wait for 1 ps;
            end loop;
            axi_full_bf_mosi.wvalid <= '0';
            wait until rising_edge(clk300);
            wait until rising_edge(clk300);
        end loop;
        wait until rising_edge(clk300);
        wait for 100 ns;
        wait until rising_edge(clk300);
        
        ---------------------------------------------------------------
        wait until rising_edge(clk300);
        wait for 100 ns;
        wait until rising_edge(clk300);
        bf_axi_full_done <= '1';
        wait;
    end process;
    
    
    process(clk300)
    begin
        if rising_edge(clk300) then
            if axi_lite_done = '0' or ct2_axi_full_done = '0' or bf_axi_full_done = '0' then
                data_rst <= '1';
            else
                data_rst <= '0';
            end if;
        end if;
    end process;
    
    --------------------------------------------------------------------------------
    -- Emulate the Filterbanks
    --  Filterbank output signals on clk450 :
    --    CT_sofFinal       ; Pulse high at the start of every frame, prior to FD_dataValid (1 frame is 53 ms of data).
    --    FD_frameCount     ; std_logic_vector(36 downto 0); Corner turn frame count; Stable for the entire corner turn frame.
    --    FD_virtualChannel ; t_slv_16_arr(2 downto 0); 3 virtual channels, one for each of the PST data streams.
    --    FD_headerValid    ; std_logic_vector(2 downto 0); goes to "111" for the first clock cycle of each burst of 216 fine channels.
    --    FD_data           ; t_slv_64_arr(2 downto 0); (2 pol)x(16+16 bit complex) = 64 bits, for each of 3 virtual channels.
    --    FD_dataValid      ; std_logic; Goes high in bursts of 216 out of every 258 clocks.
    --
    
    -- Per corner turn frame, beamformer has to output :
    --  (c_minGapData = time allowance for 9 packets = 1 coarse channel) * 
    --  (g_BEAMS_ENABLED beams) * (g_COARSE_CHANNELS coarse channels) * (8 packets per corner turn [each 32 time samples])
    frame_gap <= c_minGapData * TO_INTEGER(unsigned(g_BEAMS_ENABLED)) * g_COARSE_CHANNELS * 8;
    
    process(clk450)
    begin
        if rising_Edge(clk450) then
            -- generate data on pkt_virtual_channel, pkt_packet_count, pkt_valid
            -- Time between packets is at least 200 clocks
            -- (8300byte packets at 100Gbit/sec, 3.333 ns clock period)
            if data_rst = '1' then
                fb_pkt_fsm <= pkt_gap;
                pkt_frame_count <= g_FRAME_COUNT_START(36 downto 0); -- Count of corner turn frames since the SKA epoch
                pkt_packet_count <= 0;     -- Count through 256 packets for each virtual channel
                pkt_virtual_channel <= 0;  -- Count through virtual channels in steps of 3 
                pkt_wait_count <= 0;
                pkt_fine_channel <= 0;
                frame_gap_count <= frame_gap-10;
            else
                case fb_pkt_fsm is
                    when idle => -- do nothing until data_rst kicks off the data input.
                        fb_pkt_fsm <= idle;
                        
                    when send_pkt =>
                        if pkt_fine_channel = 215 then
                            pkt_fine_channel <= 0;
                            fb_pkt_fsm <= inter_packet_gap;
                        else
                            pkt_fine_channel <= pkt_fine_channel + 1;
                        end if;
                        pkt_wait_count <= 0;
                        frame_gap_count <= frame_gap_count + 1;
                    
                    when inter_packet_gap =>
                        pkt_fine_channel <= 0;
                        if pkt_wait_count = 40 then
                            -- total count of clocks when not sending is 41 (this state) + 1 (update_counts)
                            --  = 42 clocks = 258 clocks total between packets (as per the real filterbank) 
                            fb_pkt_fsm <= update_counts;
                        else
                            pkt_wait_count <= pkt_wait_count + 1;
                        end if;
                        frame_gap_count <= frame_gap_count + 1;
                    
                    when update_counts =>
                        if pkt_packet_count = 255 then
                            pkt_packet_count <= 0;
                            -- Go to the next virtual channel
                            if ((pkt_virtual_channel+3) >= c_VIRTUAL_CHANNELS) then
                                -- Just finished sending all the virtual channels
                                --   e.g. pkt_virtual_channel = 0, then we've just sent 3 channels.
                                -- Go to the next corner turn frame.
                                pkt_virtual_channel <= 0;
                                pkt_frame_count <= std_logic_vector(unsigned(pkt_frame_count) + 1);
                                fb_pkt_fsm <= pkt_gap;
                            else
                                pkt_virtual_channel <= pkt_virtual_channel + 3;
                                fb_pkt_fsm <= send_pkt;
                            end if;
                        else
                            pkt_packet_count <= pkt_packet_count + 1;
                            fb_pkt_fsm <= send_pkt;
                        end if;
                        frame_gap_count <= frame_gap_count + 1;
                        
                    when pkt_gap =>
                        -- Gap between corner turn frames
                        pkt_wait_count <= pkt_wait_count + 1;
                        pkt_fine_channel <= 0;
                        frame_gap_count <= frame_gap_count + 1;
                        if (frame_gap_count > frame_gap) then
                            fb_pkt_fsm <= pkt_SOF;
                        end if;
                        
                    when pkt_SOF =>
                        -- set start of frame before sending a new corner turn frames worth of data.
                        fb_pkt_fsm <= pkt_SOF_wait;
                        pkt_wait_count <= 0;
                        frame_gap_count <= 0;
                        
                    when pkt_SOF_wait =>
                        frame_gap_count <= frame_gap_count + 1;
                        pkt_wait_count <= pkt_wait_count + 1;
                        if (pkt_wait_count > 20) then
                            fb_pkt_fsm <= send_pkt;
                        end if;
                        
                end case;
            end if;
            
        end if;
    end process;
    
    absolute_sample <= std_logic_vector(unsigned(pkt_frame_count) * to_unsigned(256,16) + to_unsigned(pkt_packet_count,53));
    
    -- Construct the emulated filterbank data
    CT_sofFinal <= '1' when fb_pkt_fsm = pkt_SOF else '0';
    FD_frameCount <= pkt_frame_count;
    FD_virtualChannel(0) <= std_logic_vector(to_unsigned(pkt_virtual_channel,16));
    FD_virtualChannel(1) <= std_logic_vector(to_unsigned(pkt_virtual_channel+1,16));
    FD_virtualChannel(2) <= std_logic_vector(to_unsigned(pkt_virtual_channel+2,16));
    FD_headerValid <= "111" when (fb_pkt_fsm = send_pkt and pkt_fine_channel = 0) else "000";
    FD_data(0)(15 downto 0)  <= absolute_sample(15 downto 0); 
    FD_data(0)(31 downto 16) <= std_logic_vector(to_unsigned(pkt_virtual_channel,16));
    FD_data(0)(47 downto 32) <= absolute_sample(31 downto 16);
    FD_data(0)(63 downto 48) <= std_logic_vector(to_unsigned(pkt_fine_channel,16));
    FD_data(1)(15 downto 0)  <= absolute_sample(15 downto 0);
    FD_data(1)(31 downto 16) <= std_logic_vector(to_unsigned(pkt_virtual_channel+1,16));
    FD_data(1)(47 downto 32) <= absolute_sample(31 downto 16);
    FD_data(1)(63 downto 48) <= std_logic_vector(to_unsigned(pkt_fine_channel,16));
    FD_data(2)(15 downto 0)  <= absolute_sample(15 downto 0);
    FD_data(2)(31 downto 16) <= std_logic_vector(to_unsigned(pkt_virtual_channel+2,16));
    FD_data(2)(47 downto 32) <= absolute_sample(31 downto 16);
    FD_data(2)(63 downto 48) <= std_logic_vector(to_unsigned(pkt_fine_channel,16));
    FD_dataValid <= '1' when fb_pkt_fsm = send_pkt else '0';
    
    process(clk450)
    begin
        if rising_edge(clk450) then
            count1 <= std_logic_vector(unsigned(count1) + 23753);
            count2 <= std_logic_vector(unsigned(count2) + 13751);
            count3 <= std_logic_vector(unsigned(count3) + 11743);
            count4 <= std_logic_vector(unsigned(count4) + 9743);
            count5 <= std_logic_vector(unsigned(count4) + 7753);
        end if;
    end process;
    
    -- alternate data which is more like uniform random values.
--    FD_data(0)(15 downto 0)  <= count1;
--    FD_data(0)(31 downto 16) <= count2;
--    FD_data(0)(47 downto 32) <= count3;
--    FD_data(0)(63 downto 48) <= count4;
--    FD_data(1)(15 downto 0)  <= count1 xor count2;
--    FD_data(1)(31 downto 16) <= count1 xor count3;
--    FD_data(1)(47 downto 32) <= count1 xor count4;
--    FD_data(1)(63 downto 48) <= count1 xor count5;
--    FD_data(2)(15 downto 0)  <= count2 xor count3;
--    FD_data(2)(31 downto 16) <= count2 xor count4;
--    FD_data(2)(47 downto 32) <= count2 xor count5;
--    FD_data(2)(63 downto 48) <= count3 xor count4;
    

    
    
    totalStations <= std_logic_vector(to_unsigned(g_STATIONS,12));
    totalCoarse <= std_logic_vector(to_unsigned(g_COARSE_CHANNELS,12));
    totalChannels <= std_logic_vector(to_unsigned(g_STATIONS * g_COARSE_CHANNELS,12));
    bad_polynomials <= '0';
    
    BFCT : entity ct_lib.ct2_wrapper
    generic map (
        g_PST_BEAMS => g_PST_BEAMS,
        g_USE_META => False
    ) port map (
        -- Parameters, in the i_axi_clk domain.
        i_stations => totalStations(10 downto 0), -- in (10:0); Up to 1024 stations
        i_coarse   => totalCoarse(9 downto 0),    -- in (9:0); Number of coarse channels.
        i_virtualChannels => totalChannels(10 downto 0), -- in (10:0); Total virtual channels (= i_stations * i_coarse)
        i_bad_polynomials => bad_polynomials, --  in std_logic;
        -- Registers AXI Lite Interface (uses i_axi_clk)
        i_axi_mosi => axi_lite_mosi, -- in t_axi4_lite_mosi;
        o_axi_miso => axi_lite_miso, -- out t_axi4_lite_miso;
        i_axi_rst  => clk300_rst,    -- in std_logic;
        -- Polynomial memory axi full interface
        i_axi_full_mosi => axi_full_mosi, -- in t_axi4_full_mosi;
        o_axi_full_miso => axi_full_miso, -- out t_axi4_full_miso;
        -- Reset passed in from upstream.
        i_rst => data_rst,
        -- Data in from the PST filterbanks; bursts of 216 clocks for each channel.
        i_sof          => CT_sofFinal,     -- in std_logic; Pulse high at the start of every frame. (1 frame is typically 60ms of data).
        i_FB_clk       => clk450,          -- in std_logic; Filterbank clock, expected to be 450 MHz
        i_frameCount     => FD_frameCount, -- in (36:0); Frame count is the same for all simultaneous output streams.
        i_virtualChannel => FD_virtualChannel, -- in t_slv_16_arr(2:0); 3 virtual channels, one for each of the PST data streams.
        i_HeaderValid => FD_headerValid,   -- in (2:0);
        i_data        => FD_data,          -- in t_slv_64_arr(2:0); (2 pol)x(16+16 bit complex) = 64 bits, for each of 3 virtual channels.
        i_dataValid   => FD_dataValid,     -- in std_logic;
        -- Data out to the beamformer
        i_BF_clk  => clk400,    -- in std_logic; Beamformer clock, expected to be 400 MHz
        
        o_data    => BFdataIn,  -- out (191:0); -- 3 consecutive fine channels delivered every clock.
        o_flagged => BFflagged, -- out (2:0);  -- "o_flagged" aligns with "o_data"
        o_fine    => BFFine,    -- out (7:0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        o_coarse  => BFCoarse,  -- out (9:0);
        o_firstStation => BFFirstStation, -- out std_logic;
        o_lastStation => BFLastStation,   -- out std_logic;
        o_timeStep => BFtimeStep,         -- out (4:0)
        o_virtualChannel => BFVirtualChannel,  -- out std_logic_vector(9 downto 0);  -- coarse channel count.
        o_packetCount => BFPacketCount, -- out (39:0); The PST output packet count for this packet, based on the original packet count from LFAA. Each PST output packet is 6.63552 ms
        o_outputPktOdd => BFPacketOdd,  -- out std_logic;
        o_valid   => BFValidIn,  -- out std_logic;
        -- Polynomial data 
        o_phase_virtualChannel => BF_phase_virtualChannel, -- out (9:0);
        o_phase_timeStep       => BF_phase_timeStep,       -- out (7:0);
        o_phase_beam           => BF_phase_beam,           -- out (3:0);
        o_phase                => BF_phase,                -- out (23:0); Phase at the start of the coarse channel.
        o_phase_step           => BF_phase_step,           -- out (23:0); Phase step per fine channel.
        o_phase_valid          => BF_phase_valid,          -- out std_logic;
        o_phase_clear          => BF_phase_clear,          -- out std_logic;
        -- Configuration to the beamformers
        o_jonesBuffer => BFJonesBuffer,   -- out std_logic;
        o_jones_status => BFjones_status, -- out (1:0); bit 0 = used default, bit 1 = jones valid;
        o_poly_ok      => BFpoly_ok,      -- out (1:0); The polynomials used are within their valid time range;
        o_beamsEnabled => BFBeamsEnabled, -- out (7:0)
        o_scale_exp_frac => BF_scale_exp_frac, -- out (7:0); bit 3 = 0 indicates firmware should calculate the scale factor.
        -- AXI interface to the HBM
        -- Corner turn between filterbanks and beamformer
        -- aw bus = write address
        i_axi_clk => clk300, -- in std_logic;
        -- 
        m0_axi_awvalid  => m02_axi_awvalid, --  out std_logic;
        m0_axi_awready  => m02_axi_awready, -- in std_logic;
        m0_axi_awaddr   => m02_axi_awaddr(29 downto 0),  -- out (29:0);
        m0_axi_awlen    => m02_axi_awlen,   -- out (7:0);
        -- w bus - write data
        m0_axi_wvalid   => m02_axi_wvalid, -- out std_logic;
        m0_axi_wready   => m02_axi_wready, -- in std_logic;
        m0_axi_wdata    => m02_axi_wdata,  -- out (511:0);
        m0_axi_wlast    => m02_axi_wlast,  -- out std_logic;
        -- b bus - write response
        m0_axi_bvalid    => m02_axi_bvalid, -- in std_logic;
        m0_axi_bresp     => m02_axi_bresp,  -- in (1:0);
        -- ar bus - read address
        m0_axi_arvalid   => m02_axi_arvalid, -- out std_logic;
        m0_axi_arready   => m02_axi_arready, -- in std_logic;
        m0_axi_araddr    => m02_axi_araddr(29 downto 0),  -- out (29:0);
        m0_axi_arlen     => m02_axi_arlen,   -- out (7:0);
        -- r bus - read data
        m0_axi_rvalid    => m02_axi_rvalid, -- in std_logic;
        m0_axi_rready    => m02_axi_rready, -- out std_logic;
        m0_axi_rdata     => m02_axi_rdata,  -- in (511:0);
        m0_axi_rlast     => m02_axi_rlast,  -- in std_logic;
        m0_axi_rresp     => m02_axi_rresp,  -- in (1:0)
        --
        o_dataMismatch => dataMismatch,  -- out std_logic;
        o_dataMismatchBFclk => datamismatchBFclk, -- out std_logic;
        
        o_hbm_reset        => open,
        i_hbm_status       => x"00"
    );
    
    
    -- Beamformer
    BFi : entity bf_lib.PSTbeamformerTop_dp
    generic map (
        g_PIPE_INSTANCE => 1,
        g_PST_BEAMS     => g_PST_BEAMS -- integer := 16
    ) port map (
        -- Registers axi full interface
        i_MACE_clk => clk300, -- in std_logic;
        i_MACE_rst => clk300_rst, -- in std_logic;
        i_axi_mosi => axi_full_BF_mosi, -- in  t_axi4_full_mosi;
        o_axi_miso => axi_full_BF_miso, -- out t_axi4_full_miso;
        -- Beamformer data from the corner turn
        i_BF_clk         => clk400,           -- in std_logic;
        i_data           => BFdataIn,         -- in (191:0);  3 consecutive fine channels delivered every clock.
        i_flagged        => BFflagged,        -- in (2:0);
        i_fine           => BFFine,           -- in (7:0);   fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        i_coarse         => BFCoarse,         -- in (9:0);
        i_firstStation   => BFFirstStation,   -- in std_logic;  First station (used to trigger a new accumulator cycle in the beamformers).
        i_lastStation    => BFLastStation,    -- in std_logic;
        i_timeStep       => BFTimeStep,       -- in (4:0);  Timestep, runs from 0 to 31. There are 32 timesteps per output packet.
        i_virtualChannel => BFvirtualChannel, -- in (9:0);  virtual channel
        i_packetCount    => BFPacketCount,    -- in (36:0); The packet count for this packet, based on the original packet count from LFAA.
        i_outputPktOdd   => BFPacketOdd,      -- in std_logic;
        i_valid          => BFValidIn,        -- in std_logic;
        -- Polynomial data 
        i_phase_virtualChannel => BF_phase_virtualChannel, -- in std_logic_vector(9 downto 0);
        i_phase_timeStep       => BF_phase_timeStep,       -- in std_logic_vector(4 downto 0);
        i_phase_beam           => BF_phase_beam,           -- in std_logic_vector(3 downto 0);
        i_phase                => BF_phase,                -- in std_logic_vector(23 downto 0);      -- Phase at the start of the coarse channel.
        i_phase_step           => BF_phase_step,           -- in std_logic_vector(23 downto 0); -- Phase step per fine channel.
        i_phase_valid          => BF_phase_valid,          -- in std_logic;
        i_phase_clear          => BF_phase_clear,          -- in std_logic;
        -- Other data from the corner turn
        i_jonesBuffer    => BFJonesBuffer,    -- in std_logic;
        i_jones_status   => BFjones_status,   -- in (1:0) bit 0 = used default, bit 1 = jones valid;
        i_poly_ok        => BFpoly_ok,        -- in (1:0) The polynomials used are within their valid time range;
        i_beamsEnabled   => BFBeamsEnabled,   -- in (7:0)
        i_scale_exp_frac => BF_scale_exp_frac, -- in (7:0)
        -- 64 bit bus out to the 100GE Packetiser
        o_BFdata         => beamData,         -- out (63:0);
        o_BFpacketCount  => beamPacketCount,  -- out (39:0);
        o_BFBeam         => beamBeam,         -- out (7:0);
        o_BFFreqIndex    => beamFreqIndex,    -- out (10:0);
        o_BFvalid        => beamValid,        -- out std_logic
        o_BFjones_status => beamJonesStatus,  -- out (1:0);
        o_BFpoly_ok      => beamPoly_ok,      -- out (1:0);
        --
        i_badPacket => bdbg_ILA_trigger
    );
    
    -----------------------------------------------------------------------------------------
    -- packetiser
    test_runner_proc_clk300: process(clk300)
    begin
        if rising_edge(clk300) then
            -- power up reset logic
            if power_up_rst_clock_300(31) = '1' then
                power_up_rst_clock_300(31 downto 0) <= power_up_rst_clock_300(30 downto 0) & '0';
                clock_300_rst   <= '1';
                testCount_300   <= 0;
            else
                clock_300_rst   <= '0';
                testCount_300   <= testCount_300 + 1;
            end if;
        end if;
    end process;

    test_runner_proc_clk322: process(clock_322)
    begin
        if rising_edge(clock_322) then
            -- power up reset logic
            if power_up_rst_clock_322(31) = '1' then
                power_up_rst_clock_322(31 downto 0) <= power_up_rst_clock_322(30 downto 0) & '0';
                clock_322_rst   <= '1';
                testCount_322   <= 0;
            else
                clock_322_rst   <= '0';
                testCount_322   <= testCount_322 + 1;
            end if;
        end if;
    end process;

    test_runner_proc_clk400: process(clk400)
    begin
        if rising_edge(clk400) then
            -- power up reset logic
            if power_up_rst_clock_400(31) = '1' then
                power_up_rst_clock_400(31 downto 0) <= power_up_rst_clock_400(30 downto 0) & '0';
                clock_400_rst       <= '1';
            else
                clock_400_rst   <= '0';
            end if;
        end if;
    end process;
    
    args_stim_proc : process(clk300)
    begin
        if rising_edge(clk300) then
            -- 0 - enable packetiser
            -- 1 - enable test gen
            -- 2 - limited runs
            -- 3 - use defaults
            --  0x03 = Run Test gen
            --  0x09 = Run packetiser with defaults, assume BF sourced data.
            if (clock_300_rst = '1') then
                packetiser_host_bus_ctrl.instruct    <= x"00";
                packetiser_host_bus_ctrl_2.instruct  <= x"00";
                packetiser_host_bus_ctrl_3.instruct  <= x"00";
            else 
                if (testCount_300 = 1000) then
                    packetiser_host_bus_ctrl.instruct    <= x"09";
                    packetiser_host_bus_ctrl_2.instruct  <= x"00";
                    packetiser_host_bus_ctrl_3.instruct  <= x"00";
                end if;
                if (testCount_300 = 20000) then
                    packetiser_host_bus_ctrl.instruct    <= x"09";
                    packetiser_host_bus_ctrl_2.instruct  <= x"00";
                    packetiser_host_bus_ctrl_3.instruct  <= x"00";
                end if;
                if (testCount_300 = 200000) then
                    packetiser_host_bus_ctrl.instruct    <= x"09";
                    packetiser_host_bus_ctrl_2.instruct  <= x"00";
                    packetiser_host_bus_ctrl_3.instruct  <= x"00";
                end if;
            end if;
        end if;
    end process;
     
    cmac_emulator : process(clock_322)
    begin
        if rising_edge(clock_322) then
            if (testCount_322 > 15) then
                if testCount_322 = 200 or testCount_322 = 250 then
                    i_data_to_transmit_ctl.ready    <= '0';
                    i_tx_axis_tready                <= '0';
                else    
                    i_data_to_transmit_ctl.ready    <= '1';
                    i_tx_axis_tready                <= '1';
                end if;
            else
                i_data_to_transmit_ctl.ready        <= '0';   
                i_data_to_transmit_ctl.overflow     <= '0';
                i_data_to_transmit_ctl.underflow    <= '0';
                i_tx_axis_tready                    <= '0';     
            end if;
        end if;
    end process;
    
    packetiser_stream_1_host_bus_in.config_data_clk <= clk400;
    packetiser_stream_2_host_bus_in.config_data_clk <= clk400;
    packetiser_stream_3_host_bus_in.config_data_clk <= clk400;
    
    --------------------------------------------------------------------------------------------------
    
    beamformer_to_packetiser_data.data_clk                  <= clk400;
    beamformer_to_packetiser_data.data_in_wr                <= beamValid;
    beamformer_to_packetiser_data.data(511 downto 64)       <= (others =>'0');
    beamformer_to_packetiser_data.data(63 downto 0)         <= beamData;
    beamformer_to_packetiser_data.bytes_to_transmit         <= (others =>'0');
    
    beamformer_to_packetiser_data.PSR_beam_freq_index       <= beamFreqIndex;
    beamformer_to_packetiser_data.PSR_beam                  <= '0' & beamBeam;
    beamformer_to_packetiser_data.PSR_time_ref              <= x"00" & beamPacketCount;
    beamformer_to_packetiser_data.PSR_jones                 <= "00" & beamJonesStatus;
    beamformer_to_packetiser_data.PSR_delay_poly            <= beamPoly_ok;
    
    beamformer_to_packetiser_data_2.data_clk                <= clk400;
    beamformer_to_packetiser_data_2.data_in_wr              <= '0';-- AND (beamformer_to_packetiser_data.PSR_beam(0));
    beamformer_to_packetiser_data_2.data                    <= (others => '0');
    beamformer_to_packetiser_data_2.bytes_to_transmit       <= "00" & x"000";    
    
    beamformer_to_packetiser_data_2.PSR_beam_freq_index     <= (others => '0');
    beamformer_to_packetiser_data_2.PSR_beam                <= (others => '0');
    beamformer_to_packetiser_data_2.PSR_time_ref            <= (others => '0');
    beamformer_to_packetiser_data_2.PSR_jones               <= "0010";
    beamformer_to_packetiser_data_2.PSR_delay_poly          <= "01";
    
    beamformer_to_packetiser_data_3.data_clk                <= clk400;
    beamformer_to_packetiser_data_3.data_in_wr              <= '0';-- AND (beamformer_to_packetiser_data.PSR_beam(0));
    beamformer_to_packetiser_data_3.data                    <= (others => '0');
    beamformer_to_packetiser_data_3.bytes_to_transmit       <= "00" & x"000";    
    
    beamformer_to_packetiser_data_3.PSR_beam_freq_index     <= (others => '0');
    beamformer_to_packetiser_data_3.PSR_beam                <= (others => '0');
    beamformer_to_packetiser_data_3.PSR_time_ref            <= (others => '0');
    beamformer_to_packetiser_data_3.PSR_jones               <= "0010";
    beamformer_to_packetiser_data_3.PSR_delay_poly          <= "01";
    
    
    
    DUT_1 : entity PSR_Packetiser_lib.psr_packetiser100G_Top 
    Generic Map (
        g_DEBUG_ILA                 => TRUE,
        g_Number_of_streams         => 3,
        g_PST_config                => TRUE
    )
    Port Map ( 
        -- ~322 MHz
        i_cmac_clk                  => clock_322,
        i_cmac_rst                  => clock_322_rst,
        
        i_packetiser_clk            => clk400,
        i_packetiser_rst            => clock_400_rst,
        
        -- Lbus to MAC
        o_data_to_transmit          => o_data_to_transmit,
        i_data_to_transmit_ctl      => i_data_to_transmit_ctl,
        
        -- AXI to CMAC interface to be implemented
        o_tx_axis_tdata             => o_tx_axis_tdata,
        o_tx_axis_tkeep             => o_tx_axis_tkeep,
        o_tx_axis_tvalid            => o_tx_axis_tvalid,
        o_tx_axis_tlast             => o_tx_axis_tlast,
        o_tx_axis_tuser             => o_tx_axis_tuser,
        i_tx_axis_tready            => i_tx_axis_tready,
        
        -- PSS beamformer output streams
        --i_beam_output_stream_1      => beam_output_stream_1,
        --i_beam_output_stream_2      => beam_output_stream_2,
        
        -- signals from signal processing/HBM/the moon/etc
        
        -- beamformer output streams
        i_beam_output_stream_1      => null_t_bf_output_stream,
        i_beam_output_stream_2      => null_t_bf_output_stream,
        
        i_packet_stream_ctrl(0)     => packetiser_host_bus_ctrl,
        i_packet_stream_ctrl(1)     => packetiser_host_bus_ctrl_2,
        i_packet_stream_ctrl(2)     => packetiser_host_bus_ctrl_3,
        
        o_packet_stream_stats       => packet_stream_stats,
                
        i_packet_stream(0)          => beamformer_to_packetiser_data,
        i_packet_stream(1)          => beamformer_to_packetiser_data_2,
        i_packet_stream(2)          => beamformer_to_packetiser_data_3,
        o_packet_stream_out         => open,
        
        -- AXI BRAM to packetiser
        i_packet_config_in_stream_1 => packetiser_stream_1_host_bus_in,
        i_packet_config_in_stream_2 => packetiser_stream_2_host_bus_in,
        i_packet_config_in_stream_3 => packetiser_stream_3_host_bus_in,
        
        -- AXI BRAM return path from packetiser 
        o_packet_config_stream_1    => packetiser_host_bus_out.config_data_out,
        o_packet_config_stream_2    => packetiser_host_bus_out_2.config_data_out,
        o_packet_config_stream_3    => packetiser_host_bus_out_3.config_data_out
        
        
    );
    
    process(clock_322)
    begin
        if rising_edge(clock_322) then
            
            if o_tx_axis_tvalid = '1' then
                if o_tx_axis_tlast = '1' then
                    pkt_word <= (others => '0');
                else
                    pkt_word <= std_logic_vector(unsigned(pkt_word) + 1);
                end if;
            end if;
            tdata_del <= o_tx_axis_tdata;
            pkt_data_word <= std_logic_vector(unsigned(pkt_word) - 2);
        end if;
    end process;
    
    d_samples(0) <= tdata_del(495 downto 464);
    d_samples(1)(15 downto 0) <= tdata_del(511 downto 496);
    d_samples(1)(31 downto 16) <= o_tx_axis_tdata(15 downto 0);
    dgen : for w in 2 to 15 generate
        d_samples(w) <= o_tx_axis_tdata((16 + (w-2)*32 + 31) downto (16 + (w-2)*32));
    end generate;
    
    d_samples_pol <= pkt_data_word(1);
    d_samples_fine <= pkt_data_word(15 downto 2);


    -----------------------------------------------------------------------------------------
    
    m02_axi_awaddr(31 downto 30) <= "00";
    m02_axi_araddr(31 downto 30) <= "00";
    
    -- Other unused axi ports
    m02_axi_awsize <= get_axi_size(M02_DATA_WIDTH);
    m02_axi_awburst <= "01";   -- "01" indicates incrementing addresses for each beat in the burst. 
    m02_axi_bready  <= '1';  -- Always accept acknowledgement of write transactions. -- out std_logic;
    m02_axi_wstrb  <= (others => '1');  -- We always write all bytes in the bus. --  out std_logic_vector(63 downto 0);
    m02_axi_arsize  <= get_axi_size(M02_DATA_WIDTH);   -- 6 = 64 bytes per beat = 512 bit wide bus. -- out std_logic_vector(2 downto 0);
    m02_axi_arburst <= "01";    -- "01" = incrementing address for each beat in the burst. -- out std_logic_vector(1 downto 0);
    
    m02_axi_arlock <= "00";
    m02_axi_awlock <= "00";
    m02_axi_awid(0) <= '0';   -- We only use a single ID -- out std_logic_vector(0 downto 0);
    m02_axi_awcache <= "0011";  -- out std_logic_vector(3 downto 0); bufferable transaction. Default in Vitis environment.
    m02_axi_awprot  <= "000";   -- Has no effect in Vitis environment. -- out std_logic_vector(2 downto 0);
    m02_axi_awqos   <= "0000";  -- Has no effect in vitis environment, -- out std_logic_vector(3 downto 0);
    m02_axi_awregion <= "0000"; -- Has no effect in Vitis environment. -- out std_logic_vector(3 downto 0);
    m02_axi_arid(0) <= '0';     -- ID are not used. -- out std_logic_vector(0 downto 0);
    m02_axi_arcache <= "0011";  -- out std_logic_vector(3 downto 0); bufferable transaction. Default in Vitis environment.
    m02_axi_arprot  <= "000";   -- Has no effect in vitis environment; out std_logic_Vector(2 downto 0);
    m02_axi_arqos    <= "0000"; -- Has no effect in vitis environment; out std_logic_vector(3 downto 0);
    m02_axi_arregion <= "0000"; -- Has no effect in vitis environment; out std_logic_vector(3 downto 0);
    
    -- m02 HBM interface : 2nd stage corner turn between filterbanks and beamformer, 1 GByte
    HBM3G_1 : entity pst_lib.HBM_axi_tbModel
    generic map (
        AXI_ADDR_WIDTH => 32, -- : integer := 32;   -- Byte address width. This also defines the amount of data. Use the correct width for the HBM memory block, e.g. 28 bits for 256 MBytes.
        AXI_ID_WIDTH => 1, -- integer := 1;
        AXI_DATA_WIDTH => 512, -- integer := 256;  -- Must be a multiple of 32 bits.
        READ_QUEUE_SIZE => 16, --  integer := 16;
        MIN_LAG => 60,  -- integer := 80   
        INCLUDE_PROTOCOL_CHECKER => TRUE,
        RANDSEED => 43526, -- : natural := 12345;
        LATENCY_LOW_PROBABILITY => 90, --  natural := 95;   -- probability, as a percentage, that non-zero gaps between read beats will be small (i.e. < 3 clocks)
        LATENCY_ZERO_PROBABILITY => 80 -- natural := 80   -- probability, as a percentage, that the gap between read beats will be zero.
    ) Port map (
        i_clk        => clk300,
        i_rst_n      => rst_n,
        axi_awaddr   => m02_axi_awaddr(31 downto 0),
        axi_awid     => m02_axi_awid, -- in std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_awlen    => m02_axi_awlen,
        axi_awsize   => m02_axi_awsize,
        axi_awburst  => m02_axi_awburst,
        axi_awlock   => m02_axi_awlock,
        axi_awcache  => m02_axi_awcache,
        axi_awprot   => m02_axi_awprot,
        axi_awqos    => m02_axi_awqos, -- in(3:0)
        axi_awregion => m02_axi_awregion, -- in(3:0)
        axi_awvalid  => m02_axi_awvalid,
        axi_awready  => m02_axi_awready,
        axi_wdata    => m02_axi_wdata,
        axi_wstrb    => m02_axi_wstrb,
        axi_wlast    => m02_axi_wlast,
        axi_wvalid   => m02_axi_wvalid,
        axi_wready   => m02_axi_wready,
        axi_bresp    => m02_axi_bresp,
        axi_bvalid   => m02_axi_bvalid,
        axi_bready   => m02_axi_bready,
        axi_bid      => m02_axi_bid, -- out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_araddr   => m02_axi_araddr(31 downto 0),
        axi_arlen    => m02_axi_arlen,
        axi_arsize   => m02_axi_arsize,
        axi_arburst  => m02_axi_arburst,
        axi_arlock   => m02_axi_arlock,
        axi_arcache  => m02_axi_arcache,
        axi_arprot   => m02_axi_arprot,
        axi_arvalid  => m02_axi_arvalid,
        axi_arready  => m02_axi_arready,
        axi_arqos    => m02_axi_arqos,
        axi_arid     => m02_axi_arid,
        axi_arregion => m02_axi_arregion,
        axi_rdata    => m02_axi_rdata,
        axi_rresp    => m02_axi_rresp,
        axi_rlast    => m02_axi_rlast,
        axi_rvalid   => m02_axi_rvalid,
        axi_rready   => m02_axi_rready,
        i_write_to_disk => write_HBM_to_disk, -- in std_logic;
        i_write_to_disk_addr => hbm_dump_addr, -- address to start the memory dump at.
        -- 4 Mbytes = first 8 data streams
        i_write_to_disk_size => 4194304, -- size in bytes
        i_fname => hbm_dump_filename, -- : in string
        -- Initialisation of the memory
        -- The memory is loaded with the contents of the file i_init_fname in 
        -- any clock cycle where i_init_mem is high.
        i_init_mem   => init_mem, --  in std_logic;
        i_init_fname => init_fname -- : in string
    );

    write_HBM_to_disk <= '0';
    hbm_dump_addr <= 0;
    init_mem <= '0';

    -- Write the beamformer output to a file
    process
		file logfile: TEXT;
		--variable data_in : std_logic_vector((BIT_WIDTH-1) downto 0);
		variable line_out : Line;
    begin
	    FILE_OPEN(logfile, g_BF_OUT_FILENAME, WRITE_MODE);
		loop
            wait until rising_edge(clk400);

            if beamValid = '1' then
                line_out := "";
                hwrite(line_out,beamPacketCount,RIGHT,10);
                hwrite(line_out,beamBeam,RIGHT,3);
                hwrite(line_out,beamFreqIndex,RIGHT,5);
                hwrite(line_out,beamData(63 downto 48),RIGHT,5);
                hwrite(line_out,beamData(47 downto 32),RIGHT,5);
                hwrite(line_out,beamData(31 downto 16),RIGHT,5);
                hwrite(line_out,beamData(15 downto 0),RIGHT,5);
                hwrite(line_out,beamPoly_ok & beamJonesStatus,RIGHT,2);
                writeline(logfile,line_out);
            end if;
        end loop;
    end process;
    
end Behavioral;
