# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
Standalone code to create configuration for the SKA low correlator corner turn 1 module
---------------------------------------------
Introduction:
Corner turn 2 (CT2) stores data from the filterbank to HBM and then plays them back in the order needed by the beamformer.
This code tests
   * Configuration and calculation of the delay polynomials.
   * Corner turn readout order
   * beamformer configuration
   * beamformer
This code reads in a yaml file and writes out configuration data for the yaml test case.
The configurationd data includes the polynomial coefficients and Jones matrices.
----------------------------------------------
Configuration memory:


"""

import matplotlib.pyplot as plt
import argparse
from pprint import pprint

import numpy as np
import yaml
import typing

# number of distinct random samples to generate per virtual channel. 
RNG_SAMPLES = 8192

def command_line_args():
    parser = argparse.ArgumentParser(description="PST Corner turn 2 + beamformer configuration generator")
    parser.add_argument(
        "-p",
        "--poly",
        type=argparse.FileType(mode="wt"),
        help="File to write polynomial configuration data to",
        required=False,
    )

    parser.add_argument(
        "-j",
        "--jones",
        type=argparse.FileType(mode="wt"),
        help="File to write jones matrices to",
        required=False,
    )
    
    parser.add_argument(
        "-b",
        "--bf_data",
        type=argparse.FileType(mode="rt"),
        help="File to read the beamformer output from",
        required=False,
    )
    
    parser.add_argument(
        "configuration",
        help="Test Configuration (YAML)",
        type=argparse.FileType(mode="r"),
    )
    return parser.parse_args()

def parse_config(file: typing.IO) -> typing.Dict:
    """
    Reads configuration YAML file, checks if required values are present.

    :param file: The YAML file object
    :return: Settings dict, guaranteed to contain the keys specified in required_keys
    :raises AssertionError: if required key(s) are missing
    """
    config = yaml.safe_load(file)

    required_keys = {"stations_buf0","channels","weights_buf0","weights_buf1","jones"}
    assert config.keys() >= required_keys, "Configuration YAML missing required key(s)"
    print("\n??\tSettings:")
    for parameter, value in config.items():
        if parameter == "stations_buf0" or parameter == "stations_buf1":
            print(f"\t:{parameter}")
            for n, stn_cfg in config[parameter].items():
                print(f"\t  {n}:{stn_cfg}")
              #  print(f"\t {stn_cfg[0][0]}, {stn_cfg[0][1]}, {stn_cfg[1][0]}, {stn_cfg[1][1]}")
        else:
            print(f"\t{parameter}: {value}")
    return config

def ct2_polynomial_config(poly_data):
    """
    :param poly_data: dictionary indexed by the station ID, with up to 16 polynomials each, one for each beam.
    :return: numpy array of uint32 to be loaded into the polynomial configuration memory, and the total number of 
    stations included.
    """
    # [512 stations] x [16 beams] x [6 coefficients per polynomial] (x 4 bytes per single precision value)
    # In the polynomial configuration memory,
    #  (4-byte word) address = (buffer * 49152) + (beam * 3072) + (station * 6)
    # This function reads for a single buffer (whichever is passed to the function in poly_data)
    config_array = np.zeros((512,16,6), np.float32)
    total_stations = 0
    total_beams = 0
    for station, polynomials in poly_data.items():
        beams = len(polynomials)
        if beams > total_beams :
            total_beams = beams
        if (station+1) > total_stations:
            total_stations = station+1
        for beam in range(beams):
            for coef in range(6):
                config_array[station,beam,coef] = np.float32(polynomials[beam][coef])
                
    return (config_array, total_stations, total_beams)
    
def get_tb_data(tb_file, total_frames, total_beams, total_coarse):
    # Load PST packet output data saved by the testbench
    # Saved at the beamformer output, it includes 1 header word then the 
    # File Format : text
    # Each line contains hex data for:
    #   packetCount beam freq_index data0 data1 data2 data3 status
    #   (note freq_index bits (3:0) runs 0 to 8, for 9 blocks of 24 fine channels per coarse channel,
    #         freq_index bits (15:4) counts coarse channels)
    # Each packet consist of XX lines in the file :
    #   - 1 line meta data
    #      data0 = (virtual channel) * 64
    #               (i.e. top 10 bits are the first virtual channel used in the packet, 
    #               but it is packed into a 16-bit word, so it appears as virtual_channel * 64)
    #      data1 is unused
    #      data2*65536 + data3 = 32 bit value representing the scale factor as a single precision value
    #   - 6 lines weight data
    #      6x4 = 24 values, one for each fine channel in the packet.
    #   - 768 lines of beamformed data
    #      Each line is data0 = time N, real, data1 = time N, imaginary, data2 = time (N+1), real, data3 = time (N+1), imaginary
    #      Data order is
    #    -- The output packets are in the order :
    #        data line in file |  Fine  |  Polarisation  |  Times
    #                     0        0           0             0, 1     <-- 4 values per line, in data0, data1, data2, data3
    #                     1        0           0             2, 3
    #                                  ...
    #                    15        0           0             30, 31
    #                    16        0           1             0, 1
    #                                  ...
    #                    31        0           1             30, 31
    #     etc, so lines 32-47 = fine 1, pol 0
    #             lines 48-63 = fine 1, pol 1
    #     Total of (32 lines) * (24 fine channels)= 768 lines of data
    #
    # e.g. first few lines of the file :
    #  pkt_count beam freq  d0   d1   d2   d3 status 
    #      0       1    2    3    4    5    6 7   <= index into dint = [int(di,16) for di in line.split()]
    #  00000087F8 00  000 0000 0000 3F20 0000 6   <= scale factor 0x3F200000 = 0.625
    #  00000087F8 00  000 8000 8000 8000 8000 6   <= 24 scale values
    #  00000087F8 00  000 8000 8000 8000 8000 6
    #  00000087F8 00  000 8000 8000 8000 8000 6
    #  00000087F8 00  000 8000 8000 8000 8000 6
    #  00000087F8 00  000 8000 8000 8000 8000 6
    #  00000087F8 00  000 8000 8000 8000 8000 6
    #  00000087F8 00  000 01DF FF4B 01DD FF4C 6   <= data
    #  00000087F8 00  000 01DB FF4C 01DA FF4C 6
    #

    # packet_data is the data as it appears in the packet
    packet_data = np.zeros((total_frames, total_beams, total_coarse * 216, 256, 2), dtype=np.complex128)
    # scale factors : one for each group of 24 fine channels, and for each block of 32 time samples.
    packet_scales = np.zeros((total_frames, total_beams, total_coarse * 9,8))
    # raw_beamformed is the value after undoing the scale factor in the packet
    raw_beamformed = np.zeros((total_frames, total_beams, total_coarse * 216, 256, 2), dtype=np.complex128)
    
    pkt_count = 0  # count of packets being read from the file
    line_count = 0 # Line in the file that we are up to for the current packet
    for line in tb_file:
        dint = [int(di,16) for di in line.split()]
        for ds in range(3,7):
            if dint[ds] > 32767:
                dint[ds] = dint[ds] - 65536  # correct for signed values
        if line_count == 0:
            # get the scale factor for the packet
            sc_int = np.uint32(dint[5] * 65536)  # note scale factor is always positive, low 16 bits are always 0s.
            sc_fp32 = np.frombuffer(sc_int, dtype=np.float32)
            if pkt_count == 0:
                first_pkt = dint[0]
                time_pkt_start = 0
                ct_frame = 0
                pkt_in_frame = 0
            else:
                pkt_offset = dint[0] - first_pkt  # packets since the first packet, effectively time in units of 32 time samples.
                time_pkt_start = (pkt_offset * 32) % 256   # 256 samples per corner turn frame
                ct_frame = pkt_offset//8   # 8 packets in time per corner turn frame 
                pkt_in_frame = pkt_offset % 8  # packet number within the corner turn frame
            beam = dint[1]
            fine_channel = dint[2] % 16
            coarse_channel = dint[2] // 16
            # 216 fine channels per coarse, 24 fine channels per packet
            freq_base = coarse_channel * 216 + fine_channel * 24
            if ct_frame >= total_frames:
                break
            packet_scales[ct_frame, beam, freq_base//24, pkt_in_frame] = sc_fp32
        elif line_count > 6:
            # >6 then we are into the data part of the packet
            fine_channel = (line_count - 7) // 32
            time_sample = time_pkt_start + 2 * ((line_count - 7) % 16)   # 2 time samples per line, 32 per packet
            # Every second group of 16 lines is the second polarisation
            pol = ((line_count - 7) % 32) // 16
            v0 = np.double(dint[3]) + 1j * np.double(dint[4])
            v1 = np.double(dint[5]) + 1j * np.double(dint[6])
            packet_data[ct_frame, beam, freq_base + fine_channel, time_sample, pol] = v0
            packet_data[ct_frame, beam, freq_base + fine_channel, time_sample+1, pol] = v1
            raw_beamformed[ct_frame, beam, freq_base + fine_channel, time_sample, pol] = v0 / sc_fp32
            raw_beamformed[ct_frame, beam, freq_base + fine_channel, time_sample+1, pol] = v1 / sc_fp32
            
        if line_count == 774:
            # 1 header + 6 scale + 768 data = 775 lines per PST packet (line_count = 0 to 774)
            line_count = 0
            pkt_count += 1
        else:
            line_count += 1
    
    return (packet_data, packet_scales, raw_beamformed)

def main():
    # Read command-line arguments
    args = command_line_args()
    config = parse_config(args.configuration)
    
    # convert config into a data file to load into the firmware for the polynomials
    first_block = True
    for poly_buffer in range(2):
        if poly_buffer == 0:
            cfg_key = "stations_buf0"
            base_addr = 0
        else:
            cfg_key = "stations_buf1"
            base_addr = 196608  # byte address for the start of the buffer
        if cfg_key in config:
            (config_array, total_stations, total_beams) = ct2_polynomial_config(config[cfg_key])
            for station in range(total_stations):
                for beam in range(total_beams):
                    # Each station is (6 words) * (16 beams) = 96 words = 384 bytes of polynomial data 
                    # write data for 1 polynomial at a time = 24 words; only write up to total_stations and total_beams
                    block_addr = beam * 3072 + station * 6
                    if not first_block:
                        args.poly.write("\n")
                    first_block = False
                    args.poly.write(f"[{(base_addr + 4*block_addr):08x}]")   # byte address
                    for n in range(6):
                        coef_fp32 = config_array[station,beam,n]
                        coef_uint32 = np.frombuffer(coef_fp32, dtype=np.uint32)
                        args.poly.write(f"\n{coef_uint32[0]:08x}")
    # Write the sky frequencies for each virtual channel
    # These go to a block of memory starting at byte address 393216 in the polynomial memory.
    sky_freq = np.zeros(2052,dtype = np.uint32) # 2048 words in the sky frequency memory, but make this a multiple of 6 so all writes can be the same size.
    for coarse in range(config["g_COARSE_CHANNELS"]):
        for station in range(config["g_STATIONS"]):
            sky_fp32 = np.float32(config["channels"][coarse])
            sky_freq[2*(coarse * config["g_STATIONS"] + station)] =  np.frombuffer(sky_fp32, dtype=np.uint32)
    virtual_channels = config["g_COARSE_CHANNELS"] * config["g_STATIONS"]
    base_addr = 393216   # base address of the sky frequencies memory
    for vc_div3 in range((virtual_channels+3) // 3):
        vc = vc_div3 * 3
        args.poly.write(f"\n[{(base_addr + 8 * vc):08x}]") # 8 bytes per virtual channel
        for n in range(6):
            args.poly.write(f"\n{sky_freq[vc*2 + n]:08x}") # 2 entries in sky_freq per virtual channel (one unused)
    
    ## Create Jones matrices and write to a file
    # (beams) x (Two buffers) x (1024 virtual channels) x (2 rows) x (2 columns) x (2 [real + imaginary])
    jones = np.zeros((total_beams,2,1024,2,2,2),dtype = np.int16)
    if config["jones"] == 0:
        print('Setting Jones matrices to 0 (beamformer output will be zeros)')
    if config["jones"] == 1:
        # Identity matrices everywhere
        print("setting Jones matrices to identity matrices")
        for beam in range(total_beams):
            for buf in range(2):
                for vc in range(1024):
                    jones[beam,buf,vc,0,0,0] = 32767  # real part = 1, for top left of the Jones matrix
                    jones[beam,buf,vc,1,1,0] = 32767  # real part = 1 for bottomright of the Jones matrix
    elif config["jones"] == 2:
        # pseudo-random values everywhere
        print("Setting Jones matrices to pseudo random numbers")
        rng = np.random.default_rng(1234)
        jones = np.int16(np.round(32766 * (rng.random((total_beams,2,1024,2,2))-0.5)))
    else:
        print("! Unrecognised Jones matrix configuration")

    # Write weights to the Jones matrices config file
    # (2 buffers) x (1024 virtual channels)
    weights = np.zeros((2,1024),dtype = np.uint16)
    for coarse in range(config["g_COARSE_CHANNELS"]):
        for station in range(config["g_STATIONS"]):
            weights[0, coarse * config["g_STATIONS"] + station] = np.uint16(config['weights_buf0'][station])
            weights[1, coarse * config["g_STATIONS"] + station] = np.uint16(config['weights_buf1'][station])
    
    # Write Jones matrices to the config file
    # Writes are in blocks of 4 virtual channels = 16 x (32 bit words)
    # Flatten Jones array and reinterpret as uint32
    # There are (2 buffers) * (1024 virtual channels) * (2 rows) * (2 columns) = 8192 uint32 per beam in "jones_uint32"
    jones_flattened = np.reshape(jones,total_beams*2*1024*2*2*2)
    jones_uint32 = np.frombuffer(jones_flattened, dtype = np.uint32)
    print("writing jones matrices")
    for beam in range(total_beams):
        for buffer in range(2):
            # Write Jones
            base_addr = beam * 65536 + buffer * 16384
            for data_block in range((virtual_channels+3) // 4):
                args.jones.write(f"[{(base_addr + 64 * data_block):08x}]\n")  # byte address; 16 x 4 byte words per write.
                for data_word in range(16):
                    # Write uint32 values to file.
                    args.jones.write(f"{jones_uint32[beam * 8192 + buffer * 4096 + data_block*16 + data_word]:08x}\n")
            # Also write weights
            weights_base_addr = beam*65536 + 32768 + buffer * 2048
            for data_block in range((virtual_channels+31) // 32):  # 2 virtual channels per 32-bit word in the file, 16 words per write.
                args.jones.write(f"[{(weights_base_addr + 64 * data_block):08x}]\n")  # byte address; 16 x 4 byte words per write.
                for data_word in range(16):
                    # Write uint32 values to file.
                    w0 = weights[buffer, data_block * 32 + data_word * 2]
                    w1 = weights[buffer, data_block * 32 + data_word * 2 + 1]
                    args.jones.write(f"{(w1*65536 + w0):08x}\n")
    
    # find the expected output.
    # output packets consist of 32 time samples and 24 frequency samples.
    total_frames = 2
    print(f"computing expected beamformer result for {total_frames} corner turn frames")
    total_coarse = config["g_COARSE_CHANNELS"]
    total_stations = config["g_STATIONS"]
    raw_bf_samples = np.zeros((total_frames,total_beams,total_coarse,256,216),dtype = np.complex64)
    # 9x24 groups of fine channels per coarse channel, 256 time samples per corner turn, 2 polarisations
    raw_beamformed = np.zeros((total_frames,total_beams,total_coarse*216,256,2), dtype = np.complex128)
    for frame in range(total_frames):
        # Get the Polynomials to use
        this_frame = config["g_FRAME_COUNT_START"] + frame
        if (this_frame >= config["g_POLY_BUF0_FRAME_START"]) and (this_frame < (config["g_POLY_BUF0_FRAME_START"] + config["g_POLY_BUF0_VALID_DURATION"])):
            poly_buf0_valid = True
        else:
            poly_buf0_valid = False
        if (this_frame >= config["g_POLY_BUF1_FRAME_START"]) and (this_frame < (config["g_POLY_BUF1_FRAME_START"] + config["g_POLY_BUF1_VALID_DURATION"])):
            poly_buf1_valid = True
        else:
            poly_buf1_valid = False
        if config["g_POLY_BUF0_FRAME_START"] > config["g_POLY_BUF1_FRAME_START"]:
            poly_buf0_more_recent = True
        else:
            poly_buf0_more_recent = False
        if poly_buf0_valid and (poly_buf0_more_recent or (not poly_buf1_valid)):
            (poly_config, total_stations, total_beams) = ct2_polynomial_config(config["stations_buf0"])
            # Time to use in the polynomial at the start of the corner turn frame
            t_poly_ns = (this_frame - config["g_POLY_BUF0_FRAME_START"]) * 24 * 2048 * 1080.0 + config["g_POLY_BUF0_OFFSET_NS"]
        else:
            (poly_config, total_stations, total_beams) = ct2_polynomial_config(config["stations_buf1"])
            t_poly_ns = (this_frame - config["g_POLY_BUF1_FRAME_START"]) * 24 * 2048 * 1080.0 + config["g_POLY_BUF1_OFFSET_NS"]
        
        # Get the Jones matrices to use
        if (this_frame >= config["g_JONES_BUF0_FRAME_START"]) and (this_frame < (config["g_JONES_BUF0_FRAME_START"] + config["g_JONES_BUF0_VALID_DURATION"])): 
            jones_buf0_valid = True
        else:
            jones_buf0_valid = False
        if (this_frame >= config["g_JONES_BUF1_FRAME_START"]) and (this_frame < (config["g_JONES_BUF1_FRAME_START"] + config["g_JONES_BUF1_VALID_DURATION"])): 
            jones_buf1_valid = True
        else:
            jones_buf1_valid = False
        if config["g_JONES_BUF0_FRAME_START"] > config["g_JONES_BUF1_FRAME_START"] :
            jones_buf0_more_recent = True
        else:
            jones_buf0_more_recent = False
        if jones_buf0_valid and (jones_buf0_more_recent or (not jones_buf1_valid)):
            jones_select = 0
        else:
            jones_select = 1
        
        for beam in range(total_beams):
            for coarse in range(total_coarse):
                for time_sample in range(256):  # 256 time samples per corner turn frame
                    abs_sample = (config["g_FRAME_COUNT_START"] + frame) * 256 + time_sample
                    # Evaluate the polynomials for all stations for this time sample
                    # Find the time to use in the polynomials
                    t_poly_s = 1e-9 * (t_poly_ns + time_sample * 192 * 1080.0)
                    beam_poly = np.zeros(total_stations)
                    beam_phase = np.zeros(total_stations)
                    beam_phase_step = np.zeros(total_stations)
                    for station in range(total_stations):
                        beam_poly[station] = (poly_config[station, beam, 0] + poly_config[station, beam, 1] * t_poly_s + 
                            poly_config[station, beam, 2] * (t_poly_s**2) + poly_config[station, beam, 3] * (t_poly_s**3) +
                            poly_config[station, beam, 4] * (t_poly_s**4) + poly_config[station, beam, 5] * (t_poly_s**5))
                        # phase to apply (as a number of rotations)
                        beam_phase[station] = beam_poly[station] * config["channels"][coarse]
                        beam_phase_step[station] = beam_poly[station] * 3.616898148e-6 # 3.6e-6 = Frequency step between fine channels in GHz
                    
                    for fine in range(216):  # 216 fine channels per coarse channel 
                        # Compute the beam output (weighted sum of all the stations)
                        pol0_sum = 0
                        pol1_sum = 0
                        for station in range(total_stations):
                            # Get the sample generated in the testbench
                            sample_low16bits = np.uint16(abs_sample % 65536)
                            sample_high16bits = np.uint16(abs_sample // 65536)
                            pol0_re = np.frombuffer(sample_low16bits, dtype=np.int16)
                            pol0_im = coarse * total_stations + station  # = virtual channel
                            pol1_re = np.frombuffer(sample_high16bits, dtype=np.int16)
                            pol1_im = fine
                            pol0 = np.double(pol0_re) + 1j * np.double(pol0_im)
                            pol1 = np.double(pol1_re) + 1j * np.double(pol1_im)
                            # Multiply by the Jones Matrix
                            # (beams) x (Two buffers) x (1024 virtual channels) x (2 rows) x (2 columns) x (2 [real + imaginary])
                            vc = station + coarse * total_stations
                            jones00 = np.double(jones[beam,jones_select,vc,0,0,0]) + 1j * np.double(jones[beam,jones_select,vc,0,0,1])
                            jones01 = np.double(jones[beam,jones_select,vc,0,1,0]) + 1j * np.double(jones[beam,jones_select,vc,0,1,1])
                            jones10 = np.double(jones[beam,jones_select,vc,1,0,0]) + 1j * np.double(jones[beam,jones_select,vc,1,0,1])
                            jones11 = np.double(jones[beam,jones_select,vc,1,1,0]) + 1j * np.double(jones[beam,jones_select,vc,1,1,1])
                            pol0_jones = jones00 * pol0 + jones01 * pol1
                            pol1_jones = jones10 * pol0 + jones11 * pol1
                            # Apply the phase correction
                            phase_offset = beam_phase[station] + beam_phase_step[station] * fine
                            phase_correction = np.exp(1j * 2 * np.pi * phase_offset)
                            pol0_phase = pol0_jones * phase_correction
                            pol1_phase = pol1_jones * phase_correction
                            # accumulate across the stations
                            pol0_sum += pol0_phase
                            pol1_sum += pol1_phase
                        # total_frames,total_beams,total_coarse*216,256,2
                        raw_beamformed[frame, beam, coarse*216 + fine, time_sample, 0] = pol0_sum/32768
                        raw_beamformed[frame, beam, coarse*216 + fine, time_sample, 1] = pol1_sum/32768
                        
    # Calculate the amplitude in each packet
    # 9 bands (of 24 fine channels) per coarse channel, 8 time blocks (of 32 time samples)
    print("Calculating packet RMS values")
    packet_rms = np.zeros((total_frames, total_beams, 9 * total_coarse, 8))
    for frame in range(total_frames):
        for beam in range(total_beams):
            for freq in range(9 * total_coarse):
                for times in range(8):
                    pkt_samples = raw_beamformed[frame, beam, freq*24 : (freq*24+24), times*32 : (times*32 + 32), :]
                    total_samples = len(pkt_samples.flatten())
                    packet_rms[frame, beam, freq, times] = np.sqrt(np.sum(np.abs(pkt_samples.flatten()) ** 2) / total_samples)
    
    # Get the output of the simulation
    if args.bf_data:
        print("Loading the simulation output")
        (tb_packet_data, tb_packet_scales, tb_raw_beamformed) = get_tb_data(args.bf_data, total_frames, total_beams, total_coarse)

        # Get the power in the packet data, which should be stable since we haven't applied the scale factor
        print("Calculating the power in the simulation output raw packet values")
        tb_pkt_rms = np.zeros((total_frames, total_beams, 9*total_coarse, 8))
        for frame in range(total_frames):
            for beam in range(total_beams):
                for freq in range(9 * total_coarse):
                    for times in range(8):
                        tb_pkt = tb_packet_data[frame, beam, freq*24 : (freq*24+24), times*32 : (times*32 + 32), :]
                        tb_pkt_rms[frame, beam, freq, times] = np.sqrt(np.sum(tb_pkt * np.conj(tb_pkt)) / (2*32*24))
        
        fig, ax = plt.subplots()
        ax.plot(tb_pkt_rms.flatten(),'r.-') 
        plt.title('Simulation Packet un-unscaled RMS (flattened array of all packets)')
        fig, ax = plt.subplots()
        ax.plot(tb_packet_scales.flatten(),'r.-') 
        plt.title('Simulation Packet scale factors in the simulation')
        plt.show()
        
        # compare data on a packet basis between python and firmware 
        print("Comparing python and simulation output")
        max_max_diff = 0
        for frame in range(total_frames):
            for beam in range(total_beams):
                for freq in range(9 * total_coarse):
                    for times in range(8):
                        tb_pkt = tb_raw_beamformed[frame, beam, freq*24 : (freq*24+24), times*32 : (times*32 + 32), :]
                        python_pkt = raw_beamformed[frame, beam, freq*24 : (freq*24+24), times*32 : (times*32 + 32), :]
                        tb_diff = tb_pkt - python_pkt
                        max_diff = np.max(np.abs(tb_diff))
                        if max_diff > max_max_diff :
                            max_max_diff = max_diff
                            max_diff_loc = (frame, beam, freq, times)
                        if max_diff > 5 :
                            print(f"frame {frame}, beam {beam}, freq {freq}, time {times}, max diff = {max_diff}")
        print(f"max diff python vs firmware = {max_max_diff}, at frame {max_diff_loc[0]}, beam {max_diff_loc[1]}, freq {max_diff_loc[2]}, time {max_diff_loc[3]}")

        frame_plt = 0
        beam_plt = 1
        pol_plt = 0
        for fine_plt in range(8,216,123):
            fig, ax = plt.subplots()
            ax.plot(np.real(tb_raw_beamformed[frame_plt, beam_plt, fine_plt,:,pol_plt]),'r.-')
            ax.plot(np.real(raw_beamformed[frame_plt, beam_plt, fine_plt,:,pol_plt]),'go')
            ax.plot(np.imag(tb_raw_beamformed[frame_plt, beam_plt, fine_plt, :, pol_plt]), 'b.-')
            ax.plot(np.imag(raw_beamformed[frame_plt, beam_plt, fine_plt, :, pol_plt]), 'co')
            plt.title(f"fine = {fine_plt}")
            plt.show()
            #input("press enter")
    else:
        frame_plt = 1
        beam_plt = 0
        pol_plt = 0
        for fine_plt in range(8,216,13):
            fig, ax = plt.subplots()
            ax.plot(np.real(raw_beamformed[frame_plt, beam_plt, fine_plt,:,pol_plt]),'g.-')
            ax.plot(np.imag(raw_beamformed[frame_plt, beam_plt, fine_plt, :, pol_plt]), 'c.')
            plt.title(f"Python only, fine = {fine_plt}")
            plt.show()

    #if tb_valid:
    #    print(f"checked {sim_frames} frames against simulation")
    #    print(f"    data sample mismatch = {data_mismatch}, data samples matched = {data_match} ")
    #    print(f"    meta data mismatch = {meta_mismatch}, meta data matched = {meta_match}")

if __name__ == "__main__":
    main()