----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 09.11.2020 09:40:15
-- Module Name: jonesMatrixMult - Behavioral
-- Description: 
--  Full parallel 2x2 matrix-vector multiplier. 
--  
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity jonesMatrixMult_16b is
    Port(
        i_clk : in std_logic;
        -- The 2x2 matrix
        i_m00real : in std_logic_vector(15 downto 0);
        i_m00imag : in std_logic_vector(15 downto 0);
        i_m01real : in std_logic_vector(15 downto 0);
        i_m01imag : in std_logic_vector(15 downto 0);
        i_m10real : in std_logic_vector(15 downto 0);
        i_m10imag : in std_logic_vector(15 downto 0);
        i_m11real : in std_logic_vector(15 downto 0);
        i_m11imag : in std_logic_vector(15 downto 0);
        -- 2x1 vector
        i_v0real : in std_logic_vector(15 downto 0);
        i_v0imag : in std_logic_vector(15 downto 0);
        i_v1real : in std_logic_vector(15 downto 0);
        i_v1imag : in std_logic_vector(15 downto 0);
        i_rfi    : in std_logic;
        -- 2x1 vector result (5 clock latency)
        o_r0real : out std_logic_vector(35 downto 0);
        o_r0imag : out std_logic_vector(35 downto 0);
        o_r1real : out std_logic_vector(35 downto 0);
        o_r1imag : out std_logic_vector(35 downto 0)
    );
end jonesMatrixMult_16b;

architecture Behavioral of jonesMatrixMult_16b is

    -- create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name complexMult16x8
    -- set_property -dict [list CONFIG.Component_Name {complexMult16x8} CONFIG.BPortWidth {16} CONFIG.OptimizeGoal {Performance} CONFIG.OutputWidth {33} CONFIG.MinimumLatency {4}] [get_ips complexMult16x8]
    -- generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201030_212319/vitisAccelCore.srcs/sources_1/ip/complexMult16x8/complexMult16x8.xci]
    
    component complexMult16x16
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(79 DOWNTO 0));  -- real output in bits 32:0, imaginary output in bits 72:40
    end component;
    
    signal m00, m01, m10, m11 : std_logic_vector(31 downto 0);
    signal v0, v1 : std_logic_vector(31 downto 0);
    signal m00v0, m01v1, m10v0, m11v1 : std_logic_vector(79 downto 0);
    signal m00v0real, m00v0imag : std_logic_vector(35 downto 0);
    signal m01v1real, m01v1imag : std_logic_vector(35 downto 0);
    signal m10v0real, m10v0imag : std_logic_vector(35 downto 0);
    signal m11v1real, m11v1imag : std_logic_vector(35 downto 0);
    
    signal r0real, r0imag, r1real, r1imag : std_logic_vector(35 downto 0);
    
begin
  
    o_r0real <= r0real(35 downto 0);
    o_r0imag <= r0imag(35 downto 0);
    o_r1real <= r1real(35 downto 0);
    o_r1imag <= r1imag(35 downto 0);
  
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            
            r0real <= std_logic_vector(signed(m00v0real) + signed(m01v1real));
            r0imag <= std_logic_vector(signed(m00v0imag) + signed(m01v1imag));
            
            r1real <= std_logic_vector(signed(m10v0real) + signed(m11v1real));
            r1imag <= std_logic_vector(signed(m10v0imag) + signed(m11v1imag));
            
        end if;
    end process;

    m00 <= i_m00imag & i_m00real;
    m01 <= i_m01imag & i_m01real;
    m10 <= i_m10imag & i_m10real;
    m11 <= i_m11imag & i_m11real;

    v0 <= i_v0imag & i_v0real when i_rfi = '0' else (others => '0');
    v1 <= i_v1imag & i_v1real when i_rfi = '0' else (others => '0');
    
    cmult1 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m00, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v0, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m00v0   -- out(79:0)
    );
    m00v0real <= m00v0(32) & m00v0(32) & m00v0(32) & m00v0(32 downto 0);
    m00v0imag <= m00v0(72) & m00v0(72) & m00v0(72) & m00v0(72 downto 40);

    cmult2 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m01, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v1, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m01v1   -- out(79:0)
    );
    m01v1real <= m01v1(32) & m01v1(32) & m01v1(32) & m01v1(32 downto 0);
    m01v1imag <= m01v1(72) & m01v1(72) & m01v1(72) & m01v1(72 downto 40);

    cmult3 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m10, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v0, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m10v0   -- out(79:0)
    );
    m10v0real <= m10v0(32) & m10v0(32) & m10v0(32) & m10v0(32 downto 0);
    m10v0imag <= m10v0(72) & m10v0(72) & m10v0(72) & m10v0(72 downto 40);
    
    cmult4 : complexMult16x16
    port map (
        aclk  => i_clk,
        s_axis_a_tvalid => '1',
        s_axis_a_tdata  => m11, --  in(31:0);
        s_axis_b_tvalid => '1',
        s_axis_b_tdata  => v1, -- in(31:0);
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => m11v1   -- out(79:0)
    );
    m11v1real <= m11v1(32) & m11v1(32) & m11v1(32) & m11v1(32 downto 0);
    m11v1imag <= m11v1(72) & m11v1(72) & m11v1(72) & m11v1(72 downto 40);
    
end Behavioral;
