----------------------------------------------------------------------------------
-- Company: CSIRO 
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 07.12.2020 15:18:43
-- Module Name: PSTscaleFinal - Behavioral
-- Description: 
--  Scale 32 bit data to 16 bits, according to the shift and 4-bit mantissa.
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity PSTscaleFinal is
    port(
        i_clk : in std_logic;
        -- data input, with required scaling
        i_data : in std_logic_vector(31 downto 0);
        i_shift : in std_logic_vector(3 downto 0);  -- "0000" = use low 16 bits, "0001" = scale down by 2, i.e. use bits 16:1, etc.
        i_scale : in std_logic_vector(3 downto 0);  -- Unsigned scale factor, 1 integer bit + 3 fractional bits = 4 bits.
        -- data output
        o_data : out std_logic_vector(15 downto 0)  -- 5 clock latency from i_data, i_shift, i_scale.
    );
end PSTscaleFinal;

architecture Behavioral of PSTscaleFinal is

    signal dataDel1 : std_logic_vector(49 downto 0);  -- 32 bits input + 2 low bits + 16 sign-extension high bits.
    signal shiftDel1 : std_logic_vector(3 downto 0);
    signal scaleDel1 : std_logic_vector(4 downto 0);
    
    signal dataBitsDel2 : std_logic_vector(19 downto 0);
    signal dataBitsDel2ext, dataBitsDel2ext_x2, dataBitsDel2ext_x4, dataBitsDel2ext_x8 : std_logic_vector(23 downto 0);
    
    signal highBitsDel2 : std_logic_vector(13 downto 0);
    signal scaleDel2, scaleDel3 : std_logic_vector(4 downto 0);
    
    signal overflowDel3, overflowDel4 : std_logic;
    signal multSum0, multSum1, resultDel4 : std_logic_vector(23 downto 0);
    signal signDel3, signDel4 : std_logic;
    
    signal result : signed(29 downto 0);
    
begin
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
    
            -------------------------------------------------------------------
            -- 1st pipeline stage
            dataDel1 <= i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data(31) & i_data & "00";
            shiftDel1 <= i_shift;
            scaleDel1 <= '0' & i_scale;  -- Actual values from the "PSTBFScaling" module are from b0111 (=7/8) to b1111 (=1+7/8)
            
            ---------------------------------------------------------------------
            -- 2nd pipeline stage - scale by a power of 2
            
            --dataBitsDel2 <= dataDel1((to_integer(unsigned(i_shift))+19) downto (to_integer(unsigned(i_shift))));  -- 16 bit, with 2 extra high bits and 2 extra low bits.
            --highBitsDel2 <= dataDel1((to_integer(unsigned(i_shift))+33) downto (to_integer(unsigned(i_shift))+20));
            
            dataBitsDel2 <= dataDel1((to_integer(unsigned(shiftDel1))+19) downto (to_integer(unsigned(shiftDel1))));  -- 16 bit, with 2 extra high bits and 2 extra low bits.
            highBitsDel2 <= dataDel1((to_integer(unsigned(shiftDel1))+33) downto (to_integer(unsigned(shiftDel1))+20));            
            
            scaleDel2 <= scaleDel1;
            
            ----------------------------------------------------------------------------------
            -- 3rd pipeline stage - scale by i_scale
            if ((highBitsDel2 = "00000000000000" and dataBitsDel2(19) = '0') or
                (highBitsDel2 = "11111111111111" and dataBitsDel2(19) = '1')) then
                overflowDel3 <= '0';
            else
                overflowDel3 <= '1';
            end if;
            scaleDel3 <= scaleDel2;
            signDel3 <= highBitsDel2(13);
            
            -- Two pipeline stages implement a 4 bit multiplication:
            --resultDel3 <= signed(dataBitsDel2) * signed(scaleDel2);  -- 20 bit signed * 5 bit signed = 25 bit signed result. 
            if scaleDel2(1 downto 0) = "00" then
                multSum0 <= (others => '0');
            elsif scaleDel2(1 downto 0) = "01" then
                multSum0 <= dataBitsDel2ext;
            elsif scaleDel2(1 downto 0) = "10" then
                multSum0 <= dataBitsDel2ext_x2;
            else
                multSum0 <= std_logic_vector(unsigned(dataBitsDel2ext_x2) + unsigned(dataBitsDel2ext));
            end if;
            
            if scaleDel2(3 downto 2) = "00" then
                multSum1 <= (others => '0');
            elsif scaleDel2(3 downto 2) = "01" then
                multSum1 <= dataBitsDel2ext_x4;
            elsif scaleDel2(3 downto 2) = "10" then
                multSum1 <= dataBitsDel2ext_x8;
            else
                multSum1 <= std_logic_vector(unsigned(dataBitsDel2ext_x8) + unsigned(dataBitsDel2ext_x4));
            end if;
            
            ---------------------------------------------------------------------------------
            -- 4th pipeline stage
            -- second stage of the multiplier
            resultDel4 <= std_logic_vector(unsigned(multSum0) + unsigned(multSum1));
            overflowDel4 <= overflowDel3;
            signDel4 <= signDel3;
            
            -------------------------------------------------------------------------------------------
            -- 5th pipeline stage -- convert back to 16 bits, and saturate if needed.
            if (overflowDel4 = '0' and (resultDel4(23 downto 20) = "0000" or resultDel4(23 downto 20) = "1111")) then
                o_data <= std_logic_vector(resultDel4(20 downto 5));  -- Normal result; drop 3 bits since i_scale has 3 fractional bits, plus drop 2 bits since dataDel1 was scaled up by 2 bits. 
            elsif (overflowDel4 = '1') then
                if signDel4 = '0' then
                    o_data <= "0111111111111111"; -- biggest positive value
                else
                    o_data <= "1000000000000000";  -- Saturate negative
                end if;
            else -- saturate
                if resultDel4(23) = '0' then
                    o_data <= "0111111111111111";
                else
                    o_data <= "1000000000000000";
                end if;
            end if;
        end if;
    end process;
    
    
    dataBitsDel2ext    <= dataBitsDel2(19) & dataBitsDel2(19) & dataBitsDel2(19) & dataBitsDel2(19) & dataBitsDel2(19 downto 0);
    dataBitsDel2ext_x2 <= dataBitsDel2(19) & dataBitsDel2(19) & dataBitsDel2(19) & dataBitsDel2(19 downto 0) & '0';
    dataBitsDel2ext_x4 <= dataBitsDel2(19) & dataBitsDel2(19) & dataBitsDel2(19 downto 0) & "00";
    dataBitsDel2ext_x8 <= dataBitsDel2(19) & dataBitsDel2(19 downto 0) & "000";
    
    
end Behavioral;
