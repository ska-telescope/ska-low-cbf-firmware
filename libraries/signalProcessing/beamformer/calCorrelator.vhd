----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 05.05.2020 09:48:17 
-- Module Name: calCorrelator - Behavioral
-- Project Name: CryoPAF
-- Description: 
--  Calibration Correlator for the CryoPAF.
--  Calculates 2x224 correlation array.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library xpm;
use xpm.vcomponents.all;

entity calCorrelator is
    port(
        i_clk : in std_logic;
        -- interface to the memory with the port data
        o_portAddr : out std_logic_vector(7 downto 0);
        i_portData : in std_logic_vector(23 downto 0); -- 3 cycle read latency.
        -- the ports to correlate against
        i_port0 : in std_logic_vector(7 downto 0);
        i_port1 : in std_logic_vector(7 downto 0);
        -- start correlation
        i_start : in std_logic;  -- new data available
        i_reset : in std_logic;  -- reset the correlation, sampled on i_start;
        -- Read out the correlation result
        i_calDump  : in std_logic;  -- trigger dump of the correlation; Read on i_start; if 0, then normal correlation processing, otherwise dump the correlation. Generates a packet 4x224 = 896 clocks long.
        o_corData  : out std_logic_vector(35 downto 0);
        o_corValid : out std_logic
    );
end calCorrelator;

architecture Behavioral of calCorrelator is

    -- 12+12bit x 12+12bit complex multiplier
    --  create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name cmult12x12
    --  set_property -dict [list CONFIG.Component_Name {cmult12x12} CONFIG.APortWidth {12} CONFIG.BPortWidth {12} CONFIG.OptimizeGoal {Performance} CONFIG.OutputWidth {25} CONFIG.MinimumLatency {4}] [get_ips cmult12x12]
    --  generate_target {instantiation_template} [get_files /data/GIANT_1/ateg/hum089/cryoPAF/test_BF/test_BF.srcs/sources_1/ip/cmult12x12/cmult12x12.xci]
    component cmult12x12
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    end component;

    signal port0, port1 : std_logic_vector(7 downto 0);
    type corfsm_type is (start, send_packet_start, send_packet_run, get_port0, cor_port0_start, cor_port0_run, get_port1, cor_port1_start, cor_port1_run, done);
    signal corfsm, corfsmDel1, corfsmDel2, corfsmDel3, corfsmDel4, corfsmDel5, corfsmDel6, corfsmDel7, corfsmDel8, corfsmDel9, corfsmDel10, corfsmDel11 : corfsm_type := done;
    signal portAddr, portAddrDel2, portAddrDel3, portAddrDel4, portAddrDel5, portAddrDel6, portAddrDel7, portAddrDel8, portAddrDel9, portAddrDel10 : std_logic_vector(7 downto 0);
    signal portDataRe : std_logic_vector(11 downto 0);
    signal portDataIm : std_logic_vector(11 downto 0);
    signal colPortDataRe, colPortDataIm : std_logic_vector(11 downto 0);
    signal rowPortDataRe, rowPortDataIm : std_logic_vector(11 downto 0);
    signal loadColDel4, loadRowDel4 : std_logic;
    signal corMultDin1, corMultDin2 : std_logic_vector(31 downto 0);
    
    signal corRdAddr, corWrAddr : std_logic_vector(8 downto 0);
    signal corWrEn : std_logic_vector(0 downto 0);
    signal corReset, corResetDel1, corResetDel2, corResetDel3, corResetDel4, corResetDel5, corResetDel6, corResetDel7, corResetDel8, corResetDel9 : std_logic;
    signal sumRe0, sumIm0, sumRe1, sumIm1 : std_logic_vector(35 downto 0);
    signal corMultDout : std_logic_vector(63 downto 0);
    signal corDout, corDin : std_logic_vector(71 downto 0);
    signal corSumRe, corSumIm : std_logic_vector(35 downto 0);
    signal calDump : std_logic;
    signal corReIm, corReImDel2, corReImDel3, corReImDel4, corReImDel5, corReImDel6, corReImDel7, corReImDel8, corReImDel9 : std_logic;
    signal corCol, corColDel2, corColDel3, corColDel4, corColDel5 : std_logic;
    signal corValid, corValidDel2, corValidDel3, corValidDel4, corValidDel5, corValidDel6, corValidDel7, corValidDel8, corValidDel9 : std_logic;
    
begin

    o_portAddr <= portAddr;

    portDataRe <= i_portData(11 downto 0);
    portDataIm <= i_portData(23 downto 12);

    process(i_clk)
    begin
        if rising_edge(i_clk) then
            calDump <= i_calDump;
            if i_start = '1' then
                port0 <= i_port0;
                port1 <= i_port1;
                corfsm <= start;
                corReset <= i_reset;
            else
                case corfsm is
                    when start =>
                        if calDump = '1' then
                            corfsm <= send_packet_start;
                        else
                            corfsm <= get_port0;
                        end if;
                        corValid <= '0';
                        
                    -- States for reading out the correlation products.
                    when send_packet_start =>
                        portAddr <= (others => '0');
                        corReIm <= '0'; -- toggles between 0 and 1 to enable holding the portAddr at the same value for 2 clocks, so re and im data can be muxed onto the output bus.
                        corCol <= '0';  
                        corfsm <= send_packet_run;
                        corValid <= '0';
                        
                    when send_packet_run =>
                        corValid <= '1';
                        if corReIm = '0' then
                            corReIm <= '1';
                        else
                            corReIm <= '0';
                            if (unsigned(portAddr) = 223) then
                                portAddr <= (others => '0');
                                if corCol = '1' then
                                    corfsm <= done;
                                else
                                    portAddr <= (others => '0');
                                    corCol <= '1';
                                end if;
                            else
                                portAddr <= std_logic_vector(unsigned(portAddr) + 1);
                            end if;
                        end if;
                    
                    -- states for running the correlation
                    when get_port0 =>
                        corValid <= '0';
                        portAddr <= port0;
                        corfsm <= cor_port0_start;

                    when cor_port0_start =>
                        corValid <= '0';
                        portAddr <= (others => '0');
                        corfsm <= cor_port0_run;
                    
                    when cor_port0_run =>
                        corValid <= '0';
                        portAddr <= std_logic_vector(unsigned(portAddr) + 1);
                        if (unsigned(portAddr) = 254) then
                            corfsm <= get_port1;
                        end if;
                    
                    when get_port1 =>
                        corValid <= '0';
                        portAddr <= port1;
                        corfsm <= cor_port1_start;
                        
                    when cor_port1_start =>
                        corValid <= '0';
                        portAddr <= (others => '0');
                        corfsm <= cor_port1_run;
                        
                    when cor_port1_run =>
                        corValid <= '0';
                        portAddr <= std_logic_vector(unsigned(portAddr) + 1);
                        if (unsigned(portAddr) = 254) then
                            corfsm <= done;
                        end if;
                    
                    when done =>
                        corValid <= '0';
                        corfsm <= done; -- wait till next time frame is triggered with i_start.
                    
                    when others =>
                        corfsm <= done;
                end case;
            end if;
            corfsmDel1 <= corfsm;
            corResetDel1 <= corReset;
            
            -- processing pipeline
            --
            -- clk    signals
            -- ---    -------
            --  1     corfsmDel1, portAddr, 
            --  2               
            --  3     
            --  4     corfsmDel4, i_portData, 
            --  5     corfsmDel5, colPortData, rowPortData
            --  6     corfsmDel6,                           corRdAddr
            --  7     corfsmDel7, 
            --  8     corfsmDel8, 
            --  9     corfsmDel9, corMultDout               corDout
            --  10    corfsmDel10, sumRe0, sumIm0, sumRe1, sumIm1 
            --  11    corfsmDel11, corSumRe, corSumIm
            --  12    corfsmDel12, 
            --  13    corfsmDel13, 
            
            -- Pipeline stage 1. 
            corfsmDel2 <= corfsmDel1;
            corResetDel2 <= corResetDel1;
            corColDel2 <= corCol;
            corReImDel2 <= corReIm;
            portAddrDel2 <= portAddr;
            corValidDel2 <= corValid;
            
            -- Pipeline stage 2. 
            corfsmDel3 <= corfsmDel2;
            corResetDel3 <= corResetDel2;
            corColDel3 <= corColDel2;
            corReImDel3 <= corReImDel2;
            portAddrDel3 <= portAddrDel2;
            corValidDel3 <= corValidDel2;
            
            -- Pipeline stage 3. 
            corfsmDel4 <= corfsmDel3;
            corResetDel4 <= corResetDel3;
            corColDel4 <= corColDel3;
            corReImDel4 <= corReImDel3;
            corValidDel4 <= corValidDel3;
            portAddrDel4 <= portAddrDel3;
            if corfsmDel3 = get_port0 or corfsmDel3 = get_port1 then
                loadColDel4 <= '1';
            else
                loadColDel4 <= '0';
            end if;
            if corfsmDel3 = cor_port0_start or corfsmDel3 = cor_port0_run or corfsmDel3 = cor_port1_start or corfsmDel3 = cor_port1_run then
                loadRowDel4 <= '1';
            else
                loadRowDel4 <= '0';
            end if;

            -- Pipeline stage 4. 
            corfsmDel5 <= corfsmDel4;
            corResetDel5 <= corResetDel4;
            portAddrDel5 <= portAddrDel4;
            corValidDel5 <= corValidDel4;
            corColDel5 <= corColDel4;
            corReImDel5 <= corReImDel4;
            if loadColDel4 = '1' then -- load column value for the correlation
                colPortDataRe <= portDataRe;
                colPortDataIm <= portDataIm;
            end if;
            if loadRowDel4 = '1' then
                rowPortDataRe <= portDataRe;
                rowPortDataIm <= std_logic_vector(-signed(portDataIm)); -- conjugate needed for correlation.
            end if;
            
            -- Pipeline stage 5.
            corfsmDel6 <= corfsmDel5;
            corResetDel6 <= corResetDel5;
            corReImDel6 <= corReImDel5;
            corValidDel6 <= corValidDel5;
            portAddrDel6 <= portAddrDel5;
            corRdAddr(7 downto 0) <= portAddrDel5;
            if (corfsmDel5 = send_packet_start or corfsmDel5 = send_packet_run) then
                corRdAddr(8) <= corColDel5;
            elsif (corfsmDel5 = cor_port0_start or corfsmDel5 = cor_port0_run) then
                corRdAddr(8) <= '0';
            else
                corRdAddr(8) <= '1';
            end if;

            -- Pipeline stage 6. 
            corfsmDel7 <= corfsmDel6;
            corResetDel7 <= corResetDel6;
            corValidDel7 <= corValidDel6;
            corReImDel7 <= corReImDel6;
            portAddrDel7 <= portAddrDel6;
            
            -- Pipeline stage 7. 
            corfsmDel8 <= corfsmDel7;
            corResetDel8 <= corResetDel7;
            corValidDel8 <= corValidDel7;
            corReImDel8 <= corReImDel7;
            portAddrDel8 <= portAddrDel7;
            
            -- Pipeline stage 8. 
            corfsmDel9 <= corfsmDel8;
            corResetDel9 <= corResetDel8;
            corValidDel9 <= corValidDel8;
            corReImDel9 <= corReImDel8;
            portAddrDel9 <= portAddrDel8;
            
            -- Pipeline stage 9. 
            corfsmDel10 <= corfsmDel9;
            portAddrDel10 <= portAddrDel9;
            
            sumRe0 <= corMultDout(24) & corMultDout(24) & corMultDout(24) & corMultDout(24) & corMultDout(24) & corMultDout(24) & corMultDout(24) & corMultDout(24) & corMultDout(24) & corMultDout(24) & corMultDout(24) & corMultDout(24 downto 0);
            sumIm0 <= corMultDout(56) & corMultDout(56) & corMultDout(56) & corMultDout(56) & corMultDout(56) & corMultDout(56) & corMultDout(56) & corMultDout(56) & corMultDout(56) & corMultDout(56) & corMultDout(56) & corMultDout(56 downto 32);
            if corResetDel9 = '1' then
                sumRe1 <= (others => '0');
                sumIm1 <= (others => '0');
            else
                sumRe1 <= corDout(35 downto 0);
                sumIm1 <= corDout(71 downto 36);
            end if;
            
            -- Put data onto the output bus
            if corReImDel9 = '0' then
                o_corData <= corDout(35 downto 0);
            else
                o_corData <= corDout(71 downto 36);
            end if;
            o_corValid <= corValidDel9;
            
            -- Pipeline stage 10. 
            corfsmDel11 <= corfsmDel10;
            --portAddrDel11 <= portAddrDel10;
            corSumRe <= std_logic_vector(unsigned(sumRe0) + unsigned(sumRe1));
            corSumIm <= std_logic_vector(unsigned(sumIm0) + unsigned(sumIm1));
            corWrAddr(7 downto 0) <= portAddrDel10;
            if (corfsmDel10 = cor_port0_start or corfsmDel10 = cor_port0_run) then
                corWrAddr(8) <= '0';
                corWrEn(0) <= '1';
            elsif (corfsmDel10 = cor_port1_start or corfsmDel10 = cor_port1_run) then
                corWrAddr(8) <= '1';
                corWrEn(0) <= '1';
            else
                corWrAddr(8) <= '0';
                corWrEn(0) <= '0';
            end if;
            -- 
            
        end if;
    end process;
    
    corMultDin1(11 downto 0) <= colPortDataRe;
    corMultDin1(15 downto 12) <= "0000";
    corMultDin1(27 downto 16) <= colPortDataIm;
    corMultDin1(31 downto 28) <= "0000";
    corMultDin2(11 downto 0) <= rowPortDataRe;
    corMultDin2(15 downto 12) <= "0000";
    corMultDin2(27 downto 16) <= rowPortDataIm;
    corMultDin2(31 downto 28) <= "0000";
    
    your_instance_name : cmult12x12
    PORT MAP (
        aclk => i_clk,
        s_axis_a_tvalid    => '1',
        s_axis_a_tdata     => corMultDin1,   -- data in, 32 bit; (11:0) real, (27:16) imaginary
        s_axis_b_tvalid    => '1',
        s_axis_b_tdata     => corMultDin2,   -- data in, 32 bit; (11:0) real, (27:16) imaginary
        m_axis_dout_tvalid => open,
        m_axis_dout_tdata  => corMultDout -- data out, (24:0) real, (56:32) imaginary
    );
    
    -- BRAM to hold the correlation.
    -- 512x72;
    -- Each correlation is stored as 36+36 bit complex.
    -- Single Buffered, space for a 2x256 correlation matrix.
    BEAM_BRAM_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 9,               -- DECIMAL  -- 512 deep
        ADDR_WIDTH_B => 9,               -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 72,        -- DECIMAL
        --CASCADE_HEIGHT => 0,           -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "auto",      -- String
        MEMORY_SIZE => 512*72,           -- DECIMAL; total size in bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 72,         -- DECIMAL
        READ_LATENCY_B => 3,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        --SIM_ASSERT_CHK => 0,           -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 72,        -- DECIMAL
        WRITE_MODE_B => "write_first"      -- String
    )
    port map (
        dbiterrb => open,        -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port B.
        doutb    => corDout,    -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,        -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
        addra    => corWrAddr,  -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb    => corRdAddr,  -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka     => i_clk,       -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
        clkb     => i_clk,       -- 1-bit input, unused when clocking mode is "common_clock"
        dina     => corDin,     -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena      => '1',         -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when write operations are initiated. Pipelined internally.
        enb      => '1',         -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read operations are initiated. Pipelined internally.
        injectdbiterra => '0',   -- 1-bit input: Controls double bit error injection on input data when ECC enabled 
        injectsbiterra => '0',   -- 1-bit input: Controls single bit error injection on input data when ECC enabled
        regceb => '1',           -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb   => '0',           -- 1-bit input: Reset signal for the final port B output register stage. Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.
        sleep  => '0',           -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea    => corWrEn       -- WRITE_DATA_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );
    
    corDin <= corSumIm & corSumRe;
    
end Behavioral;
