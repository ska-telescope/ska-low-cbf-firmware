----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 08.11.2020 09:04:05
-- Module Name: PSTbeamformer - Behavioral
-- Description: 
--
--  PST beamformer Data flow:
--  ------------------------
--  
--                                          i_data -------> 2x2 Jones matrix multiplier  ---------->  phase rotation multiplier --> Accumulate across stations --> output packetiser
--   i_virtualChannel --> lookup Jones Matrix -> Jones --/                                       /
--                                                                                              /
--   i_virtualChannel --> phase lookup --------> time+frequency correction --> sin/cos lookup -
--
--
-- Packetiser Data Flow
-- --------------------
--
--   Notation : T = Time, runs from 0 to 31
--              S = Station, runs from 0 to however many stations there are (up to 511)
--              F = Fine channel, run from 0 to 216 = 9 packets of fine channels = 1 coarse channel
--
--    For timeGroup = 0:(<time samples per corner turn>/32 - 1) -- 53 ms corner turn, so this is 0:(256/32-1) = 0:7
--       For Coarse = 0:(i_coarse-1)                            -- For 512 stations, 2 coarse, this is 0:1
--          For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
--             For Station = 0:(i_stations-1)
--                For fine_offset = 0:3:213                     -- Total of 216 fine channels per SPS channel
--                   Send a 192 bit word (i.e. 3 fine channels x (2 pol) x (16+16 bit complex data)).
--
--
--   So Data in :  | All data for 9 packets = 32 times, 216 fine channels | All data for 9 packets = 216 fine channels for the next coarse channel | < etc..>
--                  <T=0                 > <T=1>         ...        <T=31> <T=0> <T=1> ...                                             <T=31>
--                   <S=0     > <S=1>... 
--                    <F=0:216>
--                     |
--                 Fine channels are delivered in groups of 3,
--                 so 216 fine channels takes 72 clocks.
--  
--  Registers
--  Each beamformer instance has 64kb of register address space, implemented in an ultraRAM.
--   Address 0-32767 = Jones Matrices memory.
--                     This is double buffered so that it can be updated.
--                     Each half of the buffer is 16 kbytes = (1024 matrices) * (16 bytes/matrix)
--                     Each matrix is 4 x (16+16) bit complex numbers.
--                     There is one matrix for each of the 1024 virtual channels.
--
--   Address 32768-36863 = weights.
--                         Double buffered so it can be updated.
--                         Each half of the buffer is 2 kbytes = (1024 virtual channels) * (2 bytes each)
--                         2 byte weight per virtual channel.
--
--  Note that the weight value is used to calculated the overall weight for a sample,
--  but is not used to actually weight the data; the weight must also be incorporated into the Jones matrices.
--
--  ------------------------------------------------------------------------------
--  Data output
--   - Output packets are 
--      (32 time samples) x (24 fine channels) x (2 pol) x (2+2 bytes) = 6144 bytes
--      8-byte output bus, so clocks per output packet = 768*(16 beams) = 12288
--      Total clocks for a full coarse channel = 9 * 12288 = 110592
--   - There are 9 output packets per coarse channel
--      minimum clocks for data input for 32 time samples for a coarse channel 
--       = 32 x (216/3) x (S) = 2304*S  (where S is the number of stations)
--   - So provided there are at least 8 stations, there will be time to get the data packets out.
--       2304 * 8 = 18432 clocks.
----------------------------------------------------------------------------------
library IEEE, bf_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;

entity PSTbeamformer_dp is
    generic(
        g_BEAM_NUMBER : integer;
        -- Number of clock cycles to wait after a packet is ready to send before actually sending it on the BFData interface.
        -- This is to ensure there are no clashes on the BFdata bus between different beamformers.
        -- Up to 20 bits. It takes about 776 clocks to send a packet, so a 20 bit value enables sharing between up to 1048575/776 = 1351 beamformer instances. 
        g_PACKET_OFFSET : natural range 0 to 1048575
    );
    Port(
        i_BF_clk : in std_logic; -- 400MHz clock
        ---------------------------------------------------------------------------
        -- Data in to the beamformer
        i_data    : in std_logic_vector(191 downto 0); -- 3 consecutive fine channels delivered every clock. Must have a six cycle latency relative to the other inputs ("i_fine", "i_station", etc)
        i_flagged : in std_logic_vector(2 downto 0);
        i_fine    : in std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in i_data is (i_fine*3)
        i_coarse  : in std_logic_vector(9 downto 0);  -- Coarse channel index.
        i_firstStation : in std_logic;                -- Indicates that this is the first station in a group of stations. Used to reset the beam accumulator.
        i_lastStation  : in std_logic;                -- Indicates that this is the last station in a group of stations. Used to move the result of the accumulation to the output buffer.
        i_timeStep     : in std_logic_vector(4 downto 0);   -- Timestep, runs from 0 to 31. There are 32 timesteps per output packet.
        i_virtualChannel : in std_logic_vector(9 downto 0); -- virtual channel.
        -- i_packetCount should be a count of the output frame relative to the SKA epoch 
        -- Each output packet is 32 time samples * 207.36us/sample = 6.635520 ms. (40 bit value = 2^40 * 6.6ms = 230 years)
        i_packetCount : in std_logic_vector(39 downto 0);   -- The packet count for this packet, based on the original packet count from LFAA.
        i_pktOdd  : in std_logic;
        i_valid   : in std_logic;
        --------------------------------------------------------------------------
        -- Phase data
        -- This is captured in a pair of registers, which form a 2-deep FIFO, so data for the next station or time can be
        -- loaded while the current station/time is being used.
        i_phase_virtualChannel : in std_logic_vector(9 downto 0);
        i_phase_timeStep : in std_logic_vector(7 downto 0);
        i_phase_beam : in std_logic_vector(3 downto 0);
        i_phase : in std_logic_vector(23 downto 0);      -- Phase at the start of the coarse channel.
        i_phase_step : in std_logic_vector(23 downto 0); -- Phase step per fine channel.
        i_phase_valid : in std_logic;
        i_phase_clear : in std_logic; -- start of a corner turn frame, ensure we don't use old phase values.
        -- Pass on phase data to the next beamformer.
        o_phase_virtualChannel : out std_logic_vector(9 downto 0);
        o_phase_timeStep : out std_logic_vector(7 downto 0);
        o_phase_beam : out std_logic_vector(3 downto 0);
        o_phase : out std_logic_vector(23 downto 0);      -- Phase at the start of the coarse channel.
        o_phase_step : out std_logic_vector(23 downto 0); -- Phase step per fine channel.
        o_phase_valid : out std_logic;
        o_phase_clear : out std_logic;
        -- Data out to the next beamformer (pipelined version of the data in) (2 pipeline stages in this module)
        o_data    : out std_logic_vector(191 downto 0);  -- pipelined i_data 
        o_flagged : out std_logic_vector(2 downto 0);
        o_fine    : out std_logic_vector(7 downto 0);   -- pipelined i_fine
        o_coarse  : out std_logic_vector(9 downto 0);   -- pipelined i_coarse
        o_firstStation : out std_logic;                 -- pipelined i_firstStation
        o_lastStation  : out std_logic;                 -- pipelined i_lastStation
        o_timeStep     : out std_logic_vector(4 downto 0);   -- pipelined i_timeStep
        o_virtualChannel : out std_logic_vector(9 downto 0); --
        o_packetCount : out std_logic_vector(39 downto 0);   -- The packet count for this packet, based on the original packet count from LFAA.
        o_pktOdd  : out std_logic;
        o_valid   : out std_logic;
        ------------------------------------------------------------------------
        -- Register interface (just address + data)
        i_regAddr : in std_logic_vector(23 downto 0); -- Byte address of the data (low two bits will always be "00")
        i_regData : in std_logic_vector(31 downto 0);
        i_WrEn    : in std_logic;
        -- Pipelined version of the register interface, for the next beamformer.
        o_regAddr : out std_logic_Vector(23 downto 0);
        o_regData : out std_logic_Vector(31 downto 0);
        o_wrEn    : out std_logic;
        -- register read data, 5 cycle latency from i_regAddr
        o_regReadData : out std_logic_vector(31 downto 0);
        -- Miscellaneous register settings:
        -- The phase information updates with every timestep, so i_phaseBuffer must be set at the correct time, i.e. corresponding to the time for the initial value of the phase in the registers. 
        i_jonesBuffer : in std_logic;  -- which of the two buffers to use for the Jones matrices
        i_beamsEnabled : in std_logic_vector(7 downto 0); -- number of beams enabled; This beamformer will output data packets if g_BEAM_NUMBER < i_beamsEnabled
        i_scale_exp_frac : in std_logic_vector(7 downto 0);
        -- pipelined outputs for buffer settings
        o_jonesBuffer : out std_logic;
        o_beamsEnabled : out std_logic_vector(7 downto 0);
        o_scale_exp_frac : out std_logic_vector(7 downto 0);
        ---------------------------------------------------------------------
        -- Packets of data from the previous beamformer, to be passed on to the next beamformer.
        i_BFdata        : in std_logic_vector(63 downto 0);
        i_BFpacketCount : in std_logic_vector(39 downto 0);
        i_BFBeam        : in std_logic_vector(7 downto 0);
        i_BFFreqIndex   : in std_logic_vector(10 downto 0);
        i_BFvalid       : in std_logic;
        -- Packets of data out to the 100G interface
        o_BFdata        : out std_logic_vector(63 downto 0);  -- PSR packetiser expects the first 16 bit value in bits 63:48, next in bits 47:32, etc.
        o_BFpacketCount : out std_logic_vector(39 downto 0);  -- Copy of i_packetCount, held for the duration of the output packets with the same timestamp. 
        o_BFFreqIndex   : out std_logic_vector(10 downto 0);  -- bits(3:0) index the fine channel, and count from 0 to 8 inclusive for the 9 different output packets per LFAA coarse channel, bits(10:4) index the coarse channel
        o_BFBeam        : out std_logic_vector(7 downto 0);  
        o_BFvalid       : out std_logic;
        i_badPacket     : in std_logic;  -- just used to trigger the debug core.
        o_badPacket     : out std_logic
    );
    
    -- prevent optimisation between adjacent instances of the PSTbeamformer.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of PSTbeamformer_dp : entity is "yes";
    
end PSTbeamformer_dp;

architecture Behavioral of PSTbeamformer_dp is

    -- create_ip -name dds_compiler -vendor xilinx.com -library ip -version 6.0 -module_name sincosLookup
    -- set_property -dict [list CONFIG.Component_Name {sincosLookup} CONFIG.PartsPresent {SIN_COS_LUT_only} CONFIG.Noise_Shaping {None} CONFIG.Phase_Width {12} CONFIG.Output_Width {18} CONFIG.Amplitude_Mode {Unit_Circle} CONFIG.Parameter_Entry {Hardware_Parameters} CONFIG.Has_Phase_Out {false} CONFIG.DATA_Has_TLAST {Not_Required} CONFIG.S_PHASE_Has_TUSER {Not_Required} CONFIG.M_DATA_Has_TUSER {Not_Required} CONFIG.Latency {6} CONFIG.Output_Frequency1 {0} CONFIG.PINC1 {0}] [get_ips sincosLookup]
    --generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201030_212319/vitisAccelCore.srcs/sources_1/ip/sincosLookup/sincosLookup.xci]
    -- 12 bit phase gives 10 effective bits of precision in the sin/cos output. Since the input data to the beamformer is only 8 bits, 
    -- errors in the sin/cos will be negligible.
    -- 12 bit phase uses 1x18k BRAM.
    component sincosLookup
    port (
        aclk                : in std_logic;
        s_axis_phase_tvalid : in std_logic;
        s_axis_phase_tdata  : in std_logic_vector(15 downto 0);  -- Phase in bits 11:0
        m_axis_data_tvalid  : out std_logic;
        m_axis_data_tdata   : out std_logic_vector(47 downto 0)); -- cosine in bits 17:0, sine in bits 41:24, 6 clock latency
    end component;

    -- create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name BF_cMult27x18
    -- set_property -dict [list CONFIG.Component_Name {BF_cMult27x18} CONFIG.APortWidth {27} CONFIG.BPortWidth {18} CONFIG.OptimizeGoal {Performance} CONFIG.OutputWidth {46} CONFIG.MinimumLatency {4}] [get_ips BF_cMult27x18]
    -- generate_target {instantiation_template} [get_files /home/hum089/data/low-cbf-firmware/build/alveo/vivado/vitisAccelCore/vitisAccelCore_build_201030_212319/vitisAccelCore.srcs/sources_1/ip/BF_cMult27x18/BF_cMult27x18.xci]
    component BF_cMult27x18
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(95 DOWNTO 0));
    end component;
    
    -- convergent_round 35:0 -> 26:0, drop msb
    function convergent_round(din : std_logic_vector(35 downto 0)) return std_logic_vector is
    begin
        if (din(7 downto 0) = "10000000" and din(8) = '1') or (din(7) = '1' and din(6 downto 0) /= "0000000") then
            return std_logic_vector(unsigned(din(34 downto 8)) + 1);
        else
            return din(34 downto 8);
        end if;
    end convergent_round;
    
    
    signal jonesBuffer : std_logic;
    signal regAddr, regAddrDel1, regAddrDel2, regAddrDel3 : std_logic_vector(23 downto 0);
    signal regData : std_logic_vector(31 downto 0);
    signal regWrEn : std_logic;
    signal BFdata : std_logic_vector(63 downto 0);
    signal BFpacketCount : std_logic_vector(39 downto 0);
    signal BFbeam : std_logic_vector(7 downto 0);
    signal regModuleSelect : std_logic_vector(7 downto 0);
    signal jonesWrEn, weightWrEn : std_logic;
    signal jonesRegReadData, weightRegReadData : std_logic_vector(31 downto 0);
    
    signal data    : std_logic_vector(191 downto 0); -- 3 consecutive fine channels delivered every clock.
    signal fine    : std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
    signal coarse  : std_logic_vector(9 downto 0);  -- Coarse channel index.
    signal firstStation : std_logic;                -- First station.
    signal lastStation : std_logic;                 -- Last Station.
    signal timeStep    : std_logic_vector(4 downto 0);  -- time step within the output packet.
    signal virtualChannel : std_logic_vector(9 downto 0);
    signal virtualChannelDel : t_slv_10_arr(15 downto 0);
    signal packetCount : std_logic_vector(39 downto 0); -- The packet count for this output packet relative to the SKA epoch
    signal valid   : std_logic;
    
    signal fineDel : t_slv_8_arr(31 downto 0); -- delay line for fine frequency channel
    
    attribute max_fanout : integer;
    attribute max_fanout of fineDel : signal is 200;        -- FANOUT of 1000+ at LL 1
    
    signal timeStepDel : t_slv_5_arr(31 downto 0); -- delay line for the time step
    attribute max_fanout of timeStepDel : signal is 10;     -- FANOUT of 97 at LL 5

    signal flaggedDel : t_slv_3_arr(31 downto 0);  -- delay line for RFI flagging. 

    signal firstStationDel : std_logic_vector(31 downto 0);
    attribute max_fanout of firstStationDel : signal is 10; -- FANOUT of 96 at LL 6
    
    
    signal lastStationDel : std_logic_vector(31 downto 0);
    signal validDel : std_logic_vector(31 downto 0);
    
    signal loadJones : std_logic := '0';
    signal jonesAddr : std_logic_vector(14 downto 0);
    signal jonesRdData : std_logic_vector(63 downto 0);
    signal loadJonesDel1, loadJonesDel2, loadJonesDel3, loadJonesDel4, loadJonesDel5, loadJonesDel6 : std_logic := '0';
    signal JonesWord0Temp : std_logic_vector(63 downto 0);
    signal jonesWord0, jonesWord1 : std_logic_vector(63 downto 0);
    signal matrixMultR0real, matrixMultR1real, matrixMultR0imag, matrixMultR1imag : t_slv_36_arr(2 downto 0);
    signal phaseDel2 : std_logic_vector(15 downto 0);
    signal sinCos : t_slv_48_arr(2 downto 0);
    --signal phaseRdData : std_logic_vector(63 downto 0);
    signal matrixMultPol0 : t_slv_64_arr(2 downto 0);
    signal matrixMultPol1 : t_slv_64_arr(2 downto 0);
    
    signal weightedPol0, weightedPol1 : t_slv_96_arr(2 downto 0);
    signal weightedPol0Real, weightedPol0Imag, weightedPol1Real, weightedPol1Imag : t_slv_32_arr(2 downto 0);
    signal weightedSumPol0Real, weightedSumPol1Real, weightedSumPol0Imag, weightedSumPol1Imag : t_slv_32_arr(2 downto 0);
    signal accDout, accDin : t_slv_128_arr(2 downto 0);
    
    signal phaseStepFreqTemp_x48, phaseStepTime : std_logic_vector(23 downto 0);
    signal phaseStepFreq, phaseStepFreq_x2, phaseDel1, phaseDel1_plus1, phaseDel1_plus2 : std_logic_vector(23 downto 0);
    signal phaseWeight : std_logic_vector(15 downto 0);
    signal loadPhase : std_logic_vector(1 downto 0);
    
    signal accumulatorPol0Real, accumulatorPol0Imag, accumulatorPol1Real, accumulatorPol1Imag : t_slv_32_arr(2 downto 0);
    signal lastFineGroup : std_logic := '0';
    signal phaseWeightDel1 : std_logic_vector(31 downto 0);
    signal flagged : std_logic_vector(2 downto 0);
    signal weightSum : t_slv_32_arr(2 downto 0);
    signal weightAccDout : t_slv_32_arr(2 downto 0);
    
    signal weightAccReadAddr : t_slv_7_arr(2 downto 0);  -- std_logic_vector(6 downto 0);
    signal weightAccWriteAddr : t_slv_7_arr(2 downto 0); -- std_logic_vector(6 downto 0);
    signal AccWriteAddr : t_slv_7_arr(2 downto 0); -- std_logic_vector(6 downto 0);
    signal AccReadAddr : t_slv_7_arr(2 downto 0); -- std_logic_vector(6 downto 0);

    attribute max_fanout of weightAccReadAddr   : signal is 100;        -- FANOUT of 775+ at LL 2
    attribute max_fanout of weightAccWriteAddr  : signal is 100;        
    attribute max_fanout of AccWriteAddr        : signal is 100;        
    attribute max_fanout of AccReadAddr         : signal is 100;        
    
    signal weightAccWrEn : std_logic_vector(0 downto 0);
    
    signal maxWeightSum : t_slv_32_arr(2 downto 0);
    attribute max_fanout of maxWeightSum : signal is 10;     -- FANOUT of 97 at LL 5

    signal maxWeightAccDout : t_slv_32_arr(2 downto 0);
    signal weightStoreDin, weightStoreDout, tempWeightStoreDout : t_slv_64_arr(2 downto 0);
    signal tempWeightStoreWrEn : std_logic_vector(0 downto 0);
    signal weightsCountUp, weightsCountDown : std_logic_vector(11 downto 0);
    signal div32ValidIn : std_logic;
    signal dividerRunning, getWeights : std_logic;
    
    signal packetBufPol0Din, packetBufPol1Din : t_slv_64_arr(2 downto 0);
    signal packetBufPol0Dout, packetBufPol1Dout : t_slv_64_arr(2 downto 0);
    signal packetBufWrAddr : std_logic_vector(12 downto 0);
    signal packetBufWrEn : std_logic_vector(2 downto 0);
    signal AccWrEn : std_logic_vector(0 downto 0);
    signal absPol0Sum : t_slv_32_arr(2 downto 0);
    signal absPol1Sum : t_slv_32_arr(2 downto 0);
    signal firstAbsSum : std_logic := '0';
    signal absSumAll : std_logic_vector(31 downto 0);
    signal absSum : t_slv_32_arr(2 downto 0);
    signal absSumAllExt : std_logic_vector(43 downto 0);
    --signal packetAbsSum  : t_slv_44_arr(8 downto 0);
    signal packetAbsSum2 : t_slv_44_arr(8 downto 0);
    --signal packet0AbsSum, packet1AbsSum : std_logic_vector(43 downto 0);
    signal getScalingInputValid, packetAbsSumValid : std_logic;
    signal packetAbsSumSelect, packetAbsSumSelectDel1, packetAbsSumSelectDel2, packetAbsSumSelectDel3, packetAbsSumSelectDel4 : std_logic_vector(3 downto 0);
    signal getScalingInput : std_logic_vector(43 downto 0);
 
    signal packetAbsSumValidDel2, packetAbsSumValidDel1, packetAbsSumValidDel3, packetAbsSumValidDel4 : std_logic;
    
    signal output32bitFine0Time0real, output32bitFine0Time0Imag, output32bitFine1Time0real : std_logic_vector(31 downto 0);
    signal output32bitFine1Time0Imag, output32bitFine2Time0real, output32bitFine2Time0Imag : std_logic_vector(31 downto 0);    
    signal output32bitFine0Time1real, output32bitFine0Time1Imag, output32bitFine1Time1real : std_logic_vector(31 downto 0);
    signal output32bitFine1Time1Imag, output32bitFine2Time1real, output32bitFine2Time1Imag : std_logic_vector(31 downto 0);
    signal output32bitFine2Time0RealDel1, output32bitFine2Time0ImagDel1 : std_logic_vector(31 downto 0);
    signal sample32bit0, sample32bit1, sample32bit2, sample32bit3 : std_logic_vector(31 downto 0);
 
    signal pol0re : std_logic_vector(31 downto 0);
    signal pol0im : std_logic_vector(31 downto 0);
    signal pol1re : std_logic_vector(31 downto 0);
    signal pol1im : std_logic_vector(31 downto 0);
    signal outputBufDin : std_logic_vector(63 downto 0);
    signal scalingShift : std_logic_vector(3 downto 0);
    signal scalingMantissa : std_logic_vector(3 downto 0);
    
    signal packetShift, packetShiftHold : t_slv_4_arr(8 downto 0);
    signal packetMantissa, packetMantissaHold : t_slv_4_arr(8 downto 0);
    signal scale_exp_frac : std_logic_vector(7 downto 0);
    
    signal packetBufRdAddr : std_logic_vector(11 downto 0);
    
    signal outputFine : std_logic_vector(6 downto 0);
    signal outputTime : std_logic_vector(4 downto 0);
    type output_fsm_type is (idle, pol0read0, pol0read1, pol0wait, pol1read0, pol1read1, pol1wait, done);
    signal output_fsm : output_fsm_type := idle;
    signal output_fsmDel1, output_fsmDel2, output_fsmDel3, output_fsmDel4 : output_fsm_type := idle;
    signal outputPacket0Only : std_logic := '0';
    signal shiftFinal, scaleFinal : std_logic_vector(3 downto 0);
    signal sample16bit0, sample16bit1, sample16bit2, sample16bit3 : std_logic_vector(15 downto 0);
    signal finalWeightDin : std_logic_vector(63 downto 0);
    signal weight16bit : t_slv_16_arr(2 downto 0);
    signal finalWeightOverflow : std_logic_vector(31 downto 0);
    signal div32ValidOut : std_logic_vector(2 downto 0);
    signal finalWeightWrEn : std_logic;
    signal finalWeightPhase : std_logic_vector(3 downto 0);
    signal finalWeightsSOF, finalWeightsSOFDel1 : std_logic;

    signal waitCount2Done : std_logic := '0';
    signal finalWeightRdWordsSent : std_logic_vector(2 downto 0);
    
    type readout_fsm_type is (start, sendHeaderWord, sendWeights, sendData, done);
    type readout_fsm_arr is array(15 downto 0) of readout_fsm_type;
    
    signal readout_fsm : readout_fsm_type := done;
    signal readout_fsm_del : readout_fsm_arr;
    
    signal outputSelectData, outputSelectWeights : std_logic;
    signal finalWeightDout : std_logic_vector(63 downto 0);
    signal BFValid : std_logic;
    
    signal triggerSendPacket : std_logic;
    signal triggerSendOnePacketOnly : std_logic := '0';
    signal readoutBufferSelect : std_logic;
    
    signal shiftFinalExt : std_logic_vector(7 downto 0);
    signal fp32Exp : std_logic_vector(7 downto 0);
    signal fp32Frac : std_logic_vector(3 downto 0);
    signal headerWord : std_logic_vector(12 downto 0);
    signal outputSelectHeaderWord : std_logic;
    signal outputBufferPacketCount, BFpacketCountHold, BFoutputBufferPacketCount : std_logic_vector(39 downto 0) := (others => '0');
    signal outputBufferVirtualChannel, BFvirtualChannelHold, BFvirtualChannel : std_logic_vector(9 downto 0);
    signal phaseDel1_plusX_16bit : t_slv_16_arr(2 downto 0);
    
    signal weightedSumPol0RealAbs, weightedSumPol0ImagAbs, weightedSumPol1RealAbs, weightedSumPol1ImagAbs : t_slv_32_arr(2 downto 0);
    signal absSum01 : std_logic_vector(31 downto 0);
    signal absSum2 : std_logic_vector(31 downto 0);
    signal loadJonesDel7 : std_logic;
    signal beamsEnabled : std_logic_vector(7 downto 0);
    signal beamEnabled : std_logic;
    signal lastStationAndValidDel16 : std_logic;
    signal BFFreqIndex : std_logic_vector(10 downto 0);
    signal BFCoarse, BFCoarseDel, BFCoarseHold, outputBufferCoarse : std_logic_vector(9 downto 0);
    signal BFFine, BFFineDel : std_logic_vector(3 downto 0);
    signal lastStationDel1 : std_logic;
    signal wholeFrameDone : std_logic := '0';
    signal triggerSendPacketHold : std_logic := '0';
    signal triggerSendOnePacketOnlyHold : std_logic := '0';
    signal outputBufferHold : std_logic := '0';

    signal zeros : std_logic_vector(63 downto 0);
    signal dTriggerSendPacket, dTriggerSendOnePacketOnly, dTriggerSendPacketHold, dOutputBufferHold, dTriggerSendOnePacketOnlyHold : std_logic;
    signal dbadPacket : std_logic;
    signal badPacket : std_logic;
    
    signal phase_virtualChannel, phase_virtualChannel0, phase_virtualChannel1 : std_logic_vector(9 downto 0) := (others => '0');
    signal phase_timeStep, phase_timeStep0, phase_timeStep1 : std_logic_vector(7 downto 0) := (others => '0');
    signal phase_beam : std_logic_vector(3 downto 0) := "0000";
    signal phase_phase, phase_phase0, phase_phase1, phase_phase0_del1, phase_phase1_del1 : std_logic_vector(23 downto 0) := (others => '0');  -- Phase at the start of the coarse channel.
    signal phase_step, phase_step0, phase_step1, phase_step0_del1, phase_step1_del1 : std_logic_vector(23 downto 0) := (others => '0');  -- Phase step per fine channel.
    signal phase_valid, phase_valid0, phase_valid1, phase_clear : std_logic := '0';    -- 
    
    signal weightAddr : std_logic_vector(11 downto 0);
    signal weightRdData : std_logic_vector(15 downto 0);
    
    signal finalWeightRst, finalWeightEmpty : std_logic;
    signal finalWeight_overflow : std_logic := '0';
    signal finalWeightFull : std_logic := '0';
    signal finalWeightWrCount, finalWeightRdCount : std_logic_vector(6 downto 0);
    
    signal packetCountDel : t_slv_40_arr(31 downto 0);
    signal packetBufWrBuffer : std_logic;
    signal packetBufWrTime : std_logic_vector(4 downto 0);
    signal packetBufWrFreq : std_logic_vector(6 downto 0);
    
    signal readout_pending : std_logic := '0';
    signal readoutFrameCount : std_logic_vector(3 downto 0);
    signal readoutFrameCount_del : t_slv_4_arr(9 downto 0);
    signal readout_pending_waitcount : std_logic_vector(3 downto 0) := "0000";
    signal frameSend : std_logic;
    signal frameCount : std_logic_vector(3 downto 0);
    
    signal packetBufRdBuffer : std_logic;
    signal packetBufRdTime : std_logic_vector(4 downto 0);
    signal packetBufRdPol : std_logic;
    signal packetBufRdFreq : std_logic_vector(6 downto 0);
    signal packetBufRdFreq_3 : std_logic_vector(1 downto 0);
    signal packetBufRdFreq_3_del : t_slv_2_arr(15 downto 0);
    
    signal packetBufTime0Dout, packetBufTime1Dout : t_slv_64_arr(2 downto 0);
    signal finalWeightRdEn : std_logic;
    signal finalWeightsBuffer : std_logic;
    
    signal pktOdd : std_logic;
    signal pktOddDel : std_logic_vector(16 downto 0);
    
    signal firstAbsSum_in_timestep : std_logic;
    signal packetAbsSum_temp, getScalingInput2, packetAbsSum_FineDel21 : std_logic_vector(43 downto 0);
    
    component ila_2
    port (
        clk : in std_logic;
        probe0 : in std_logic_vector(63 downto 0)); 
    end component;
    
    component ila_beamData
    port (
        clk : in std_logic;
        probe0 : in std_logic_vector(119 downto 0)); 
    end component;
    
    signal beamEnabled_del : std_logic;
    signal output_sel : std_logic_vector(1 downto 0);
    signal phase_step_sel_del3_x2 : std_logic_vector(23 downto 0);
    signal phase_phase_sel_del3, phase_step_sel_del3, phaseStepFreq_x3 : std_logic_vector(23 downto 0);
    signal vc_match0_del2, timeStep_match0_del2 : std_logic;
    signal ts_eq0, fine_eq_15 : std_logic;
    
begin
    
    -- Pipelines that go through to the next beamformer
    
    regModuleSelect <= i_regAddr(23 downto 16);  -- Each beamformer has 64kbytes of address space; register writes are for this module if regModuleSelect = g_BEAM_NUMBER
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            -- Buffer settings
            jonesBuffer <= i_jonesBuffer;
            beamsEnabled <= i_beamsEnabled;
            scale_exp_frac <= i_scale_exp_frac;
            
            o_jonesBuffer <= jonesBuffer;
            o_beamsEnabled <= beamsEnabled;
            o_scale_exp_frac <= scale_exp_frac;
            
            badPacket <= i_badPacket;
            o_badPacket <= badPacket;
            
            if (g_BEAM_NUMBER < unsigned(beamsEnabled)) then
                beamEnabled <= '1';
            else
                beamEnabled <= '0';
            end if; 
            beamEnabled_del <= beamEnabled;
            -- Register writes
            regAddr <= i_regAddr;
            regData <= i_regData;
            regWrEn <= i_wrEn;
            
            if (unsigned(regModuleSelect) = g_BEAM_NUMBER) and i_regAddr(15) = '0' and i_wrEn = '1' then
                jonesWrEn <= '1';
            else
                jonesWrEn <= '0';
            end if;
            
            if (unsigned(regModuleSelect) = g_BEAM_NUMBER) and i_regAddr(15) = '1' and i_wrEn = '1' then
                weightWrEn <= '1';
            else
                weightWrEn <= '0';
            end if;
            
            -- Output registers to pipeline the register commands to the next module.
            o_regAddr <= regAddr;
            o_regData <= regData;
            o_wrEn <= regWrEn;
            
            -- Output registers to select read data from the memory
            regAddrDel1 <= regAddr;
            regAddrDel2 <= regAddrDel1;
            regAddrDel3 <= regAddrDel2;
            
            if regAddrDel3(15) = '0' then
                o_regReadData <= jonesRegReadData;
            else 
                o_regReadData <= weightRegReadData;
            end if;            
            
            
            -- Pipeline the input data
            data <= i_data;               -- (191:0); 3 consecutive fine channels delivered every clock.
            flagged <= i_flagged;         -- (2:0);
            fine <= i_fine;               -- (7:0); fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
            coarse <= i_coarse;           -- (9:0);
            firstStation <= i_firstStation; -- first Station in the burst.
            lastStation <= i_lastStation;   -- last station in the burst.
            timeStep <= i_timeStep;         -- (4:0); Time step within the output packet.
            virtualChannel <= i_virtualChannel; -- (9:0)
            packetCount <= i_packetCount;       -- (39:0); The packet count for this packet, based on the original packet count from LFAA.
            pktOdd <= i_pktOdd;
            valid <= i_valid;
            -- 
            phase_virtualChannel <= i_phase_virtualChannel; -- (9:0);
            phase_timeStep <= i_phase_timeStep; -- (7:0);
            phase_beam <= i_phase_beam;         -- (3:0);
            phase_phase <= i_phase;             -- (23:0);  -- Phase at the start of the coarse channel.
            phase_step  <= i_phase_step;        -- (23:0);  -- Phase step per fine channel.
            phase_valid <= i_phase_valid;     -- 
            phase_clear <= i_phase_clear;
            --
            o_phase_virtualChannel <= phase_virtualChannel; -- (9:0);
            o_phase_timeStep  <= phase_timeStep;            -- (7:0);
            o_phase_beam  <= phase_beam;      -- (3:0);
            o_phase       <= phase_phase;     -- (23:0);  -- Phase at the start of the coarse channel.
            o_phase_step  <= phase_step;      -- (23:0);  -- Phase step per fine channel.
            o_phase_valid <= phase_valid;     -- 
            o_phase_clear <= phase_clear;
            --
            o_data <= data;
            o_flagged <= flagged;
            o_fine <= fine;  
            o_coarse <= coarse;
            o_firstStation <= firstStation;
            o_lastStation <= lastStation;
            o_timeStep <= timeStep;
            o_virtualChannel <= virtualChannel;
            o_packetCount <= packetCount;
            o_pktOdd <= pktOdd;
            o_valid <= valid;
            
            -- Capture the phase data in a two deep FIFO
            -- Either phase_XXX0 or phase_XXX1 registers are loaded at the start of each burst of fine frequency channels.
            if phase_clear = '1' then
                phase_valid0 <= '0';
                phase_valid1 <= '0';
            elsif ((phase_valid = '1') and (unsigned(phase_beam) = g_BEAM_NUMBER)) then
                phase_virtualChannel1 <= phase_virtualChannel;
                phase_timeStep1 <= phase_timeStep;
                phase_phase1 <= phase_phase;
                phase_step1 <= phase_step;
                phase_valid1 <= phase_valid;
                phase_virtualChannel0 <= phase_virtualChannel1;
                phase_timeStep0 <= phase_timeStep1;
                phase_phase0 <= phase_phase1;
                phase_step0 <= phase_step1;
                phase_valid0 <= phase_valid1;
            end if;
            
            -- Capture packetCount and virtualChannel and delay them so the are aligned with the output packets. 
            if unsigned(fine) = 15 then
                fine_eq_15 <= '1';
            else
                fine_eq_15 <= '0';
            end if;
            if unsigned(timeStep) = 0 then
                ts_eq0 <= '1';
            else
                ts_eq0 <= '0';
            end if;
            --if unsigned(fine) = 15 and firstStation = '1' and unsigned(timeStep) = 0 and valid = '1' then
            if fine_eq_15 = '1' and firstStationDel(0) = '1' and ts_eq0 = '1' and validDel(0) = '1' then
                BFpacketCount <= packetCount;  -- capture part way through the first packet, so we don't overwrite it before it is used.
                BFvirtualChannel <= virtualChannel;
            end if;
            
            if valid = '1' and fine(3 downto 0) = "0000" and firstStation = '1' then
                BFCoarse <= coarse;
                BFFine <= fine(7 downto 4); -- high 4 bits of fine is the group;
            end if;
            
            lastStationDel1 <= lastStation;
            if lastStation = '1' and lastStationDel1 = '0' then
                BFCoarseDel <= BFCoarse;
                BFFineDel <= BFFine;
            end if;
            
            if (getWeights = '1') then
                BFpacketCountHold <= BFpacketCount;
                BFvirtualChannelHold <= BFvirtualChannel;
                BFCoarseHold <= BFCoarseDel;
            end if;
            
            if (readout_fsm = start) then
                outputBufferPacketCount <= BFpacketCountHold;
                outputBufferVirtualChannel <= BFvirtualChannelHold;
                outputBufferCoarse <= BFCoarseHold;
            end if;
            
            --  Packets of beamformed data
            --if (outputSelectHeaderWord = '1' and beamEnabled = '1') then
            if output_sel = "00" then
                BFdata(63 downto 54) <= outputBufferVirtualChannel;
                BFdata(53 downto 32) <= (others => '0');
                
                BFdata(31 downto 19) <= headerword; -- header word with the scaling factor
                BFdata(18 downto 0) <= (others => '0');
                
                BFbeam <= std_logic_vector(to_unsigned(g_BEAM_NUMBER,8));
                BFFreqIndex <= outputBufferCoarse(6 downto 0) & readoutFrameCount_del(9);
                BFoutputBufferPacketCount <= outputBufferPacketCount;
                BFValid <= '1';
            elsif output_sel = "01" then
            --elsif outputSelectWeights = '1' and beamEnabled = '1' then
                BFdata <= finalWeightDout(15 downto 0) & finalWeightDout(31 downto 16) & finalWeightDout(47 downto 32) & finalWeightDout(63 downto 48);
                BFbeam <= std_logic_vector(to_unsigned(g_BEAM_NUMBER,8));
                BFFreqIndex <= outputBufferCoarse(6 downto 0) & readoutFrameCount_del(9);
                BFoutputBufferPacketCount <= outputBufferPacketCount;
                BFValid <= '1';
            elsif output_sel = "10" then
            --elsif outputSelectData = '1' and beamEnabled = '1' then
                BFdata <= sample16bit0 & sample16bit1 & sample16bit2 & sample16bit3;
                BFbeam <= std_logic_vector(to_unsigned(g_BEAM_NUMBER,8));
                BFFreqIndex <= outputBufferCoarse(6 downto 0) & readoutFrameCount_del(9);
                BFoutputBufferPacketCount <= outputBufferPacketCount;
                BFValid <= '1';
            else
                BFdata <= i_BFdata;
                BFbeam <= i_BFbeam;
                BFFreqIndex <= i_BFFreqIndex;
                BFoutputBufferPacketCount <= i_BFPacketCount;
                BFValid <= i_BFvalid;
            end if;
            o_BFdata <= BFdata;
            o_BFpacketCount <= BFoutputBufferPacketCount;
            o_BFFreqIndex <= BFFreqIndex;
            o_BFbeam <= BFbeam;
            o_BFValid <= BFValid;
        end if;
    end process; 
    
    
    --------------------------------------------------------------
    -- Registers :
    --  The beamformer has the following register map :
    --    - Jones Matrices (= Address 0-32767)
    --       - 1024 channels (e.g. 512 stations x 2 coarse)
    --       - 2x2 Jones Matrix
    --       - 16+16bit values
    --       - (1024 matrices) * (4 complex numbers per matrix) * (4 bytes per number) * (2 <double buffered>) = 32768 bytes = 1 URAM
    --       - 128 bits per Jones matrix = 2 memory locations, which are fixed by the virtual channel
    --    - Weights  (= Address 32768-36863)
    --       - 1024 channels x (2 byte weight)
    --       - (1024 numbers) * (2 bytes) * (2 <double buffered>) = 4096 bytes (use 1 BRAM)
    --  So with 64kbytes of memory used, we have 16 bits to address within the module, and the remaining address bits to select 
    --  which beamformer to use.
    --
    
    jonesmem : entity bf_lib.PSTBF_uram_wrapper
    port map(
        i_clk   => i_BF_clk, -- in std_logic;
        -- Port A. Both reads and writes are 32 bits wide.
        i_addrA => regAddr(14 downto 0), -- in(14:0); -- byte address
        i_dataA => regData,              -- in(31:0); -- 
        o_dataA => jonesRegReadData,    -- out(31:0); -- 3 cycle latency
        i_wrEnA => jonesWrEn,            -- in std_logic;
        -- Port B. Reads are 64 bits wide, write are 32 bits wide.
        i_addrB => jonesAddr,        -- in(14:0); -- byte address
        i_dataB => (others => '0'),  -- in(31:0); -- 
        o_dataB => jonesRdData,      -- out(63:0); -- 3 cycle latency
        i_wrEnB => '0'               -- in std_logic
    );
    
    -- Weights are encoded in the jones matrices, and also here explicitly in order to calculate weights in the output packet.
    weightmem : entity bf_lib.PSTBF_Bram_wrapper
    port map(
        i_clk   => i_BF_clk, -- in std_logic;
        -- Port A. Both reads and writes are 32 bits wide.
        i_addrA => regAddr(11 downto 0), -- in(11:0); -- byte address (11) = buffer, (10:1) = virtual channel, (0) unused (2 bytes per weight)
        i_dataA => regData,              -- in(31:0); -- 
        o_dataA => weightRegReadData,    -- out(31:0); -- 3 cycle latency
        i_wrEnA => weightWrEn,           -- in std_logic;
        -- Port B. 16-bit wide reads.
        i_addrB => weightAddr,     -- in(11:0); Byte address, top bit selects buffer, 10 bits selects virtual channel, low 1 bit unused. 
        o_dataB => weightRdData    -- out(15:0); 3 cycle latency
    );

    --------------------------------------------------------------------------------------------
    --
    -- Processing pipeline
    --  Jones correction -> phase correction -> sum -> output packet memory.
    -- 
    phaseStepFreq_x2 <= phaseStepFreq(22 downto 0) & '0';
    phase_step_sel_del3_x2 <= phase_step_sel_del3(22 downto 0) & '0';
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            -- Use the virtual channel to look up the required matrix in the Jones matrix memory.
            -- Detect when we need a new jones matrix, and copy Jones data into registers to use in the matrix multiplier.
            -- Note the processing order for data coming in is:
            --   
            --    For timeGroup = 0:(<time samples per corner turn>/32 - 1)    -- For 60 ms corner turn, this is 0:(288/32-1) = 0:8
            --       For Coarse = 0:(i_coarse-1)                               -- For 512 stations, 2 coarse, this is 0:1
            --             For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
            --                For Station = 0:(i_stations-1)
            --                   For fine_offset = 0:3:215 
            --                      Process a 96 bit word (i.e. 3 fine channels).
            --                *On the last time step (Time = 31), write the weights to the tempWeightStore memory.     
            --             Output packet(s) complete, send the packet(s) (two packets for fineGroup 0,1,2,3, one packet for fineGroup = 4)
            --
            -- 
            -- So we get bursts of 72 x 96 bit words for the same station and coarse channel
            if ((unsigned(fine) = 0) and valid = '1') then
                -- This is the start of a new station, so get the Jones Matrix for this station/coarse channel
                loadJones <= '1';
                jonesAddr <= jonesBuffer & virtualChannel & '0' & "000";
                weightAddr <= jonesBuffer & virtualChannel & '0';
            elsif loadJones = '1' then
                jonesAddr(3) <= '1';
                loadJones <= '0';
            else
                loadJones <= '0';
            end if;
            
            loadJonesDel1 <= loadJones;
            loadJonesDel2 <= loadJonesDel1;
            loadJonesDel3 <= loadJonesDel2;
            loadJonesDel4 <= loadJonesDel3;
            loadJonesDel5 <= loadJonesDel4;
            loadJonesDel6 <= loadJonesDel5;
            loadJonesDel7 <= loadJonesDel6;
            
            if loadJonesDel3 = '1' then
                JonesWord0Temp <= jonesRdData;
            end if;
            if loadJonesDel4 = '1' then
                JonesWord0 <= jonesWord0Temp;
                JonesWord1 <= jonesRdData;  -- JonesWord1 has a 6 cycle latency relative to "fine", "station", "coarse", "virtualChannel" etc.
            end if;
            
            -- Data returned from memory in phaseRdData will align with JonesRdData
            -- So the 12 bit phaseDel1 will align with JonesWord0, JonesWord1
            -- 6 cycles latency in the sincos lookup, so "sincos" aligns with "matrixMultPol0"
            if (unsigned(fineDel(3)) = 0 or unsigned(fineDel(3)) = 16 or unsigned(fineDel(3)) = 32 or unsigned(fineDel(3)) = 48) then
                lastFineGroup <= '0';
            else
                lastFineGroup <= '1';
            end if;
            
            -- pipelined selection of new phase data
            if (virtualChannelDel(1) = phase_virtualChannel0) then
                vc_match0_del2 <= '1';
            else
                vc_match0_del2 <= '0';
            end if;
            if timeStepDel(1) = phase_timeStep0(4 downto 0) and phase_valid0 = '1' then
                timeStep_match0_del2 <= '1';
            else
                timeStep_match0_del2 <= '0';
            end if;
            phase_phase0_del1 <= phase_phase0;
            phase_step0_del1 <= phase_step0;
            phase_phase1_del1 <= phase_phase1;
            phase_step1_del1 <= phase_step1;
            
            if vc_match0_del2 = '1' and timeStep_match0_del2 = '1' then
                phase_phase_sel_del3 <= phase_phase0_del1;
                phase_step_sel_del3 <= phase_step0_del1;
            else
                phase_phase_sel_del3 <= phase_phase1_del1;
                phase_step_sel_del3 <= phase_step1_del1;
            end if;
            
            if loadJonesDel3 = '1' then
                -- Get the phase. Occurs once at the start of each coarse channel, i.e. at the start of 216 fine channels.
                phaseDel1 <= phase_phase_sel_del3;
                phaseStepFreq <= phase_step_sel_del3;
                phaseStepFreq_x3 <= std_logic_vector(unsigned(phase_step_sel_del3_x2) + unsigned(phase_step_sel_del3));
            elsif (validDel(3) = '1') then
                phaseDel1 <= std_logic_vector(unsigned(phaseDel1) + unsigned(phaseStepFreq_x3));
            end if;
            
    
--            if loadJonesDel3 = '1' then
--                -- Get the phase. Occurs once at the start of each coarse channel, i.e. at the start of 216 fine channels.
--                if ((virtualChannelDel(3) = phase_virtualChannel0) and (timeStepDel(3) = phase_timeStep0(4 downto 0)) and (phase_valid0 = '1')) then
--                    phaseDel1 <= phase_phase0;
--                    phaseStepFreq <= phase_step0;
--                else -- Must be the other entry.
--                    phaseDel1 <= phase_phase1;
--                    phaseStepFreq <= phase_step1;
--                end if;
--            elsif (validDel(3) = '1') then
--                phaseDel1 <= std_logic_vector(unsigned(phaseDel1) + unsigned(phaseStepFreq_x2) + unsigned(phaseStepFreq));
--            end if;
            
            -- loaded on validDel(4)
            phaseDel1_plusX_16bit(0) <= phaseDel1(23) & phaseDel1(23) & phaseDel1(23) & phaseDel1(23) & phaseDel1(23 downto 12);
            phaseDel1_plus1 <= std_logic_vector(unsigned(phaseDel1) + unsigned(phaseStepFreq));
            phaseDel1_plus2 <= std_logic_vector(unsigned(phaseDel1) + unsigned(phaseStepFreq_x2));
            
            if loadJonesDel4 = '1' then -- second word of phase data back from the memory, one clock after the first word.
                phaseWeight <= weightRdData;
            end if;
            
            phaseWeightDel1 <= x"0000" & phaseWeight;  -- Delay 1 clock so it aligns with "flagged" signal, and extend to 32 bits. We need enough bits for 32 times and up to 512 stations.
            
            ----------------------------------------------------------------------------------
            -- Accumulate across all the different stations
            --  Delay from data input:
            -- count   Delayed signal Sample signal                                          Comment
            -- -----   -------------- -------------                                          -------
            --   0     fine,valid
            --   1     fineDel(0)     loadJones, phaseAddr, jonesAddr
            --   2     fineDel(1)     loadJonesDel1
            --   3     fineDel(2)     loadJonesDel2
            --   4     fineDel(3)     loadJonesDel3
            --   5     fineDel(4)     loadJonesDel4, jonesWord0Temp, phaseBaseTemp         
            --   6     fineDel(5)     JonesWord0, JonesWord1, data, phaseDel1, phaseWeight   Input to the matrix multiplier. Note i_data has a 6 clock latency relative to the other inputs such as i_fine. 
            --   7     fineDel(6)     flagged(2:0) 
            --   ...                  
            --   11    fineDel(10)    matrixMultR0real                                       Output from the matrix multiplier (5 clock latency from "data")
            --   12    fineDel(11)    matrixMultPol0, sincos                                 6 clock latency to get sin+cos from "phaseDel1"
            --   ...
            --   15    fineDel(14)    accReadAddr                                            read address into the accumulator memory, so 
            --   16    fineDel(15)    weightedPol0Real                                       output from the phase multiplier (4 clock latency from "matrixMultPol0")
            --   17    fineDel(16)    weightedSumPol0Real                                    accumulated beam
            --
            
            fineDel(0) <= fine;
            fineDel(31 downto 1) <= fineDel(30 downto 0);
            
            packetCountDel(0) <= packetCount;
            packetCountDel(31 downto 1) <= packetCountDel(30 downto 0);
            
            pktOddDel(0) <= pktOdd;
            pktOddDel(16 downto 1) <= pktOddDel(15 downto 0);
            
            timeStepDel(0) <= timeStep;
            timeStepDel(31 downto 1) <= timeStepDel(30 downto 0);
            
            virtualChannelDel(0) <= virtualChannel;
            virtualChannelDel(15 downto 1) <= virtualChannelDel(14 downto 0);
            
            firstStationDel(0) <= firstStation;
            firstStationDel(31 downto 1) <= firstStationDel(30 downto 0);
            
            lastStationDel(0) <= lastStation;
            lastStationDel(31 downto 1) <= lastStationDel(30 downto 0);
            
            validDel(0) <= valid;
            validDel(31 downto 1) <= validDel(30 downto 0);
            
            flaggedDel(0) <= flagged;
            flaggedDel(31 downto 1) <= flaggedDel(30 downto 0);
            
            -- Accumulate weights. A separate weight is calculated for each fine channel.
            weightAccWrEn(0) <= validDel(6);
            
            for i in 0 to 2 loop
                if validDel(6) = '1' then
                    if (firstStationDel(6) = '1' and timeStepDel(6) = "00000") then
                        if flaggedDel(6)(i) = '1' then
                            weightSum(i) <= (others => '0');
                        else
                            weightSum(i) <= phaseWeightDel1;
                        end if;
                    elsif flaggedDel(6)(i) = '0' then
                        weightSum(i) <= std_logic_vector(unsigned(weightAccDout(i)) + unsigned(phaseWeightDel1));
                    else
                        weightSum(i) <= weightAccDout(i);
                    end if;
                end if;
            end loop;
            
            -- Accumulate the maximum possible weights. This is used to scale the weight at the end of the packet.
            for i in 0 to 2 loop
                if validDel(6) = '1' then
                    if (firstStationDel(6) = '1' and timeStepDel(6) = "00000") then
                        maxWeightSum(i) <= phaseWeightDel1;
                    elsif flaggedDel(6)(i) = '0' then
                        maxWeightSum(i) <= std_logic_vector(unsigned(maxWeightAccDout(i)) + unsigned(phaseWeightDel1));
                    else
                        maxWeightSum(i) <= std_logic_vector(maxWeightAccDout(i));
                    end if;
                end if;
            end loop;
            
            -- weightSum and maxWeightSum are usually written to the weightAcc_i memories, where they are accumulated across 
            -- all the time samples in the packet for each fine frequency channel. 
            -- For the last time sample, weightSum and maxWeightSum are written to tempWeightStore_i, where they are held while
            -- the division operation runs.
            -- The division is serial, with 3 parallel instances, each of which needs 16 clocks per division.
            -- 3 dividers in parallel, so (8 fine channels) * (16 clocks) = 128 clocks to get all the weights for a single (24-fine channel) output packet.
            --
            --  Note : The time required for data to come in is at least :
            --   (72 fine channel groups) * (N stations) * (32 times) = 2304 * N
            -- 
            -- 
            --       (accumulate path)
            --    ----------<-----------\
            --    |                     |
            -- ---+--> weightAcc_i ---->|                   
            --    |
            --    \-------------------------> tempWeightStore_i --> divider ---> 3 to 4 wide --> finalWeightStore_i
            --      (final weight,maxWeight)                                 
            --  |                                                            |
            --  \------------------------------------------------------------/
            --         3 copies of this part (3 parallel fine channels)  
            --
            --
            --  Memories (3 parallel memories since 3 fine channels at a time) :
            --    - weightAcc_i       : (3 memories) * (72 deep) * (64 bits wide) = (216 fine channels) * (64 bits wide)
            --       Holds both 32 bit accumulated weight, and 32 bit accumulated maximum possible weight.
            --    - tempWeightStore_i : (3 memories) * (72 deep) * (64 bits wide) = (216 fine channels) * (64 bits wide)
            --       Same data as weightAcc_i, but is only written for the final value.
            --    - finalWeightStore_i : FIFO holding final weights as used in the PST output packets. 
            --       64 bits wide, 4 x 16bit values per word.
            --
            
            if (validDel(6) = '1' and (timeStepDel(6) = "11111") and lastStationDel(6) = '1') then
                tempWeightStoreWrEn(0) <= '1';
            else
                tempWeightStoreWrEn(0) <= '0';
            end if;
            
            if validDel(6) = '1' and timeStepDel(6) = "11111" and lastStationDel(6) = '1' and unsigned(fineDel(6)) = 71 then
                getWeights <= '1';  -- All weight data has been written to tempWeightStore, start processing through the divider.
            else
                getWeights <= '0';
            end if;
            
            
            if getWeights = '1' then
                weightsCountDown <= "010010000000"; -- 216 weights, 3 parallel dividers, count down from (216/3) * 16 = 1152. 
                weightsCountUp   <= "000000000000";  -- top 8 bits of the count up are used as the address into the memory, so we get new data every 16 clocks.
                dividerRunning   <= '1';
            elsif unsigned(weightsCountDown) /= 0 then
                weightsCountDown <= std_logic_vector(unsigned(weightsCountDown) - 1);
                weightsCountUp   <= std_logic_vector(unsigned(weightsCountUp) + 1);
            else
                dividerRunning <= '0';
            end if;
            
            if weightsCountUp(3 downto 0) = "0000" and dividerRunning = '1' then
                div32ValidIn <= '1';
            else
                div32ValidIn <= '0';
            end if;
            
            finalWeightsSOF <= getWeights;
            finalWeightsBuffer <= pktOddDel(7);
            finalWeightsSOFDel1 <= finalWeightsSOF;
            
            -- Do beamforming : Accumulate weighted values across all the stations.
            
            if validDel(15) = '1' then
                AccWrEn(0) <= '1';
            else
                AccWrEn(0) <= '0';
            end if;
            
            
            lastStationAndValidDel16 <= lastStationDel(15) and validDel(15);
            
            for i in 0 to 2 loop
                if validDel(15) = '1' then
                    if firstStationDel(15) = '1' then
                        weightedSumPol0Real(i) <= weightedPol0Real(i);
                        weightedSumPol0Imag(i) <= weightedPol0Imag(i);
                        weightedSumPol1Real(i) <= weightedPol1Real(i);
                        weightedSumPol1Imag(i) <= weightedPol1Imag(i);
                    else
                        weightedSumPol0Real(i) <= std_logic_vector(signed(weightedPol0Real(i)) + signed(accumulatorPol0Real(i)));
                        weightedSumPol0Imag(i) <= std_logic_vector(signed(weightedPol0Imag(i)) + signed(accumulatorPol0Imag(i)));
                        weightedSumPol1Real(i) <= std_logic_vector(signed(weightedPol1Real(i)) + signed(accumulatorPol1Real(i)));
                        weightedSumPol1Imag(i) <= std_logic_vector(signed(weightedPol1Imag(i)) + signed(accumulatorPol1Imag(i)));
                    end if;
                end if;

                -- find absolute sum of the values for use in the scaling
                if (signed(weightedSumPol0Real(i)) < 0) then
                    weightedSumPol0RealAbs(i) <= std_logic_vector(-signed(weightedSumPol0Real(i)));
                else
                    weightedSumPol0RealAbs(i) <= weightedSumPol0Real(i);
                end if;
                
                if (signed(weightedSumPol0Imag(i)) < 0) then
                    weightedSumPol0ImagAbs(i) <= std_logic_vector(-signed(weightedSumPol0Imag(i)));
                else
                    weightedSumPol0ImagAbs(i) <= weightedSumPol0Imag(i);
                end if;

                if (signed(weightedSumPol1Real(i)) < 0) then
                    weightedSumPol1RealAbs(i) <= std_logic_vector(-signed(weightedSumPol1Real(i)));
                else
                    weightedSumPol1RealAbs(i) <= weightedSumPol1Real(i);
                end if;
                
                if (signed(weightedSumPol1Imag(i)) < 0) then
                    weightedSumPol1ImagAbs(i) <= std_logic_vector(-signed(weightedSumPol1Imag(i)));
                else
                    weightedSumPol1ImagAbs(i) <= weightedSumPol1Imag(i);
                end if;
                
                absPol0Sum(i) <= std_logic_vector(signed(weightedSumPol0RealAbs(i)) + signed(weightedSumPol0ImagAbs(i)));
                absPol1Sum(i) <= std_logic_vector(signed(weightedSumPol1RealAbs(i)) + signed(weightedSumPol1ImagAbs(i)));
                
                absSum(i) <= std_logic_vector(unsigned(absPol0Sum(i)) + unsigned(absPol1Sum(i)));
                
                
                -- Write to the packet buffer. After a complete packet is in the packet buffer, it is read out and converted to 16 bits and sent out to the ethernet interface
                if lastStationAndValidDel16 = '1' then
                    -- Matrix multiplier is 16 bit * 8 bit => 24 bit result
                    -- Phase correction is 24 bit * 17 bit sin+cos, drop 22 bits => 19 bit result
                    -- Sum across 512 antennas could give up to 28 bits, but for gaussian noise it will be about 18 bits.
                    packetBufPol0Din(i)(31 downto 0) <= weightedSumPol0Real(i);
                    packetBufPol0Din(i)(63 downto 32) <= weightedSumPol0Imag(i);
                    packetBufPol1Din(i)(31 downto 0) <= weightedSumPol1Real(i);
                    packetBufPol1Din(i)(63 downto 32) <= weightedSumPol1Imag(i);
                    packetBufWrEn(i) <= '1';
                else
                    packetBufWrEn(i) <= '0';
                end if;
                
            end loop;
            
            -- On accumulating the last station, sum it to find the scaling factor.
            -- there are enough spare bits to ensure that absSumAll cannot overflow.
            absSum01 <= std_logic_vector(unsigned(absSum(0)) + unsigned(absSum(1)));
            absSum2 <= absSum(2);
            
            absSumAll <= std_logic_vector(unsigned(absSum01) + unsigned(absSum2));
            
            if fineDel(20)(2 downto 0) = "000" and timeStepDel(20) = "00000" then
                firstAbsSum <= '1';
            else
                firstAbsSum <= '0';
            end if;
            
--            if lastStationDel(21) = '1' and validDel(21) = '1' then
--                -- fineDel runs from 0 to 71 (i.e. 3 fine channels per clock are fineDel*3, fineDel*3+1, fineDel*3+2)
--                -- packetAbsSum is a 9-element array, one for each packet that is being formed simultaneously,
--                -- Each value is summed across 24 frequency channels, 
--                --  i.e. 8 values of fineDel, hence sum is indexed by bits fineDel bits 6:3.
--                if firstAbsSum = '1' then
--                    packetAbsSum(to_integer(unsigned(fineDel(21)(6 downto 3)))) <= absSumAllext;
--                else
--                    packetAbsSum(to_integer(unsigned(fineDel(21)(6 downto 3)))) <= std_logic_vector(unsigned(packetAbsSum(to_integer(unsigned(fineDel(21)(6 downto 3))))) + unsigned(absSumAllext));
--                end if;
--            end if;
            
            -------------------------------------------------------------------------
            -- To improve timing, sum within a group of 24 fine channels (one packet) with a separate register, 
            -- then add into packetAbsSum(to_integer(unsigned(fineDel(20)(6 downto 3)))) at the end of the 24 fine channels.
            -- Note the 24 fine channels are processed in a block of 8 clock cycles.
            --packetAbsSum_FineDel21 <= packetAbsSum(to_integer(unsigned(fineDel(20)(6 downto 3))));
            
            if fineDel(20)(2 downto 0) = "000" then
                firstAbsSum_in_timestep <= '1';
            else
                firstAbsSum_in_timestep <= '0';
            end if;
            
            if lastStationDel(21) = '1' and validDel(21) = '1' then
                -- fineDel runs from 0 to 71 (i.e. 3 fine channels per clock are fineDel*3, fineDel*3+1, fineDel*3+2)
                -- packetAbsSum is a 9-element array, one for each packet that is being formed simultaneously,
                -- Each value is summed across 24 frequency channels, 
                --  i.e. 8 values of fineDel, hence sum is indexed by bits fineDel bits 6:3.
                if firstAbsSum = '1' then
                    packetAbsSum_temp <= absSumAllext;
                elsif firstAbsSum_in_timestep = '1' then
                    packetAbsSum_temp <= std_logic_vector(unsigned(packetAbsSum_FineDel21) + unsigned(absSumAllext));
                else
                    packetAbsSum_temp <= std_logic_vector(unsigned(packetAbsSum_temp) + unsigned(absSumAllext));
                end if;
            end if;
            if lastStationDel(22) = '1' and validDel(22) = '1' and fineDel(22)(2 downto 0) = "111" then
                packetAbsSum2(to_integer(unsigned(fineDel(22)(6 downto 3)))) <= packetAbsSum_temp;
            end if;
            packetAbsSum_FineDel21 <= packetAbsSum2(to_integer(unsigned(fineDel(20)(6 downto 3))));
            
            if packetAbsSumValid = '1' then
                getScalingInput <= packetAbsSum_temp;
            end if;
            
            -------------------------------------------------------------------------
            
            if lastStationDel(21) = '1' and validDel(21) = '1' and fineDel(21)(2 downto 0) = "111" and timeStepDel(21) = "11111" then
                -- The absolute sum for the packet is valid on the last fine channel and last time sample 
                packetAbsSumValid <= '1';
                packetAbsSumSelect <= fineDel(21)(6 downto 3);
            else
                packetAbsSumValid <= '0';
            end if;
            
            if packetAbsSumValid = '1' then
                getScalingInputValid <= '1';
            --    getScalingInput <= packetAbsSum(to_integer(unsigned(packetAbsSumSelect)));
            else
                getScalingInputValid <= '0';
            end if;
            
            -- Trigger readout state machine to copy beamformed data to the output (after scaling to 16 bits).
            packetAbsSumValidDel1 <= packetAbsSumValid;      -- del1 aligns with getScalingInputValid
            packetAbsSumSelectDel1 <= packetAbsSumSelect;
            
            packetAbsSumValidDel2 <= packetAbsSumValidDel1;
            packetAbsSumSelectDel2 <= packetAbsSumSelectDel1;
            
            packetAbsSumValidDel3 <= packetAbsSumValidDel2;
            packetAbsSumSelectDel3 <= packetAbsSumSelectDel2;
            
            packetAbsSumValidDel4 <= packetAbsSumValidDel3;  -- Del4 aligns with the output of the PSTBFScaling block.
            packetAbsSumSelectDel4 <= packetAbsSumSelectDel3;
            
            -- The first stage output buffer stores 32+32 bit complex data. (data is converted to 16+16 bit complex when it is sent out as packets)
            -- It is doubled buffered with space in each buffer for 9 packets = 1 SPS channel = 216 fine channels.
            
            -- lsb of packetCount selects which half of the buffer we are writing to.
            packetBufWrBuffer <= pktOddDel(16);
            packetBufWrTime <= timeStepDel(16);
            packetBufWrFreq <= fineDel(16)(6 downto 0);
            
        end if;
    end process;
    
    absSumAllExt <= "000000000000" & absSumAll;
    
    -- Convert the absolute sum of the data in the packet into a scaling factor that we can scale all the data back to 16 bits when copying from the packet buffer to the output buffer.
    PSTscaleInst : entity bf_lib.PSTBFScaling
    port map (
        i_clk   => i_BF_clk,
        i_data  => getScalingInput,      -- in (43:0);
        i_valid => getScalingInputValid, -- in std_logic;
        --
        o_shift    => scalingShift,    --  out(3:0);
        o_mantissa => scalingMantissa, -- out(3:0);
        o_valid    => open             -- out std_logic  -- 3 clock latency; not used, separate delay line in this module is used, "packet0AbsSumValidDel4", "packet1AbsSumValidDel4"
    );
    
    -- full parallel 2x2 complex matrix multiplier
    -- Matrix data is in JonesWord0, JonesWord1, vector data is in "data"
    -- 3 instances, one for each of data(31:0), data(63:32), data(95:64)
    matrixmultGen : for i in 0 to 2 generate
        mmult : entity bf_lib.jonesMatrixMult_16b
        port map (
            i_clk => i_BF_clk,
            -- The 2x2 matrix
            i_m00real => jonesWord0(15 downto 0),  -- in(15:0);
            i_m00imag => jonesWord0(31 downto 16), -- in(15:0);
            i_m01real => jonesWord0(47 downto 32), -- in(15:0);
            i_m01imag => jonesWord0(63 downto 48), -- in(15:0);
            i_m10real => jonesWord1(15 downto 0),  -- in(15:0);
            i_m10imag => jonesWord1(31 downto 16), -- in(15:0);
            i_m11real => jonesWord1(47 downto 32), -- in(15:0);
            i_m11imag => jonesWord1(63 downto 48), -- in(15:0);
            -- 2x1 vector
            i_v0real => data(i*64 + 15 downto i*64),     -- in(15:0);
            i_v0imag => data(i*64 + 31 downto i*64+16),  -- in(15:0);
            i_v1real => data(i*64 + 47 downto i*64+32), -- in(15:0);
            i_v1imag => data(i*64 + 63 downto i*64+48), -- in(15:0);
            i_rfi    => flagged(i),
            -- 2x1 vector result (5 clock latency)
            o_r0real => matrixMultR0real(i), -- out(35:0)  (top bit is superfluous)
            o_r0imag => matrixMultR0imag(i), -- out(35:0)
            o_r1real => matrixMultR1real(i), -- out(35:0)
            o_r1imag => matrixMultR1imag(i)  -- out(35:0)
        );
        
        process(i_BF_clk)
        begin
            if rising_edge(i_BF_clk) then
                -- Drop the low 8 bits using convergent rounding
                matrixMultPol0(i)(26 downto 0) <= convergent_round(matrixMultR0real(i));
                matrixMultPol0(i)(58 downto 32) <= convergent_round(matrixMultR0imag(i));
                matrixMultPol1(i)(26 downto 0) <= convergent_round(matrixMultR1real(i));
                matrixMultPol1(i)(58 downto 32) <= convergent_round(matrixMultR1imag(i));
                
                -- read and write address registers inside the generate statement in order to enforce duplication of the registers,
                -- since they go to a lot of distributed memory LUTs.
                AccWriteAddr(i) <= fineDel(15)(6 downto 0);
                AccReadAddr(i) <= fineDel(13)(6 downto 0);  -- we operate on groups of 216 fine channels, 3 at a time, so fine counts from 0 to 71 within a coarse channel.
            
                weightAccReadAddr(i) <= fineDel(4)(6 downto 0);   -- So weights read address is valid on (5), read data ("weightAccDout") is valid on (6), and weightSum valid on fineDel(7)
                weightAccWriteAddr(i) <= fineDel(6)(6 downto 0);
                
            end if;
        end process;
        
        sclookupPol0 : sincosLookup
        port map (
            aclk                => i_BF_clk,  -- in std_logic;
            s_axis_phase_tvalid => '1',       -- in std_logic;
            s_axis_phase_tdata  => phaseDel1_plusX_16bit(i), -- in (15:0);  -- Phase in bits 11:0
            m_axis_data_tvalid  => open,      -- out std_logic;
            m_axis_data_tdata   => sinCos(i)  -- out (47:0)); -- cosine in bits 17:0, sine in bits 41:24, 6 clock latency
        );
        
        -- complex multiplier for the phase, first polarisation 
        bmultPol0 : BF_cMult27x18
        port map (
            aclk            => i_BF_clk,
            s_axis_a_tvalid => '1',
            s_axis_a_tdata  => matrixMultPol0(i), -- in (63:0); real in 26:0, imaginary in 58:32
            s_axis_b_tvalid => '1',
            s_axis_b_tdata  => sinCos(i),  -- in 47:0; real in 17:0, imaginary in 41:24; unity is 2^17.
            m_axis_dout_tvalid => open,    -- out std_logic;
            m_axis_dout_tdata  => weightedPol0(i)    -- out 95:0, real in 45:0, imaginary in 93:48
        );
        
        -- keep the top 23 bits, sign extend by 8 bits for use in the accumulator
        weightedPol0Real(i) <= weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45) & weightedPol0(i)(45 downto 22);
        weightedPol0Imag(i) <= weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93) & weightedPol0(i)(93 downto 70);
        
        -- second polarisation
        bmultPol1 : BF_cMult27x18
        port map (
            aclk            => i_BF_clk,
            s_axis_a_tvalid => '1',
            s_axis_a_tdata  => matrixMultPol1(i), -- in (63:0); real in 26:0, imaginary in 58:32
            s_axis_b_tvalid => '1',
            s_axis_b_tdata  => sinCos(i),  -- in 47:0; real in 17:0, imaginary in 41:24
            m_axis_dout_tvalid => open, -- out STD_LOGIC;
            m_axis_dout_tdata  => weightedPol1(i) -- out 95:0, real in 45:0, imaginary in 93:48
        );
        
        weightedPol1Real(i) <= weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45) & weightedPol1(i)(45 downto 22);
        weightedPol1Imag(i) <= weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93) & weightedPol1(i)(93 downto 70);
        
        -- Distributed memories to accumulate results for 72 fine channels.
        -- 3 memories (one for each parallel input sample), each 72 deep, 128 bits wide (real+imag, dual pol, each 32 bits)
        -- for a total of 216 fine channels that are being accumulated simultaneously.
        xpm_memory_sdpram_Acc_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 7,               -- DECIMAL
            ADDR_WIDTH_B => 7,               -- DECIMAL
            AUTO_SLEEP_TIME => 0,            -- DECIMAL
            BYTE_WRITE_WIDTH_A => 128,       -- DECIMAL
            CASCADE_HEIGHT => 0,             -- DECIMAL
            CLOCKING_MODE => "common_clock", -- String
            ECC_MODE => "no_ecc",            -- String
            MEMORY_INIT_FILE => "none",      -- String
            MEMORY_INIT_PARAM => "0",        -- String
            MEMORY_OPTIMIZATION => "true",   -- String
            MEMORY_PRIMITIVE => "distributed", -- String
            MEMORY_SIZE => 9216,             -- 128 bits wide x 72 deep = 9216 bits
            MESSAGE_CONTROL => 0,            -- DECIMAL
            READ_DATA_WIDTH_B => 128,        -- DECIMAL
            READ_LATENCY_B => 1,             -- DECIMAL
            READ_RESET_VALUE_B => "0",       -- String
            RST_MODE_A => "SYNC",            -- String
            RST_MODE_B => "SYNC",            -- String
            SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
            USE_MEM_INIT => 0,               -- DECIMAL
            WAKEUP_TIME => "disable_sleep",  -- String
            WRITE_DATA_WIDTH_A => 128,       -- DECIMAL
            WRITE_MODE_B => "read_first"      -- String
        ) port map (
            dbiterrb => open,  
            doutb => AccDout(i),     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,
            addra => AccWriteAddr(i),   -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => AccReadAddr(i),    -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_BF_clk,                 
            clkb => i_BF_clk,             
            dina => accDin(i), -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',    
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',     -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',       -- 1-bit input: Reset signal for the final port B output register stage. 
            sleep => '0',      -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => AccWrEn     -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );

        -- Distributed memories to accumulate the weights for each fine channel.
        -- Three memories, one for each parallel input sample, each 72 deep, 64 bits wide. 
        -- 3 memories * 72 deep = 216 weights, one for each fine channel that is being accumulated.
        -- bits (31:0) = Accumulated weight
        -- bits (63:32) = Accumulated maximum possible weight
        weightAcc_i : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 7,                 -- DECIMAL
            ADDR_WIDTH_B => 7,                 -- DECIMAL
            AUTO_SLEEP_TIME => 0,              -- DECIMAL
            BYTE_WRITE_WIDTH_A => 64,          -- DECIMAL
            CASCADE_HEIGHT => 0,               -- DECIMAL
            CLOCKING_MODE => "common_clock",   -- String
            ECC_MODE => "no_ecc",              -- String
            MEMORY_INIT_FILE => "none",        -- String
            MEMORY_INIT_PARAM => "0",          -- String
            MEMORY_OPTIMIZATION => "true",     -- String
            MEMORY_PRIMITIVE => "distributed", -- String
            MEMORY_SIZE => 4608,               -- 64 bits wide x 72 deep = 2304 bits
            MESSAGE_CONTROL => 0,              -- DECIMAL
            READ_DATA_WIDTH_B => 64,           -- DECIMAL
            READ_LATENCY_B => 1,               -- DECIMAL
            READ_RESET_VALUE_B => "0",         -- String
            RST_MODE_A => "SYNC",              -- String
            RST_MODE_B => "SYNC",              -- String
            SIM_ASSERT_CHK => 0,               -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,      -- DECIMAL
            USE_MEM_INIT => 0,                 -- DECIMAL
            WAKEUP_TIME => "disable_sleep",    -- String
            WRITE_DATA_WIDTH_A => 64,          -- DECIMAL
            WRITE_MODE_B => "read_first"       -- String
        ) port map (
            dbiterrb => open,  
            doutb => weightStoreDout(i),     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,
            addra => weightAccWriteAddr(i),   -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => weightAccReadAddr(i),    -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_BF_clk,                 
            clkb => i_BF_clk,             
            dina => weightStoreDin(i),      -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',    
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',           -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',             -- 1-bit input: Reset signal for the final port B output register stage. 
            sleep => '0',            -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => weightAccWrEn     -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );
        
        weightAccDout(i) <= weightStoreDout(i)(31 downto 0);
        maxWeightAccDout(i) <= weightStoreDout(i)(63 downto 32);
        
        weightStoreDin(i)(31 downto 0) <= weightSum(i);
        weightStoreDin(i)(63 downto 32) <= maxWeightSum(i);
        
        -- Store the weights and max weights while we do the divisions required to get the 16-bit final weight used in the output packet.
        -- The weights are written to this memory at the conclusion of the packet input data, 
        -- i.e. when the last sample weight for the fine channel is accumulated, instead of being written to the weightAcc_i memories.
        -- The data input to the memory is the same signal as the input to the weight and max weight accumulator memories.
        TempWeightStore_i : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 7,                 -- DECIMAL
            ADDR_WIDTH_B => 7,                 -- DECIMAL
            AUTO_SLEEP_TIME => 0,              -- DECIMAL
            BYTE_WRITE_WIDTH_A => 64,          -- DECIMAL
            CASCADE_HEIGHT => 0,               -- DECIMAL
            CLOCKING_MODE => "common_clock",   -- String
            ECC_MODE => "no_ecc",              -- String
            MEMORY_INIT_FILE => "none",        -- String
            MEMORY_INIT_PARAM => "0",          -- String
            MEMORY_OPTIMIZATION => "true",     -- String
            MEMORY_PRIMITIVE => "distributed", -- String
            MEMORY_SIZE => 4608,               -- DECIMAL   64 bits wide x 72 deep = 4608 bits
            MESSAGE_CONTROL => 0,              -- DECIMAL
            READ_DATA_WIDTH_B => 64,           -- DECIMAL
            READ_LATENCY_B => 1,               -- DECIMAL
            READ_RESET_VALUE_B => "0",         -- String
            RST_MODE_A => "SYNC",              -- String
            RST_MODE_B => "SYNC",              -- String
            SIM_ASSERT_CHK => 0,               -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,      -- DECIMAL
            USE_MEM_INIT => 0,                 -- DECIMAL
            WAKEUP_TIME => "disable_sleep",    -- String
            WRITE_DATA_WIDTH_A => 64,          -- DECIMAL
            WRITE_MODE_B => "read_first"       -- String
        ) port map (
            dbiterrb => open,  
            doutb => tempWeightStoreDout(i),     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,
            addra => weightAccWriteAddr(i),         -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => weightsCountUp(10 downto 4), -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_BF_clk,                 
            clkb => i_BF_clk,             
            dina => weightStoreDin(i),       -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',    
            enb => '1',
            injectdbiterra => '0',
            injectsbiterra => '0',
            regceb => '1',              -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',                -- 1-bit input: Reset signal for the final port B output register stage. 
            sleep => '0',               -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => tempWeightStoreWrEn  -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );
        
        
        -- 3 Dividers to get the final weights used in the output packet.
        div32 : entity bf_lib.div32_32_16
        port map (
            i_clk => i_BF_clk, -- in std_logic;
            -- data input; numerator should be less than or equal to denominator
            i_numerator => tempWeightStoreDout(i)(31 downto 0), -- in std_logic_vector(31 downto 0);  -- unsigned input, but top bit should be zero.
            i_denominator => tempWeightStoreDout(i)(63 downto 32), -- in std_logic_vector(31 downto 0); -- unsigned input, but top bit should be zero.
            i_valid => div32ValidIn, -- in std_logic;  -- Can take new input data every 16 clocks.
            -- Data out
            o_quotient => weight16bit(i),   -- out std_logic_vector(15 downto 0); -- output is an unsigned value.
            o_valid    => div32ValidOut(i)  -- out std_logic  -- will go high 17 clocks after i_valid
        );
        
        
        AccDin(i)(31 downto 0) <= weightedSumPol0Real(i);
        AccDin(i)(63 downto 32) <= weightedSumPol0Imag(i);
        AccDin(i)(95 downto 64) <= weightedSumPol1Real(i);
        AccDin(i)(127 downto 96) <= weightedSumPol1Imag(i);
        
        accumulatorPol0Real(i) <= accDout(i)(31 downto 0);
        accumulatorPol0Imag(i) <= accDout(i)(63 downto 32);
        accumulatorPol1Real(i) <= accDout(i)(95 downto 64);
        accumulatorPol1Imag(i) <= accDout(i)(127 downto 96);
        
        -- Block RAM to hold the output packet data while we find the scale factor.
        --  The beamformed data is stored as 32+32 bit complex numbers prior to scaling.
        --  Each of the three memories stores data for 1/3 of the fine channels.
        --  Output packets have :
        --   (24 fine channels) * (2 bytes) * (2 [complex]) * (2 pol) * (32 times) = 6144 bytes
        --  1/3 of these come from each memory, i.e. 8 fine channels.
        --  So each memory has packets with 8*2*2*2*32 = 2048 bytes.
        --  We generate 9 packets at a time, since we process 216 fine channels in a burst
        --  These are stored here since we don't know the scaling factor until the end of the packet.
        --  At the end of the packet, the data is read out, scaled and sent out on the packet output bus.
        --
        --  Altogether 6 ultraRAMs + 6 BRAMs are instantiated here.
        --  The memories are 4608 deep x 128 bits wide;
        --  There is space for 9 packets, double buffered. The write address is
        --       bits 12:5 = floor((fine channel)/3), where fine channel runs from 0 to 431 to cover all the fine channels in (2 time frames) * (9 packets/coarse channel).
        --                   0-71 : first 32 time samples
        --                   72-143 : Second 32 time samples
        --                   144-255 : Invalid.
        --       bits 4:0 = time sample
        --
        --  fine 0,3,6,9...  |  fine 1,4,7...    |  fine 2,5,8,...   |
        --  Pol 0  |  Pol 1  |  Pol 0  |  Pol 1  |  Pol 0  |  Pol 1  |
        --
        
        packetStore_i : entity bf_lib.PST_comboRAM_dp
        port map(
            i_clk => i_BF_clk, -- in std_logic;
            ------------------------------------------------------------
            -- Writes : 2 polarisations for the same time
            -- 128 bits of input data
            i_din_pol0 => packetBufPol0Din(i), -- in (63:0);
            i_din_pol1 => packetBufPol1Din(i), -- in (63:0);
            -- Write address is made up of buffer, time sample and fine channel
            i_wr_buffer => packetBufWrBuffer, -- in std_logic;
            i_wr_time   => packetBufWrTime,   -- in (4:0); 32 time samples
            i_wr_freq   =>  packetBufWrFreq,  -- in (6:0); 72 frequency channels (7 bit value with valid range 0 to 71) 
            i_wrEn => packetBufWrEn(i),    -- in std_logic;
            ------------------------------------------------------------
            -- Reads : 2 time samples per clock
            -- 128 bit read data, 4 cycle latency
            o_dout_time0 => packetBufTime0Dout(i), -- out (63:0);
            o_dout_time1 => packetBufTime1Dout(i), -- out (63:0);
            i_rd_buffer  => packetBufRdBuffer,  -- in std_logic;
            i_rd_time    => packetBufRdTime,    -- in (4:0);  -- two time samples read out, i_rd_time and i_rd_time+1. The LSB of i_rd_time is ignored. 
            i_rd_pol     => packetBufRdPol,     -- in std_logic;
            i_rd_freq    => packetBufRdFreq     -- in (6:0)
        );
        
    end generate;
    
    phaseDel1_plusX_16bit(1) <= phaseDel1_plus1(23) & phaseDel1_plus1(23) & phaseDel1_plus1(23) & phaseDel1_plus1(23) & phaseDel1_plus1(23 downto 12);
    phaseDel1_plusX_16bit(2) <= phaseDel1_plus2(23) & phaseDel1_plus2(23) & phaseDel1_plus2(23) & phaseDel1_plus2(23) & phaseDel1_plus2(23 downto 12);
    
    ---------------------------------------------------------------------------------------------------
    -- Store final weights for each fine channel.
    -- Each output packet has 24 weights, one for each fine channel.
    -- Each weight is 2 bytes, so 48 bytes total.
    -- This memory has space for 256 bytes = 4 * 64 = space for weights for 4 packets.
    -- The weights come from the 3 dividers, so we get data in blocks of 3*16 = 48 bits.
    -- We have to convert to 64 bit wide data to write to the finalWeightStore memory.  
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            
            if (finalWeightsSOF = '1') then
                -- finalWeightsSOF occurs a few clocks after data for the the last time, last fine channel (216), last station
                finalWeightPhase <= "0000";
                finalWeightWrEn <= '0';
            elsif (div32ValidOut(0) = '1') then   
                -- 3 dividers, all valid at the same time, so div32ValidOut(1) and div32Validout(2) are the same as div32ValidOut(0).
                -- Convert 3-wide output from dividers to 4-wide input to the finalWeight FIFO.
                finalWeightPhase <= std_logic_vector(unsigned(finalWeightPhase) + 1);
                if finalWeightPhase(1 downto 0) = "00" then
                    finalWeightDin(47 downto 0) <= weight16bit(2) & weight16bit(1) & weight16bit(0);
                    finalWeightWrEn <= '0'; 
                elsif finalWeightPhase(1 downto 0) = "01" then
                    finalWeightDin(63 downto 48) <= weight16bit(0);
                    finalWeightOverflow <= weight16bit(2) & weight16bit(1);
                    finalWeightWrEn <= '1';
                elsif finalWeightPhase(1 downto 0) = "10" then
                    finalWeightDin(31 downto 0) <= finalWeightOverflow;
                    finalWeightDin(63 downto 32) <= weight16bit(1) & weight16bit(0);
                    finalWeightOverflow(15 downto 0) <= weight16bit(2);
                    finalWeightWrEn <= '1';
                elsif finalWeightPhase(1 downto 0) = "11" then
                    finalWeightDin(15 downto 0) <= finalWeightOverflow(15 downto 0);
                    finalWeightDin(63 downto 16) <= weight16bit(2) & weight16bit(1) & weight16bit(0);
                    finalWeightWrEn <= '1';
                end if;
            else
                finalWeightWrEn <= '0';
            end if;
            
            -- The FIFO should be emptied from the previous time frame before we get to writing the
            -- next time frame (time frame = 32 time samples). This should happen by design.
            finalWeightRst <= finalWeightsSOF;
            if finalWeightsSOF = '1' and finalWeightEmpty = '0' then
                finalWeight_overflow <= '1';  -- This should never happen.
            elsif finalWeightFull = '1' then
                finalWeight_overflow <= '1';  -- Nor should this.
            end if;
        end if;
    end process;
    

    finalWeightStore_i : xpm_fifo_sync
    generic map (
        CASCADE_HEIGHT => 0,        -- DECIMAL
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "auto", -- String
        FIFO_READ_LATENCY => 1,     -- DECIMAL
        FIFO_WRITE_DEPTH => 64,     -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 7,   -- DECIMAL = log2(read depth) + 1
        READ_DATA_WIDTH => 64,      -- DECIMAL
        READ_MODE => "std",         -- String
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "1404", -- String : 1404 = data valid, read and write data counts enabled.
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 64,     -- DECIMAL
        WR_DATA_COUNT_WIDTH => 7    -- DECIMAL
    ) port map (
        almost_empty => open,       -- 1-bit output: Almost Empty
        almost_full => open,        -- 1-bit output: Almost Full
        data_valid => open, -- 1-bit output: Read Data Valid
        dbiterr => open,            -- 1-bit output: Double Bit Error
        dout => finalWeightDout,    -- READ_DATA_WIDTH-bit output: Read Data
        empty => finalWeightEmpty,  -- 1-bit output: Empty Flag
        full => finalWeightFull,    -- 1-bit output: Full Flag
        overflow => open,           -- 1-bit output: Overflow
        prog_empty => open,         -- 1-bit output: Programmable Empty
        prog_full => open,          -- 1-bit output: Programmable Full
        rd_data_count => finalWeightRdCount, -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count
        rd_rst_busy => open,        -- 1-bit output: Read Reset Busy
        sbiterr => open,            -- 1-bit output: Single Bit Error
        underflow => open,          -- 1-bit output: Underflow: read request rejected because the fifo was empty.
        wr_ack => open,             -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => finalWeightWrCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count
        wr_rst_busy => open,      -- 1-bit output: Write Reset Busy
        din => finalWeightDin,    -- WRITE_DATA_WIDTH-bit input: Write Data
        injectdbiterr => '0',     -- 1-bit input
        injectsbiterr => '0',     -- 1-bit input
        rd_en => finalWeightRdEn, -- 1-bit input: Read Enable
        rst => finalWeightRst,    -- 1-bit input: Reset: Must be synchronous to wr_clk. 
        sleep => '0',            -- 1-bit input
        wr_clk => i_BF_clk,      -- 1-bit input Write clock
        wr_en => finalWeightWrEn -- 1-bit input Write Enable
    );
    

    ----------------------------------------------------------------------------------------------

    
    -------------------------------------------------------------------------
    -- Readout of data to form packets and send out to the ethernet interface
    --
    -- Inputs used : 
    --   - packetStore_i 
    --       - packetBufPol{0/1}Dout(0/1/2) : 64 bit values = 32+32 complex beamformed data.
    --       - See the description above for addressing (packetBufRdAddr), holds 2 blocks of 32 time samples for all 216 fine channels.
    --   - scalingShift, scalingMantissa : valid for packet "packetAbsSumSelectDel4" on packetAbsSumValidDel4 = '1'
    --   - finalWeightDout : 64-bits wide, goes at the start of the packet, 6 x 64bit words per packet.
    --      (read from the "finalWeightStore_i" FIFO).
    --
    --
    -- Reading from the packet buffers to the output buffer.
    -- The packet buffers consist of 6 (=2pol x 3fine channels) memories,
    -- organised as (2 buffers) x 2304 (=32 times x 72 fine) deep x 64 bits wide.
    -- Data is written to those memories 2 polarisations at a time, but is read 2 time samples at a time.
    -- (see PST_comboRAM_dp.vhd for a detailed description).
    --  The six memories have outputs packetBufPol0Dout(0:2), packetBufPol1Dout(0:2):
    --  
    -- The output packets are in the order :
    --
    --  Sample |  Fine  |  Polarisation  |  Time
    --    0        0           0             0      <-- In the output buffer, a sample is 32 bits (16+16 complex)
    --    1        0           0             1      <-- The output bus is 64 bits wide, so two time samples are sent per clock.
    --    ...
    --    31       0           0             31
    --    32       0           1             0
    --    33       0           1             1
    --    ...
    --    63       0           1             31
    --    64       1           0             0
    --    etc. 
    --
    -- Reads are addressed by 
    --  packetBufRdBuffer (1 bit)
    --  packetBufRdTime (5 bits); Note two time samples are read out, packetBufRdTime and packetBufRdTime+1. The LSB of packetBufRdTime is ignored. 
    --  packetBufRdPol  (1 bit)
    --  packetBufRdFreq (7 bits, 0 to 71)
    -- and data is returned 4 clocks later in packetBufTime0Dout(63:0), packetBufTime1Dout(63:0)
    --
    
    beamTriggeri : entity bf_lib.PSTbeamTrigger_dp
    generic map (
        g_BEAM_NUMBER => g_BEAM_NUMBER,    --  integer;
        -- Number of clock cycles to wait after a packet is ready to send before actually sending it on the BFData interface.
        -- This is to ensure there are no clashes on the BFdata bus between different beamformers.
        -- Up to 20 bits. It takes about 776 clocks to send a packet, so a 20 bit value enables sharing between up to 1048575/776 = 1351 beamformer instances. 
        g_PACKET_OFFSET => g_PACKET_OFFSET --  natural range 0 to 1048575
    ) Port map (
        i_BF_clk => i_BF_clk,
        ---------------------------------------------------------------------------
        -- configuration
        -- Number of beams enabled; This beamformer will output data packets if g_BEAM_NUMBER < i_beamsEnabled
        i_beamsEnabled => beamsEnabled, -- in std_logic_vector(7 downto 0); 
        -- pulse high to indicate data is available to read out
        i_readStart  => triggerSendPacket, --  in std_logic;
        -- Sequence of 9 pulses to trigger reading of a frame
        -- Pulses are sequenced based on g_BEAM_NUMBER such that each beam has its own timeslot for the output bus.
        o_frameSend  => frameSend, -- out std_logic;
        o_frameCount => frameCount -- out (3:0)
    );
    

    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            if packetAbsSumValidDel4 = '1' then
                packetShift(to_integer(unsigned(packetAbsSumSelectDel4))) <= scalingShift;
                packetMantissa(to_integer(unsigned(packetAbsSumSelectDel4))) <= scalingMantissa;
            end if;
            
            if finalWeightsSOF = '1' then
                -- finalWeightsSOF occurs a few clocks after data for the the last time, last fine channel (216), last station
                -- weights have to be processed through the divider before we can read data out.
                -- So wait until finalWeightRdCount >= 6 (6 words in the FIFO = enough for the first packet)
                readout_pending <= '1';
                readout_pending_waitcount <= "1111";
                outputBufferHold <= finalWeightsBuffer;
                triggerSendPacket <= '0';
            elsif readout_pending = '1' then
                if readout_pending_waitcount /= "0000" then
                    -- the finalWeight FIFO has a reset, wait until it is complete before checking the FIFO read count.
                    readout_pending_waitcount <= std_logic_vector(unsigned(readout_pending_waitcount) - 1);
                end if;
                if readout_pending_waitcount = "0000" and readout_pending = '1' then
                    if unsigned(finalWeightRdCount) >= 6 then
                        triggerSendPacket <= '1';
                        readout_pending <= '0';
                    else
                        triggerSendPacket <= '0';
                    end if;
                else
                    triggerSendPacket <= '0';
                end if;
            else
                triggerSendPacket <= '0';
            end if;
            
            if triggerSendPacket = '1' then
                packetShiftHold <= packetShift;
                packetMantissaHold <= packetMantissa;
            end if;

            ------------------------------------------------------------------
            -- State machine to control sending a packet.
            if (frameSend = '1') then
                readout_fsm <= start;
                readoutBufferSelect <= outputBufferHold;
                readoutFrameCount <= frameCount;
            else
                case readout_fsm is
                    when start =>
                        finalWeightRdWordsSent <= "000";
                        readout_fsm <= sendHeaderWord;
                    
                    when sendHeaderWord =>
                        -- 1 word at the start of the packet with meta data in it (virtual channel + scaling factor)
                        readout_fsm <= sendWeights;
                    
                    when sendWeights =>
                        -- Count out 6 words from the FIFO
                        finalWeightRdWordsSent <= std_logic_vector(unsigned(finalWeightRdWordsSent) + 1);
                        if unsigned(finalWeightRdWordsSent) = 5 then
                            -- 6 words of weights.
                            readout_fsm <= sendData;
                        end if;
                        ---------------------------
                        --- Where to read from in the buffer
                        packetBufRdBuffer <= readoutBufferSelect; -- 0 or 1;
                        packetBufRdTime <= "00000"; -- in (4:0);  -- two time samples read out, i_rd_time and i_rd_time+1. The LSB of i_rd_time is ignored. 
                        packetBufRdPol <= '0';     -- in std_logic;
                        -- packetBufRdFreq runs from 0 to 71.
                        -- (3 fine frequencies for each step in packetBufRdFreq)
                        -- frame 0 => freq = 0, 
                        -- frame 1 => freq = 8,  <-- 8 * 3 = 24 frequency channels per packet.
                        -- up to frame 8 => freq = 64
                        packetBufRdFreq <= readoutFrameCount & "000";
                        packetBufRdFreq_3 <= "00";
                        
                    when sendData =>
                        -- Loop through the data in the packet buffer to generate the output packet
                        --  Sample |  Fine  |  Polarisation  |  Time
                        --    0        0           0             0      
                        --    1        0           0             1      <-- Note : reads from the buffer get two time samples per clock.
                        --    ...
                        --    31       0           0             31
                        --    32       0           1             0
                        --    33       0           1             1
                        --    ...
                        --    63       0           1             31
                        --    64       1           0             0
                        --    etc. 
                        --
                        -- 4 clock latency for the packetstore memories, 
                        -- 1 clock to select correct fine channel
                        -- 5 clock latency for scaling operation 
                        -- So total of 10 clocks from this state to data output.
                        if unsigned(packetBufRdTime) = 30 then
                            packetBufRdTime <= (others => '0');
                            packetBufRdPol <= not packetBufRdPol;
                            if packetBufRdPol = '1' then
                                if packetBufRdFreq_3 = "00" then
                                    packetBufRdFreq_3 <= "01";
                                elsif packetBufRdFreq_3 = "01" then
                                    packetBufRdFreq_3 <= "10";
                                else
                                    packetBufRdFreq_3 <= "00";
                                    packetBufRdFreq <= std_logic_vector(unsigned(packetBufRdFreq) + 1);
                                    if packetBufRdFreq(2 downto 0) = "111" then
                                        readout_fsm <= done;
                                    end if;
                                end if;
                            end if;
                        else
                            packetBufRdTime <= std_logic_vector(unsigned(packetBufRdTime) + 2);
                        end if;
                    
                    when done =>
                        readout_fsm <= done;
                        
                    when others =>
                        readout_fsm <= done;
                end case;
            end if;
            
            -- Data output is ?? clocks later than the state machine.
            -- There are 3 components of the packet, with correponding states in readout_fsm
            --  (1) Header Word (readout_fsm = sendHeaderWord)
            --      - loaded from registers
            --  (2) Weights (readout_fsm = sendWeights)
            --      - read from the finalWeight FIFO
            --  (3) main packet data (readout_fsm = sendData)
            --      - read from the "packetStore_i" buffer, and then scaled
            -- Each component has a different latency to get the data :
            -- 
            --                          header word    |  weights             | main packet data
            --  readout_fsm                                                     packetBufRdTime, packetBufRdFreq etc
            --  readout_fsm_del(0)                                              
            --  readout_fsm_del(1)                                              
            --  readout_fsm_del(2)                                              
            --  readout_fsm_del(3)                                              packetBufTime0Dout  (4 cycle latency in "packetStore_i" from packetBufRdTime etc)
            --  readout_fsm_del(4)                                              sample32bit0/1/2/3
            --  readout_fsm_del(5)                                         
            --  readout_fsm_del(6)                                         
            --  readout_fsm_del(7)                                         
            --  readout_fsm_del(8)                             finalWeightRdEn                 
            --  readout_fsm_del(9)      outputSelectHeaderWord finalWeightDout  sample16bit0/1/2/3  (5 cycle latency in "scaleSample0Rei")
            --  readout_fsm_del(10)     BFData                 BFData           BFData
            --  readout_fsm_del(11)     o_BFData               o_BFData         o_BFData
            
            readout_fsm_del(0) <= readout_fsm;
            readout_fsm_del(15 downto 1) <= readout_fsm_del(14 downto 0);
            
            packetBufRdFreq_3_del(0) <= packetBufRdFreq_3;
            packetBufRdFreq_3_del(15 downto 1) <= packetBufRdFreq_3_del(14 downto 0);
            
            readoutFrameCount_del(0) <= readoutFrameCount;
            readoutFrameCount_del(9 downto 1) <= readoutFrameCount_del(8 downto 0);
            
            if readout_fsm_del(7) = sendWeights then
                finalWeightRdEn <= '1';
            else
                finalWeightRdEn <= '0';
            end if;
            
            if (packetBufRdFreq_3_del(3) = "00") then
                sample32bit0 <= packetBufTime0Dout(0)(31 downto 0);
                sample32bit1 <= packetBufTime0Dout(0)(63 downto 32);
                sample32bit2 <= packetBufTime1Dout(0)(31 downto 0);
                sample32bit3 <= packetBufTime1Dout(0)(63 downto 32);
            elsif (packetBufRdFreq_3_del(3) = "01") then
                sample32bit0 <= packetBufTime0Dout(1)(31 downto 0);
                sample32bit1 <= packetBufTime0Dout(1)(63 downto 32);
                sample32bit2 <= packetBufTime1Dout(1)(31 downto 0);
                sample32bit3 <= packetBufTime1Dout(1)(63 downto 32);
            else
                sample32bit0 <= packetBufTime0Dout(2)(31 downto 0);
                sample32bit1 <= packetBufTime0Dout(2)(63 downto 32);
                sample32bit2 <= packetBufTime1Dout(2)(31 downto 0);
                sample32bit3 <= packetBufTime1Dout(2)(63 downto 32);
            end if;
            
            if scale_exp_frac(3) = '1' then
                shiftFinal <= scale_exp_frac(7 downto 4);
                scaleFinal <= scale_exp_frac(3 downto 0);
            else
                shiftFinal <= packetShiftHold(to_integer(unsigned(readoutFrameCount_del(3))));
                scaleFinal <= packetMantissaHold(to_integer(unsigned(readoutFrameCount_del(3))));
            end if;
            
--            if readout_fsm_del(8) = sendHeaderWord then
--                outputSelectHeaderWord <= '1';
--            else
--                outputSelectHeaderWord <= '0';
--            end if;
            
--            if readout_fsm_del(8) = sendWeights then
--                outputSelectWeights <= '1';
--            else
--                outputSelectWeights <= '0';
--            end if;
            
--            if readout_fsm_del(8) = sendData then
--                outputSelectData <= '1';
--            else
--                outputSelectData <= '0';
--            end if;
            
            if readout_fsm_del(7) = sendHeaderWord  and beamEnabled_del = '1' then
                outputSelectHeaderWord <= '1';
            else
                outputSelectHeaderWord <= '0';
            end if;
            
            if readout_fsm_del(7) = sendWeights and beamEnabled_del = '1' then
                outputSelectWeights <= '1';
            else
                outputSelectWeights <= '0';
            end if;
            
            if readout_fsm_del(7) = sendData and beamEnabled_del = '1' then
                outputSelectData <= '1';
            else
                outputSelectData <= '0';
            end if;
            
            if outputSelectHeaderWord = '1' then
                output_sel <= "00";
            elsif outputSelectWeights = '1' then
                output_sel <= "01";
            elsif outputSelectData = '1' then
                output_sel <= "10";
            else
                output_sel <= "11";
            end if;
            
            
            -- Convert shiftFinal and scaleFinal to a 32 bit floating point value
            -- The scale parameter is, from the PSR ICD, "The value by which beamformer voltage samples are scaled before conversion to the values in the packet"
            --
            -- Arithmetic Scaling :
            --   * Matrix multiplier is 16 bit * 16 bit => 32 bit result, then drop 8 bits
            --   * Phase correction is 24 bit * 17 bit sin+cos, then drops 22 bits
            -- Treat unity for the matrix coefficients as 32768, so the matrix multiplier scales up by a factor of 2^7
            -- unity for the sin/cos lookup is 2^16
            -- So altogether the matrix multipler and phase correction scale up by 2^23, but then drop 22 bits, so they scale up by 2.
            -- 
            if scaleFinal(3) = '0' then -- we only generate one case where this is true, scaleFinal = "0111", so we need to add one extra to the exponent, since FP32 assumes the mantissa starts with a 1.
                fp32Exp <= std_logic_vector(to_unsigned(126 + 1,8) - unsigned(shiftFinalExt));
                fp32Frac <= scaleFinal(1 downto 0) & "00";
            else
                -- i.e. to get the output value, we took the original value,
                -- scaled it up by 2^1 via the matrix multiplier and phase correction, 
                -- and then scaled it down by 2^shiftFinal.
                fp32Exp <= std_logic_vector(to_unsigned(127 + 1,8) - unsigned(shiftFinalExt)); 
                fp32Frac <= scaleFinal(2 downto 0) & '0';
            end if;
            headerWord <= '0' & fp32Exp & fp32Frac;
            
        end if;
    end process;
    
    shiftFinalExt <= "0000" & shiftFinal;
    
    scaleSample0Rei : entity bf_lib.PSTscaleFinal
    port map (
        i_clk => i_BF_clk,
        -- data input, with required scaling
        i_data => sample32bit0,  -- in(31:0);
        i_shift => shiftFinal,  -- in(3:0);  -- "0000" = use low 16 bits, "0001" = scale down by 2, i.e. use bits 16:1, etc.
        i_scale => scaleFinal, -- in(3:0);  -- Unsigned scale factor, 1 integer bit + 3 fractional bits = 4 bits.
        -- data output
        o_data => sample16bit0 -- out(15:0)  -- 5 clock latency from i_data, i_shift, i_scale.
    );
    
    scaleSample0Imi : entity bf_lib.PSTscaleFinal
    port map (
        i_clk => i_BF_clk,
        -- data input, with required scaling
        i_data => sample32bit1,  -- in(31:0);
        i_shift => shiftFinal,  -- in(3:0);  -- "0000" = use low 16 bits, "0001" = scale down by 2, i.e. use bits 16:1, etc.
        i_scale => scaleFinal, -- in(3:0);  -- Unsigned scale factor, 1 integer bit + 3 fractional bits = 4 bits.
        -- data output
        o_data => sample16bit1 -- out(15:0)  -- 5 clock latency from i_data, i_shift, i_scale.
    );

    scaleSample1Rei : entity bf_lib.PSTscaleFinal
    port map (
        i_clk => i_BF_clk,
        -- data input, with required scaling
        i_data => sample32bit2,  -- in(31:0);
        i_shift => shiftFinal,  -- in(3:0);  -- "0000" = use low 16 bits, "0001" = scale down by 2, i.e. use bits 16:1, etc.
        i_scale => scaleFinal, -- in(3:0);  -- Unsigned scale factor, 1 integer bit + 3 fractional bits = 4 bits.
        -- data output
        o_data => sample16bit2 -- out(15:0)  -- 5 clock latency from i_data, i_shift, i_scale.
    );
    
    scaleSample1Imi : entity bf_lib.PSTscaleFinal
    port map (
        i_clk => i_BF_clk,
        -- data input, with required scaling
        i_data => sample32bit3,  -- in(31:0);
        i_shift => shiftFinal,  -- in(3:0);  -- "0000" = use low 16 bits, "0001" = scale down by 2, i.e. use bits 16:1, etc.
        i_scale => scaleFinal, -- in(3:0);  -- Unsigned scale factor, 1 integer bit + 3 fractional bits = 4 bits.
        -- data output
        o_data => sample16bit3 -- out(15:0)  -- 5 clock latency from i_data, i_shift, i_scale.
    );    
    
    
    ---------------------------------------------------------------------------------
    -- Debug Cores
    ---------------------------------------------------------------------------------
    
    
    
--    ila_beamdebug : ila_2
--    port map (
--        clk => i_BF_clk,
--        probe0(0) => dbadPacket,
--        probe0(20 downto 1) => dreadout_waitCount2,
--        probe0(40 downto 21) => dwholeFrameWaitCount,
--        probe0(44 downto 41) => dreadout_fsm,
--        probe0(45) => dTriggerSendPacket,
--        probe0(46) => dTriggerSendOnePacketOnly,
--        probe0(47) => dTriggerSendPacketHold,
--        probe0(48) => dOutputBufferHold,
--        probe0(49) => dTriggerSendOnePacketOnlyHold,
--        probe0(63 downto 50) => zeros(63 downto 50)
--    );
    zeros <= (others => '0');
    
--    ila_beamdebug : ila_beamdata
--    port map (
--        clk => i_BF_clk,
--        probe0(31 downto 0) => phaseBaseUpdated,
--        probe0(79 downto 32) => phaseRdData(47 downto 0),
--        probe0(80) => loadJonesDel7,
--        probe0(95 downto 81) => phaseAddr,
--        probe0(96) => valid,
--        probe0(104 downto 97) => fine,
--        probe0(109 downto 105) => timestep(4 downto 0),
--        probe0(110) => firstStation,
--        probe0(111) => lastStation,
--        probe0(119 downto 112) => zeros(7 downto 0)
--    );
    
    
end Behavioral;

