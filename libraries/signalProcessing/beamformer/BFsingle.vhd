----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 02.05.2020 20:29:14
-- Design Name: 
-- Module Name: BFsingle - Behavioral
-- Project Name: CryoPAF
-- Description: 
--  Single channel beamformer.
--  This module is intended to be chained together with up to 11 other BFSingle instances.
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library xpm;
use xpm.vcomponents.all;

entity BFsingle is
    Generic(
        g_N_CHANNELS : integer := 10;   -- Number of channels packed together in the input data stream
        g_THIS_CHANNEL : integer := 0;  -- Which channel to extract in this module (0 to (g_N_CHANNELS-1)). 
        g_BEAM_ORDER : integer := 0;    -- When is it this modules turn to put data on the beam bus ? 0 = immediately after i_update, otherwise wait for g_BEAM_ORDER packets to go past.
        g_BEAM_OUTPUT : integer := 0    -- Which of the two beam output buses to use ?
    );
    Port(
        i_clk : in std_logic; -- only one clock domain
        ----------------------------------------------------------------------------
        -- PORT DATA
        ----------------------------------------------------------------------------
        -- input Data Bus; all the data for all the ports.
        -- input data is 12+12 bits complex, packed into 4 frequency samples in each clock.
        -- up to 12 frequency channels are carried, with a new port every 3 clocks.
        -- 
        --                    port 0          port 1        port 2         port 3      ...
        --                /------------\  /-----------\  /-----------\  /-----------\  ...
        --    bits(95:72)  f9   f10  f11  f9   f10  f11  f9   f10  f11  f9   f10  f11  ...
        --    bits(71:48)  f6   f7   f8   f6   f7   f8   f6   f7   f8   f6   f7   f8   ...
        --    bits(47:24)  f3   f4   f5   f3   f4   f5   f3   f4   f5   f3   f4   f5   ...
        --    bits(23:0)   f0   f1   f2   f0   f1   f2   f0   f1   f2   f0   f1   f2   ...
        --    clock         0    1    2    3    4    5    6    7    8    9   10   11   ...
        -- So the module picks up a sample every 3rd clock.
        -- e.g. if 
        --   - g_THIS_CHANNEL = 0 => use bits(23:0), starting at clock 0
        --   - g_THIS_CHANNEL = 1 => use bits(23:0), starting at clock 1
        --   - g_THIS_CHANNEL = 2 => use bits(23:0), starting at clock 2, and set output bus bits(23:0) = 0, so it will be optimised away. 
        --   - g_THIS_CHANNEL = 3 => use bits(47:24), starting at clock 0
        --     etc.
        -- Total number of data words should be 3 * 224 = 672 (there are 224 ports; a port gets delivered every 3 clocks)
        i_portData      : in std_logic_vector(95 downto 0);
        i_portDataValid : in std_logic;
        i_portStart     : in std_logic; -- start of a new frame; This signals both that new data is arriving for this frame and that the beams computed for the previous frame should start being sent.
        i_dataBuffer    : in std_logic; -- which data buffer to put the data into; Other buffer is read from.
        i_weightSelect  : in std_logic_vector(11 downto 0); -- Indicates which set of beamform coefficients should be used (coefficients are double-buffered). This module uses the bottom bit and shifts all the bits right before passing to the next module.
        i_calStart      : in std_logic; -- check on i_portStart; Indicates that the calibration correlator should start. It will run for the number of time frames previously configured over the register interface.
        -- output Data bus; pipelined version of the input data bus, so BFSingle units can be chained together.
        o_portData      : out std_logic_Vector(95 downto 0);
        o_portDataValid : out std_logic;
        o_portStart     : out std_logic;
        o_dataBuffer    : out std_logic;
        o_weightSelect  : out std_logic_vector(11 downto 0);
        o_calStart      : out std_logic;
        -----------------------------------------------------------------------------
        -- BEAM DATA
        -----------------------------------------------------------------------------
        -- One beam output per clock.
        -- So e.g. for 72 dual-pol beams, there are 144 outputs cycles.
        -- Data for all beams is generated either
        --  - When i_portStart occurs, if g_BEAM_ORDER = 0
        --  - After g_BEAM_ORDER packets have gone past otherwise.
        -- Output packets are always at least one clock long; to facilitate this, the last clock of the packet is always invalid data.
        -- So, e.g. if there were 17 beams calculated, then an output packet 18 clocks long would be generated, with the last cycle of the
        -- output packet holding invalid data.
        i_beamData0     : in std_logic_vector(31 downto 0); -- 16+16 bit complex beam data.
        i_beamValid0    : in std_logic;
        o_beamData0     : out std_logic_vector(31 downto 0);
        o_beamValid0    : out std_logic;
        -- Data is put on this bus if g_BEAM_OUTPUT = 1.
        i_beamData1     : in std_logic_vector(31 downto 0); -- 16+16 bit complex beam data.
        i_beamValid1    : in std_logic;
        o_beamData1     : out std_logic_vector(31 downto 0);
        o_beamValid1    : out std_logic;
        -----------------------------------------------------------------------------
        -- Register reads + writes
        -----------------------------------------------------------------------------
        -- Register data could be
        --  - Beamformer weights. These packets must align with i_portStart, since that is the only time there is bandwidth to write them to the weights ultraRAMs.
        --      - Header word on the packet is 
        --        - bits(3:0) = 0x1  - indicates that these are beamformer weights
        --        - bits(7:4) = destination; ignore this packet unless this matches g_BEAM_ORDER
        --        - bits(15:8) = number of weights in the packet, maximum per packet is 256.
        --        - bits(18:16) = Memory address to write them to, mod 6
        --        - bits(30:19) = Memory address to write them to, divided by 6
        --            - The address is sent this way to map to the weights ultraRAMs.
        --              There are 3 ultraRAMs used, with 2 weights per 72 bit word at an ultraRAM address
        --              A total of 24576 weights can be specified, double buffered with 12288 in each buffer.
        --              address/6 => 0 to 4095, with 2048 in each buffer 
        --
        --      - Data words in the packet are
        --        - 14+14 bit complex, 8 bit port to use, 3 bit instruction. Instruction is 
        --          - bit(0) = start of a new beam
        --          - bit(1) = end of a beam
        --          - bit(2) = end of all beams      
        --             -- NOTE - Weights are dealt with in groups of 12. Even though these bits come into the module for all weights, they are condensed down to 1 value for every 12 weights. 
        --                       If the number of weights desired is not a multiple of 12, then unused weights must be set to zero to make up a multiple of 12 weights for each beam.
        --                       There is no checking in this module that these values are consistent across groups of 12 weights; the last weight written in a group of 12 sets these bits for all 12 weights.
        --        - Beamformer weights come in packets of up to 256 weights.
        --  - Calibration setup (1)
        --      - Header word on the packet is
        --        - bits(3:0) = 0x2  - indicates calibration setup command
        --        - bits(7:4) = destination; ignore this packet unless this value is "1111" or g_BEAM_ORDER
        --        - bits(15:8) = port 1 to calibrate against
        --        - bits(23:16) = port 2 to calibrate against
        --      - No data.
        --  - Calibration setup (2)
        --      - Header word on the packet is
        --        - bits(3:0) = 0x3  - indicates calibration setup command
        --        - bits(7:4) = destination; only use the configured value if "1111" or g_BEAM_ORDER.
        --        - bits(31:8) = Number of time frames to integrate
        --      - No data.
        --  - Read out calibration correlator data
        --      - Header word on the packet is
        --        - bits(3:0) = 0x4  - indicates calibration correlator readout command. This generate a packet 896 clocks long on o_regReadData (36 bit real, then 36 bit imaginary, for 2 columns and 224 rows of the correlation).
        --        - bits(7:4) = destination; only use if this is g_BEAM_ORDER.
        --      - No data.
        --  - Read back of beamformer weights
        --      - Header word on the packet is
        --        - bits(3:0) = 0x5  - indicates readback command
        --        - bits(7:4) = destination; ignore this packet unless this matches g_BEAM_ORDER
        --        - bits(15:8) = Number of weights to read
        --        - bits(31:16) = start read address of weights.
        i_regWriteData : in std_logic_vector(38 downto 0);
        i_regWriteValid : in std_logic;
        -- Pipelined input bus to chain to the next instance of BFSingle
        o_regWriteData : out std_logic_vector(38 downto 0);
        o_regWriteValid : out std_logic;
        -- Register output bus, used to read back weights and also to dump calibration correlator data.
        i_regReadData  : in std_logic_vector(35 downto 0);
        i_regReadValid : in std_logic;
        o_regReadData  : out std_logic_vector(35 downto 0);
        o_regReadValid : out std_logic
    );
end BFsingle;

architecture Behavioral of BFsingle is

    signal portDataDel1, portDataDel2  : std_logic_vector(95 downto 0);
    signal portDataValidDel1, portDataValidDel2 : std_logic;
    signal portStartDel1, portStartDel2 : std_logic; -- start of a new frame; This signals both that new data is arriving for this frame and that the beams computed for the previous frame should start being sent.
    signal weightSelectDel1, weightSelectDel2 : std_logic_vector(11 downto 0); -- Indicates which set of beamform coefficients should be used (coefficients are double-buffered).
    signal calStartDel1, calStartDel2 : std_logic;
    signal dataBufferDel1, dataBufferDel2 : std_logic;
    signal frequencyCountDel1 : std_logic_vector(1 downto 0);
    signal portCountDel1 : std_logic_vector(7 downto 0);
    signal freqSelect : integer range 0 to 2;
    signal busSelect : integer range 0 to 3;
    
    signal wrPortData : std_logic_vector(23 downto 0);
    signal wrEnPortData : std_logic_vector(0 downto 0);
    signal wrAddrPortData : std_logic_vector(8 downto 0);
    
    type t_slv_8_arr   IS ARRAY (INTEGER RANGE <>) OF STD_LOGIC_VECTOR(7 DOWNTO 0);
    type t_slv_9_arr   IS ARRAY (INTEGER RANGE <>) OF STD_LOGIC_VECTOR(8 DOWNTO 0);
    type t_slv_24_arr  IS ARRAY (INTEGER RANGE <>) OF STD_LOGIC_VECTOR(23 DOWNTO 0);
    type t_slv_32_arr  is array (integer range <>) of std_logic_vector(31 downto 0);
    type t_slv_36_arr  is array (integer range <>) of std_logic_vector(35 downto 0);
    type t_slv_64_arr  is array (integer range <>) of std_logic_vector(63 downto 0);
    type t_slv_72_arr  is array (integer range <>) of std_logic_vector(71 downto 0);
    
    signal rdPortData : t_slv_24_arr(12 downto 0);
    signal rdAddrPortData : t_slv_9_arr(12 downto 0);
    
    signal weightsRdAddr : std_logic_vector(9 downto 0);
    signal weightsRdAddrFull1 : std_logic_vector(11 downto 0);
    signal weightsRdAddrFull2 : std_logic_vector(11 downto 0);
    signal weightsRdBuffer : std_logic;
    signal rdPortBuffer : std_logic;
    signal beamformRunning : std_logic;
    
    signal weightsAddrFull1, weightsAddrFull2 : std_logic_vector(11 downto 0);
    signal weightsRdAddrFull1Shadow : std_logic_vector(8 downto 0);
    signal weightsDout1, weightsDout2, weightsDout1Shadow : t_slv_72_arr(2 downto 0); -- Weights out of the 3 ultraRAMs.
    signal weightsDout1Del5, weightsDout2Del5 : t_slv_72_arr(2 downto 0);
    signal weightsAddr1ultraRAM, weightsAddr1ultraRAMDel2, weightsAddr1ultraRAMDel3, weightsAddr1ultraRAMDel4 : std_logic;
    
    signal BFStartDel0, BFStartDel1, BFStartDel2, BFStartDel3 : std_logic;
    
    signal BFCmdDel5, BFCmdDel6, BFCmdDel7, BFCmdDel8, BFCmdDel9,  BFCmdDel10 : std_logic_vector(3 downto 0);
    signal BFCmdDel11, BFCmdDel12, BFCmdDel13, BFCmdDel14, BFCmdDel15 : std_logic_vector(3 downto 0);
    signal BFCmdDel16, BFCmdDel17, BFCmdDel18, BFCmdDel19, BFCmdDel20 : std_logic_vector(3 downto 0);
    
    signal rdPortBufferDel1, rdPortBufferDel2, rdPortBufferDel3, rdPortBufferDel4, rdPortBufferDel5 : std_logic; 
    signal rdPortBufferDel6, rdPortBufferDel7, rdPortBufferDel8, rdPortBufferDel9, rdPortBufferDel10 : std_logic;
    signal rdPortBufferDel11, rdPortBufferDel12, rdPortBufferDel13, rdPortBufferDel14, rdPortBufferDel15 : std_logic;
    signal rdPortBufferDel16, rdPortBufferDel17, rdPortBufferDel18, rdPortBufferDel19, rdPortBufferDel20 : std_logic;
    signal weightsDout1Del6, weightsDout1Del7, weightsDout1Del8, weightsDout2Del6, weightsDout2Del7, weightsDout2Del8 : t_slv_72_arr(2 downto 0);
    
    signal BFMultDinA, BFMultDinB : t_slv_32_arr(11 downto 0);
    signal BFCmdDout : std_logic_vector(2 downto 0);
    signal BFMultDout : t_slv_64_arr(11 downto 0);
    signal BFMultDoutReDel14, BFMultDoutImDel14 : t_slv_36_arr(11 downto 0);
    signal BFSum1ReDel15, BFSum1ImDel15 : t_slv_36_arr(5 downto 0);
    signal BFSum2ReDel16, BFSum2ImDel16 : t_slv_36_arr(2 downto 0);
    signal BFSum3ReDel17, BFSum3ImDel17 : t_slv_36_arr(1 downto 0);
    signal BFSum4ReDel18, BFSum4ImDel18 : std_logic_vector(35 downto 0);
    
    signal beamReDel19, beamImDel19 : std_logic_vector(35 downto 0);
    signal beamWrEn : std_logic_vector(0 downto 0);
    signal beamDin : std_logic_vector(31 downto 0);
    signal beamWrAddr : std_logic_vector(8 downto 0);
    signal beamCount : std_logic_vector(7 downto 0);
    
    signal regWriteDataDel1, regWriteDataDel2 : std_logic_vector(38 downto 0);
    signal regWriteValidDel1, regWriteValidDel2 : std_logic;
    signal calPort0, calPort1 : std_logic_vector(7 downto 0);

    signal weightWrEn : std_logic;
    signal mainWeightWrEn, shadowWeightWrEn : t_slv_8_arr(2 downto 0);
    signal Nweights : std_logic_vector(7 downto 0);
    signal shadowWeightsWrAddr : std_logic_vector(8 downto 0);

    signal weightWrAddrMod6 : std_logic_vector(2 downto 0);
    signal weightWrAddrDiv6 : std_logic_vector(11 downto 0);
    signal weightsDin : std_logic_vector(71 downto 0);
    
    signal BFCmdWrAddrDel1 : std_logic_vector(10 downto 0);
    signal BFCmdDinDel1, BFCmdDin : std_logic_vector(2 downto 0);
    signal BFCmdWrEnDel1 : std_logic_vector(0 downto 0);
    signal BFCmdRdAddr : std_logic_vector(10 downto 0);
    
    signal beamData0Del1, beamData1Del1 : std_logic_vector(31 downto 0);
    signal beamValid0Del1, beamValid1Del1 : std_logic;
    
    signal beamDout : std_logic_vector(31 downto 0);
    signal localBeamValid, localBeamValidDel1, localBeamValidDel2, localBeamValidDel3 : std_logic;
    type beamOut_fsm_type is (wait_to_send, send_data, send_done);
    signal beamOut_fsm : beamOut_fsm_type := send_done;
    signal beamDataDel1, beamDataOtherDel1 : std_logic_vector(31 downto 0);
    signal beamValidOtherDel1, beamValidDel1, beamValidDel2 : std_logic;
    signal beamRdCount : std_logic_vector(7 downto 0);
    signal totalBeams : std_logic_vector(7 downto 0);
    signal beamRdAddr : std_logic_vector(8 downto 0);
    signal beamsRemaining : std_logic_vector(3 downto 0);
    signal beamRdBuffer : std_logic;
    
    signal calPortAddr : std_logic_vector(7 downto 0);
    signal integrationTime, calCount : std_logic_vector(23 downto 0);
    signal calCount0 : std_logic;
    signal calStart : std_logic;
    signal calReset : std_logic;
    signal calBuffer : std_logic;
    
    signal regReadDataDel1, regReadDataDel2 : std_logic_vector(35 downto 0);
    signal regReadValidDel1, regReadValidDel2 : std_logic;
    signal calReadout : std_logic;
    signal corValid : std_logic;
    signal corData : std_logic_vector(35 downto 0);
    signal calDump : std_logic;
    -- Complex multiplier for beamformer; 12 bit port data * 14 bit weights.
    -- TCL : 
    --  create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name cmult12x14
    --  set_property -dict [list CONFIG.Component_Name {cmult12x14} CONFIG.APortWidth {12} CONFIG.BPortWidth {14} CONFIG.OptimizeGoal {Performance} CONFIG.OutputWidth {27} CONFIG.MinimumLatency {4}] [get_ips cmult12x14]
    --  generate_target {instantiation_template} [get_files /data/GIANT_1/ateg/hum089/cryoPAF/test_BF/test_BF.srcs/sources_1/ip/cmult12x14/cmult12x14.xci]
    -- 4 clock latency.
    component cmult12x14
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  -- (11:0) real, (27:16) imaginary
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);  -- (13:0) real, (29:16) imaginary
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)); -- output; (26:0) real, (58:32) imaginary
    end component;
    
begin

    o_portData      <= portDataDel2;
    o_portDataValid <= portDataValidDel2;
    o_portStart     <= portStartDel2;
    o_dataBuffer    <= dataBufferDel2;
    o_weightSelect  <= weightSelectDel2;
    o_calStart      <= calStartDel2;
    
    o_regWriteData <= regWriteDataDel2;
    o_regWriteValid <= regWriteValidDel2;
    
    -- Which clock cycle and which bit selection to use from i_portData ? 
    freqSelect <= g_THIS_CHANNEL mod 3; -- Note when freqSelect = 2, then all data from the bus is used and the output bus can be set to zero.
    busSelect <= g_THIS_CHANNEL / 3;
    
    process(i_clk) 
    begin
        if rising_edge(i_clk) then
            -- Pipeline the port data.
            portDataDel1 <= i_portData;
            portDataDel2 <= portDataDel1;
            dataBufferDel1 <= i_dataBuffer;
            dataBufferDel2 <= dataBufferDel1;
            if freqSelect = 2 then  -- last use of this bus, set the bus to zero so it doesn't use any more resources.
                if busSelect = 0 then
                    portDataDel2(23 downto 0) <= (others => '0');
                elsif busSelect = 1 then
                    portDataDel2(47 downto 24) <= (others => '0');
                elsif busSelect = 2 then
                    portDataDel2(71 downto 48) <= (others => '0');
                else
                    portDataDel2(95 downto 71) <= (others => '0');
                end if;
            end if;
            portDataValidDel1 <= i_portDataValid;
            portDataValidDel2 <= portDataValidDel1;
            portStartDel1 <= i_portStart;
            portStartDel2 <= portStartDel1;
            weightSelectDel1 <= i_weightSelect;
            weightSelectDel2 <= '0' & weightSelectDel1(11 downto 1);
            calStartDel1 <= i_calStart;
            calStartDel2 <= calStartDel1;
            
            -- Pipeline the register data
            regWriteDataDel1 <= i_regWriteData; --  : in std_logic_vector(38 downto 0);
            regWriteValidDel1 <= i_regWriteValid; --  : in std_logic;
            regWriteDataDel2 <= regWriteDataDel1; 
            regWriteValidDel2 <= regWriteValidDel1;
            
            -- Capture the configuration for this module
            if (regWriteValidDel1 = '1' and regWriteValidDel2 = '0' and
                (to_integer(unsigned(regWriteDataDel1(7 downto 4))) = g_THIS_CHANNEL) and
                (regWriteDataDel1(3 downto 0) = "0100")) then
                calReadout <= '1'; -- read correlator command
            else
                calReadout <= '0';
            end if;
            
            if regWriteValidDel1 = '1' and regWriteValidDel2 = '0' then -- this is the header word of the register packet
                if ((to_integer(unsigned(regWriteDataDel1(7 downto 4))) = g_THIS_CHANNEL) or (regWriteDataDel1(7 downto 4) = "1111"))  then
                    -- Packet is for this module
                    if regWriteDataDel1(3 downto 0) = "0001" then
                        -- bits(3:0) = 0x1  - indicates that these are beamformer weights
                        -- bits(15:8) = number of weights in the packet-1 (So maximum per packet is 256)
                        -- bits(31:16) = Memory address to write them to.
                        Nweights <= regWriteDataDel1(15 downto 8);
                        weightWrAddrMod6 <= regWriteDataDel1(18 downto 16);
                        weightWrAddrDiv6 <= regWriteDataDel1(30 downto 19);
                        weightWrEn <= '1';
                    elsif regWriteDataDel1(3 downto 0) = "0010" then
                        -- (3:0) = 0x2  - indicates calibration setup command
                        calPort0 <= regWriteDataDel1(15 downto 8);
                        calPort1 <= regWriteDataDel1(23 downto 16);
                        weightWrEn <= '0';
                    elsif regWriteDataDel1(3 downto 0) = "0011" then
                        weightWrEn <= '0';
                        integrationTime <= regWriteDataDel1(31 downto 8);
                    elsif regWriteDataDel1(3 downto 0) = "0100" then
                        -- read correlator command
                        weightWrEn <= '0';
                    else
                        weightWrEn <= '0';
                    end if;
                end if;
            else
                -- Data portion of the packet; only used for writing beamformer weights.
                if (unsigned(Nweights) /= 0) then
                    NWeights <= std_logic_vector(unsigned(Nweights) - 1);
                    if weightWrAddrMod6 = "101" then
                        weightWrAddrMod6 <= "000";
                        weightWrAddrDiv6 <= std_logic_vector(unsigned(weightWrAddrDiv6) + 1);
                    else
                        weightWrAddrMod6 <= std_logic_vector(unsigned(weightWrAddrMod6) + 1);
                    end if;
                    weightWrEn <= '1';
                else
                    weightWrEn <= '0';
                end if;
            end if;
            
            -- Control writing of the port data into the port memories.
            -- Find the values that are used in this module.
            if i_portStart = '1' then
                portCountDel1 <= (others => '0'); -- del1 since it aligns with portDataDel1
                frequencyCountDel1 <= "00";       -- i_portData steps through 3 different frequencies, then through 224 different ports. 
            elsif i_portDataValid = '1' then
                if frequencyCountDel1 = "10" then
                    frequencyCountDel1 <= "00";
                    portCountDel1 <= std_logic_vector(unsigned(portCountDel1) + 1);
                else
                    frequencyCountDel1 <= std_logic_vector(unsigned(frequencyCountDel1) + 1);
                end if;
            end if;
            
            if to_integer(unsigned(frequencyCountDel1)) = freqSelect and portDataValidDel1 = '1' then
                -- Get the sample for port portCountDel1
                if busSelect = 0 then
                    wrPortData <= portDataDel1(23 downto 0);
                elsif busSelect = 1 then
                    wrPortData <= portDataDel1(47 downto 24);
                elsif busSelect = 2 then
                    wrPortData <= portDataDel1(71 downto 48);
                else
                    wrPortData <= portDataDel1(95 downto 72);
                end if;
                wrEnPortData(0) <= '1';
                wrAddrPortData <= dataBufferDel1 & portCountDel1;
            else
                wrEnPortData(0) <= '0';
            end if;
            
            -- Control beamforming
            -- Triggered by i_portStart, using the other data set, i.e. (not i_dataBuffer) 
            --
            -- Beamformer pipeline :
            -- clk   signals
            -- -1  : portStartDel1
            --  0  : weightsRdAddr, weightsRdBuffer, rdPortBuffer, beamformRunning, weightWrAddrMod6, weightWrAddrDiv6, weightWrEn
            --  1  : weightsAddrFull1, weightsAddrFull2, rdPortBufferDel1, mainWeightWrEn, shadowWeightWrEn
            --  2  : 
            --  3  : 
            --  4  : weightsDout1, weightsDout2, BFCmdDout                          <-- Data out of the weights ultraRAMs (3 cycle latency)
            --  5  : rdAddrPortData, weightsDout1Del5, weightsDout2Del5, BFCmdDel5  <-- "del" names reflect the full pipeline
            --  6  :                 weightsDout1Del6, weightsDout2Del6, BFCmdDel6
            --  7  :                 weightsDout1Del7, weightsDout2Del7, BFCmdDel7
            --  8  : rdPortData,     weightsDout1Del8, weightsDout2Del8, BFCmdDel8  <-- 3 cycle latency for port data from the BRAMs
            --  9  : BFMultDinA, BFMultDinB, BFCmdDel9                              <-- inputs to the complex multipliers; BFCmd is 3 bits : (0) = start beam, (1) = done beam, (2) = done all beams.
            --  10 :                         BFCmdDel10
            --  11 :                         BFCmdDel11
            --  12 :                         BFCmdDel12
            --  13 : BFMultDout              BFCmdDel13                             <-- 4 cycle latency for the comple multipliers
            --  14 : BFMultDoutReDel14, BFMultDoutImDel14, BFCmdDel14
            --  15 : BFSum1ReDel15, BFSum1ImDel15, BFCmdDel15
            --  16 : BFSum2ReDel16, BFSum2ImDel16, BFCmdDel16
            --  17 : BFSum3ReDel17, BFSum3ImDel17, BFCmdDel17
            --  18 : BFSum4ReDel18, BFSum4ImDel18, BFCmdDel18
            --  19 : BFSum5ReDel19, BFSum5ImDel19, BFCmdDel19                             <-- end of the adder tree.
            --  20 : DinBeam, wrAddrBeam                                            <-- data into the beam memory.
            --
            if portStartDel1 = '1' then
                weightsRdAddr <= (others => '0');          
                weightsRdBuffer <= weightSelectDel1(0);  -- Which weights buffer to read from.
                rdPortBuffer <= not dataBufferDel1;       -- Read from the port data buffer that we are not writing to.
                beamformRunning <= '1';
            elsif beamformRunning = '1' then
                weightsRdAddr <= std_logic_vector(unsigned(weightsRdAddr) + 1);
                if (BFCmdDel5(2) = '1' and (unsigned(weightsRdAddr) > 4)) or unsigned(weightsRdAddr) = 1023 then
                    beamformRunning <= '0';
                end if;
            else
                weightsRdAddr <= (others => '0');
            end if;
            BFStartDel0 <= portStartDel1;
            
            -- Pipeline Stage 1
            rdPortBufferDel1 <= rdPortBuffer;
            BFStartDel1 <= BFStartDel0;
            -- Generate the address to the ultraRAM weights memories.
            -- The first 256 addresses are mirrored in a BRAM so that writes of weights can happen.
            if (unsigned(weightsRdAddr) < 256)  then
            
                -- Convert signals from the register interface to address and write enables for the weights memories.
                --  weightWrAddrMod6 <= regWriteDataDel1(18 downto 16);
                --  weightWrAddrDiv6 <= regWriteDataDel1(30 downto 19);
                --  weightWrEn
                
                if weightWrAddrMod6 = "000" then
                    mainWeightWrEn(0) <= "0000" & weightWrEn & weightWrEn & weightWrEn & weightWrEn;
                    if (unsigned(weightWrAddrDiv6) < 256) then
                        shadowWeightWrEn(0) <= "0000" & weightWrEn & weightWrEn & weightWrEn & weightWrEn;
                    else
                        shadowWeightWrEn(0) <= "00000000";
                    end if;
                elsif weightWrAddrMod6 = "001" then
                    mainWeightWrEn(0) <= weightWrEn & weightWrEn & weightWrEn & weightWrEn & "0000";
                    if (unsigned(weightWrAddrDiv6) < 256) then
                        shadowWeightWrEn(0) <= weightWrEn & weightWrEn & weightWrEn & weightWrEn & "0000";
                    else
                        shadowWeightWrEn(0) <= "00000000";
                    end if;
                else
                    mainWeightWrEn(0) <= "00000000";
                    shadowWeightWrEn(0) <= "00000000";
                end if;
                
                if weightWrAddrMod6 = "010" then
                    mainWeightWrEn(1) <= "0000" & weightWrEn & weightWrEn & weightWrEn & weightWrEn;
                    if (unsigned(weightWrAddrDiv6) < 256) then
                        shadowWeightWrEn(1) <= "0000" & weightWrEn & weightWrEn & weightWrEn & weightWrEn;
                    else
                        shadowWeightWrEn(1) <= "00000000";
                    end if;
                elsif weightWrAddrMod6 = "011" then
                    mainWeightWrEn(1) <= weightWrEn & weightWrEn & weightWrEn & weightWrEn & "0000";
                    if (unsigned(weightWrAddrDiv6) < 256) then
                        shadowWeightWrEn(1) <= weightWrEn & weightWrEn & weightWrEn & weightWrEn & "0000";
                    else
                        shadowWeightWrEn(1) <= "00000000";
                    end if;
                else
                    mainWeightWrEn(1) <= "00000000";
                    shadowWeightWrEn(1) <= "00000000";
                end if;
                
                if weightWrAddrMod6 = "100" then
                    mainWeightWrEn(2) <= "0000" & weightWrEn & weightWrEn & weightWrEn & weightWrEn;
                    if (unsigned(weightWrAddrDiv6) < 256) then
                        shadowWeightWrEn(2) <= "0000" & weightWrEn & weightWrEn & weightWrEn & weightWrEn;
                    else
                        shadowWeightWrEn(2) <= "00000000";
                    end if;
                elsif weightWrAddrMod6 = "101" then
                    mainWeightWrEn(2) <= weightWrEn & weightWrEn & weightWrEn & weightWrEn & "0000";
                    if (unsigned(weightWrAddrDiv6) < 256) then
                        shadowWeightWrEn(2) <= weightWrEn & weightWrEn & weightWrEn & weightWrEn & "0000";
                    else
                        shadowWeightWrEn(2) <= "00000000";
                    end if;
                else
                    mainWeightWrEn(2) <= "00000000";
                    shadowWeightWrEn(2) <= "00000000";
                end if;
                
                weightsAddrFull1 <= weightWrAddrDiv6;
                weightsAddr1ultraRAM <= '0';  -- Don't use the weights that came out of the ultraRAM on port 1, use the shadow ones instead.
            else
                weightsAddrFull1 <= weightsRdBuffer & weightsRdAddr & '0';
                weightsAddr1ultraRAM <= '1';  -- indicates that weights read data should be taken from the ultraRAM.
                mainWeightWrEn(0) <= "00000000";
                mainWeightWrEn(1) <= "00000000";
                mainWeightWrEn(2) <= "00000000";
            end if;
            weightsRdAddrFull1Shadow <= weightsRdBuffer & weightsRdAddr(7 downto 0);  -- shadow BRAM only shadows the first 256 even addresses in each half of the ultraRAM.
            weightsAddrFull2 <= weightsRdBuffer & weightsRdAddr & '1';
            weightsDin <= regWriteDataDel1(35 downto 0) & regWriteDataDel1(35 downto 0);
            BFCmdDin <= regWriteDataDel1(38 downto 36);
            -- Pipeline Stage 2
            rdPortBufferDel2 <= rdPortBufferDel1;
            BFStartDel2 <= BFStartDel1;
            weightsAddr1ultraRAMdel2 <= weightsAddr1ultraRAM;
            
            -- Pipeline Stage 3
            rdPortBufferDel3 <= rdPortBufferDel2;
            BFStartDel3 <= BFStartDel2;
            weightsAddr1ultraRAMdel3 <= weightsAddr1ultraRAMdel2;
            
            -- Pipeline Stage 4
            rdPortBufferDel4 <= rdPortBufferDel3;
            weightsAddr1ultraRAMdel4 <= weightsAddr1ultraRAMdel3;
            
            -- Pipeline stage 5 - get weights and beamform command
            if weightsAddr1ultraRAMdel4 = '0' then  -- first 256 read come from the BRAM shadowing the ultraRAM, so that there is time to do writes to the ultraRAM.
                rdAddrPortData(0) <= rdPortBufferDel4 & weightsDout1Shadow(0)(35 downto 28);
                rdAddrPortData(1) <= rdPortBufferDel4 & weightsDout1Shadow(0)(71 downto 64);
                rdAddrPortData(4) <= rdPortBufferDel4 & weightsDout1Shadow(1)(35 downto 28);
                rdAddrPortData(5) <= rdPortBufferDel4 & weightsDout1Shadow(1)(71 downto 64);
                rdAddrPortData(8) <= rdPortBufferDel4 & weightsDout1Shadow(2)(35 downto 28);
                rdAddrPortData(9) <= rdPortBufferDel4 & weightsDout1Shadow(2)(71 downto 64);
                weightsDout1Del5 <= weightsDout1Shadow;
            else
                rdAddrPortData(0) <= rdPortBufferDel4 & weightsDout1(0)(35 downto 28);
                rdAddrPortData(1) <= rdPortBufferDel4 & weightsDout1(0)(71 downto 64);
                rdAddrPortData(4) <= rdPortBufferDel4 & weightsDout1(1)(35 downto 28);
                rdAddrPortData(5) <= rdPortBufferDel4 & weightsDout1(1)(71 downto 64);
                rdAddrPortData(8) <= rdPortBufferDel4 & weightsDout1(2)(35 downto 28);
                rdAddrPortData(9) <= rdPortBufferDel4 & weightsDout1(2)(71 downto 64);
                weightsDout1Del5 <= weightsDout1;
            end if;
            -- second port of the ultraRAM is always used.
            rdAddrPortData(2) <= rdPortBufferDel4 & weightsDout2(0)(35 downto 28);
            rdAddrPortData(3) <= rdPortBufferDel4 & weightsDout2(0)(71 downto 64);
            rdAddrPortData(6) <= rdPortBufferDel4 & weightsDout2(1)(35 downto 28);
            rdAddrPortData(7) <= rdPortBufferDel4 & weightsDout2(1)(71 downto 64);
            rdAddrPortData(10) <= rdPortBufferDel4 & weightsDout2(2)(35 downto 28);
            rdAddrPortData(11) <= rdPortBufferDel4 & weightsDout2(2)(71 downto 64);
            
            weightsDout2Del5 <= weightsDout2;
            rdPortBufferDel5 <= rdPortBufferDel4;
            BFCmdDel5 <= BFStartDel3 & BFCmdDout;  -- Yes, this should be BFStartDel3 (NOT del4), as this is used to reset the beam count before the first write of a beam to the beam memory.
            
            -- Pipeline Stage 6
            BFCmdDel6 <= BFCmdDel5;
            rdPortBufferDel6 <= rdPortBufferDel5;
            weightsDout1Del6 <= weightsDout1Del5;
            weightsDout2Del6 <= weightsDout2Del5;
            
            -- Pipeline Stage 7
            BFCmdDel7 <= BFCmdDel6;
            rdPortBufferDel7 <= rdPortBufferDel6;
            weightsDout1Del7 <= weightsDout1Del6;
            weightsDout2Del7 <= weightsDout2Del6;
            
            -- Pipeline Stage 8
            BFCmdDel8 <= BFCmdDel7;
            rdPortBufferDel8 <= rdPortBufferDel7;
            weightsDout1Del8 <= weightsDout1Del7;
            weightsDout2Del8 <= weightsDout2Del7;
            
            -- Pipeline Stage 9
            BFCmdDel9 <= BFCmdDel8;
            rdPortBufferDel9 <= rdPortBufferDel8;
            for i in 0 to 11 loop
                BFMultDinA(i)(11 downto 0) <= rdPortData(i)(11 downto 0);
                BFMultDinA(i)(27 downto 16) <= rdPortData(i)(23 downto 12);
            end loop;
            
            for i in 0 to 2 loop
                BFMultDinB(i*2 + 0)(13 downto 0)  <= weightsDout1Del8(i)(13 downto 0);
                BFMultDinB(i*2 + 0)(29 downto 16) <= weightsDout1Del8(i)(27 downto 14);
                BFMultDinB(i*2 + 1)(13 downto 0)  <= weightsDout1Del8(i)(49 downto 36);
                BFMultDinB(i*2 + 1)(29 downto 16) <= weightsDout1Del8(i)(63 downto 50);
            end loop;
            
            for i in 3 to 5 loop
                BFMultDinB(i*2 + 0)(13 downto 0)  <= weightsDout2Del8(i-3)(13 downto 0);
                BFMultDinB(i*2 + 0)(29 downto 16) <= weightsDout2Del8(i-3)(27 downto 14);
                BFMultDinB(i*2 + 1)(13 downto 0)  <= weightsDout2Del8(i-3)(49 downto 36);
                BFMultDinB(i*2 + 1)(29 downto 16) <= weightsDout2Del8(i-3)(63 downto 50);
            end loop;
            
            
            -- Pipeline Stage 10
            rdPortBufferDel10 <= rdPortBufferDel9;
            BFCmdDel10 <= BFCmdDel9;
            
            -- Pipeline Stage 11
            rdPortBufferDel11 <= rdPortBufferDel10;
            BFCmdDel11 <= BFCmdDel10;
            
            -- Pipeline Stage 12
            rdPortBufferDel12 <= rdPortBufferDel11;
            BFCmdDel12 <= BFCmdDel11;
            
            -- Pipeline Stage 13
            rdPortBufferDel13 <= rdPortBufferDel12;
            BFCmdDel13 <= BFCmdDel12;
            
            -- Pipeline Stage 14 - get outputs of the complex multipliers.
            BFCmdDel14 <= BFCmdDel13;
            rdPortBufferDel14 <= rdPortBufferDel13;
            for i in 0 to 11 loop -- map complex multipler output to 36 bit real and imaginary values.
                BFMultDoutReDel14(i) <= BFMultDout(i)(27) & BFMultDout(i)(27) & BFMultDout(i)(27) & BFMultDout(i)(27) & BFMultDout(i)(27) & BFMultDout(i)(27) & BFMultDout(i)(27) & BFMultDout(i)(27) & BFMultDout(i)(27 downto 0);
                BFMultDoutImDel14(i) <= BFMultDout(i)(58) & BFMultDout(i)(58) & BFMultDout(i)(58) & BFMultDout(i)(58) & BFMultDout(i)(58) & BFMultDout(i)(58) & BFMultDout(i)(58) & BFMultDout(i)(58) & BFMultDout(i)(58 downto 32);
            end loop;
            
            -- Pipeline Stage 15 - first stage of the adder tree
            BFCmdDel15 <= BFCmdDel14;
            rdPortBufferDel15 <= rdPortBufferDel14;
            for i in 0 to 5 loop
                BFSum1ReDel15(i) <= std_logic_vector(unsigned(BFMultDoutReDel14(2*i + 0)) + unsigned(BFMultDoutReDel14(2*i + 1)));
                BFSum1ImDel15(i) <= std_logic_vector(unsigned(BFMultDoutImDel14(2*i + 0)) + unsigned(BFMultDoutImDel14(2*i + 1)));
            end loop;
            
            -- Pipeline Stage 16
            BFCmdDel16 <= BFCmdDel15;
            rdPortBufferDel16 <= rdPortBufferDel15;
            for i in 0 to 2 loop
                BFSum2ReDel16(i) <= std_logic_vector(unsigned(BFSum1ReDel15(2*i+0)) + unsigned(BFSum1ReDel15(2*i+1)));
                BFSum2ImDel16(i) <= std_logic_vector(unsigned(BFSum1ImDel15(2*i+0)) + unsigned(BFSum1ImDel15(2*i+1)));
            end loop;
            
            -- Pipeline Stage 17
            BFCmdDel17 <= BFCmdDel16;
            rdPortBufferDel17 <= rdPortBufferDel16;
            BFSum3ReDel17(0) <= BFSum2ReDel16(0);
            BFSum3ImDel17(0) <= BFSum2ImDel16(0);
            BFSum3ReDel17(1) <= std_logic_vector(unsigned(BFSum2ReDel16(1)) + unsigned(BFSum2ReDel16(2)));
            BFSum3ImDel17(1) <= std_logic_vector(unsigned(BFSum2ImDel16(1)) + unsigned(BFSum2ImDel16(2)));
            
            -- Pipeline Stage 18
            BFCmdDel18 <= BFCmdDel17;
            rdPortBufferDel18 <= rdPortBufferDel17;
            BFSum4ReDel18 <= std_logic_vector(unsigned(BFSum3ReDel17(0)) + unsigned(BFSum3ReDel17(1)));
            BFSum4ImDel18 <= std_logic_vector(unsigned(BFSum3ImDel17(0)) + unsigned(BFSum3ImDel17(1)));
            
            -- Pipeline Stage 19
            BFCmdDel19 <= BFCmdDel18;
            rdPortBufferDel19 <= rdPortBufferDel18;
            if BFCmdDel18(0) = '1' then -- this is the start of the beam, just put in the sum of the multipliers
                beamReDel19 <= BFSum4ReDel18;
                beamImDel19 <= BFSum4ImDel18;
            else -- not the first clock for this beam, so add to the sum from the previous data.
                beamReDel19 <= std_logic_vector(unsigned(beamReDel19) + unsigned(BFSum4ReDel18));
                beamImDel19 <= std_logic_vector(unsigned(beamImDel19) + unsigned(BFSum4ImDel18));
            end if;
            
            -- Pipeline Stage 20 - write to the beam memory when a beam finishes.
            BFCmdDel20 <= BFCmdDel19;
            rdPortBufferDel20 <= rdPortBufferDel19;
            if (BFCmdDel19(3) = '1') then -- command bit 3 is start; this is the first cycle of the beam calculation.
                beamCount <= (others => '0');
            elsif BFCmdDel19(1) = '1' then -- cmd bit (1) is beam done, so increment the count of the current number of beams.
                beamCount <= std_logic_vector(unsigned(beamCount) + 1);
            end if;
            if BFCmdDel19(1) = '1' then  -- cmd bit (1) is beam done, so write to the beam memory.
                beamWrEn(0) <= '1';
            else
                beamWrEn(0) <= '0';
            end if;
            beamDin(15 downto 0) <= beamReDel19(35 downto 20); -- Completely un-thought through; need to work out which bits to select...
            beamDin(31 downto 16) <= beamImDel19(35 downto 20);
            beamWrAddr <= rdPortBufferDel19 & beamCount;
            
        end if;
    end process;
    

    BFMULT_gen : for i in 0 to 11 generate
        -- 4 cycle latency.
        BF_cmult : cmult12x14
        PORT MAP (
            aclk => i_clk,
            s_axis_a_tvalid => '1',
            s_axis_a_tdata => BFMultDinA(i),  -- data in, 32 bit; (11:0) real, (27:16) imaginary
            s_axis_b_tvalid => '1',
            s_axis_b_tdata => BFMultDinB(i),  -- data in, 32 bit; (13:0) real, (29:16) imaginary
            m_axis_dout_tvalid => open,
            m_axis_dout_tdata => BFMultDout(i) -- data out, (26:0) real, (58:32) imaginary
        );
        -- tie off unused inputs.
        BFMultDinA(i)(15 downto 12) <= "0000";
        BFMultDinA(i)(31 downto 28) <= "0000";
        BFMultDinB(i)(15 downto 14) <= "00";
        BFMultDinB(i)(31 downto 30) <= "00";
    end generate;
    
    -- Memories for storing the weights
    -- 3 ultraRAMs, each true dual port, so we can read two addresses each clock.
    -- UltraRAMS are partitioned into 2 halves.
    --  - address 0 to 2047 is used for one set of weights.
    --  - address 2048 to 4094 is used for the other set of weights.
    -- The memory is 72 bits wide, with
    --  bits(27:0) = 14+14bit complex weight
    --  bits(35:28) = 8 bit selection of the port.
    --  bits(63:36) = 14+14bit complex weight
    --  bits(71:64) = 8 bit selection of the port.
    --
    --  3 ultraRAMs supports up to 12288 weights, double buffered :
    --
    --  Address    Memory 0       Memory 1       Memory 2
    --    ...         ...            ...            ...
    --    2048       w1,w0          w3,w2          w5,w4        <-- Second weights buffer.
    --    2047   w12283,w12282  w12285,w12284  w12287,w12286
    --    ...         ...            ...            ...
    --     1         w7,w6          w9,w8         w11,w10
    --     0         w1,w0          w3,w2          w5,w4
    
    shadowWeightsWrAddr <= (weightsAddrFull1(11) & weightsAddrFull1(7 downto 0)); -- shadow memory is low 256 addresses for both buffers.
    
    GEN_W_MEM : for i in 0 to 2 generate
        E_WEIGHTS : xpm_memory_tdpram
        generic map (
            ADDR_WIDTH_A => 12,               -- DECIMAL
            ADDR_WIDTH_B => 12,               -- DECIMAL
            AUTO_SLEEP_TIME => 0,            -- DECIMAL
            BYTE_WRITE_WIDTH_A => 9,        -- DECIMAL
            BYTE_WRITE_WIDTH_B => 72,        -- DECIMAL
            --CASCADE_HEIGHT => 0,             -- DECIMAL
            CLOCKING_MODE => "common_clock", -- String
            ECC_MODE => "no_ecc",            -- String
            MEMORY_INIT_FILE => "none",      -- String
            MEMORY_INIT_PARAM => "0",        -- String
            MEMORY_OPTIMIZATION => "true",   -- String
            MEMORY_PRIMITIVE => "ultra",      -- String
            MEMORY_SIZE => 4096*72,          -- DECIMAL
            MESSAGE_CONTROL => 0,            -- DECIMAL
            READ_DATA_WIDTH_A => 72,         -- DECIMAL
            READ_DATA_WIDTH_B => 72,         -- DECIMAL
            READ_LATENCY_A => 3,             -- DECIMAL
            READ_LATENCY_B => 3,             -- DECIMAL
            READ_RESET_VALUE_A => "0",       -- String
            READ_RESET_VALUE_B => "0",       -- String
            RST_MODE_A => "SYNC",            -- String
            RST_MODE_B => "SYNC",            -- String
            --SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
            USE_MEM_INIT => 1,               -- DECIMAL
            WAKEUP_TIME => "disable_sleep",  -- String
            WRITE_DATA_WIDTH_A => 72,        -- DECIMAL
            WRITE_DATA_WIDTH_B => 72,        -- DECIMAL
            WRITE_MODE_A => "no_change",     -- String
            WRITE_MODE_B => "no_change"      -- String
        )
        port map (
            dbiterra => open,          -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port A.
            dbiterrb => open,          -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port A.
            douta => weightsDout1(i),  -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
            doutb => weightsDout2(i),  -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterra => open,          -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port A.
            sbiterrb => open,          -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
            addra => weightsAddrFull1, -- ADDR_WIDTH_A-bit input: Address for port A write and read operations.
            addrb => weightsAddrFull2, -- ADDR_WIDTH_B-bit input: Address for port B write and read operations.
            clka => i_clk,             -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
            clkb => i_clk,             -- 1-bit input: Clock signal for port B when parameter CLOCKING_MODE is "independent_clock". Unused when parameter CLOCKING_MODE is "common_clock".
            dina => weightsDin,        -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            dinb => (others => '0'),   -- WRITE_DATA_WIDTH_B-bit input: Data input for port B write operations.
            ena => '1',                -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when read or write operations are initiated. Pipelined internally.
            enb => '1',                -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read or write operations are initiated. Pipelined internally.
            injectdbiterra => '0',     -- 1-bit input: Controls double bit error injection on input data when ECC enabled (Error injection capability is not available in "decode_only" mode).
            injectdbiterrb => '0',     -- 1-bit input: Controls double bit error injection on input data when ECC enabled (Error injection capability is not available in "decode_only" mode).
            injectsbiterra => '0',     -- 1-bit input: Controls single bit error injection on input data when ECC enabled (Error injection capability is not available in "decode_only" mode).
            injectsbiterrb => '0',     -- 1-bit input: Controls single bit error injection on input data when ECC enabled (Error injection capability is not available in"decode_only" mode).
            regcea => '1',             -- 1-bit input: Clock Enable for the last register stage on the output data path.
            regceb => '1',             -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rsta => '0',               -- 1-bit input: Reset signal for the final port A output register stage. Synchronously resets output port douta to the value specified by parameter READ_RESET_VALUE_A.
            rstb => '0',               -- 1-bit input: Reset signal for the final port B output register stage. Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.
            sleep => '0',              -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => MainWeightWrEn(i),       -- WRITE_DATA_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
            web => (others => '0')     -- WRITE_DATA_WIDTH_B-bit input: Write enable vector for port B input data port dinb. 1 bit wide when word-wide writes are used.
        );
        
        -- BRAM used to shadow first 256 addresses in each buffer of each of the ultraRAMs, so that there is some time to use one of the ultraRAM memory ports to write new weights.
        URAM_SHADOW_BRAM : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 9,               -- DECIMAL  -- 512 deep
            ADDR_WIDTH_B => 9,               -- DECIMAL
            AUTO_SLEEP_TIME => 0,            -- DECIMAL
            BYTE_WRITE_WIDTH_A => 9,         -- DECIMAL
            --CASCADE_HEIGHT => 0,           -- DECIMAL
            CLOCKING_MODE => "common_clock", -- String
            ECC_MODE => "no_ecc",            -- String
            MEMORY_INIT_FILE => "none",      -- String
            MEMORY_INIT_PARAM => "0",        -- String
            MEMORY_OPTIMIZATION => "true",   -- String
            MEMORY_PRIMITIVE => "auto",      -- String
            MEMORY_SIZE => 512*72,           -- DECIMAL; total size in bits
            MESSAGE_CONTROL => 0,            -- DECIMAL
            READ_DATA_WIDTH_B => 72,         -- DECIMAL
            READ_LATENCY_B => 2,             -- DECIMAL
            READ_RESET_VALUE_B => "0",       -- String
            RST_MODE_A => "SYNC",            -- String
            RST_MODE_B => "SYNC",            -- String
            --SIM_ASSERT_CHK => 0,           -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
            USE_MEM_INIT => 0,               -- DECIMAL
            WAKEUP_TIME => "disable_sleep",  -- String
            WRITE_DATA_WIDTH_A => 72,        -- DECIMAL
            WRITE_MODE_B => "write_first"      -- String
        )
        port map (
            dbiterrb => open,                      -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port B.
            doutb    => weightsDout1Shadow(i),     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,                      -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
            addra    => shadowWeightsWrAddr,       -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb    => weightsRdAddrFull1Shadow,  -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka     => i_clk,                     -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
            clkb     => i_clk,                     -- 1-bit input, unused when clocking mode is "common_clock"
            dina     => weightsDin,                -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena      => '1',                       -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when write operations are initiated. Pipelined internally.
            enb      => '1',                       -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read operations are initiated. Pipelined internally.
            injectdbiterra => '0',                 -- 1-bit input: Controls double bit error injection on input data when ECC enabled 
            injectsbiterra => '0',                 -- 1-bit input: Controls single bit error injection on input data when ECC enabled
            regceb => '1',                         -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',                           -- 1-bit input: Reset signal for the final port B output register stage. Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.
            sleep => '0',                          -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => shadowWeightWrEn(i)                -- WRITE_DATA_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );
    
    end generate;
    
    
    -- 18K RAM used to store the commands (start of beam, end of beam, end of all beams)
    -- 2048x3, so 1024 for each weights buffer.
    BFCMD_BRAM : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 11,              -- DECIMAL  -- 2048 deep
        ADDR_WIDTH_B => 11,              -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 3,         -- DECIMAL
        --CASCADE_HEIGHT => 0,           -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "auto",      -- String
        MEMORY_SIZE => 2048*3,           -- DECIMAL; total size in bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 3,          -- DECIMAL
        READ_LATENCY_B => 3,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        --SIM_ASSERT_CHK => 0,           -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 3,         -- DECIMAL
        WRITE_MODE_B => "write_first"     -- String
    )
    port map (
        dbiterrb => open,         -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port B.
        doutb    => BFCmdDout,    -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,         -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
        addra    => BFCmdWrAddrDel1,  -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb    => BFCmdRdAddr,  -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka     => i_clk,        -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
        clkb     => i_clk,        -- 1-bit input, unused when clocking mode is "common_clock"
        dina     => BFCmdDinDel1,     -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena      => '1',          -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when write operations are initiated. Pipelined internally.
        enb      => '1',          -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read operations are initiated. Pipelined internally.
        injectdbiterra => '0',    -- 1-bit input: Controls double bit error injection on input data when ECC enabled 
        injectsbiterra => '0',    -- 1-bit input: Controls single bit error injection on input data when ECC enabled
        regceb => '1',            -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb   => '0',            -- 1-bit input: Reset signal for the final port B output register stage. Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.
        sleep  => '0',            -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea    => BFCmdWrEnDel1       -- WRITE_DATA_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );
    
    BFCmdRdAddr <= weightsAddrFull2(11 downto 1);
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            BFCmdWrAddrDel1 <= weightsAddrFull1(11 downto 1);
            BFCmdDinDel1 <= BFCmdDin;
            BFCmdWrEnDel1(0) <= mainWeightWrEn(0)(0) or mainWeightWrEn(0)(4);  -- writes to either the low or high 36 bits in the mainWeights memory also write to the command memory.
        end if;
    end process;
    
    ----------------------------------------------------------------
    -- Memories for storing the port data.
    -- Each memory is an 18K ram (i.e. half a BRAM)
    -- Each memory is configured as simple dual port, 512 deep x 24 bits.
    -- 13 memories to supply data to 12 simultaneous CMACs + calibration correlator
    E_PORTDATAMEM : for i in 0 to 12 generate

        PORT_DATA_BRAM_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 9,               -- DECIMAL  -- 512 deep
            ADDR_WIDTH_B => 9,               -- DECIMAL
            AUTO_SLEEP_TIME => 0,            -- DECIMAL
            BYTE_WRITE_WIDTH_A => 24,        -- DECIMAL
            --CASCADE_HEIGHT => 0,           -- DECIMAL
            CLOCKING_MODE => "common_clock", -- String
            ECC_MODE => "no_ecc",            -- String
            MEMORY_INIT_FILE => "none",      -- String
            MEMORY_INIT_PARAM => "0",        -- String
            MEMORY_OPTIMIZATION => "true",   -- String
            MEMORY_PRIMITIVE => "auto",      -- String
            MEMORY_SIZE => 512*24,           -- DECIMAL; total size in bits
            MESSAGE_CONTROL => 0,            -- DECIMAL
            READ_DATA_WIDTH_B => 24,         -- DECIMAL
            READ_LATENCY_B => 3,             -- DECIMAL
            READ_RESET_VALUE_B => "0",       -- String
            RST_MODE_A => "SYNC",            -- String
            RST_MODE_B => "SYNC",            -- String
            --SIM_ASSERT_CHK => 0,           -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
            USE_MEM_INIT => 0,               -- DECIMAL
            WAKEUP_TIME => "disable_sleep",  -- String
            WRITE_DATA_WIDTH_A => 24,        -- DECIMAL
            WRITE_MODE_B => "write_first"      -- String
        )
        port map (
            dbiterrb => open,              -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port B.
            doutb    => rdPortData(i),     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,              -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
            addra    => wrAddrPortData,    -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb    => rdAddrPortData(i), -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka     => i_clk,             -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
            clkb     => i_clk,             -- 1-bit input, unused when clocking mode is "common_clock"
            dina     => wrPortData,        -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena      => '1',               -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when write operations are initiated. Pipelined internally.
            enb      => '1',               -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read operations are initiated. Pipelined internally.
            injectdbiterra => '0',         -- 1-bit input: Controls double bit error injection on input data when ECC enabled 
            injectsbiterra => '0',         -- 1-bit input: Controls single bit error injection on input data when ECC enabled
            regceb => '1',                 -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',                   -- 1-bit input: Reset signal for the final port B output register stage. Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.
            sleep => '0',                  -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => wrEnPortData            -- WRITE_DATA_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );
        
    end generate;
    
    -- BRAM to hold the beams that have been calculated.
    -- 512x32;
    -- Each beam is 16+16 bit complex.
    -- Double Buffered, so space for 256 beams. 
    BEAM_BRAM_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 9,               -- DECIMAL  -- 512 deep
        ADDR_WIDTH_B => 9,               -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 32,        -- DECIMAL
        --CASCADE_HEIGHT => 0,           -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "auto",      -- String
        MEMORY_SIZE => 512*32,           -- DECIMAL; total size in bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 32,         -- DECIMAL
        READ_LATENCY_B => 3,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        --SIM_ASSERT_CHK => 0,           -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 32,        -- DECIMAL
        WRITE_MODE_B => "write_first"      -- String
    )
    port map (
        dbiterrb => open,        -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port B.
        doutb    => beamDout,    -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,        -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
        addra    => beamWrAddr,  -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb    => beamRdAddr,  -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka     => i_clk,       -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
        clkb     => i_clk,       -- 1-bit input, unused when clocking mode is "common_clock"
        dina     => BeamDin,     -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena      => '1',         -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when write operations are initiated. Pipelined internally.
        enb      => '1',         -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read operations are initiated. Pipelined internally.
        injectdbiterra => '0',   -- 1-bit input: Controls double bit error injection on input data when ECC enabled 
        injectsbiterra => '0',   -- 1-bit input: Controls single bit error injection on input data when ECC enabled
        regceb => '1',           -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb   => '0',           -- 1-bit input: Reset signal for the final port B output register stage. Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.
        sleep  => '0',           -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea    => BeamWrEn       -- WRITE_DATA_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );


    -----------------------------------------------------------------------------
    -- BEAM DATA output
    -----------------------------------------------------------------------------
    -- One beam output per clock.
    -- So e.g. for 72 dual-pol beams, there are 144 outputs cycles.
    -- Data for all beams is generated either
    --  - When i_update occurs, if g_BEAM_ORDER = 0
    --  - After g_BEAM_ORDER packets have gone past otherwise.
   --i_beamData0     : in std_logic_vector(31 downto 0); -- 16+16 bit complex beam data.
   --i_beamValid0    : in std_logic;
   --o_beamData0     : out std_logic_vector(31 downto 0);
   --o_beamValid0    : out std_logic;
    -- Data is put on this bus if g_BEAM_OUTPUT = 1.
   --i_beamData1     : in std_logic_vector(31 downto 0); -- 16+16 bit complex beam data.
   --i_beamValid1    : in std_logic;
   --o_beamData1     : out std_logic_vector(31 downto 0);
   --o_beamValid1    : out std_logic;
    
    
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            -- Just pipeline the output that we are not using
            if g_BEAM_OUTPUT = 1 then
                -- Not using bus0; just pass it through.
                beamDataOtherDel1 <= i_beamData0; 
                beamValidOtherDel1 <= i_beamValid0;
                o_beamData0 <= beamDataOtherDel1;
                o_beamValid0 <= beamValidOtherDel1;
                -- input pipeline for the bus that we are using.
                beamDataDel1 <= i_beamData1;
                beamValidDel1 <= i_beamValid1;
            else
                -- Not using bus1; just pass it through.
                beamDataOtherDel1 <= i_beamData1;
                beamValidOtherDel1 <= i_beamValid1;
                o_beamData1 <= beamDataOtherDel1;
                o_beamValid1 <= beamValidOtherDel1;
                -- input pipeline for the bus that we are using.
                beamDataDel1 <= i_beamData0;
                beamValidDel1 <= i_beamValid0;
            end if;
            beamValidDel2 <= beamValidDel1;
            
            if (BFCmdDel19(3) = '1')  then -- this is the first cycle of the beam calculation, after the pipeline delay for the CMACs. So beamCount will be the total number of beams written in the previous frame.
                -- Number of beams packets on i_beamData to wait for before putting out this modules beam data.
                beamsRemaining <= std_logic_vector(to_unsigned(g_BEAM_ORDER,4));
                beamOut_fsm <= wait_to_send;
                totalBeams <= beamCount;
                beamRdBuffer <= rdPortBufferDel20;  -- the buffer the previous time frame was written to.
            else
                case beamOut_fsm is
                    when wait_to_send =>
                        if beamsRemaining = "0000" then
                            beamOut_fsm <= send_data;
                        elsif (beamValidDel1 = '1' and beamValidDel2 = '0') then
                            beamsRemaining <= std_logic_vector(unsigned(beamsRemaining) - 1);
                        end if;
                        beamRdCount <= (others => '0');
                    when send_data =>
                        if (unsigned(beamRdCount) < unsigned(totalBeams)) then
                            beamRdCount <= std_logic_vector(unsigned(beamRdCount) + 1);
                        else
                            beamOut_fsm <= send_done;
                        end if;
                        
                    when send_done =>
                        beamOut_fsm <= send_done; -- stay here until start of the next time frame signalled by BFCmdDel19(3).
                        
                    when others =>
                        beamOut_fsm <= send_done;
                end case;
            end if;
            
            beamRdAddr <= beamRdBuffer & beamRdCount;
            if beamOut_fsm = send_data then
                localBeamValid <= '1';
            else
                localBeamValid <= '0';
            end if;
            -- pipeline to account for memory latency
            localBeamValidDel1 <= localBeamValid;
            localBeamValidDel2 <= localBeamValidDel1;
            localBeamValidDel3 <= localBeamValidDel2; -- 3 cycle read latency
            
            if beamOut_fsm = wait_to_send then
                -- upstream modules are sending data.
                if g_BEAM_OUTPUT = 0 then
                    o_beamData0 <= beamDataDel1;
                    o_beamValid0 <= beamValidDel1;
                else
                    o_beamData1 <= beamDataDel1;
                    o_beamValid1 <= beamValidDel1;
                end if;
            else
                -- we are sending data.
                if g_BEAM_OUTPUT = 0 then
                    o_beamData0 <= BeamDout;
                    o_beamValid0 <= localBeamValidDel3;
                else
                    o_beamData1 <= BeamDout;
                    o_beamValid1 <= localBeamValidDel3;
                end if;
            end if;
            
        end if;
    end process;
    
    
    ---------------------------------------------------------------------
    -- calibration correlator
    ---------------------------------------------------------------------
    
    CALi : entity work.calCorrelator
    port map(
        i_clk => i_clk,
        -- interface to the memory with the port data
        o_portAddr => calPortAddr,    -- out(7:0);
        i_portData => rdPortData(12), -- in(23:0); -- 3 cycle read latency.
        -- The ports to correlate against
        i_port0 => calPort0, -- in(7:0);
        i_port1 => calPort1, -- in(7:0);
        -- start correlation
        i_start => calStart, --  in std_logic;  -- new data available
        i_reset => calReset, --  in std_logic;  -- reset the correlation, sampled on i_start;
        -- Read out the correlation result
        i_calDump  => calDump, -- in std_logic;  -- trigger dump of the correlation; Read on i_start; if 0, then normal correlation processing, otherwise dump the correlation. Generates a packet 4x224 = 896 clocks long.
        o_corData  => corData, -- out(35:0);
        o_corValid => corValid -- out std_logic
    );

    -- BRAM 12 holds the port data for the calibration correlator
    rdAddrPortData(12) <= calBuffer & calPortAddr;

    process(i_clk)
    begin
        if rising_edge(i_clk) then
            -- integrationTime
            -- calStartDel1
            -- portStartDel1
            if (unsigned(calCount) = 0) then  -- pipeline checking of whether calCount is zero.
                calCount0 <= '1';
            else
                calCount0 <= '0';
            end if;
            
            if portStartDel2 = '1' then
                calBuffer <= rdPortBuffer;
            end if;
            
            if calReadout = '1' then
                calStart <= '1';
                calDump <= '1';
            elsif portStartDel1 = '1' then
                if calStartDel1 = '1' then
                    calReset <= '1';
                    calStart <= '1';
                    calCount <= integrationTime;
                elsif (calCount0 = '0') then  -- i.e. not zero.
                    calStart <= '1';
                    calReset <= '0';
                    calCount <= std_logic_vector(unsigned(calCount) - 1);
                else
                    calStart <= '0';
                    calReset <= '0';
                end if;
                calDump <= '0';
            else
                calStart <= '0';
                calReset <= '0';
                calDump <= '0';
            end if;

            regReadDataDel1 <= i_regReadData;
            regReadValidDel1 <= i_regReadValid;
            
            if corValid = '1' then  -- Note the onus is on the external controller to ensure there are no clashes on this bus.
                regReadDataDel2 <= corData;
                regReadValidDel2 <= '1';
            else
                regReadDataDel2 <= regReadDataDel1;
                regReadValidDel2 <= regReadValidDel1;
            end if;
            
        end if;
    end process;
    
    o_regReadData <= regReadDataDel2;
    o_regReadValid <= regReadValidDel2;
    
    
end Behavioral;
