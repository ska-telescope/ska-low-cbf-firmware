----------------------------------------------------------------------------------
-- Company: CSIRO 
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 04.12.2020 13:29:24
-- Module Name: PSTBFScaling - Behavioral
-- Description: 
--   i_data is 44 bits.
--   This is the absolute sum of the components of a PST packet, as 32 bit numbers.
--   In total a PST packet has 3072 numbers = (24 fine channels) * (32 times) * (2 pol) * (2 complex)
--   For simplicity, rather than calculate the standard deviation we calculate the absolute sum.
--   For normally distributed data, the absolute sum is about 80% of the standard deviation.
--
--   The target value for the output packets is 360.
--   So the scale factor we need to apply is roughly
--     Scale = 360/((sum(x)/3072)*5/4)
--           = 884736/sum(x)
--
--   This modules does two things:
--    (1) Find the shift required to make sum(x) be in the range 2^19 -> 2^20 (i.e. 524288 to 1048576)
--          - Values of sum(x) in the range 0 to 2^20 return a shift of 0.
--               - This implies using the low 16 bits in the original data. 
--          - The shift goes up to a maximum of 15.
--               - This should be sufficient for even the most pathological input data to the beamformer.
--    (2) Lookup the inverse based on the top few bits of sum(x)
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity PSTBFScaling is
    port(
        i_clk : in std_logic;
        i_data : in std_logic_vector(43 downto 0);
        i_valid : in std_logic;
        --
        o_shift : out std_logic_vector(3 downto 0);  -- unsigned shift; "0000" means use the low 16 bits, "0001" shift down by 1, etc. 
        o_mantissa : out std_logic_vector(3 downto 0);
        o_valid : out std_logic  -- 3 clock latency
    );
end PSTBFScaling;

architecture Behavioral of PSTBFScaling is

    signal shifted1 : std_logic_vector(7 downto 0);
    signal shifted2 : std_logic_vector(3 downto 0);
    signal shift1 : std_logic_vector(5 downto 0);
    signal shift2 : std_logic_vector(5 downto 0);
    signal validDel1, validDel2 : std_logic;

begin

    process(i_clk)
    begin
        if rising_edge(i_clk) then
            -- first pipeline stage
            if    i_data(43 downto 20) = "000000000000000000000000" then
                shifted1 <= i_data(19 downto 12);  -- only need 8 bits, since we only use this to determine an approximate inverse.
                shift1 <= "000000";
            elsif i_data(43 downto 24) = "00000000000000000000" then
                shifted1 <= i_data(23 downto 16);
                shift1 <= "000100";
            elsif i_data(43 downto 28) = "0000000000000000" then
                shifted1 <= i_data(27 downto 20);
                shift1 <= "001000";
            elsif i_data(43 downto 32) = "000000000000" then
                shifted1 <= i_data(31 downto 24);
                shift1 <= "001100";
            elsif i_data(43 downto 36) = "00000000" then
                shifted1 <= i_data(35 downto 28);
                shift1 <= "010000";
            elsif i_data(43 downto 40) = "0000" then
                shifted1 <= i_data(39 downto 32);
                shift1 <= "010100";
            else
                shifted1 <= i_data(43 downto 36);
                shift1 <= "011000";
            end if;
            validDel1 <= i_valid;
            
            -- Second pipeline stage for the shift
            if shifted1(7 downto 4) = "0000" then
                -- too small; just mark shift2 as negative, so the next pipeline stage picks up mantisssa = "1111", shift = "0000";
                shift2 <= "111111";
                shifted2 <= (others => '0');
            elsif shifted1(7 downto 4) = "0001" then
                shifted2 <= shifted1(4 downto 1);
                shift2 <= std_logic_vector(unsigned(shift1) - 3);
            elsif shifted1(7 downto 5) = "001" then
                shifted2 <= shifted1(5 downto 2);
                shift2 <= std_logic_vector(unsigned(shift1) - 2);
            elsif shifted1(7 downto 6) = "01" then
                shifted2 <= shifted1(6 downto 3);
                shift2 <= std_logic_vector(unsigned(shift1) - 1);
            else
                shifted2 <= shifted1(7 downto 4);
                shift2 <= shift1;
            end if;
            validDel2 <= validDel1;
            
            -- Third pipeline stage
            -- The mantissa is given by round(8*(884736/x))
            -- where x = (0:15) * 2^16, i.e. 0, 65536, 131072, 196608, 262144, 327680, 393216, 458
            if shift2(5) = '1' then -- shift is negative, but we don't support negative shifts, so just choose the maximum multiplier.
                o_mantissa <= "1111";
                o_shift <= "0000";
            else
                case shifted2 is
                    when "0000" => o_mantissa <= "1111";  -- round(8*(884736/0)) = inf -> saturate to 15 = 1+7/8.
                    when "0001" => o_mantissa <= "1111";  -- round(8*(884736/65536)) = 108 -> saturate to 15 = 1+7/8.
                    when "0010" => o_mantissa <= "1111";  -- round(8*(884736/131072)) = 54 -> saturate to 15 = 1+7/8.
                    when "0011" => o_mantissa <= "1111";  -- round(8*(884736/196608)) = 36 -> saturate to 15 = 1+7/8.
                    when "0100" => o_mantissa <= "1111";  -- round(8*(884736/262144)) = 27 -> saturate to 15 = 1+7/8.
                    when "0101" => o_mantissa <= "1111";  -- round(8*(884736/327680)) = 22 -> saturate to 15 = 1+7/8.
                    when "0110" => o_mantissa <= "1111";  -- round(8*(884736/393216)) = 18 -> saturate to 15 = 1+7/8.
                    when "0111" => o_mantissa <= "1111";  -- round(8*(884736/458752)) = 15 = 1+7/8
                    when "1000" => o_mantissa <= "1110";  -- round(8*(884736/524288)) = 14 = 1+3/4
                    when "1001" => o_mantissa <= "1100";  -- round(8*(884736/589824)) = 12 = 1+1/2
                    when "1010" => o_mantissa <= "1011";  -- round(8*(884736/655360)) = 11 = 1+3/8
                    when "1011" => o_mantissa <= "1010";  -- round(8*(884736/720896)) = 10 = 1+1/4
                    when "1100" => o_mantissa <= "1001";  -- round(8*(884736/786432)) = 9  = 1+1/8
                    when "1101" => o_mantissa <= "1000";  -- round(8*(884736/851968)) = 8  = 1
                    when "1110" => o_mantissa <= "1000";  -- round(8*(884736/917504)) = 8  = 1
                    when others => o_mantissa <= "0111";  -- round(8*(884736/983040)) = 7  = 7/8
                end case;
                if (unsigned(shift2) > 15) then
                    o_shift <= "1111";
                else
                    o_shift <= shift2(3 downto 0);
                end if;
            end if;
            o_valid <= validDel2;
        end if;
    end process;

end Behavioral;
