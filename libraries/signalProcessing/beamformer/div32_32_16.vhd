----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 30.11.2020 21:12:59
-- Module Name: div32_32_16 - Behavioral
-- Description: 
--   divide two 32-bit numbers, to produce a 16 bit result, where the numerator 
-- is less than the denominator.
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity div32_32_16 is
    port(
        i_clk : in std_logic;
        -- data input; numerator should be less than or equal to denominator
        i_numerator : in std_logic_vector(31 downto 0);  -- unsigned input, but top bit should be zero.
        i_denominator : in std_logic_vector(31 downto 0); -- unsigned input, but top bit should be zero.
        i_valid : in std_logic;  -- Can take new input data every 16 clocks.
        -- Data out
        o_quotient : out std_logic_vector(15 downto 0); -- output is an unsigned value.
        o_valid : out std_logic  -- will go high 17 clocks after i_valid
    );
end div32_32_16;

architecture Behavioral of div32_32_16 is

    signal remainder, denominator : std_logic_vector(31 downto 0);
    signal remainder_minus_denominator : std_logic_vector(31 downto 0);
    signal count : std_logic_vector(3 downto 0);
    signal running : std_logic := '0';
    signal quotient : std_logic_vector(15 downto 0);
    signal remainder_gte_denominator : std_logic;
    
begin
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            if i_valid = '1' then
                remainder <= i_numerator;
                denominator <= i_denominator;
                count <= "1111";
                running <= '1';
            elsif count = "0000" then
                running <= '0';
            end if;
            
            if running = '1' and i_valid = '0' then
                if remainder_gte_denominator = '1' then
                    remainder <= remainder_minus_denominator(30 downto 0) & '0';
                    quotient(0) <= '1';
                else
                    remainder <= remainder(30 downto 0) & '0';
                    quotient(0) <= '0';
                end if;
                quotient(15 downto 1) <= quotient(14 downto 0);
                count <= std_logic_vector(unsigned(count) - 1);
            end if;
            
            if running = '1' and count = "0000" then
                o_valid <= '1';
                if remainder_gte_denominator = '1' then
                    o_quotient(0) <= '1';
                else
                    o_quotient(0) <= '0';
                end if;
                o_quotient(15 downto 1) <= quotient(14 downto 0);
            else
                o_valid <= '0';
            end if;
            
        end if;
    end process;
    
    remainder_minus_denominator <= std_logic_vector(unsigned(remainder) - unsigned(denominator));
    remainder_gte_denominator <= '1' when (unsigned(remainder) >= unsigned(denominator)) else '0';
    
    
end Behavioral;
