----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 08.11.2020 09:04:05
-- Module Name: PSTbeamformer - Behavioral
-- Description: 
--
----------------------------------------------------------------------------------
library IEEE, bf_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;

entity PSTbeamTrigger_dp is
    generic(
        g_BEAM_NUMBER : integer;
        -- Number of clock cycles to wait after a packet is ready to send before actually sending it on the BFData interface.
        -- This is to ensure there are no clashes on the BFdata bus between different beamformers.
        -- Up to 20 bits. It takes about 776 clocks to send a packet, so a 20 bit value enables sharing between up to 1048575/776 = 1351 beamformer instances. 
        g_PACKET_OFFSET : natural range 0 to 1048575
    );
    Port(
        i_BF_clk : in std_logic; -- 400MHz clock
        ---------------------------------------------------------------------------
        -- configuration
        -- Number of beams enabled; This beamformer will output data packets if g_BEAM_NUMBER < i_beamsEnabled
        i_beamsEnabled : in std_logic_vector(7 downto 0); 
        -- pulse high to indicate data is available to read out
        i_readStart : in std_logic;
        -- Sequence of 9 pulses to trigger reading of a frame
        o_frameSend : out std_logic;
        o_frameCount : out std_logic_vector(3 downto 0)
    );
end PSTbeamTrigger_dp;

architecture Behavioral of PSTbeamTrigger_dp is

    signal beamsEnabled_x512, beamsEnabled_x256, beamsEnabled_x32, packetGap : std_logic_vector(15 downto 0);
    signal beamsEnabled_x768, beamsEnabled_x32_plus100, packetOffset : std_logic_vector(15 downto 0);
    signal waitCount : std_logic_vector(19 downto 0) := (others => '0');
    signal frameCount, frameCountSend : std_logic_vector(3 downto 0) := (others => '0');
    signal output_enabled : std_logic;
    signal running : std_logic := '0';
    signal frameSend : std_logic := '0';
    
begin
    
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            
            
            -- Timing of transmission of the output packets
            -- ----------------------------------------
            --
            -- Output packets are 6192 bytes in length.
            --  = (24 fine channels) x (32 time sampels) x (2 pol) x (2+2 bytes) + (24 fine channels)x(2 bytes weights) 
            --  = 6144 + 48 = 6192 bytes.
            -- The output bus is 8-bytes wide, so it takes 6192/8 = 774 clocks to output a packet.
            -- We allow 800 clocks between packets to have some margin for pipeline delays.
            -- 
            -- The beamformers take turns to output packets, so if there are N beams enabled, the 
            -- overall output order is :
            --   beam0, fine 0-23
            --   beam1, fine 0-23
            --   ...
            --   beam N-1, fine 0-23
            --   beam 0, fine 24-47
            --   etc.
            --
            -- So this beamformer instance needs to:
            --  (1) Wait g_PACKET_OFFSET = g_BEAM_NUMBER*800 clocks
            --  (2) Output a packet (fine channels 0-23)
            --  (3) Wait beamsEnabled * 800 clocks
            --  (4) Output a packet (fine chanenels 24-47)
            --  (5) Repeat (3) and (4) until all 9 packets have been sent.
            --
            -- So we want :
            --  secondPacketOffset <= 800 * beamsEnabled + 100
            -- Note 800 = 512 + 256 + 32
          
            if unsigned(i_beamsEnabled) > g_BEAM_NUMBER then
                output_enabled <= '1';
            else
                output_enabled <= '0';
            end if;
          
            if i_readStart = '1' and output_enabled = '1' then
                waitCount <= std_logic_vector(to_unsigned(g_PACKET_OFFSET,20));
                frameCount <= (others => '0');
                running <= '1';
                frameSend <= '0';
            elsif running = '1' then
                if unsigned(waitCount) = 0 then
                    frameSend <= '1';
                    frameCountSend <= frameCount;
                    if frameCount = "1000" then
                        frameCount <= (others => '0');
                        waitCount <= (others => '0');
                        running <= '0';
                    else
                        frameCount <= std_logic_vector(unsigned(frameCount) + 1);
                        waitCount <= "0000" & packetGap;
                    end if;
                else
                    frameSend <= '0';
                    waitCount <= std_logic_vector(unsigned(waitCount) - 1);
                end if;
            else
                frameSend <= '0';
            end if;
            
            beamsEnabled_x768 <= std_logic_vector(unsigned(beamsEnabled_x512) + unsigned(beamsEnabled_x256));
            beamsEnabled_x32_plus100 <= std_logic_vector(unsigned(beamsEnabled_x32) + 100);
            packetGap <= std_logic_vector(unsigned(beamsEnabled_x768) + unsigned(beamsEnabled_x32_plus100));
            
        end if;
    end process;
    
    beamsEnabled_x512 <= i_beamsEnabled(6 downto 0) & "000000000";  -- Support a maximum of 127 beams.
    beamsEnabled_x256 <= '0' & i_beamsEnabled(6 downto 0) & "00000000";
    beamsEnabled_x32  <=  "0000" & i_beamsEnabled(6 downto 0) & "00000";
    
    o_frameCount <= frameCountSend;
    o_frameSend <= frameSend;
    
end Behavioral;

