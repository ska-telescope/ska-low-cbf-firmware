%% Get the ROM contents for the FIR filter coefficients
%PSTFilterbankTaps = round(2^17 * generate_MaxFlt(256,12));  % PST, 256 point FFT, 12 taps

% File "Prototype_FIR.new.4-3.256.3072.mat" provided by Pulsar Timing group,
% selected to be suitable for the reconstruction algorithm in the GPU.
f_new = load('Prototype_FIR.new.4-3.256.3072.mat');
f_new = f_new.h;
f_new_scaled = f_new * 2^24/sum(f_new);


%% Plot coeficients
figure(1);
clf;
hold on;
grid on;
plot(f_new_scaled(1:3072),'r.-');
title('PST FIR Taps');


%% Plot the frequency response
nTap = 12;
% Upsample by 32x => 3072 * 32 = 98304 points.
W2ca2 = zeros(1,98304);
F_base = (-49152:49151) * (1/98304) * (1/1080e-9);
W2ca2(1:3072) = round(f_new_scaled(1:3072))/(2^24);
freq_response = fft(W2ca2);
fr2 = circshift(freq_response,nTap*32);
fr3 = circshift(freq_response,-nTap*32);
fr4 = circshift(freq_response,2*nTap*32);
fr5 = circshift(freq_response,-2*nTap*32);
figure(2);
clf;
hold on;
grid on;
plot(F_base,fftshift(10*log10(abs(freq_response).^2)),'r.-');
plot(F_base,fftshift(10*log10(abs(fr2).^2)),'g.-');
plot(F_base,fftshift(10*log10(abs(fr3).^2)),'b.-');
plot(F_base,fftshift(10*log10(abs(fr4).^2)),'c.-');
plot(F_base,fftshift(10*log10(abs(fr5).^2)),'m.-');
%plot(F_base,fftshift(10*log10(abs(freq_response).^2 + abs(fr2).^2 + abs(fr3).^2 + abs(fr4).^2 + abs(fr5).^2)),'b.-');
title('PST filterbank freq response');
ylabel('dB')
xlabel('Frequency (Hz)')

figure(3);
clf;
hold on;
grid on;
plot(F_base,fftshift(10*log10(abs(freq_response).^2)),'r.-');
title('PST filterbank freq response');
ylabel('dB')
xlabel('Frequency (Hz)')


write_files = 1;
if write_files 
    filtertaps = round(f_new_scaled);
    
    %% Save as a text file
    save('PST_filtertaps.txt','filtertaps','-ascii');
    
    
    %% Save PST FIR taps - .coe files
    for rom = 1:12
        fid = fopen(['PSTFIRTaps' num2str(rom) '.coe'],'w');
        fprintf(fid,'memory_initialization_radix = 2;\n');
        fprintf(fid,'memory_initialization_vector = ');
        % First half of the memory.
        for rline = 1:256
            dstr = dec2binX(filtertaps((rom-1)*256 + (rline-1) + 1),18);
            fprintf(fid,['\n' dstr]);
        end
        % Another copy for the other half of the memory (double buffered).
        for rline = 1:256
            dstr = dec2binX(filtertaps((rom-1)*256 + (rline-1) + 1),18);
            fprintf(fid,['\n' dstr]);
        end    
        fprintf(fid,';\n');
        fclose(fid);
    end

    %% mem file for loading in xpm macros (in place of bram IP that uses .coe) 
    for rom = 1:12
        % Write initialisation file for xpm memory (used in versal)
        fid = fopen(['PSTFIRTaps' num2str(rom) '.mem'],'w');
        for rline = 1:256
            dval = filtertaps((rom-1)*256 + (rline-1) + 1);
            if (dval < 0)
                dval = dval + 2^20; % 5 hex digits, 20 bit value, low 18 bits actually used in the memory.
            end
            dstr = dec2hex(dval,5); % 5 hex digits
            fprintf(fid,[dstr '\n']);
        end
        fclose(fid);
    end
    
end