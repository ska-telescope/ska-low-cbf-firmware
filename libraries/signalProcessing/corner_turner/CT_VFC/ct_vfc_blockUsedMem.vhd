----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 20.03.2020 23:22:30
-- Module Name: ct_vfc_blockUsedMem - Behavioral
-- Description: 
--  Wrapper for the block used mem.
--  This memory holds pointers into the used list, recording where in the used list the readout is up to for each channel.
--  
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
Library xpm;
use xpm.vcomponents.all;

entity ct_vfc_blockUsedMem is
    Port(
        i_clk : in std_logic;
        -- memory writes.
        i_wrAddr : in std_logic_vector(8 downto 0);
        i_wrData0 : in std_logic_vector(11 downto 0); -- data to write 0.
        i_wrData1 : in std_logic_vector(11 downto 0); -- data to write 1.
        i_wrEn0 : in std_logic;
        i_wrEn1 : in std_logic;
        -- memory reads
        i_rdAddr : in std_logic_vector(8 downto 0);  -- Reads both pointers at the same time.
        o_rdData0 : out std_logic_Vector(11 downto 0); -- 2 cycle latency.
        o_rdData1 : out std_logic_vector(11 downto 0)
    );
end ct_vfc_blockUsedMem;

architecture Behavioral of ct_vfc_blockUsedMem is

    signal wrEn : std_logic_vector(3 downto 0);
    signal dout : std_logic_vector(31 downto 0);
    signal din : std_logic_vector(31 downto 0);

begin
    
    din <= "0000" & i_wrData1 & "0000" & i_wrData0; -- padding to byte align the data so we can use byte write enable. 
    wrEn(0) <= i_wrEn0;
    wrEn(1) <= i_wrEn0;
    wrEn(2) <= i_wrEn1;
    wrEn(3) <= i_wrEn1;
    
    o_rdData0 <= dout(11 downto 0);
    o_rdData1 <= dout(27 downto 16);
    
    bram_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 9,    -- DECIMAL  -- 72 bit wide by 8192 or 16384 deep
        ADDR_WIDTH_B => 9,    -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 8,         -- DECIMAL
        --CASCADE_HEIGHT => 0,           -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "true",   -- String
        MEMORY_PRIMITIVE => "auto",      -- String
        MEMORY_SIZE => 512*24,           -- DECIMAL; total size in bits
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 32,         -- DECIMAL
        READ_LATENCY_B => 2,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        --SIM_ASSERT_CHK => 0,           -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 32,        -- DECIMAL
        WRITE_MODE_B => "read_first"      -- String
    )
    port map (
        dbiterrb => open,           -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port B.
        doutb    => Dout,   -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        sbiterrb => open,           -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
        addra    => i_wrAddr, -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        addrb    => i_rdAddr, -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        clka     => i_clk,          -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
        clkb     => i_clk,          -- 1-bit input, unused when clocking mode is "common_clock"
        dina     => Din,    -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
        ena      => '1',            -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when write operations are initiated. Pipelined internally.
        enb      => '1',            -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read operations are initiated. Pipelined internally.
        injectdbiterra => '0',      -- 1-bit input: Controls double bit error injection on input data when ECC enabled 
        injectsbiterra => '0',      -- 1-bit input: Controls single bit error injection on input data when ECC enabled
        regceb => '1',              -- 1-bit input: Clock Enable for the last register stage on the output data path.
        rstb => '0',                -- 1-bit input: Reset signal for the final port B output register stage. Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.
        sleep => '0',               -- 1-bit input: sleep signal to enable the dynamic power saving feature.
        wea => wrEn         -- WRITE_DATA_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
    );

end Behavioral;
