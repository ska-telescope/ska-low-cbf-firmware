----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au) & Norbert Abel
-- 
-- Create Date: 13.03.2020 09:47:34
-- Module Name: CT_VFC_top - Behavioral
-- Description: 
--  Corner turn for the Perentie VFC (very fine channeliser) 
--  The corner turn takes data for all channels for some number of stations, buffers the data, 
--  and outputs data in bursts for each channel. Burst length is programmable via MACE.
-- 
-- INPUT DATA [x1]:
-- for time = ... (forever)
--    for coarse_group = 1:384/8-1  (order of the coarse groups may vary)
--       for coarse = 1:8
--          for time = 1:2:2048
--             [[ts0, pol0], [ts0, pol1], [ts1, pol0], [ts1, pol1]]
--
-- OUTPUT DATA:
-- Output <BURST LENGTH> and <PRELOAD LENGTH> is configurable via MACE.
-- for coarse = <order defined by a table, programmed via MACE>
--    for time = 1:(<BURST LENGTH> + <PRELOAD LENGTH>)x4096
--       if station_group == 1: [station0, pol0], [station0, pol1]
--
----------------------------------------------------------------------------------
-- Structure
-- ---------
-- This is the top level of the corner turn; it contains :
--  + a timing module,
--  + Registers which apply to all corner turns
--  + A separate corner turn module for each station (up to 16 stations supported)
--
----------------------------------------------------------------------------------
-- Timing
--  There are two modes for timing of output data :
--   (1) Timed output
--        Output frames start based on the clock
--   (2) Non-timed output
--        Output frames start when we get an incoming packet with a count past some threshold.
--
----------------------------------------------------------------------------------
-- Default Numbers:
--  LFAA time samples = 1080 ns
--  LFAA bandwidth/coarse channel = 1/1080ns = 925.925 KHz
--  LFAA input blocks = 2048 time samples = 2.21184 ms
--  
--  Correlator output:
--   136 LFAA blocks = 136*2.2ms = 300.81024 ms
--   Preload 22 LFAA blocks.
--   384 channels.
--   Total LFAA blocks input per second = (384 channels) * (1 sample/1080 ns) / (2048 samples/ LFAA packet) = 173611.11 LFAA blocks/second
--   Total LFAA blocks output per second = (384 channels) * (136 + 22) / 300.81024ms = 201695.2614379 LFAA blocks/second (Note blocks are dual-pol)
--   Used clock cycles on the output bus (dual-pol per cycle) = 201695.2614379 LFAA Blocks/second * 2048 samples/block = 413.07 Mcycles/second
--   Output packets/second = 201695.2614379/2 = 100847.63  (Note 4096 samples in output packets vs. 2048 samples in input packets).
--   For Correlator Filterbanks running at 450 MHz.
--     - Packets/second = 100847.63   (note : 4096 time samples/packet)
--     - clocks/second = 450,000,000
--     - Clocks/packet = 4462 clocks (note actual packet length is 4096 clocks)
--
--  PSS/PST output:
--   Output is in 64 sample blocks
--   24 LFAA blocks = 24 * 2.2ms = 53.08416 ms
--   24 LFAA blocks = 24 * (2048/64) = 768 output blocks
--   Preload samples = 256 * 11 = 44 * (64 sample output blocks)
--   64 sample output blocks per second = (384 channels) * (768+44) / 53.08416 ms  = 5,873,842.59 
--   Used clock cycles on the output bus (dual-pol per cycle) = 5873842.59 * 64 = 375,925,925.925
--   The PST filterbank is 4/3 oversampled, so it needs at least (4/3) * 64 clocks per output block = 86 clocks.
--   So Used clock cycles on the output bus including padding for oversampling = 5873842.59 * 86 = 505150463 clocks/second.
--   So for PSS/PST filterbanks running at 510 MHz
--     - Packets/second = 5873842  (note 64 time samples/packet)
--     - Clocks/second = 510000000
--     - Clocks/packet = 86
--
----------------------------------------------------------------------------------

library IEEE, ct_vfc_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library DSP_top_lib;
use DSP_top_lib.DSP_top_pkg.all;
USE ct_vfc_lib.ct_vfc_config_reg_pkg.ALL;
USE common_lib.common_pkg.ALL;

Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;

--library UNISIM;
--use UNISIM.VComponents.all;

entity CT_VFC_top is
    generic(
        -- How much memory to allow for.
        -- True : 2 Gbytes have been allocated to the corner turn.
        -- False : 1 Gbyte is allocated to the corner turn.
        -- This defines the amount of space allocated for pointers.
        -- The actual amount of memory used and the base address of the memory is defined via MACE. 
        g_USE_2GBYTE : boolean := false;
        -- Width of the input data bus in multiples of 128 bits, Should be either 1 or 2 (for 256 bit wide input buses).
        -- Expected width for data coming from the 40GE interface is 128. 100GE interfaces will generate 256 bit wide data.
        --   - 1 : 128 bit wide interface; Assumed to have 2 stations per input bus.
        --   - 2 : 256 bit wide interface; Assumed to have 4 stations per input bus.
        g_N_128BIT_SEGMENTS : integer := 1;
        -- Number of stations to support. Each station has it's own memory block and output.
        --  - For 128 bit input buses, this should be a multiple of 2 (since there are 2 stations per input bus)
        --  - For 256 bit input buses, this should be a multiple of 4 (since there are 4 stations per input bus)
        g_N_STATIONS : integer := 8
    );
    Port(
        -- Processing & HBM clock
        i_hbm_clk         : in std_logic;  -- AXI clock: for ES and -1 devices: <=400MHz, for PS of -2 and higher: <=450MHz (HBM core and most of the CTC run in this clock domain)  
        i_hbm_clk_rst     : in std_logic;
        --MACE:
        i_mace_clk        : in  std_logic;  -- clock connected to MACE
        i_mace_clk_rst    : in  std_logic;  -- this is the only incoming reset - all other resets are created internally in the config module
        i_saxi_mosi       : in  t_axi4_lite_mosi; -- MACE IN
        o_saxi_miso       : out t_axi4_lite_miso; -- MACE OUT
        --wall time:
        i_input_clk_wall_time   : in t_wall_time; --wall time in input_clk domain           
        i_output_clk_wall_time  : in t_wall_time; --wall time in output_clk domain           
        --ingress (in input_clk):
        -- For a 128 bit wide interface, packets will be 513 clocks (1 header word + 512 data words).
        -- For a 256 bit wide interface, packets will be 257 clocks (1 header word + 256 data words).
        i_input_clk   : in std_logic;                      -- clock domain for the ingress.
        i_input_clk_rst : in std_logic;                    -- reset for the input clock domain.
        i_data        : in t_slv_256_arr((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0);    -- Incoming data streams; If g_N_128BIT_SEGMENTS = 1, then only the low 128 bits of each bus are used.
        i_dataValid   : in std_logic_vector((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0); -- is the current data cycle valid
        ------------------------------------------------------------------------------------
        -- Data output, to go to the filterbanks.
        -- Types are defined in DSP_top_pkg.vhd :
        --    t_ctc_output_header_arr = array of records, with
        --        .timestamp(31:0), .coarse_delay(15:0), .virtual_channel(15:0), .station_id(15:0), .hpol_phase_shift(15:0), .vpol_phase_shift(15:0)  
        --    t_ctc_output_payload_arr is array(integer range <>) of t_ctc_output_payload, which has fields
        --        .hpol.re, .hpol.im, .vpol.re, .vpol.im 
        -- Readout for the correlator filterbank
        i_cor_clk          : in  std_logic;                                         -- correlator filterbank output clock domain
        o_cor_sof          : out std_logic_vector(g_N_STATIONS-1 downto 0);         -- single cycle pulse: this cycle is the first of the frame (e.g. first in 0.3 sec frame)
        o_cor_header       : out t_ctc_output_header_arr(g_N_STATIONS-1 downto 0);  -- meta data belonging to the data coming out
        o_cor_header_valid : out std_logic_vector(g_N_STATIONS-1 downto 0);         -- new meta data (every output packet, aka 4096 cycles) 
        o_cor_data         : out t_ctc_output_payload_arr(g_N_STATIONS-1 downto 0); -- The actual output data, complex data for 2 polarisations.
        o_cor_data_valid   : out std_logic_vector(g_N_STATIONS-1 downto 0);         -- High for the duration of the packet (4096 clocks for correlator data)
        o_cor_flagged      : out std_logic_vector(g_N_STATIONS-1 downto 0);         -- is this 2048 cycle half of the packet valid or RFI? (RFI = missing input packet)
        -- Readout for PSS & PST filterbanks
        i_PSSPST_clk          : in  std_logic;                                         -- PSS/PST filterbank output clock domain
        o_PSSPST_sof          : out std_logic_vector(g_N_STATIONS-1 downto 0);         -- single cycle pulse: this cycle is the first of the frame (e.g. first in 0.3 sec frame)
        o_PSSPST_header       : out t_ctc_output_header_arr(g_N_STATIONS-1 downto 0);  -- meta data belonging to the data coming out
        o_PSSPST_header_valid : out std_logic_vector(g_N_STATIONS-1 downto 0);         -- new meta data (every output packet, aka 4096 cycles) 
        o_PSSPST_data         : out t_ctc_output_payload_arr(g_N_STATIONS-1 downto 0); -- The actual output data, complex data for 2 polarisations. (fields are .hpol.re, .hpol.im, .vpol.re, .vpol.im)
        o_PSSPST_data_valid   : out std_logic_vector(g_N_STATIONS-1 downto 0);         -- High for the duration of the packet (4096 clocks for correlator data)
        o_PSSPST_flagged      : out std_logic_vector(g_N_STATIONS-1 downto 0);         -- is this packet valid or RFI? (RFI = missing input packet)        
        --------------------------------------------------------------------------------- 
        -- HBM INTERFACE
        o_hbm_clk_rst : out std_logic;          -- reset going to the HBM core
        o_hbm_mosi    : out t_axi4_full_mosi_arr(g_N_STATIONS-1 downto 0);   -- data going to the HBM core
        o_hbm_ecc     : out t_slv_32_arr(g_N_STATIONS-1 downto 0);
        i_hbm_miso    : in  t_axi4_full_miso_arr(g_N_STATIONS-1 downto 0);   -- data coming from the HBM core
        i_hbm_ecc     : in  t_slv_32_arr(g_N_STATIONS-1 downto 0);
        i_hbm_ready   : in  std_logic           -- HBM reset finished? (=apb_complete)        
    );
end CT_VFC_top;

architecture Behavioral of CT_VFC_top is
    
    -- Bus to communicate HBM addresses to the input buffer (ct_vfc_input_buffer) from the memory allocation module (ct_vfc_malloc)
    signal writePacketCount : std_logic_vector(31 downto 0);  -- Packet count from the packet header
    signal writeChannel : std_logic_vector(15 downto 0);  -- virtual channel from the packet header
    signal writePacketCountValid : std_logic;                      -- Goes high to indicate o_packet_count is valid, and stays high until a response comes back
    signal writeAddress : std_logic_vector(23 downto 0); -- Address to write this packet to, in units of 8192 bytes.
    signal writeOK : std_logic;                     -- Write address is valid; if low, then the packet should be dropped as it is either too early or too late.
    signal writeAddressValid : std_logic;
    
    signal dataValidDel1, dataValidDel2, dataValidDel3, dataValidDel4 : std_logic_vector((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0); -- one bit per input bus
    signal dataValidDel5, pktValidDel5 : t_slv_4_arr((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0);  -- Broken into individual valid signals for each station.
    signal headerValidDel2, headerValidDel3, headerValidDel4 : std_logic_vector((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0);
    signal dataValidToggle : std_logic_vector((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0); -- one bit per input bus
    signal pktValidDel1, pktValidDel2, pktValidDel3, pktValidDel4 : std_logic_vector((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0); -- one bit per input bus
    signal dataInDel1      : t_slv_256_arr((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0);    -- 256 bit wide data bus
    signal stationSelectedDel3, stationSelectedDel4, stationSelectedDel5 : t_slv_4_arr((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0);
    signal dataInDel2, dataInDel3, dataInDel4, dataInDel5 : t_slv_256_arr((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0); 
    type hdr_array is array((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0) of t_ctc_input_header;
    signal headerInDel2 : hdr_array;
    signal dataSum0Del2, dataSum1Del2, dataSum2Del2, dataSum3Del2 : t_slv_32_arr((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0);
    signal dataSum0Del3, dataSum1Del3 : t_slv_32_arr((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0);
    signal dataSum0Del4, dataSumDel5 : t_slv_32_arr((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0);
    signal wordCount : t_slv_2_arr((g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) downto 0);
    
    -- register interface
    signal global_config_fields_rw : t_config_rw;
    type config_fields_rw_v is array(0 to g_N_STATIONS-1) of t_config_rw;
    signal global_config_fields_rw_del : config_fields_rw_v;
    signal freeMax : std_logic_vector(23 downto 0);
    signal global_config_fields_ro : t_config_ro;
    signal station_config_fields_rw : t_ct_station_config_rw;
    signal station_config_fields_ro : t_ct_station_config_ro;
    signal station_config_table_0_in : t_ct_station_config_table_0_ram_in;
    signal station_config_table_0_out : t_ct_station_config_table_0_ram_out;
    signal station_config_table_1_in : t_ct_station_config_table_1_ram_in;
    signal station_config_table_1_out : t_ct_station_config_table_1_ram_out;
    signal correlator_order_in : t_ct_station_config_correlator_order_ram_in;
    signal correlator_order_out : t_ct_station_config_correlator_order_ram_out;
    signal PSSPST_order_in : t_ct_station_config_psspst_order_ram_in;
    signal PSSPST_order_out : t_ct_station_config_psspst_order_ram_out;
    signal monitoring_fields_rw : t_ct_monitoring_rw;
    signal monitoring_fields_ro : t_ct_monitoring_ro;
    signal correlator_output_count_in : t_ct_monitoring_correlator_output_count_ram_in;
    signal correlator_output_count_out : t_ct_monitoring_correlator_output_count_ram_out;
    signal PSSPST_output_count_in : t_ct_monitoring_psspst_output_count_ram_in;
    signal PSSPST_output_count_out : t_ct_monitoring_psspst_output_count_ram_out;
    signal hbm_ready : std_logic;
    signal fullReset, fullResetDel1, fullResetDel2, fullResetDel3, fullResetDel4, fullResetDel5, fullResetDel6 : std_logic;
    signal useNewConfig, useNewConfigDel1, useNewConfigDel2, loadNewConfig : std_logic := '0';
    
begin
    
    ------------------------------------------------------------------------------------
    -- CONFIG (TO/FROM MACE)
    ------------------------------------------------------------------------------------
    -- + create internal resets
    -- + connect config & error signals to MACE 
    -- Note : This relies on the "number_of_slaves" field being set correctly in ct_vfc.peripheral.yaml; it should match g_N_STATIONs
    -- 
    ------------------------------------------------------------------------------------
    E_TOP_CONFIG : entity ct_vfc_lib.ct_vfc_config_reg
    port map (
        MM_CLK              => i_mace_clk, -- in std_logic;
        MM_RST              => i_mace_clk_rst, -- in std_logic;
        st_clk_ct_station_config => (others => i_hbm_clk),       -- in(0:7);
        st_rst_ct_station_config => (others => i_hbm_clk_rst), -- in(0:7);
        st_clk_ct_monitoring => (others => i_hbm_clk),           -- in(0:7);
        st_rst_ct_monitoring => (others => i_hbm_clk_rst),     -- in(0:7);
        SLA_IN               => i_saxi_mosi,  -- IN    t_axi4_lite_mosi;
        SLA_OUT              => o_saxi_miso,  -- OUT   t_axi4_lite_miso;
        -- Config common to all stations
        CONFIG_FIELDS_RW     => global_config_fields_rw, -- OUT t_config_rw;
        CONFIG_FIELDS_RO     => global_config_fields_ro, -- IN  t_config_ro;
        --------------------------------------------------------------------------------------------
        -- per station config registers
        -- general config - .packet_count, .table_select, .active_table : These set up which delay table to use (table_0 or table_1)
        CT_STATION_CONFIG_FIELDS_RW  => station_config_fields_rw,    -- OUT t_ct_station_config_rw;
        CT_STATION_CONFIG_FIELDS_RO  => station_config_fields_ro,    -- IN  t_ct_station_config_ro;
        -- coarse delay table, 512x32 bits for each station
		CT_STATION_CONFIG_TABLE_0_IN  => station_config_table_0_in,  -- IN  t_ct_station_config_table_0_ram_in;
		CT_STATION_CONFIG_TABLE_0_OUT => station_config_table_0_out, -- OUT t_ct_station_config_table_0_ram_out;
		CT_STATION_CONFIG_TABLE_1_IN  => station_config_table_1_in,	 -- IN  t_ct_station_config_table_1_ram_in;
		CT_STATION_CONFIG_TABLE_1_OUT => station_config_table_1_out, -- OUT t_ct_station_config_table_1_ram_out;
		-- correlator virtual channel output order, 512 x 16bits 
		CT_STATION_CONFIG_CORRELATOR_ORDER_IN  => correlator_order_in,  -- IN  t_ct_station_config_correlator_order_ram_in;
		CT_STATION_CONFIG_CORRELATOR_ORDER_OUT => correlator_order_out, -- OUT t_ct_station_config_correlator_order_ram_out;
		-- PSS/PST virtual channel output order, 512 x 16 bits
		CT_STATION_CONFIG_PSSPST_ORDER_IN      => PSSPST_order_in,        -- IN  t_ct_station_config_psspst_order_ram_in;
		CT_STATION_CONFIG_PSSPST_ORDER_OUT     => PSSPST_order_out,       -- OUT t_ct_station_config_psspst_order_ram_out;
		-- Monitoring; _rw is just reset signal to reset the monitoring registers
        CT_MONITORING_FIELDS_RW	               => monitoring_fields_rw,   -- OUT t_ct_monitoring_rw;
        -- Monitoring; free_size, free_min, input_late_count, input_too_soon_count, duplicates, correlator_missin_block, PSSPST_missing_blocks, ... 
        CT_MONITORING_FIELDS_RO                => monitoring_fields_ro,   -- IN  t_ct_monitoring_ro;
        -- 512x32 RAM,number of correlator output packets sent for each virtual channel. 
		CT_MONITORING_CORRELATOR_OUTPUT_COUNT_IN => correlator_output_count_in,     -- IN  t_ct_monitoring_correlator_output_count_ram_in;
		CT_MONITORING_CORRELATOR_OUTPUT_COUNT_OUT => correlator_output_count_out,   -- OUT t_ct_monitoring_correlator_output_count_ram_out;
		-- 512x32 RAM, number of PSSPST output packets sent for each virtual channel.
		CT_MONITORING_PSSPST_OUTPUT_COUNT_IN => PSSPST_output_count_in,   -- IN  t_ct_monitoring_psspst_output_count_ram_in;
		CT_MONITORING_PSSPST_OUTPUT_COUNT_OUT => PSSPST_output_count_out  -- OUT t_ct_monitoring_psspst_output_count_ram_out
    );
    
    -- To Do : deal with
--    TYPE t_config_rw is RECORD
--        halt_packet_count			: std_logic_vector(31 downto 0);
        
--        starting_wall_time_seconds	: std_logic_vector(31 downto 0);
--        starting_wall_time_nanos	: std_logic_vector(31 downto 0);
        
--        control_enable_timed_output	: std_logic;
    
    
    -- Miscellaneous register settings and tasks
    process(i_hbm_clk)
    begin
        if rising_edge(i_hbm_clk) then
            
            -- Reset everything
            fullReset <= global_config_fields_rw.full_reset;
            fullResetDel1 <= fullReset;
            fullResetDel2 <= fullResetDel1;
            fullResetDel3 <= fullResetDel2;
            fullResetDel4 <= fullResetDel3;
            fullResetDel5 <= fullResetDel4;
            fullResetDel6 <= fullResetDel5;
            o_hbm_clk_rst <= fullResetDel1;
            if fullResetDel1 = '1' or fullResetDel2 = '1' or fullResetDel3 = '1' or fullResetDel4 = '1' or fullResetDel5 = '1' or fullResetDel6 = '1' then
                hbm_ready <= '0';
            else
                hbm_ready <= i_hbm_ready;
            end if;
            
            -- Ensure freeMax is less than or equal to baseAddressStep, so stations don't overwrite each other.
            if (unsigned(global_config_fields_rw.freeMax) > unsigned(global_config_fields_rw.baseAddressStep)) then
                freeMax <= global_config_fields_rw.baseAddressStep;
            else
                freeMax <= global_config_fields_rw.freeMax;
            end if;
            
            -- Copy config info used in all instances of CT_VFC_Single so we have a pipeline register for timing.
            useNewConfig <= global_config_fields_rw.control_use_new_config;
            useNewConfigDel1 <= useNewConfig;
            useNewConfigDel2 <= useNewConfigDel1;
            
            if (useNewConfigDel1 = '1' and useNewConfigDel2 = '0') or (fullResetDel1 = '0' and fullResetDel2 = '1') then -- rising edge of useNewConfig or falling edge of fullReset.
                loadNewConfig <= '1';
            else
                loadNewConfig <= '0';
            end if;
            
            if loadNewConfig = '1' then
                for i in 0 to (g_N_STATIONS-1) loop
                    global_config_fields_rw_del(i) <= global_config_fields_rw;
                    global_config_fields_rw_del(i).freeMax <= freeMax;
                end loop;
                -- Overwrite the base address with individual base addresses for each station.
                for i in 1 to (g_N_STATIONS - 1) loop
                    global_config_fields_rw_del(i).baseAddress <= std_logic_vector(unsigned(global_config_fields_rw_del(i-1).baseAddress) + unsigned(global_config_fields_rw_del(i).baseAddressStep));
                end loop;
            end if;
            
        end if;
    end process;
    
    
    ------------------------------------------------------------------------------------
    -- Input processing 
    --  + Convert 128 bit input stream to 256 bit wide input
    --  + Generate the checksum for each input bus
    --  + Generate packetValid and dataValid signals
    --    - These are separate for each station; have to examine the header to determine which station this data is from.
    ------------------------------------------------------------------------------------    
    CHECKSUM_GEN : for i in 0 to (g_N_STATIONS/(2*g_N_128BIT_SEGMENTS) - 1) generate  -- i.e. one instance of the following code per input bus
        
        
        process(i_input_clk)
        begin
            if rising_edge(i_input_clk) then
                
                -- Generate packet valid and data valid signals, with a 256 bit wide bus
                PktValidDel1(i) <= i_dataValid(i);
                if (g_N_128BIT_SEGMENTS = 1) then
                    -- Convert 128 bit input bus to a 256 bit wide bus.
                    -- note : incoming packets are 513 clocks (1 header word + 512 data words).
                    -- After conversion to 256 bits, there will be 1 header word (with the top 128 bits unused), then a data word on every second cycle. 
                    if i_dataValid(i) = '0' then
                        dataValidToggle(i) <= '0';
                    else
                        dataValidToggle(i) <= not dataValidToggle(i);  -- dataValidToggle will be '0' for the first cycle of i_dataValid, then flips 0,1,0,1, etc.
                    end if;
                    
                    if (i_dataValid(i) = '1' and pktValidDel1(i) = '0') or dataValidToggle(i) = '1' then
                        dataInDel1(i)(127 downto 0) <= i_data(i)(127 downto 0);
                    else
                        dataInDel1(i)(255 downto 128) <= i_data(i)(127 downto 0);
                    end if;
                    
                    if dataValidToggle(i) = '0' and i_dataValid(i) = '1' then
                        dataValidDel1(i) <= '1';
                    else
                        dataValidDel1(i) <= '0';
                    end if;
                    
                else
                    -- Already a 256 bit wide bus.
                    dataValidDel1(i) <= i_dataValid(i);
                    dataInDel1(i) <= i_data(i);
                end if;
                
                
                -- Calculate a simple 32 bit checksum on each group of 4x(256 bit words).
                pktValidDel2(i) <= pktValidDel1(i);
                if pktValidDel2(i) = '0' and pktValidDel1(i) = '1' then
                    -- The header is in dataInDel1; get the station ID
                    headerInDel2(i) <= slv_to_header(dataInDel1(i)(127 downto 0));
                    headerValidDel2(i) <= '1';
                else
                    headerValidDel2(i) <= '0';
                end if;
                
                dataSum0Del2(i) <= std_logic_vector(unsigned(dataInDel1(i)(31 downto 0)) + unsigned(dataInDel1(i)(63 downto 32)));
                dataSum1Del2(i) <= std_logic_vector(unsigned(dataInDel1(i)(95 downto 64)) + unsigned(dataInDel1(i)(127 downto 96)));
                dataSum2Del2(i) <= std_logic_vector(unsigned(dataInDel1(i)(159 downto 128)) + unsigned(dataInDel1(i)(191 downto 160)));
                dataSum3Del2(i) <= std_logic_vector(unsigned(dataInDel1(i)(223 downto 192)) + unsigned(dataInDel1(i)(255 downto 224)));
                dataInDel2(i) <= dataInDel1(i);
                dataValidDel2(i) <= dataValidDel1(i);
                
                -- Pipeline stage for sum
                dataSum0Del3(i) <= std_logic_vector(unsigned(dataSum0Del2(i)) + unsigned(dataSum1Del2(i)));
                dataSum1Del3(i) <= std_logic_vector(unsigned(dataSum2Del2(i)) + unsigned(dataSum3Del2(i)));
                dataInDel3(i) <= dataInDel2(i);
                dataValidDel3(i) <= dataValidDel2(i);
                pktValidDel3(i) <= pktValidDel2(i);
                headerValidDel3(i) <= headerValidDel2(i);
                stationSelectedDel3(i) <= headerInDel2(i).station_selected(3 downto 0);
                
                -- Final pipeline stage for sum
                dataSum0Del4(i) <= std_logic_vector(unsigned(dataSum0Del3(i)) + unsigned(dataSum1Del3(i)));
                dataInDel4(i) <= dataInDel3(i);
                dataValidDel4(i) <= dataValidDel3(i);
                pktValidDel4(i) <= pktValidDel3(i);
                stationSelectedDel4(i) <= stationSelectedDel3(i);
                headerValidDel4(i) <= headerValidDel3(i);
                
                -- Accumulate sum, reset every 4 clocks.
                if headerValidDel4(i) = '1' then
                    wordCount(i) <= "00";
                elsif dataValidDel4(i) = '1' then
                    wordCount(i) <= std_logic_vector(unsigned(wordCount(i)) + 1);
                end if;
                if dataValidDel4(i) = '1' then
                    if wordCount(i) = "00" then
                        dataSumDel5(i) <= dataSum0Del4(i);
                    else
                        dataSumDel5(i) <= std_logic_vector(unsigned(dataSum0Del4(i)) + unsigned(dataSumDel5(i)));
                    end if;
                end if;
                dataInDel5(i) <= dataInDel4(i);
                -- decode dataValid and pktValid into up to 4 different signals, depending on which station the data is from, 
                -- since the CT_VFC_Single instances need valid signals for the particular station.
                if stationSelectedDel4(i) = "0000" then
                    dataValidDel5(i) <= "000" & dataValidDel4(i);
                    pktValidDel5(i) <= "000" & pktValidDel4(i);
                elsif stationSelectedDel4(i) = "0001" then
                    dataValidDel5(i) <= "00" & dataValidDel4(i) & '0';
                    pktValidDel5(i) <= "00" & pktValidDel4(i) & '0';
                elsif stationSelectedDel4(i) = "0010" then
                    dataValidDel5(i) <= '0' & dataValidDel4(i) & "00";
                    pktValidDel5(i) <= '0' & pktValidDel4(i) & "00";
                elsif stationSelectedDel4(i) = "0011" then
                    dataValidDel5(i) <= dataValidDel4(i) & "000";
                    pktValidDel5(i) <= pktValidDel4(i) & "000";
                else  -- More that 4 stations on one link; should be impossible.
                    dataValidDel5(i) <= "0000";
                    pktValidDel5(i) <= "0000";
                end if;
                stationSelectedDel5(i) <= stationSelectedDel4(i);
                
            end if;
        end process;
    
    end generate;
    
    ------------------------------------------------------------------------------------
    -- Corner turn for each each station
    ------------------------------------------------------------------------------------
    
    CT_GEN : for i in 0 to (g_N_STATIONS-1) generate
    
        -- indexing of dataIn etc assumes 2 stations for 128bit interface and 4 stations on a 256 bit interface.
        E_SINGLE : entity ct_vfc_lib.CT_VFC_Single
        generic map (
            -- How much memory to allow for.
            -- True : 2 Gbytes have been allocated to the corner turn.
            -- False : 1 Gbyte is allocated to the corner turn.
            -- This defines the amount of space allocated for pointers.
            -- The actual amount of memory used and the base address of the memory is defined via MACE. 
            g_USE_2GBYTE => g_USE_2GBYTE --  boolean := false;
        ) port map (
            -- Processing & HBM clock
            i_hbm_clk       => i_hbm_clk, -- AXI clock: for ES and -1 devices: <=400MHz, for PS of -2 and higher: <=450MHz (HBM core and most of the CTC run in this clock domain)  
            i_hbm_clk_rst   => i_hbm_clk_rst,
            ----------------------------------------------------------------------------------
            --ingress (in input_clk):
            i_input_clk     => i_input_clk,     -- in std_logic; clock domain for the ingress
            i_input_clk_rst => i_input_clk_rst, -- in std_logic;
            -- g_N_128BIT_SEGMENTS determines how many stations there are on each input bus, and thus which bus to send into this corner turn module (4 stations for 256 bit wide bus, 2 stations for 128 bit wide bus).
            i_data          => dataInDel5(i/(2*g_N_128BIT_SEGMENTS)),    -- in(255:0);    Incoming data stream (header, data)
            i_checksum      => dataSumDel5(i/(2*g_N_128BIT_SEGMENTS)),   -- in(31:0);     Checksum every 4 words for the packet; used to check for HBM errors.
            i_pktValid      => pktValidDel5(i/(2*g_N_128BIT_SEGMENTS))(i - (2*g_N_128BIT_SEGMENTS) * (i/(2*g_N_128BIT_SEGMENTS))),  -- in std_logic; Packet valid; goes high for a continuous block.
            i_dataValid     => dataValidDel5(i/(2*g_N_128BIT_SEGMENTS))(i - (2*g_N_128BIT_SEGMENTS) * (i/(2*g_N_128BIT_SEGMENTS))), -- in std_logic; The current data cycle is valid. Will be high every second clock when the input is on a 128 bit wide bus.
            ----------------------------------------------------------------------------------
            -- Readout for the correlator filterbank
            i_cor_clk          => i_cor_clk,             -- in  std_logic;            clock domain for the egress
            o_cor_sof          => o_cor_sof(i),          -- out std_logic;            single cycle pulse: this cycle is the first of the frame (e.g. first in 0.3 sec frame)
            o_cor_header       => o_cor_header(i),       -- out t_ctc_output_header;  meta data belonging to the data coming out
            o_cor_header_valid => o_cor_header_valid(i), -- out std_logic;            new meta data (every output packet, aka 4096 cycles) 
            o_cor_data         => o_cor_data(i),         -- out t_ctc_output_payload; The actual output data, complex data for 2 polarisations.
            o_cor_data_valid   => o_cor_data_valid(i),   -- out std_logic;            High for the duration of the packet (4096 clocks for correlator data)
            o_cor_flagged      => o_cor_flagged(i),      -- out std_logic;            is this 2048 cycle half of the packet valid or RFI? (RFI = missing input packet)
            -- Readout for PSS & PST filterbanks
            i_PSSPST_clk          => i_PSSPST_clk,             -- in  std_logic;            Clock domain for the egress
            o_PSSPST_sof          => o_PSSPST_sof(i),          -- out std_logic;            Single cycle pulse: this cycle is the first of the frame (e.g. first in 0.3 sec frame)
            o_PSSPST_header       => o_PSSPST_header(i),       -- out t_ctc_output_header;  Meta data belonging to the data coming out
            o_PSSPST_header_valid => o_PSSPST_header_valid(i), -- out std_logic;            New meta data (every output packet, aka 4096 cycles) 
            o_PSSPST_data         => o_PSSPST_data(i),         -- out t_ctc_output_payload; The actual output data, complex data for 2 polarisations. (fields are .hpol.re, .hpol.im, .vpol.re, .vpol.im)
            o_PSSPST_data_valid   => o_PSSPST_data_valid(i),   -- out std_logic;            High for the duration of the packet (4096 clocks for correlator data)
            o_PSSPST_flagged      => o_PSSPST_flagged(i),      -- out std_logic;            Is this 2048 cycle half of the packet valid or RFI? (RFI = missing input packet)
            --HBM INTERFACE
            o_hbm_mosi    => o_hbm_mosi(i), -- out t_axi4_full_mosi; Data going to the HBM core
            o_wdata_extra => o_hbm_ecc(i),  -- out(31:0); extra ECC bits
            i_hbm_miso    => i_hbm_miso(i), -- in  t_axi4_full_miso; Data coming from the HBM core
            i_rdata_extra => i_hbm_ecc(i),  -- in(31:0); extra ECC bits
            i_hbm_ready   => hbm_ready,     -- in  std_logic;        HBM reset finished? (=apb_complete)
            -- registers
            i_rst_status  => monitoring_fields_rw.reset(i),
            i_switch_packet_count => station_config_fields_rw.packet_count(i), -- in(31:0);  -- Which packet count to switch delay tables at.
            i_table_select        => station_config_fields_rw.table_select(i), -- in std_logic; -- table to use after i_switch_packet_count table.
            i_correlator_totalChannels => station_config_fields_rw.correlator_totalChannels(i), -- in(8:0) -- Total number of correlator channels to output.
            i_PSSPST_totalChannels => station_config_fields_rw.PSSPST_totalChannels(i), -- in (8:0) -- Total number of PSS/PST channels to output.
            o_table_used          => station_config_fields_ro.active_table(i), -- out std_logic; -- Current delay table in use.
            -- general configuration from (or derived from) global config registers
            i_freeMax              => global_config_fields_rw_del(i).freeMax,              -- in(15:0); maximum number of 64K blocks in the memory pool.
            i_baseAddress          => global_config_fields_rw_del(i).baseAddress,          -- in(23:0); Base address in memory of the memory pool.
            i_maxFutureCount       => global_config_fields_rw_del(i).maxfuturecount,       -- in(15:0); Maximum time in the future the packet count can be without dropping the packet.
            i_scanStartPacketCount => global_config_fields_rw_del(i).scanstartpacketcount, -- in(31:0); first packet count in the scan
            i_blocksPerFrame0      => global_config_fields_rw_del(i).blocksperframe0,      -- in(10:0); Number of LFAA blocks (block = 2048 samples) per frame for the correlator output.
            i_preload0             => global_config_fields_rw_del(i).preload0,             -- in(15:0); Number samples for the preload for the correlator.
            i_blocksPerFrame1      => global_config_fields_rw_del(i).blocksperframe1,      -- in(10:0); Number of LFAA blocks per frame for the PSS/PST output.
            i_preload1             => global_config_fields_rw_del(i).preload1,             -- in(15:0); Number samples for the preload for the PSS/PST output.
            i_CorOutputCycles      => global_config_fields_rw_del(i).cor_output_cycles,    -- in(15:0); Number of clock cycles per packet output for the correlator output.
            i_PSSPSTOutputCycles   => global_config_fields_rw_del(i).psspst_output_cycles, -- in(15:0); Number of clock cycles per packet output for the PSS/PST output.
            -- Delay tables
            o_delay_table_0_addr => station_config_table_0_in.adr(i),     -- out(9:0);
            i_delay_table_0_data => station_config_table_0_out.rd_dat(i), --  in(31:0);
            o_delay_table_1_addr => station_config_table_1_in.adr(i),     -- out(9:0);
            i_delay_table_1_data => station_config_table_1_out.rd_dat(i), --  in(31:0);
            -- Virtual Channel output order for the correlator
            o_correlator_order_addr => correlator_order_in.adr(i),      -- out(8:0)
            i_correlator_order_data => correlator_order_out.rd_dat(i),  -- in(15:0)
            -- Virtual channel output order for PSS and PST
            o_PSSPST_order_addr => PSSPST_order_in.adr(i),              -- out(8:0)
            i_PSSPST_order_data => PSSPST_order_out.rd_dat(i),          -- in(15:0)
            -- memory to record counts of valid output packets for the correlator
            o_correlator_count_addr => correlator_output_count_in.adr(i),
            o_correlator_count_wrEn => correlator_output_count_in.wr_en(i),
            o_correlator_count_WrData => correlator_output_count_in.wr_dat(i),
            i_correlator_count_RdData => correlator_output_count_out.rd_dat(i),
            -- memory to record counts of valid output packets for PSS/PST
            o_PSSPST_count_addr => PSSPST_output_count_in.adr(i),
            o_PSSPST_count_WrEn => PSSPST_output_count_in.wr_en(i),
            o_PSSPST_count_WrData => PSSPST_output_count_in.wr_dat(i),
            i_PSSPST_count_RdData => PSSPST_output_count_out.rd_dat(i),
            -- status registers
            o_free_size                 => monitoring_fields_ro.free_size(i),
            o_free_min                  => monitoring_fields_ro.free_min(i), -- out(15:0);
            o_input_late_count          => monitoring_fields_ro.input_late_count(i), -- out(15:0);
            o_input_too_soon_count      => monitoring_fields_ro.input_too_soon_count(i), -- out(15:0);
            o_duplicates                => monitoring_fields_ro.duplicates(i), -- out(7:0);
            o_correlator_missing_blocks	=> monitoring_fields_ro.correlator_missing_blocks(i), -- out(15:0);
            o_psspst_missing_blocks     => monitoring_fields_ro.psspst_missing_blocks(i), -- out(15:0);
            o_error_preload_config      => monitoring_fields_ro.error_preload_config(i), -- out std_logic;
            o_error_free_bad            => monitoring_fields_ro.error_free_bad(i),       -- out std_logic;
            o_error_input_overflow      => monitoring_fields_ro.error_input_overflow(i), -- out std_logic;
            o_error_ctc_underflow       => monitoring_fields_ro.error_ctc_underflow(i)   -- out std_logic;
        );
        
        -- All the ARGs signals that are not used.
        station_config_table_0_in.rst(i) <= '0';
        station_config_table_0_in.wr_en(i) <= '0';
        station_config_table_0_in.wr_dat(i) <= (others => '0');
        station_config_table_0_in.rd_en(i) <= '1';
        station_config_table_0_in.clk(i) <= i_hbm_clk;
        
        correlator_order_in.rst(i) <= '0';
        correlator_order_in.wr_en(i) <= '0';
        correlator_order_in.wr_dat(i) <= (others => '0');
        correlator_order_in.rd_en(i) <= '1';
        correlator_order_in.clk(i) <= i_hbm_clk;
        
        PSSPST_order_in.rst(i) <= '0';
        PSSPST_order_in.wr_en(i) <= '0';
        PSSPST_order_in.wr_dat(i) <= (others => '0');
        PSSPST_order_in.rd_en(i) <= '1';
        PSSPST_order_in.clk(i) <= i_hbm_clk;        
        
        correlator_output_count_in.rst(i) <= '0';
        correlator_output_count_in.rd_en(i) <= '1';
        correlator_output_count_in.clk(i) <= i_hbm_clk;
        
        PSSPST_output_count_in.rst(i) <= '0';
        PSSPST_output_count_in.rd_en(i) <= '1';
        PSSPST_output_count_in.clk(i) <= i_hbm_clk;
        
    end generate;
    
    
end Behavioral;
