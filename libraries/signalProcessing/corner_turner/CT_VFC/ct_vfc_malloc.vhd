----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 17.03.2020 09:34:06
-- Module Name: ct_vfc_malloc - Behavioral
-- Description: 
--  Allocate and free memory used in the corner turn.
--  There are two levels of pointers. 
--    * The first level points to 64K blocks in HBM.
--    * The second level points to the start and end of the valid entries in the lists of used and free pointers.
-- 
--
-- HBM Pointers
-- ------------
--  There are two lists of pointers, the used list and the free list.
--  There are 16384 pointers per Gbyte of memory, and each pointer is 2 bytes, so the
--  free list needs 32Kbytes of storage = 1 URAM per GByte.
--  
--  There is a separate used list for each channel. The used list for each channel needs 
--  space for enough pointers to have at least 2 full frames allocated.
--  Only the last channel that is read will use the full 2 frames of pointers. 
-- 
--   Frame length  |  Minimum memory    | # of 4096         | Pointers per     |  Pointers for 
--                 |  for 1 full block  | sample blocks (n) | frame + preload  |  2 frames
--                 |  + preload         |                   |  = (n+12)/4      |
--     0.9             1.27 GBytes           204                 54                 108
--     0.45 s          0.67 GBytes           102                 29                 58
--     0.3 s           0.47 GBytes            68                 20                 40
--     0.075s          0.17 GBytes            17                  8                 16
--
--
--  For 0.3s frames, this is 2*((68+12)/4) = 40 pointers.
--  There are 8 LFAA input packets per "used" pointer, so each "used" pointer also 
--  needs to indicate which of the 8 input packets have been written. 
--  So each pointer needs 3 bytes:
--   bits(23:16) = one bit per LFAA packet to indicate valid.
--   bits(15:0) = Pointer to a 64K block of HBM
--
--  block memory for the used lists:
--   * (384 channels) * (40 pointers) * (3 bytes/pointer) = 46080 bytes,
--   * URAMs are 72 bits wide, so they fit 3 pointers per address. 
--  so 2 URAMs for the used pointer list allows up to 63 pointers per channel.
--  This is sufficient if 1Gbyte of HBM is allocated. When 2 Gbytes are allocated, double this is needed
--  i.e, 4 URAMs and up to 126 pointers per channel.
--  For the 1 GByte case,
--  Each channel has 21 URAM addresses each, and 21*384 = 8064 of the 8192 addresses are used.
--  (note : 8192 addresses since each URAM is (4096 deep) x (64 bits wide), so 2 URAMs => 8192 addresses)
--
--  This module provides for two different reads.
--  Each pointer is 14 bits for 1 Gbyte of memory.
--  Each pointer is 15 bits for 2 Gbyte of memory.
-- 
-- When to allocate memory
-- -----------------------
--  Memory is allocated (moved from the free list to the used list) when a packet arrives at the input to the corner turn.
--  Memory is freed only after the the last data has been read from the HBM.
--  There is a corner case where memory is allocated according to the used list but the data has not yet made it into the HBM,
--  and that block is immediately read out. In this case it could be incorrectly labelled as valid according to this module.
--  This corner case will be detected by the auxiliary data written to the HBM (packet count, channel, and station information).
--
-- Management of the Free and used lists
-- -------------------------------------
--  The free list consists of 16-bit pointers to 64k blocks of memory.
--  There are 16384 x 64K blocks in 1 Gbyte, so the free list occupies 1 URAM/Gbyte of HBM  (1 URAM = 32Kbytes = 16384 pointers)
--  On reset, the free list is filled with values from 0 to 16383
--  There are two pointers into the free list, freeListRdAddr and freeListWrAddr, which are initialised to 0 and i_freeMax respectively.
--  pointers are allocated by reading from freeListRdAddr, and freed by writing to freeListWrAddr.
--  i_freeMax defines the amount of memory to allocate for the corner turn. i_freeMax = 16383 corresponds to 1Gbyte allocated.
--  
--  There are 384 separate "used" lists, one per channel.
--  These are the pointers into these lists (two copies, one for each readout)
--   - readFrameStart0/1 : 
--       The location of the start of the frame that is either being read out, or is waiting to be read out.
--       There is a single frameStart pointer for all 384 used lists.
--       When all the virtual channels have been read out, then this is incremented by the frame size.
--       The frame length need not be a multiple of 8 LFAA input frames. To account for this,
--       readFrameStart is 12 bit value, made up of 9 bits plus 3 "fractional" bits.
--       The packet counts that readFrameStart0/1 points to is kept in "frameStartPacketCount0/1"
--       "frameStartPacketCount0" is used when data is written to work out where in the used list the packet should go.
--       It is assumed that frameStartPacketCount0 relates to the longer readout (i.e. the correlator readout), which
--       makes it the safest version to choose for determining when writes should be dropped because they are either too late or too early.
--   - readChannelCount0/1 : 
--       Counts 0 to 383, through all the virtual channels as they are being read out.
--       This is not the actual virtual channel, instead it is an index into the "readChannelOrder" memory
--   - readChannel0/1 : 
--       The virtual channel currently being read out.
--   - readblock0/1[channel] : 
--       Points to the place we are reading from in the current channel.
--       readBlock starts at readFrameStart, counts up to readFrameStart + i_blocksPerFrame, 
--       then goes back to readFrameStart to read out the next channel.
--       A separate readBlock is maintained for each channel, so we know when a block can be moved to the free list.
--       i.e. when both readBlock0[channel] and readBlock1[channel] are both past a given block, then it can be freed.
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library ct_vfc_lib;
Library xpm;
use xpm.vcomponents.all;
library DSP_top_lib;
use DSP_top_lib.DSP_top_pkg.all;

entity ct_vfc_malloc is
    generic (
        -- How much memory to allow for.
        -- True : 2 Gbytes have been allocated to the corner turn.
        -- False : 1 Gbyte is allocated to the corner turn.
        -- This defines the amount of space allocated for pointers.
        -- The actual amount of memory used and the base address of the memory is defined via MACE. 
        g_USE_2GBYTE : boolean := false
    );
    Port ( 
        -- 
        i_clk : in std_logic;
        -- Interface to the data input side - writes to the HBM
        i_writePacketCount : in std_logic_vector(31 downto 0);  -- Packet count from the packet header
        i_writeChannel     : in std_logic_vector(15 downto 0);  -- virtual channel from the packet header
        i_writeValid       : in std_logic;                      -- Goes high to indicate o_packet_count is valid, and stays high until a response comes back
        o_writeAddress     : out std_logic_vector(23 downto 0); -- Address to write this packet to, in units of 8192 bytes.
        o_writeOK          : out std_logic;                     -- Write address is valid; if low, then the packet should be dropped as it is either too early or too late.
        o_writeValid       : out std_logic;                     -- i_packet_addr, i_packet_drop are valid. This concludes the transaction with the pointer management module.
        -- Interface to the data output side - Reads from the HBM
        -- If it is the last read of an address, then the address of block to be freed is put into a FIFO in this module, and read from the FIFO on i_free0/1
        -- This means no more than 3 of these addresses should be buffered by the module which reads the HBM.
        o_readAddress     : out std_logic_vector(23 downto 0);   -- HBM address to read from, in units of 8192 bytes. Points to the start of a 2048 sample packet.
        o_readPacketCount : out std_logic_vector(31 downto 0);   -- packet count for this read address
        o_readChannel     : out std_logic_vector(15 downto 0);   -- Virtual channel at this address.
        o_readStart       : out std_logic;                       -- Indicates this is the first read for this channel
        o_readOK          : out std_logic;                       -- o_readAddress points to valid data
        o_readFree        : out std_logic;                       -- Indicates readAddress can be freed once it has been read
        o_readValid0      : out std_logic;                       -- o_readAddress and o_readOK are valid; Note valid waits on ready being high.
        i_readReady0      : in std_logic;                        -- port 0 read side can accept a new address to read from. Data is transfered when valid and ready are high.
        o_readValid1      : out std_logic;                       -- o_readAddress and o_readOK are valid; Note valid waits on ready being high.
        i_readReady1      : in std_logic;                        -- port 1 read side can accept a new address to read from. Data is transfered when valid and ready are high.        
        i_free0           : in std_logic;                        -- pulse to indicate that the free operation can go ahead.
        i_free1           : in std_logic;
        -- Configuration from MACE
        -- Configuration parameters must be set prior to i_rst.
        -- Typical values for 0.3 second correlator corner turn :
        --  i_freeMax = 16384  = 1 Gbyte of memory
        --  i_baseAddr = 0x0   = Start at the start of the memory block.
        --  i_maxFutureCount = 400 (Units of LFAA blocks of 2048 samples = 2.2ms) (Minimum is 2 frames worth + preload, maximum is 63(pointers)*8(LFAA blocks/pointer) = 504, since that is the maximum length of the used list for a single channel (for the 1Gbyte case))
        --  i_scanStartPacketCount = 0 (Just depends on the input data)
        --  i_blocksPerFrame0 = 136 (=8x17, note 136*2.2ms = 300 ms)
        --  i_preload0 = 22 * 2048 = 45056; 
        --  i_totalChannels0 = 384
        --  i_blocksPerFrame1 = 16  (16*2.2ms = 35.2 ms)
        --  i_preload1 = 11*256 = 2816  (PST preload is 11*256 samples = 2816 samples)
        --  i_totalChannels1 = 384
        i_rst         : in std_logic;                         -- Reset, frees all memory. Should be common to all instances of the corner turn. 
        o_busy        : out std_logic;                        -- Indicates the fsm is busy, e.g. writing default data to the free list after a reset.
        i_freeMax     : in std_logic_vector(15 downto 0);     -- Number of 64K blocks in the free list
        i_baseAddr    : in std_logic_vector(23 downto 0);     -- base address in the HBM, in units of 8192 bytes
        i_maxFutureCount : in std_logic_vector(15 downto 0);  -- Maximum distance into the future that a packet count can be before we drop it as being too far into the future. Units 2.2ms (i.e. one 2048 sample LFAA input packet)
        i_scanStartPacketCount : in std_logic_vector(31 downto 0); -- packet count to start the scan at. LFAA data with a packet count less than this value will be dropped.
        i_blocksPerFrame0 : in std_logic_vector(10 downto 0); -- Number of LFAA blocks per frame. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
        i_preload0        : in std_logic_vector(15 downto 0); -- Number samples for the preload. e.g. 22*2048 for the correlator.
        i_totalChannels0  : in std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 0.
        i_blocksPerFrame1 : in std_logic_vector(10 downto 0); -- Number of LFAA blocks per frame for second readout. Each LFAA block is 2048 time samples. e.g. 408 for 0.9sec corner turn, or 136 for 0.3 sec corner turn.
        i_preload1        : in std_logic_vector(15 downto 0); -- Number of samples for the preload for the second readout. e.g. 11*256=2816 for PSS/PST readout.
        i_totalChannels1  : in std_logic_vector(8 downto 0);  -- Number of channels to read out and process, for readout 1.
        o_readChannelOrderAddress0 : out std_logic_vector(8 downto 0); -- Address into the read channel order memory, for read out 0
        i_readChannelOrderData0    : in  std_logic_vector(8 downto 0); -- virtual channel to read; assumes 3 clock latency from o_readChannelOrderAddress
        o_readChannelOrderAddress1 : out std_logic_vector(8 downto 0); -- Address into the read channel order memory, for read out 1
        i_readChannelOrderData1    : in  std_logic_vector(8 downto 0); -- virtual channel to read, assumes 3 clock latency for o_readChannelOrderAddress1
        -- Timing information 
        i_packetCountNotify : in std_logic_vector(31 downto 0); -- Set o_packetCountSeen when an incoming packet count (i_writePacketCount) is this value or higher.
        o_packetCountSeen   : out std_logic;                    -- Pulse to indicate reception of a packet with packet count >= i_packetCountNotify
        -- Monitoring
        i_monitor_rst : in std_logic;                           -- Reset monitoring counts and flags.
        o_freeSize    : out std_logic_vector(15 downto 0);      -- Number of entries in the free list;
        o_freeMin     : out std_logic_vector(15 downto 0);      -- low watermark for o_freeSize; reset by general module reset (Not i_monitor_rst).
        o_inputLateCount : out std_logic_vector(15 downto 0);   -- Count of input LFAA blocks which have been dropped as they are too late. Top bit is sticky.
        o_inputTooSoonCount : out std_logic_vector(15 downto 0); -- Count of input LFAA blocks which have been dropped as they are too far into the future. Top bit is sticky.
        o_duplicates     : out std_logic_vector(7 downto 0);    -- count of duplicate input packets (i.e. input packets with the same timestamp and channel as a previous packet). Top bit is sticky.
        o_missingBlocks0 : out std_logic_vector(15 downto 0);   -- Count of blocks (1 block = 2048 time samples) on output 0 read with no data; top bit is sticky; low 15 bits wrap.
        o_missingBlocks1 : out std_logic_vector(15 downto 0);   -- Count of blocks on output 1 read with no data; top bit is sticky; low 15 bits wrap.
        o_preload1_gt_preload0 : out std_logic;                 -- Error flag; i_preload0 must be greater than or equal to i_preload1. Detected when new values are loaded after a module reset (via i_rst).
        o_badFreeFIFOState  : out std_logic                     -- Error flag; Tried to read the toBeFree fifo, but it was empty; should never happen.
    );
end ct_vfc_malloc;

architecture Behavioral of ct_vfc_malloc is

    constant c_FREE_LIST_MAX : integer := sel(g_USE_2GBYTE,32767,16383);  -- Initial size of the free list
    constant c_USED_LIST_MAX_ADDR : integer := sel(g_USE_2GBYTE,125,62);  -- Maximum entry in the used list for each channel
    constant c_MAX_USED_LIST_PTR : integer := sel(g_USE_2GBYTE,1007,503);    -- 8x the max address, since the pointer points to the LFAA block within a 64K HBM block.
    type malloc_fsm_type is (idle, 
                             reset_start, read_start, reset_init_lists, reset_start1, reset_done, 
                             wr_check_packet_count, wr_set_used_list_rd_addr, wr_wait_used_list_pointer, wr_drop_packet, wr_check_ptr, wr_update_used_list, wr_done, wr_finished,
                             rd_start, rd_get_cur_channel, rd_wait_blockRd1, rd_wait_blockRd2, rd_wait_blockRd3, rd_done, rd_update_ptr, 
                             rd_wait_used_list1, rd_wait_used_list2, rd_wait_used_list3, rd_wait_used_list4, rd_wait_used_list5, rd_wait_used_list6, rd_wait_used_list7, rd_check_valid,
                             free_start, free_wait_rd_toBeFreeFIFO, free_rd_toBeFreeFIFO, free_done);
                             
    signal malloc_fsm : malloc_fsm_type := idle;
    signal fsmWaitCount : std_logic_vector(3 downto 0); -- used to hold the fsm in some states while we wait for something, e.g. block ram latency.
    
    signal freeListRdData : std_logic_vector(15 downto 0);
    signal freeListWrData : std_logic_vector(15 downto 0);
    signal freeListWrEn   : std_logic;
    signal freeListWrAddr : std_logic_vector(15 downto 0);
    signal freeListRdAddr : std_logic_vector(15 downto 0);    
    signal freeMax        : std_logic_vector(15 downto 0);
    
    signal usedListChannel : std_logic_vector(8 downto 0);
    signal usedListAddr    : std_logic_vector(8 downto 0); -- but only 7 bits used for the actual address; high order bits just for the modulo operation.
    signal usedListWrData    : std_logic_vector(23 downto 0);
    signal usedListWrEn      : std_logic;
    signal usedListRdData    : std_logic_vector(23 downto 0);
    
    signal readFrameStart0, readFrameStart1 : std_logic_vector(11 downto 0); -- Pointer into the used list to the start of the read frame. Units of LFAA blocks. There are 8 LFAA blocks per 64KB block, so divide by 8 to get the address in the used list.
    signal frameStartPacketCount0, frameStartPacketCount1 : std_logic_vector(31 downto 0); -- the packet count that readFrameStart0/1 point too. 
    signal readPacketCount0, readPacketCount1 : std_logic_vector(31 downto 0);
    signal packetCountOffset0 : std_logic_vector(31 downto 0);  -- Used for writes to the HBM; Difference between the incoming packet count and the packet count at the start of frame (i.e. frameStartPacketCount0)
    signal readChannelCount0, readChannelCount1 : std_logic_vector(8 downto 0); -- The channel we are currently reading out; counts from 0 to (i_totalChannels0/1 - 1)
    signal readChannelCountMax0, readChannelCountMax1 : std_logic_vector(8 downto 0);  -- copied from i_totalChannels0/1 on reset.
    signal maxFutureCount : std_logic_vector(15 downto 0);
    signal maxFutureCountExt : std_logic_vector(31 downto 0);

    signal curBlockWrChannel : std_logic_vector(8 downto 0);
    signal curBlockWrData0, curBlockWrData1 : std_logic_vector(11 downto 0);
    signal curBlockWrEn0 : std_logic;
    signal curBlockWrEn1 : std_logic;
    signal curBlockRdChannel : std_logic_vector(8 downto 0);
    signal curBlockRdData0, curBlockRdData1, readCurBlock0, readCurBlock1 : std_logic_vector(11 downto 0);
    signal readOffset0FromStart0, readOffset1FromStart1 : std_logic_vector(11 downto 0);
    signal readOffset0FromStart1, readOffset1FromStart0 : std_logic_vector(31 downto 0);
    signal preload0LFAABlocks : std_logic_vector(11 downto 0);
    signal preload1LFAABlocks : std_logic_vector(11 downto 0);
    signal preload0, preload1 : std_logic_vector(11 downto 0);
    signal inputLateCount : std_logic_vector(14 downto 0);
    signal inputLateStickyBit : std_logic;
    signal inputTooSoonCount : std_logic_vector(14 downto 0);
    signal inputTooSoonStickyBit : std_logic;
    signal usedListRdAddrLFAABlocks : std_logic_vector(11 downto 0);
    signal wrPtr : std_logic_vector(23 downto 0);
    signal duplicatesStickyBit : std_logic;
    signal duplicatesCount : std_logic_vector(6 downto 0);
    signal baseAddr : std_logic_vector(23 downto 0);
    signal wrPtrFull : std_logic_vector(23 downto 0);
    signal readPort : std_logic;
    signal toBeFreed0, toBeFreed1 : std_logic_vector(5 downto 0);

    signal toBeFreeDout0, toBeFreeDout1, toBeFreeDin0, toBeFreeDin1 : std_logic_vector(15 downto 0);
    signal toBeFreeEmpty0, toBeFreeEmpty1 : std_logic;
    signal toBeFreeFull0, toBeFreeFull1 : std_logic;
    signal toBeFreeRdEn0, toBeFreeRdEn1 : std_logic;
    signal toBeFreeWrEn0, toBeFreeWrEn1 : std_logic;
    signal toBeFreeFIFOSize0, toBeFreeFIFOSize1 : std_logic_vector(5 downto 0);
    signal badFreeFIFOState : std_logic;
    signal free0Done, free1Done : std_logic;
    signal resetFreed : std_logic;
    signal rdPtr : std_logic_vector(23 downto 0);
    signal usedListOffset : std_logic_vector(2 downto 0);
    
    signal readAddress : std_logic_vector(23 downto 0);
    signal readPacketCount : std_logic_vector(31 downto 0);
    signal readChannel : std_logic_vector(15 downto 0);
    signal readStart : std_logic;
    signal readOK : std_logic;
    signal readFree : std_logic;    
    signal blocksPerFrame0, blocksPerFrame1, lastBlockInFrame0, lastBlockInFrame1 : std_logic_vector(11 downto 0);
    signal read0Freeable0, read1Freeable0,  read0Freeable1, read1Freeable1 : std_logic;
    signal readPacketCount0_lt_1, readPacketCount1_lt_0 : std_logic;
    signal rdValid : std_logic_vector(7 downto 0);
    signal lastInFrame : std_logic;
    signal usedListPtrFull, usedListPtrMinusPreload, nextReadFrameStart : std_logic_vector(11 downto 0);  -- pointer into the used list. low 3 bits are the LFAA block (i.e. 2048 sample block), High 9 bits are the 64K HBM block.
    signal readValid0, readValid1, lastChannel : std_logic;
    signal freeSize, freeMin, freeDiff : std_logic_vector(15 downto 0);
    signal initFreeMinDel1, initFreeMin, initFreeMinDel2 : std_logic;
    signal missingBlocks0Count, missingBlocks1Count : std_logic_vector(14 downto 0);
    signal missingBlocks0StickyBit, missingBlocks1StickyBit : std_logic;
    signal monitor_rst : std_logic;
    
begin
    
    -- miscellaneous assignments
    o_inputLateCount <= inputLateStickyBit & inputLateCount;
    o_inputTooSoonCount <= inputTooSoonStickyBit & inputTooSoonCount;
    o_duplicates <= duplicatesStickybit & duplicatesCount;
    o_badFreeFIFOState <= badFreeFIFOState;
    maxFutureCountExt <= x"0000" & maxFutureCount;
    preload0LFAABlocks <= "0000000" & i_preload0(15 downto 11); -- Number of LFAA blocks (2048 samples) to preload
    preload1LFAABlocks <= "0000000" & i_preload1(15 downto 11);
    
    o_readChannelOrderAddress0 <= readChannelCount0;
    o_readChannelOrderAddress1 <= readChannelCount1;
    
    o_readAddress <= readAddress; 
    o_readPacketCount <= readPacketCount;
    o_readChannel <= readChannel;
    o_readStart <= readStart;
    o_readOK <= readOK;
    o_readFree <= readFree;
    o_readValid0 <= readValid0;
    o_readValid1 <= readValid1;
    
    o_freeSize <= freeSize;
    o_freeMin <= freeMin;
    
    o_missingBlocks0 <= missingBlocks0StickyBit & missingBlocks0Count;
    o_missingBlocks1 <= missingBlocks1StickyBit & missingBlocks1Count;
    
    usedListPtrFull <= usedListAddr & usedListOffset;
    
    -- Main process (fsm + other stuff)
    process(i_clk)
        variable usedListRdAddr_v : std_logic_vector(8 downto 0);
        variable preloadDifference_v : std_logic_vector(11 downto 0);
        variable correctedPtr_v : std_logic_vector(11 downto 0);
    begin
        if rising_edge(i_clk) then
            
            ------------------------------------------------------------------------------------------
            -- Miscellaneous calculations
            --
            -- How far is the new packet coming in from where we are currently up to ?
            -- This is used to determine where this packet should go in the used list, and 
            -- to check if this packet is either too early or too late.
            packetCountOffset0 <= std_logic_vector(unsigned(i_writePacketCount) - unsigned(frameStartPacketCount0));
            lastBlockInFrame0 <= std_logic_vector(unsigned(blocksPerFrame0) + unsigned(preload0) - 1);
            lastBlockInFrame1 <= std_logic_vector(unsigned(blocksPerFrame1) + unsigned(preload1) - 1);
            
            -- Report the size of the free list
            freeDiff <= std_logic_vector(unsigned(freeListWrAddr) - unsigned(freeListRdAddr));
            if (signed(freeDiff) <= 0) then
                freeSize <= std_logic_vector(unsigned(freeDiff) + unsigned(freeMax) + 1);
            else
                freeSize <= freeDiff;
            end if;
            
            if malloc_fsm = reset_done then  -- write and read addresses into the free list are only valid for calculating the size after the reset_done state 
                initFreeMin <= '1';
            else
                initFreeMin <= '0';
            end if;
            initFreeMinDel1 <= initFreeMin;
            initFreeMinDel2 <= initFreeMinDel1; -- Need a few cycles delay to wait for freeSize to be valid.
            if (initFreeMinDel2 = '1' or (unsigned(freeSize) < unsigned(freeMin))) then
                FreeMin <= freeSize;
            end if;
            
            -- count instances of missing blocks
            if initFreeMinDel1 = '1' then  -- Anything triggered by i_rst will do.
                missingBlocks0Count <= (others => '0');
                missingBlocks1Count <= (others => '0');
                missingBlocks0StickyBit <= '0';
                missingBlocks1StickyBit <= '0';
            else
                if readOK = '0' and readValid0 = '1' then
                    missingBlocks0Count <= std_logic_vector(unsigned(missingBlocks0Count) + 1);
                    if missingBlocks0Count = "111111111111111" then
                        missingBlocks0StickyBit <= '1';
                    end if;
                end if;
                if readOK = '0' and readValid1 = '1' then
                    missingBlocks1Count <= std_logic_vector(unsigned(missingBlocks1Count) + 1);
                    if missingBlocks1Count = "111111111111111" then
                        missingBlocks1StickyBit <= '1';
                    end if;
                end if;
            end if;
            
            
            if i_rst = '1' or i_monitor_rst = '1' then
                monitor_rst <= '1';
            else
                monitor_rst <= '0';
            end if;
            
            -------------------------------------------------------------------------------------------
            -- Main state machine.
            --
            if i_rst = '1' then
                malloc_fsm <= reset_start;
                
            else
                case malloc_fsm is
                    when idle =>
                        -- Wait for a request
                        if i_writeValid = '1' then
                            malloc_fsm <= wr_check_packet_count;
                        elsif (i_readReady0 = '1') then
                            malloc_fsm <= rd_start;
                            readport <= '0'; -- Selects which read port to use.
                        elsif (i_readReady1 = '1') then
                            malloc_fsm <= rd_start;
                            readport <= '1';
                        elsif (unsigned(toBeFreed0) > 0) then  
                            -- Need to move a pointer from the "toBeFree0/1" FIFO to the free list. 
                            malloc_fsm <= free_start;
                            readport <= '0';
                        elsif (unsigned(toBeFreed1) > 0) then
                            malloc_fsm <= free_start;
                            readport <= '1';
                        end if;
                        o_writeOK <= '0';                     -- Write address is valid; if low, then the packet should be dropped as it is either too early or too late.
                        o_writeValid <= '0';
                        readOK <= '0';
                        readValid0 <= '0';
                        readValid1 <= '0';
                        free0Done <= '0';
                        free1Done <= '0';
                        toBeFreeRdEn0 <= '0';
                        toBeFreeRdEn1 <= '0';
                    
                    -----------------------------------------------------------------------
                    -- wr_* states provide write addresses to the input buffer.
                    -- Steps required :
                    --   - Use the provided packet count to work out which pointer in the used list should be used.
                    --      - If the packet count is too far into the future or past, then tell the input buffer to discard the packet.
                    --   - Read the pointer from the used list.
                    --   - If the pointer is already allocated, provide it to the input buffer.
                    --      - If the pointer is already allocated, and this LFAA block is already filled, report an error.
                    --     If the pointer is not already allocated, then :
                    --      - Get a new pointer from the free list
                    --      - Advance the start of the free list
                    --      - put the new pointer into the used list
                    --      - give the pointer to the input buffer.
                    when wr_check_packet_count =>
                        -- check that the packet count is within allowed bounds.
                        -- packetCountOffset0 ( = i_writePacketCount - frameStartPacketCount0) is the location in the used list where this pointer is stored.
                        -- If it is negative, then the packet has arrived too late, since we have already read out that packet from the HBM
                        -- If it is positive, then it must be within the allowed limit for the maximum time in the future, i.e. less than i_maxFutureCount.
                        if packetCountOffset0(31) = '1' then  -- negative; drop the packet.
                            inputLateCount <= std_logic_vector(unsigned(inputLateCount) + 1);
                            if inputLateCount = "111111111111111" then
                                inputLateStickyBit <= '1';
                            end if;
                            malloc_fsm <= wr_drop_packet;
                        elsif (unsigned(packetCountOffset0) > unsigned(maxFutureCountExt)) then
                            inputTooSoonCount <= std_logic_vector(unsigned(inputTooSoonCount) + 1);
                            if inputTooSoonCount = "111111111111111" then
                                inputTooSoonStickyBit <= '1';
                            end if;
                            malloc_fsm <= wr_drop_packet;
                        else
                            -- Packet count was ok, get the pointer from the used list
                            malloc_fsm <= wr_set_used_list_rd_addr;
                        end if;
                        usedListChannel <= i_writeChannel(8 downto 0);  -- Virtual channel to read.
                        usedListRdAddrLFAABlocks <= std_logic_vector(unsigned(packetCountOffset0(11 downto 0)) + unsigned(readFrameStart0)); -- read address into the used list, in units of LFAA blocks
                        
                    when wr_drop_packet =>
                        o_writeAddress <= (others => '0'); -- Address to write this packet to.
                        o_writeOK      <= '0';             -- packet count was bad; this packet should be dropped.
                        o_writeValid   <= '1';             -- concludes the transaction 
                        malloc_fsm <= idle;
                        
                    when wr_set_used_list_rd_addr =>
                        -- take 12 bit offset into the used list in units of LFAA blocks, 
                        -- do modulo operation for the limited length of the list, and convert to 7 bits.
                        usedListRdAddr_v := usedListRdAddrLFAABlocks(11 downto 3); -- Divide by 8; there are 8 LFAA blocks per entry in the used list.
                        if (g_USE_2GBYTE) then
                            if (unsigned(usedListRdAddr_v) > 125) then
                                usedListAddr <= std_logic_vector(unsigned(usedListRdAddr_v) - 126);
                            else
                                usedListAddr <= usedListRdAddr_v;
                            end if;
                        else
                            if (unsigned(usedListRdAddr_v) > 62) then
                                usedListAddr <= std_logic_vector(unsigned(usedListRdAddr_v) - 63);
                            else
                                usedListAddr <= usedListRdAddr_v;
                            end if;
                        end if;
                        malloc_fsm <= wr_wait_used_list_pointer; -- Address into the used list is set; wait until the data comes back.
                        fsmWaitCount <= "0000";
                        
                    when wr_wait_used_list_pointer =>
                        -- Wait until the pointer comes back from the used list. 6 cycle latency to read from the used list.
                        fsmWaitCount <= std_logic_vector(unsigned(fsmWaitCount) + 1);
                        if fsmWaitCount = "0110" then
                            malloc_fsm <= wr_check_ptr;
                            wrPtr <= usedListRdData;
                        end if;
                    
                    when wr_check_ptr => 
                        -- check the pointer read from the used list; if it is allocated, return it to the input buffer. Otherwise, allocate it.
                        if wrPtr(23 downto 16) = "00000000" then -- The pointer has not been allocated. Allocate a new pointer from the free list
                            wrPtr(23 downto 16) <= "00000000";
                            wrPtr(15 downto 0) <= freeListRdData;
                            if (unsigned(freeListRdAddr) < unsigned(freeMax)) then
                                freeListRdAddr <= std_logic_vector(unsigned(freeListRdAddr) + 1);
                            else
                                freeListRdAddr <= (others => '0');
                            end if;
                        else -- The pointer is already allocated; check the LFAA block hasn't already been written
                            if wrPtr(16 + to_integer(unsigned(usedListRdAddrLFAABlocks(2 downto 0)))) = '1' then
                                -- This pointer has already been allocated; this must be a duplicate block.
                                duplicatesCount <= std_logic_vector(unsigned(duplicatesCount) + 1);
                                if duplicatesCount = "1111111" then
                                    duplicatesStickyBit <= '1';
                                end if;
                            end if;
                        end if;
                        wrPtr(16 + to_integer(unsigned(usedListRdAddrLFAABlocks(2 downto 0)))) <= '1';  -- Mark this block as used; note this must come after the assignment to 0 above.
                        malloc_fsm <= wr_update_used_list;
                        
                    when wr_update_used_list => 
                        usedListWrData <= wrPtr;
                        usedListWrEn <= '1';
                        wrPtrFull <= "00000" & wrPtr(15 downto 0) & usedListRdAddrLFAABlocks(2 downto 0);
                        malloc_fsm <= wr_done;
                    
                    when wr_done =>
                        usedListWrEn   <= '0';
                        o_writeAddress <= std_logic_vector(unsigned(baseAddr) + unsigned(wrPtrFull)); -- Address to write this packet to.
                        o_writeOK      <= '1';             -- packet count was bad; this packet should be dropped.
                        o_writeValid   <= '1';             -- concludes the transaction 
                        malloc_fsm     <= wr_finished;
                    
                    when wr_finished =>
                        o_writeValid <= '0';
                        o_writeOK <= '0';
                        malloc_fsm <= idle;
                    ----------------------------------------------------------------------
                    -- rd_* states generate read addresses.
                    -- Addresses are for the port "readport", which is set in the idle state before jumping to this state.
                    -- Next thing to read is :
                    --   (1) Use readChannelCount0/1 to look up the virtual channel that we are up to 
                    --        - This is a lookup from a memory in the registers, via o_readChannelOrderAddress0/1, i readChannelOrderData0/1.
                    --   (2) Look up the location in the used list for this virtual channel from blockUsedMem (read address is "curBlockRdChannel")
                    --   (3) Use the virtual channel and curBlockRd0/1 to look up the pointer from the the used list.
                    --   (4) Check if the data is valid for this block
                    --   (5) Check if this is the last read of this 64KB block, and if so schedule it to be freed, by setting o_free0/1
                    --        - Can't free it immediately because it takes a while to be read out.
                    --        - the pointer to the memory block is written to the toBeFree0/1 FIFO.
                    --   (6) Output the address and meta data on o_readAddress0/1 etc.
                    --   (7) Update the pointer into the used list in blockUsedMem
                    --        - If the pointer is at the end of the block, also update readChannelCount
                    --   
                    when rd_start =>
                        -- lookup which virtual channel we are up to.
                        -- Translate readChannelCount to the virtual channel we are processing, curBlockRdChannel, via the "readChannelOrder" memory.
                        -- The address into the readChannelOrder memory is already set; wait one cycle to be sure
                        -- that the read data is valid.
                        malloc_fsm <= rd_get_cur_channel;
                        
                    when rd_get_cur_channel =>
                        malloc_fsm <= rd_wait_blockRd1;
                        if readport = '0' then
                            curBlockRdChannel <= i_readChannelOrderData0;
                        else
                            curBlockRdChannel <= i_readChannelOrderData1;
                        end if; 
                    
                    when rd_wait_blockRd1 => -- curBlockRdChannel is valid in this state; this is the address into the blockUsed memory, which holds pointers to where we are reading from for each channel in the used list.
                        malloc_fsm <= rd_wait_blockRd2;
                        
                    when rd_wait_blockRd2 =>
                        malloc_fsm <= rd_wait_blockRd3;
                        
                    when rd_wait_blockRd3 => -- the output of the blockUsed memory is valid in this state; get 
                        malloc_fsm <= rd_wait_used_list1;
                        fsmWaitCount <= "0000";
                        usedListChannel <= curBlockRdChannel;
                        readCurBlock0 <= curBlockRdData0;
                        readCurBlock1 <= curBlockRdData1;
                        if readport = '0' then
                            usedListAddr <= curBlockRdData0(11 downto 3);  -- Low 3 bits of curBlockRdData are the offset within the 64KB block.
                            usedListOffset <= curBlockRdData0(2 downto 0);
                        else
                            usedListAddr <= curBlockRdData1(11 downto 3);
                            usedListOffset <= curBlockRdData1(2 downto 0);
                        end if;
                        
                    -- 6 cycle latency to read from the used list.
                    -- While we wait, calculate the packet count that we are up to for both readouts.
                    -- We need the packet count for the output that we are not currently reading so that we can determine if this is the last time this memory will be read.
                    --
                    -- Example for pointers :
                    --
                    -- used list :          N   |  N+1  |  N+2  |  N+3  |  N+4  |  N+5  |  N+6 |  N+7  |  N+8  |  N+9  |  N+10  | 
                    --                                      ^               ^               ^                      ^
                    --                                      |               |               |                      |
                    --                               readFrameStart0        |         readFrameStart1              |
                    --                                      |          readCurBlock0        |                readCurBlock1
                    --                                      |                               |
                    --                    (corresponds to frameStartPacketCount0) (frameStartPacketCount1)
                    --
                    
                    when rd_wait_used_list1 =>
                        -- Get the number of packets past the start of frame for each readout.
                        -- "unwrap" readCurBlock with respect to readFrameStart;
                        -- i.e. readCurBlock0/1 must be greater than readFrameStart0/1
                        if (unsigned(readCurBlock0) < unsigned(readFrameStart0)) then
                            if (g_USE_2GBYTE) then
                                readCurBlock0 <= std_logic_vector(unsigned(readCurBlock0) + 1008); -- Used list wraps after 126*8 = 1008 entries.
                            else
                                readCurBlock0 <= std_logic_vector(unsigned(readCurBlock0) + 504); -- Used list wraps after 63*8 =  504 entries.
                            end if;
                        else
                            readCurBlock0 <= readCurBlock0;
                        end if;
                        
                        if (unsigned(readCurBlock1) < unsigned(readFrameStart1)) then
                            if (g_USE_2GBYTE) then
                                readCurBlock1 <= std_logic_vector(unsigned(readCurBlock1) + 1008); -- Used list wraps after 126*8 = 1008 entries.
                            else
                                readCurBlock1 <= std_logic_vector(unsigned(readCurBlock1) + 504); -- Used list wraps after 63*8 = 504 entries.
                            end if;
                        end if;
                        malloc_fsm <= rd_wait_used_list2;
                        
                    when rd_wait_used_list2 =>
                        -- Find the distance from the start of frame pointer
                        readOffset0FromStart0 <= std_logic_vector(unsigned(readCurBlock0) - unsigned(readFrameStart0));
                        readOffset1FromStart1 <= std_logic_vector(unsigned(readCurBlock1) - unsigned(readFrameStart1));
                        malloc_fsm <= rd_wait_used_list3;
                        
                    when rd_wait_used_list3 =>
                        -- Add the offset from the start of frame pointer to the packet count at the start of frame pointer,
                        -- to get the packet count at the current pointer for each readout.
                        readPacketCount0 <= std_logic_vector(unsigned(frameStartPacketCount0) + unsigned(readOffset0FromStart0));
                        readPacketCount1 <= std_logic_vector(unsigned(frameStartPacketCount1) + unsigned(readOffset1FromStart1));
                        malloc_fsm <= rd_wait_used_list4;
                        
                    when rd_wait_used_list4 =>
                        -- How far ahead each read is compared with the current start of frame for the other readout.
                        -- Note these could be negative, i.e. frame start for the other readout could be ahead of where we are reading now.
                        readOffset0FromStart1 <= std_logic_vector(unsigned(readPacketCount0) - unsigned(frameStartPacketCount1));
                        readOffset1FromStart0 <= std_logic_vector(unsigned(readPacketCount1) - unsigned(frameStartPacketCount0));
                        
                        -- readPacketCount0 and readPacketCount1 are the packet counts at the place in the "used" list that 
                        -- each readout is currently up to.
                        -- readPacketCount0 is calculated over the previous few states of the fsm, for read out 0, 
                        -- based on the packet count at the start of the frame, and how far through the frame we are.
                        if (unsigned(readPacketCount0) < unsigned(readPacketCount1)) then
                            readPacketCount0_lt_1 <= '1';
                        else
                            readPacketCount0_lt_1 <= '0';
                        end if;
                        if (unsigned(readPacketCount1) < unsigned(readPacketCount0)) then
                            readPacketCount1_lt_0 <= '1';
                        else
                            readPacketCount1_lt_0 <= '0';
                        end if;                        
                        
                        malloc_fsm <= rd_wait_used_list5;
                        
                    when rd_wait_used_list5 =>
 
                        -- Check : The block cannot be freed if it is required for preload for the next frame
                        -- From the diagram below, memory can be freed providing the pointer into the used list is
                        -- in the range [readFrameStart0, (readFrameStart0 + blocksPerFrame)]
                        --
                        --         |<-- preload0 -->|<-------- blocksPerFrame0 ---------------->|
                        --         |<-------- blocksPerFrame0 ---------------->|<-- preload0 -->|
                        --         ^                                           ^
                        --         |                                           |
                        --  readFrameStart0                       readFrameStart0 + blocksPerFrame
                        --                                      (i.e. next value of readFrameStart0)
                        --
                        if (unsigned(readOffset0FromStart0) < unsigned(blocksPerFrame0)) then
                            read0Freeable0 <= '1'; -- read for output buffer 0 is freeable as far as output buffer 0 is concerned.
                        else
                            read0Freeable0 <= '0';
                        end if;
                        if (signed(readOffset0FromStart1) < signed(blocksPerFrame1)) then -- signed arithmetic since readOffset0FromStart1 could be negative.
                            read0Freeable1 <= '1'; -- read for output buffer 0 is freeable as far as output buffer 1 is concerned.
                        else
                            read0Freeable1 <= '0';
                        end if;
                        
                        if (unsigned(readOffset1FromStart1) < unsigned(blocksPerFrame1)) then
                            read1Freeable1 <= '1'; -- read for output buffer 1 is freeable as far as output buffer 1 is concerned.
                        else
                            read1Freeable1 <= '0';
                        end if;
                        if (signed(readOffset1FromStart0) < signed(blocksPerFrame0)) then -- signed arithmetic since readOffset1FromStart0 could be negative.
                            read1Freeable0 <= '1'; -- read for output buffer 1 is freeable as far as output buffer 0 is concerned.
                        else
                            read1Freeable0 <= '0';
                        end if;
                        malloc_fsm <= rd_wait_used_list6;
                        
                    when rd_wait_used_list6 =>
                        -- Work out if this read is the last for this address
                        if readport = '0' then
                            if readPacketCount0_lt_1 = '1' and read0Freeable0 = '1' and read0Freeable1 = '1' and usedListOffset = "111" then
                                readFree <= '1';
                            else
                                readFree <= '0';
                            end if;
                        else
                            if readPacketCount1_lt_0 = '1' and read1Freeable0 = '1' and read1Freeable1 = '1' and usedListOffset = "111" then
                                readFree <= '1';
                            else
                                readFree <= '0';
                            end if;
                        end if;
                        malloc_fsm <= rd_wait_used_list7;
                        
                    when rd_wait_used_list7 =>  
                        -- Read data from the used list is now valid;
                        -- 
                        malloc_fsm <= rd_check_valid;
                        rdPtr <= "00000" & usedListRdData(15 downto 0) & usedListOffset;  -- rdPtr(23:0) is the address to read from in units of 8192 bytes.
                        rdValid <= usedListRdData(23 downto 16);
                        
                        -- If we are freeing this memory then we have done the last read from it.
                        -- So set the valid bits back to zero in the used list.
                        if readFree = '1' then
                            usedListWrEn <= '1';
                        end if;
                        usedListWrData <= (others => '0');  -- set to all unused.
                    -- have to assign :
                    --o_readAddress     : out std_logic_vector(31 downto 0);   -- HBM address to read from. Points to the start of a 2048 sample packet.
                    --o_readPacketCount : out std_logic_vector(31 downto 0);   -- packet count for this read address
                    --o_readchannel     : out std_logic_vector(15 downto 0);   -- Virtual channel at this address.
                    --o_readstart       : out std_logic;                       -- Indicates this is the first read for this channel
                    --o_readOK          : out std_logic;                       -- o_readAddress points to valid data
                    --o_readfree                    
                    when rd_check_valid =>
                        malloc_fsm <= rd_update_ptr;
                        usedListWrEn <= '0';
                        readAddress <= std_logic_vector(unsigned(rdPtr) + unsigned(baseAddr));  -- The address in the HBM to read from, in units of 8192 bytes.
                        if readport = '0' then
                            -- readFrameStart0 is the pointer into the used list for the start of frame; usedListAddr
                            readPacketCount <= readPacketCount0;  
                            if ((usedListAddr & usedListOffset) = readFrameStart0) then
                                readStart <= '1';
                            else
                                readStart <= '0';
                            end if;
                            readValid0 <= '1';
                        else
                            readPacketCount <= readPacketCount1;
                            if ((usedListAddr & usedListOffset) = readFrameStart1) then
                                readStart <= '1';
                            else
                                readStart <= '0';
                            end if;
                            readValid1 <= '1';
                        end if;
                        readChannel <= "0000000" & curBlockRdChannel;
                        -- Check that this group of 2048 samples is valid;
                        readOK <= rdValid(to_integer(unsigned(usedListOffset)));
                        
                        -- Check : Will this be the last thing in the frame for this channel ? 
                        -- This is not needed until the next state (rd_update_ptr), but is put here to pipeline the calculation.
                        if ((readport = '0' and (readOffset0FromStart0 = lastBlockInFrame0)) or 
                            (readport = '1' and (readOffset1FromStart1 = lastBlockInFrame1))) then
                            lastInFrame <= '1';
                        else
                            lastInFrame <= '0';
                        end if;
                        -- check i
                        if ((readport = '0' and readChannelCount0 = readChannelCountMax0) or
                            (readport = '1' and readChannelCount1 = readChannelCountMax1)) then
                            lastChannel <= '1';
                        else
                            lastChannel <= '0';
                        end if;
                        -- Find the new pointer into the used list, assuming we are at the end of the frame and need to rewind to the start of the preload for the next frame
                        -- Note: this can give a negative number, which needs to be unwrapped before writing back to the memory.
                        if readport = '0' then
                            usedListPtrMinusPreload <= std_logic_vector(unsigned(usedListPtrFull) + 1 - unsigned(preload0));
                        else
                            usedListPtrMinusPreload <= std_logic_vector(unsigned(usedListPtrFull) + 1 - unsigned(preload1));
                        end if;
                        -- Find the new value of readFrameStart. Used in the next state, but only if we have finished the frame.
                        if readport = '0' then
                            nextReadFrameStart <= std_logic_vector(unsigned(readFrameStart0) + unsigned(blocksPerFrame0));
                        else
                            nextReadFrameStart <= std_logic_vector(unsigned(readFrameStart1) + unsigned(blocksPerFrame1));
                        end if;
                        
                    -- Update the pointer into the used list    
                    when rd_update_ptr =>
                        malloc_fsm <= rd_done;
                        if readport = '0' then
                            curBlockWrEn0 <= '1';
                            curBlockWrEn1 <= '0';
                        else
                            curBlockWrEn0 <= '0';
                            curBlockWrEn1 <= '1';
                        end if;
                        curBlockWrChannel <= curBlockRdChannel;  -- Write the updated values back to the same place we read the original value from.
                        readValid0 <= '0';
                        readValid1 <= '0';
                        -- Check : Did we just send the last thing in the frame for this channel ?
                        if lastInFrame = '1' then
                            -- Rewind the pointer into the used list ready for the preload of the next frame.
                            if (signed(usedListPtrMinusPreload) < 0) then
                                correctedPtr_v := std_logic_vector(signed(usedListPtrMinusPreload) + c_MAX_USED_LIST_PTR + 1);
                                curBlockWrData0 <= correctedPtr_v;
                                curBlockWrData1 <= correctedPtr_v;
                            else
                                curBlockWrData0 <= usedListPtrMinusPreload;
                                curBlockWrData1 <= usedListPtrMinusPreload;
                            end if;
                            -- Check - was this the last channel ?
                            if (lastChannel = '1') then
                                -- Set the channel count back to zero, and update the start of frame pointer into the used list.
                                if readport = '0' then
                                    readChannelCount0 <= (others => '0');
                                    if (unsigned(nextReadFrameStart) > c_MAX_USED_LIST_PTR) then
                                        readFrameStart0 <= std_logic_vector(unsigned(nextReadFrameStart) - (c_MAX_USED_LIST_PTR + 1));
                                    else
                                        readFrameStart0 <= nextReadFrameStart;
                                    end if;
                                    frameStartPacketCount0 <= std_logic_vector(unsigned(frameStartPacketCount0) + unsigned(blocksPerFrame0));
                                else
                                    readChannelCount1 <= (others => '0');
                                    if (unsigned(nextReadFrameStart) > c_MAX_USED_LIST_PTR) then
                                        readFrameStart1 <= std_logic_vector(unsigned(nextReadFrameStart) - (c_MAX_USED_LIST_PTR + 1));
                                    else
                                        readFrameStart1 <= nextReadFrameStart;
                                    end if;
                                    frameStartPacketCount1 <= std_logic_vector(unsigned(frameStartPacketCount1) + unsigned(blocksPerFrame1));
                                end if;
                            else
                                -- Increment the channel count
                                if readport = '0' then
                                    readChannelCount0 <= std_logic_vector(unsigned(readChannelCount0) + 1);
                                else
                                    readChannelCount1 <= std_logic_vector(unsigned(readChannelCount1) + 1);
                                end if;
                            end if;
                        else
                            -- increment the pointer into the used list.
                            if (unsigned(usedListPtrFull) = c_MAX_USED_LIST_PTR) then
                                curBlockWrData0 <= (others => '0');
                                curBlockWrData1 <= (others => '0');
                            else
                                curBlockWrData0 <= std_logic_vector(unsigned(usedListPtrFull) + 1);  -- assign both curBlockWrDat0 and curBlockWrData1
                                curBlockWrData1 <= std_logic_vector(unsigned(usedListPtrFull) + 1);  -- write enable ensures only one gets written.
                            end if;
                        end if;
                    
                    when rd_done =>
                        curBlockWrEn0 <= '0';
                        curBlockWrEn1 <= '0';
                        malloc_fsm <= idle; 
                    ----------------------------------------------------------------------
                    -- free_* states move pointers from the "toBeFree" FIFOs to the free list
                    -- Steps :
                    --  (1) check there is something to read in the "toBeFree" FIFO. There should be by design; If there is not, something has gone badly wrong. 
                    --  (2) Read the "toBeFree" FIFO
                    --  (3) write to the free list
                    --  (4) increment the pointer to the end of the free list
                    
                    when free_start =>
                        if ((readport = '0' and unsigned(toBeFreeFIFOSize0) = 0) or (readport = '1' and unsigned(toBeFreeFIFOSize1) = 0)) then
                            badFreeFIFOState <= '1';
                            malloc_fsm <= idle;
                        else
                            malloc_fsm <= free_wait_rd_toBeFreeFIFO;
                        end if;
                        if readPort = '0' then
                            toBeFreeRdEn0 <= '1';
                            toBeFreeRdEn1 <= '0';
                        else
                            toBeFreeRdEn0 <= '0';
                            toBeFreeRdEn1 <= '1';
                        end if;
                        
                    when free_wait_rd_toBeFreeFIFO =>
                        malloc_fsm <= free_rd_toBeFreeFIFO;
                        toBeFreeRdEn0 <= '0';
                        toBeFreeRdEn1 <= '0';
                        
                    when free_rd_toBeFreeFIFO =>
                        if readPort = '0' then
                            freeListWrData <= toBeFreeDout0;
                            free0Done <= '1';
                        else
                            freeListWrData <= toBeFreeDout1;
                            free1Done <= '1';
                        end if;
                        freeListWrEn <= '1';
                        malloc_fsm <= free_done;
                        
                    when free_done =>
                        freeListWrEn <= '0';
                        free0Done <= '0';
                        free1Done <= '0';
                        if (freeListWrAddr = freeMax) then
                            freeListWrAddr <= (others => '0');
                        else
                            freeListWrAddr <= std_logic_vector(unsigned(freeListWrAddr) + 1);
                        end if;
                        malloc_fsm <= idle;
                    
                    ----------------------------------------------------------------------
                    -- Reset_* states put everything in a reset state.
                    -- Reset is triggered by a MACE register.
                    -- Configuration parameters such as the size of the free list and the start packet count should be set prior to triggering a reset.
                    when reset_start =>
                        freeListWrAddr <= (others => '0');
                        freeListWrData <= (others => '0');
                        freeListWrEn <= '1';
                        freeListRdAddr <= (others => '0');
                        freeMax <= std_logic_vector(unsigned(i_freeMax) - 1);  -- so i_freeMax is the true size of the free list, while freeMax is the highest address into the free list.
                        
                        usedListChannel <= (others => '0');
                        usedListAddr <= (others => '0');
                        usedListWrData <= (others => '0');
                        usedListWrEn <= '1';
                        
                        toBeFreeRdEn0 <= '0';
                        toBeFreeRdEn1 <= '0';
                        -- i_preload0 and i_preload1 are the number of samples to preload for each readout.
                        -- This module only deals in units of LFAA blocks, so we have to round up the number of samples to a multiple of 2048
                        if i_preload0(10 downto 0) /= "00000000000" then
                            preload0 <= std_logic_vector(unsigned(preload0LFAABlocks) + 1);
                        else
                            preload0 <= preload0LFAABlocks;
                        end if;
                        if i_preload1(10 downto 0) /= "00000000000" then
                            preload1 <= std_logic_vector(unsigned(preload1LFAABlocks) + 1);
                        else
                            preload1 <= preload1LFAABlocks;
                        end if;
                        blocksPerFrame0 <= '0' & i_blocksPerFrame0; -- extend to 12 bits; this is used as a signed value in some places.
                        blocksPerFrame1 <= '0' & i_blocksPerFrame1;
                        
                        readFrameStart0 <= (others => '0');
                        
                        readChannelCount0 <= (others => '0');
                        readChannelCount1 <= (others => '0');
                        frameStartPacketCount0 <= i_scanStartPacketCount;
                        frameStartPacketCount1 <= i_scanStartPacketCount;
                        
                        readChannelCountMax0 <= std_logic_vector(unsigned(i_totalChannels0) - 1);
                        readChannelCountMax1 <= std_logic_vector(unsigned(i_totalChannels1) - 1);
                        
                        maxFutureCount <= i_maxFutureCount;
                        baseAddr <= i_baseAddr;
                        
                        -- Clear the block pointers memory
                        curBlockWrChannel <= (others => '0');
                        curBlockRdChannel <= (others => '0');
                        curBlockWrData0 <= (others => '0');
                        curBlockWrData1 <= (others => '0');
                        curBlockWrEn0 <= '0';
                        curBlockWrEn1 <= '0';
                        
                        free0Done <= '0';
                        free1Done <= '0';
                        readAddress <= (others => '0');
                        readChannel <= (others => '0');
                        readFree <= '0';
                        readStart <= '0';

                        malloc_fsm <= reset_start1;
                        
                    when reset_start1 =>
                        -- The offset between the first block read out for the two different output streams
                        preloadDifference_v := std_logic_vector(unsigned(preload0) - unsigned(preload1));
                        readFrameStart1 <= preloadDifference_v;  -- second readout starts a bit further in because of the shorter preload
                        frameStartPacketCount1 <= std_logic_vector(unsigned(frameStartPacketCount1) + unsigned(preloadDifference_v));
                        
                        curBlockWrData1 <= preloadDifference_v;
                        curBlockWrEn0 <= '1';
                        curBlockWrEn1 <= '1';
                        malloc_fsm <= reset_init_lists;
                        
                    when reset_init_lists => 
                        
                        if (signed(readFrameStart1) < 0) then
                            o_preload1_gt_preload0 <= '1';
                        else
                            o_preload1_gt_preload0 <= '0';
                        end if;
                        
                        -- Put all blocks in the free list, clear all the used lists.
                        -- clearing the free list takes c_FREE_LIST_MAX cycles, i.e. either 16384 or 32768 (for 1G and 2G cases)
                        -- clearing the used list takes 384 channels * (63 or 126 pointers) = 24192 or 48384 clocks (for 1G and 2G cases)
                        -- clearing the Block pointers memory takes 384 clocks.
                        
                        -- free list address and data
                        freeListWrEn <= '1';
                        if (unsigned(freeListWrAddr) < c_FREE_LIST_MAX) then
                            freeListWrAddr <= std_logic_vector(unsigned(freeListWrAddr) + 1);
                            freeListWrData <= std_logic_vector(unsigned(freeListWrData) + 1);
                        end if;

                        -- curBlock address
                        if (unsigned(curBlockWrChannel) < 384) then
                            curBlockWrChannel <= std_logic_vector(unsigned(curBlockWrChannel) + 1);
                            curBlockWrEn0 <= '1';
                            curBlockWrEn1 <= '1';
                        else
                            curBlockWrEn0 <= '0';
                            curBlockWrEn1 <= '0';
                        end if;
                        
                        -- Used list address
                        -- This takes the longest to write to (24192 clocks for the 1G case), so when it is done the state machine moves on.
                        usedListWrEn <= '1';
                        if (unsigned(usedListAddr) < c_USED_LIST_MAX_ADDR) then
                            usedListAddr <= std_logic_vector(unsigned(usedListAddr) + 1);
                        else
                            usedListAddr <= (others => '0');
                            if (unsigned(usedListChannel) < 383) then
                                usedListChannel <= std_logic_vector(unsigned(usedListChannel) + 1);
                            else
                                malloc_fsm <= reset_done;
                            end if;
                        end if;
                        
                    when reset_done =>
                        freeListWrAddr <= (others => '0'); -- i.e. wrapped back to the same point of freeListRdAddr, ready for the next freed pointer.
                        freeListWrEn <= '0';
                        usedListWrEn <= '0';
                        malloc_fsm <= idle;
                    
                    ----------------------------------------------------------    
                    when others =>
                        malloc_fsm <= idle;
                    
                end case;
            end if;
            
            -- These error signals are all updated in the state machine; 
            -- this code is placed after the state machine so that reset overrides the state machine assignment.
            if (monitor_rst = '1') then
                inputLateCount <= (others => '0');
                inputLateStickyBit <= '0';
                inputTooSoonCount <= (others => '0');
                inputTooSoonStickyBit <= '0';
                duplicatesStickyBit <= '0';
                duplicatesCount <= (others => '0');
                badFreeFIFOState <= '0';
            end if;

            
            
        end if;
    end process;
    
    
    -- Free list holds pointers to all the free 64K memory blocks
    freelist : entity ct_vfc_lib.free_list_URAM
    generic map (
        g_USE_2GBYTE => g_USE_2GBYTE
    ) port map (
        i_clk    => i_clk,
        i_wrAddr => freeListWrAddr, -- in(15:0);
        i_wrData => freeListWrData, -- in(15:0);
        i_wrEn   => freeListWrEn,   -- in std_logic;
        i_rdAddr => freeListRdAddr, -- in(15:0); 
        o_rdData => freeListRdData  -- out(15:0), 3 cycle read latency.
    );
    
    -- Used list holds pointers to all the used 64K memory blocks, as well as 8 bits to indicate which groups of 2048 time samples are valid.
    -- Each 64K block holds 8 x 2048 time samples. 2048 time samples is one input packet from LFAA.
    -- rdData and wrData have
    --  bits(23:16) = one bit per 2048 time samples to indicate valid data.
    --  bits(15:0) = pointer to a 64K block of HBM.
    -- Warning : No error checking; if the Write address is out of bounds (e.g. 0 to 62 for 1Gbyte option), it will return a value for the next channel.
    usedList : entity ct_vfc_lib.used_list_URAM
    generic map (
        g_USE_2GBYTE => g_USE_2GBYTE
    ) port map (
        i_clk  => i_clk,
        i_wrChannel => usedListChannel, -- in(8:0); Virtual channel
        i_wrAddr    => usedListAddr(6 downto 0),    -- in(6:0); Index of the pointer in the channel list (0 to 62 for 1Gbyte version, of 0 to 125 for 2Gbyte version)
        i_wrData    => usedListWrData,    -- in(23:0); pointer value to write to the ultra RAM
        i_wrEn      => usedListWrEn,      -- in std_logic; i_wrChannel, i_wrAddr, i_wrData are valid.
        i_rdChannel => usedListChannel,          -- in(8:0); Virtual channel to read.
        i_rdAddr    => usedListAddr(6 downto 0), -- in(7:0); Index of the pointer in the channel list to be read (0 to 62 for 1Gbyte version, of 0 to 125 for 2Gbyte version).
        o_rdData    => usedListRdData              -- out(23:0); Read data. 6 clock latency from i_rdChannel to o_rdData.
    );
    
    -- Read block holds the place in the used list that we are reading from for each channel, for both readouts.
    -- There are 8 LFAA packets worth of 2048 samples within each 64KB block allocated in the HBM;
    -- The low 3 bits of the data to/from this memory are the LFAA block, high 8 bits are the index into the used list.
    blockptrs : entity ct_vfc_lib.ct_vfc_blockUsedMem
    Port map (
        i_clk    => i_clk, --  in std_logic;
        -- memory writes.
        i_wrAddr => curBlockWrChannel,  -- in(8:0);
        i_wrData0 => curBlockWrData0,   -- in(11:0); -- data to write
        i_wrData1 => curBlockWrData1,   -- in(11:0)
        i_wrEn0  => curBlockWrEn0,      -- in std_logic;
        i_wrEn1  => curBlockWrEn1,      -- in std_logic;
        -- memory reads
        i_rdAddr  => curBlockRdChannel, -- in(8:0);  -- Reads both pointers at the same time.
        o_rdData0 => curBlockRdData0,   -- out(11:0); -- 2 cycle latency.
        o_rdData1 => curBlockRdData1    -- out(11:0)
    );
    
    
    -- Free FIFOs. Pointers to be freed are written into this FIFO as the address is sent to the output buffer.
    -- Once the data is read from the HBM, the output buffer notifies via i_free0/1, and the pointer is moved 
    -- from this fifo to the free list.
    -- Since the main fsm may be busy when a free request comes in, we have a counter here of the number of queued free requests.
    --
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            if (malloc_fsm = reset_start or malloc_fsm = reset_init_lists) then
                resetFreed <= '1';
            else
                resetFreed <= '0';
            end if;
            
            if resetFreed = '1' then
                toBeFreed0 <= (others => '0');
                toBeFreed1 <= (others => '0');
            else
                if i_free0 = '1' and free0Done = '0' then
                    toBeFreed0 <= std_logic_vector(unsigned(toBeFreed0) + 1);
                elsif i_free0 = '0' and free0Done = '1' then
                    toBeFreed0 <= std_logic_vector(unsigned(toBeFreed0) - 1);
                end if;
                
                if i_free1 = '1' and free1Done = '0' then
                    toBeFreed1 <= std_logic_vector(unsigned(toBeFreed1) + 1);
                elsif i_free1 = '0' and free1Done = '1' then
                    toBeFreed1 <= std_logic_vector(unsigned(toBeFreed1) - 1);
                end if;
            end if;
            
            if readValid0 = '1' and i_readReady0 = '1' and readFree = '1' then
                toBeFreeWrEn0 <= '1';
                toBeFreeDin0 <= rdPtr(18 downto 3);
            else
                toBeFreeWrEn0 <= '0';
            end if;
            
            if readValid1 = '1' and i_readReady1 = '1' and readFree = '1' then
                toBeFreeWrEn1 <= '1';
                toBeFreeDin1 <= rdPtr(18 downto 3);
            else
                toBeFreeWrEn1 <= '0';
            end if;            
            
        end if;
    end process;
    

    -- xpm_fifo_sync: Synchronous FIFO
    -- Xilinx Parameterized Macro, version 2019.1
    -- FIFO for pointers that are "to be freed" in the future, i.e.
    --  the pointer has been given to the output buffer, but the output buffer hasn't yet finished reading it from the HBM.
    toFreeFIFO0_inst : xpm_fifo_sync
    generic map (
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "distributed", -- String
        FIFO_READ_LATENCY => 1,     -- DECIMAL
        FIFO_WRITE_DEPTH => 32,     -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 6,   -- DECIMAL
        READ_DATA_WIDTH => 16,      -- DECIMAL
        READ_MODE => "std",         -- String
        --SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages     
        USE_ADV_FEATURES => "0004", -- String; 0x4 = only wr_data_count enabled. bit:enable => 0:overflow flag, 1:prog_full flag, 2:wr_data_count, 3:almost_full flag, 4:wr_ack flag, 8:underflow flag, 9:prog_empty flag, 10:rd_data_count, 11:almost_empty flag, 12:data_valid flag
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 16,     -- DECIMAL
        WR_DATA_COUNT_WIDTH => 6    -- DECIMAL
    )
    port map (
        almost_empty => open,     -- 1-bit output: Almost Empty : When asserted, this signal indicates that only one more read can be performed before the FIFO goes to empty.
        almost_full => open,      -- 1-bit output: Almost Full: When asserted, this signal indicates that only one more write can be performed before the FIFO is full.
        data_valid => open,       -- 1-bit output: Read Data Valid: When asserted, this signal indicates that valid data is available on the output bus (dout).
        dbiterr => open,          -- 1-bit output: Double Bit Error: Indicates that the ECC decode detected a double-bit error and data in the FIFO core is corrupted.
        dout => toBeFreeDout0,    -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven when reading the FIFO.
        empty => toBeFreeEmpty0,  -- 1-bit output: Empty Flag: When asserted, this signal indicates that the FIFO is empty. Read requests are ignored when the FIFO is empty,
        full => toBeFreeFull0,    -- 1-bit output: Full Flag: When asserted, this signal indicates that the FIFO is full.
        overflow => open,         -- 1-bit output: Overflow: This signal indicates that a write request (wren) during the prior clock cycle was rejected, because the FIFO is full
        prog_empty => open,       -- 1-bit output: Programmable Empty.
        prog_full => open,        -- 1-bit output: Programmable Full.
        rd_data_count => open,    -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates the number of words read from the FIFO.
        rd_rst_busy => open,      -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO read domain is currently in a reset state.
        sbiterr => open,          -- 1-bit output: Single Bit Error: Indicates that the ECC decoder detected and fixed a single-bit error.
        underflow => open,        -- 1-bit output: Underflow: Indicates that the read request (rd_en) during the previous clock cycle was rejected because the FIFO is empty. Under flowing the FIFO is not destructive to the FIFO.
        wr_ack => open,           -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => toBeFreeFIFOSize0, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,      -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO write domain is currently in a reset state.
        din => toBeFreeDin0,      -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',     -- 1-bit input: Double Bit Error Injection: Injects a double bit error the ECC feature is used on block RAMs or UltraRAM macros.
        injectsbiterr => '0',     -- 1-bit input: Single Bit Error Injection: Injects a single bit error if the ECC feature is used on block RAMs or UltraRAM macros.
        rd_en => toBeFreeRdEn0,   -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO.
        rst => resetFreed,        -- 1-bit input: Reset: Must be synchronous to wr_clk. The clock(s) can be unstable at the time of applying reset, but reset must be released only after the clock(s) is/are stable.
        sleep => '0',             -- 1-bit input: Dynamic power saving- If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_clk,          -- 1-bit input: Write clock: Used for write operation. wr_clk must be a free running clock.
        wr_en => toBeFreeWrEn0    -- 1-bit input: Write Enable: If the FIFO is not full, asserting this signal causes data (on din) to be written to the FIFO.
    );
    

    toFreeFIFO1_inst : xpm_fifo_sync
    generic map (
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "distributed", -- String
        FIFO_READ_LATENCY => 1,     -- DECIMAL
        FIFO_WRITE_DEPTH => 32,     -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 6,   -- DECIMAL
        READ_DATA_WIDTH => 16,      -- DECIMAL
        READ_MODE => "std",         -- String
        --SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages     
        USE_ADV_FEATURES => "0004", -- String; 0x4 = only wr_data_count enabled. bit:enable => 0:overflow flag, 1:prog_full flag, 2:wr_data_count, 3:almost_full flag, 4:wr_ack flag, 8:underflow flag, 9:prog_empty flag, 10:rd_data_count, 11:almost_empty flag, 12:data_valid flag
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 16,     -- DECIMAL
        WR_DATA_COUNT_WIDTH => 6    -- DECIMAL
    )
    port map (
        almost_empty => open,     -- 1-bit output: Almost Empty : When asserted, this signal indicates that only one more read can be performed before the FIFO goes to empty.
        almost_full => open,      -- 1-bit output: Almost Full: When asserted, this signal indicates that only one more write can be performed before the FIFO is full.
        data_valid => open,       -- 1-bit output: Read Data Valid: When asserted, this signal indicates that valid data is available on the output bus (dout).
        dbiterr => open,          -- 1-bit output: Double Bit Error: Indicates that the ECC decode detected a double-bit error and data in the FIFO core is corrupted.
        dout => toBeFreeDout1,    -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven when reading the FIFO.
        empty => toBeFreeEmpty1,  -- 1-bit output: Empty Flag: When asserted, this signal indicates that the FIFO is empty. Read requests are ignored when the FIFO is empty,
        full => toBeFreeFull1,    -- 1-bit output: Full Flag: When asserted, this signal indicates that the FIFO is full.
        overflow => open,         -- 1-bit output: Overflow: This signal indicates that a write request (wren) during the prior clock cycle was rejected, because the FIFO is full
        prog_empty => open,       -- 1-bit output: Programmable Empty.
        prog_full => open,        -- 1-bit output: Programmable Full.
        rd_data_count => open,    -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates the number of words read from the FIFO.
        rd_rst_busy => open,      -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO read domain is currently in a reset state.
        sbiterr => open,          -- 1-bit output: Single Bit Error: Indicates that the ECC decoder detected and fixed a single-bit error.
        underflow => open,        -- 1-bit output: Underflow: Indicates that the read request (rd_en) during the previous clock cycle was rejected because the FIFO is empty. Under flowing the FIFO is not destructive to the FIFO.
        wr_ack => open,           -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => toBeFreeFIFOSize1, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,      -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO write domain is currently in a reset state.
        din => toBeFreeDin1,      -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',     -- 1-bit input: Double Bit Error Injection: Injects a double bit error the ECC feature is used on block RAMs or UltraRAM macros.
        injectsbiterr => '0',     -- 1-bit input: Single Bit Error Injection: Injects a single bit error if the ECC feature is used on block RAMs or UltraRAM macros.
        rd_en => toBeFreeRdEn1,   -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO.
        rst => resetFreed,        -- 1-bit input: Reset: Must be synchronous to wr_clk. The clock(s) can be unstable at the time of applying reset, but reset must be released only after the clock(s) is/are stable.
        sleep => '0',             -- 1-bit input: Dynamic power saving- If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_clk,          -- 1-bit input: Write clock: Used for write operation. wr_clk must be a free running clock.
        wr_en => toBeFreeWrEn1    -- 1-bit input: Write Enable: If the FIFO is not full, asserting this signal causes data (on din) to be written to the FIFO.
    );    
    
end Behavioral;


