----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 30.10.2020 22:21:03
-- Module Name: ct_atomic_pst_out - Behavioral
-- Description: 
--    Corner turn between the filterbanks and the beamformer for PST processing. 
-- 
-- Data coming in from the filterbanks :
--   3 dual-pol channels, with burst of 216 fine channels at a time.
--   Total number of bytes per clock coming in is  (3 channels)*(2 pol)*(2+2 complex) = 24 bytes.
--   with roughly 216 out of every 256 clocks active.
--   Total data rate in is thus roughly (24 bytes * 8 bits)*216/256 * 450 MHz = 73 Gb/sec (this is the average data rate while data is flowing)
--   Actual total data rate in is (long term average)
--    (4/3 oversampling) * (216/256 fine channels used) * (1/1080ns sampling period) * (2 pol) * (16+16 bits/sample) * (1024 channels) = 68.3 Gb/sec 
--
-- Storing to HBM
--   We get 24*2048*1080ns = 53.08416 ms of data for 3 channels, then 53.08416ms for the next 3 channels, up to 1024 channels.
--   The beamformers need this to be rearranged so the data for all stations can be processed before moving to the next time.
--   The number of PST filterbank outputs per frame is related to the number of LFAA packets per frame by
--     (4/3) * (LFAA packets * (2048/256)); so 24 LFAA packets per frame = 256 filterbank frames
--   Each virtual channel is stored as a contiguous block of memory, so the address in the memory is calculated as:
--     Given F = Fine Channel  (0 to 215)
--           V = virtual channel (0 to up to 1023)
--           T = Time within the block (0 to 256, maximum first stage corner turn length is 24 LFAA blocks)
--     Byte Address =  T*1024*2048 + V*2048 + F*8
--   Note :
--     * 8 bytes per fine channel = (2 pol) * (2+2 complex)
--     * 216 fine channels. 216*8 = 1728 bytes, so the first 1728 bytes are used in each 2048 byte block
--     * 3 virtual channels come in at a time, so 3x1728 byte writes are done that all fall in a 6kbyte piece of HBM
--     * Then writes skip ahead by 2 Mbytes to the next time sample
--     * On the read side, blocks of 1728 bytes are read (all fine channels for a single coarse channel) for 
--       all stations used in the beamforming.
--   
--   There is a buffer in this module to hold data from the three streams while it is being written to the HBM
--   The buffer is 512 deep x 512 bits wide.
--      - Split into 3 buffers, each 128 deep (some memory spare), each holds data for a particular virtual channel.
--        (3 buffers, since 3 simultaneous virtual channels are coming in).
--        - Each of the three buffers is 128 deep x 512 bits wide
--          - Each of the smaller buffers is split into 4 regions, each of which is (27 deep) x (512 bits) wide.
--            27 x 512 bits = 1728 bytes = sufficient space for 216 fine channels (= 1 PST filterbank output frame for 1 virtual channel).
--            So as soon as a frame is written in, all the write requests are generated to write it to HBM
--   Writes to the HBM for each packet consist of 1 write of 27 words, for 27 x 512 bit words total.  
--
-- Data out to the beamformer
--   Use a 400 MHz clock, 192 bits/clock, for a (peak) data rate of  76.8 Gbits/sec. 
--
--   192 bits = 3 * 64 bits = 3 dual-pol (16+16 bit complex) samples
--   
--   Data is processed by the beamformer in groups of 216 fine channels
--   The order of data going to the beamformer :
--
--    For timeGroup = 0:(<time samples per corner turn>/32 - 1) -- 53 ms corner turn, so this is 0:(256/32-1) = 0:7
--       For Coarse = 0:(i_coarse-1)                            -- For 512 stations, 2 coarse, this is 0:1
--          For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
--             For Station = 0:(i_stations-1)
--                For fine_offset = 0:3:213                     -- Total of 216 fine channels per SPS channel
--                   Send a 192 bit word (i.e. 3 fine channels x (2 pol) x (16+16 bit complex data)).
--
----------------------------------------------------------------------------------
library IEEE, ct_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--library DSP_top_lib
--use DSP_top_lib.DSP_top_pkg.all;
USE common_lib.common_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;

entity ct2_out is
    generic (
        g_PST_BEAMS : integer := 16;
        g_USE_META : boolean := FALSE;  -- Put meta data into the memory in place of the actual data, to make it easier to find bugs in the corner turn. 
        g_PACKETS_PER_FRAME  : std_logic_vector(9 downto 0) -- Packets per frame; Should be 256 (for 24 SPS packets per frame)
    );
    port(
        -- Parameters, in the i_axi_clk domain.
        i_stations : in std_logic_vector(10 downto 0); -- up to 1024 stations
        i_coarse   : in std_logic_vector(9 downto 0);  -- Number of coarse channels.
        i_virtualChannels : in std_logic_vector(10 downto 0); -- total virtual channels (= i_stations * i_coarse)
        
        i_HBMBufferEnable : in std_logic_vector(3 downto 0);
        o_HBMBuf0PacketCount : out std_logic_vector(36 downto 0);
        o_HBMBuf1PacketCount : out std_logic_vector(36 downto 0);
        
        i_beams_enabled : in std_logic_vector(7 downto 0);
        i_bad_polynomials : in std_logic;
        
        -- Indicate start of readout of a corner turn frame
        -- Outputs on i_axi_clk
        o_readout_start : out std_logic;
        o_readout_frame : out std_logic_vector(36 downto 0); -- Corner turn frame that we are about to start reading out, relative to the SKA epoch
        -- Polynomial FIFO is empty, on i_BF_clk 
        i_poly_fifo_empty : in std_logic;
        -- Number of stations and number of coarse channels to use for readout, on bf_clk
        o_stations_bf_clk : out std_logic_vector(10 downto 0);
        o_coarse_bf_clk : out std_logic_vector(9 downto 0);
        --i_BFJonesSelect : in std_logic;
        --i_BFPhaseSelect : in std_logic;
        --i_JonesSwitch   : in std_logic_vector(31 downto 0); -- Packet count to switch to the new value of BFJonesSelect
        --i_phaseSwitch   : in std_logic_vector(31 downto 0); -- Packet count to switch to the new value of BFPhaseSelect
        i_rst : in std_logic;
        -- Data in from the PST filterbanks; bursts of 216 clocks for each channel.
        -- 
        i_sof            : in std_logic; -- pulse high at the start of every frame. (1 frame is 53ms of data).
        i_FB_clk         : in std_logic; -- filterbank clock, expected to be 450 MHz
        i_frameCount     : in std_logic_vector(36 downto 0); -- Corner turn frames since SKA epoch, same for all simultaneous output streams.
        i_virtualChannel : in t_slv_16_arr(2 downto 0);      -- 3 virtual channels, one for each of the PST data streams.
        i_HeaderValid    : in std_logic_vector(2 downto 0);
        i_data           : in t_slv_64_arr(2 downto 0); -- (2 pol)x(16+16 bit complex) = 64 bits, for each of 3 virtual channels.
        i_dataValid      : in std_logic;
        
        -- Data out to the beamformer
        i_BF_clk  : in std_logic; -- beamformer clock, expected to be 400 MHz
        o_data    : out std_logic_vector(191 downto 0); -- 3 consecutive fine channels delivered every clock. 6 clock latency relative to the other beamformer outputs, to allow time to get the phase ready in the beamformers.
        o_flagged : out std_logic_vector(2 downto 0);  -- o_flagged is aligned with o_data.
        o_fine    : out std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        o_coarse  : out std_logic_vector(9 downto 0);  -- Coarse channel, indexed from 0 to however many distinct coarse channels there are.
        o_firstStation : out std_logic;
        o_lastStation  : out std_logic;
        o_timeStep     : out std_logic_vector(4 downto 0);
        o_virtualChannel  : out std_logic_vector(9 downto 0);  -- coarse channel count.
        o_packetCount : out std_logic_vector(39 downto 0); -- The PST output packet count for this packet, relative to the SKA epoch, units of 6.63552 ms
        o_outputPktOdd : out std_logic; -- Alternates between 0 and 1 for each burst of data that corresponds to a set of 9 output packets.
        o_good_polynomials : out std_logic; -- good station polynomials.
        o_valid   : out std_logic;
        -- AXI interface to the HBM
        -- Corner turn between filterbanks and beamformer
        i_axi_clk : in std_logic;
        -- aw bus = write address
        m02_axi_awvalid  : out std_logic;
        m02_axi_awready  : in std_logic;
        m02_axi_awaddr   : out std_logic_vector(29 downto 0);
        m02_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m02_axi_wvalid    : out std_logic;
        m02_axi_wready    : in std_logic;
        m02_axi_wdata     : out std_logic_vector(511 downto 0);
        m02_axi_wlast     : out std_logic;
        -- b bus - write response
        m02_axi_bvalid    : in std_logic;
        m02_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m02_axi_arvalid   : out std_logic;
        m02_axi_arready   : in std_logic;
        m02_axi_araddr    : out std_logic_vector(29 downto 0);
        m02_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m02_axi_rvalid    : in std_logic;
        m02_axi_rready    : out std_logic;
        m02_axi_rdata     : in std_logic_vector(511 downto 0);
        m02_axi_rlast     : in std_logic;
        m02_axi_rresp     : in std_logic_vector(1 downto 0);
        
        ---------------------------------------------------
        -- Monitoring and Error signals on the i_axi_clk domain.
        o_bufferOverflowError : out std_logic; -- buffer between data input from the filterbanks and the HBM has overflowed; this should never happen.
        o_readoutError : out std_logic;  -- readout to the beamformer didn't finish before the next readout started. This should never happen.
        o_readoutClocks : out std_logic_vector(31 downto 0); -- Number of beamformer (400MHz) clocks required to read the most recent frame out from HBM
        o_readInClocks : out std_logic_vector(31 downto 0);   -- Number of axi (300MHz) clocks required to write the most recent frame into HBM
        o_readInAllClocks : out std_logic_vector(31 downto 0); -- Number of axi (300MHz) clocks between the start of one frame and the start of the next.
        o_dataMismatch : out std_logic;
        --
        o_sof_count         : out std_logic_vector(31 downto 0);
        o_fb_packet_count   : out std_logic_vector(31 downto 0);
        o_cur_frameCount    : out std_logic_vector(31 downto 0);
        o_recent_vcs        : out std_logic_vector(31 downto 0);
        o_dbg_packetInFrame : out std_logic_vector(31 downto 0);
        -- on the 400MHz clock.
        o_dataMismatchBFclk : out std_logic
    );
end ct2_out;

architecture Behavioral of ct2_out is

    signal data0, data1, data1Del1, data2, data2Del1, data2Del2 : std_logic_vector(63 downto 0);
    signal dataValid, dataValidDel1, dataValidDel2 : std_logic := '0';
    signal dataCount, dataCountDel1, dataCountDel2 : std_logic_vector(7 downto 0) := "00000000";  -- Counts through the 216 fine channels in an incoming packet.
    signal bufWe : std_logic_vector(7 downto 0);
    signal bufWrData : t_slv_64_arr(7 downto 0);
    signal bufWrAddr : t_slv_9_arr(7 downto 0); 
    signal curWrPacketBuffer : std_logic_vector(1 downto 0);
    signal frameCount : std_logic_vector(36 downto 0);
    signal virtualChannel : t_slv_16_arr(2 downto 0);
    signal packetInFrame, nextPacketInFrame : std_logic_vector(9 downto 0);
    
    signal cdc_src_send : std_logic := '0';
    signal cdc_src_rcv : std_logic;
    signal cdc_src_in : std_logic_vector(81 downto 0);
    signal cdc_dest_out : std_logic_vector(81 downto 0);
    signal cdc_dest_req : std_logic;
    
    type aw_fsm_type is (start, set_aw, wait_fifo, done);
    signal aw_fsm : aw_fsm_type := done;
    signal AWchan : std_logic_vector(1 downto 0);
    signal AWvirtualChannel : t_slv_10_arr(2 downto 0);
    signal AWpacketInFrame : std_logic_vector(9 downto 0);
    signal bufRdAddr : std_logic_vector(8 downto 0);
    type copy_fsm_type is (idle, copyToFIFO, copyToFIFOWait, copyDone);
    signal copy_fsm : copy_fsm_type := idle;
    signal bufRdPointer : std_logic_vector(4 downto 0);
    signal copyBuffer : std_logic_vector(1 downto 0);
    signal stream : std_logic_vector(1 downto 0);
    signal wdataFIFO_wrDataCount : std_logic_vector(5 downto 0);
    signal bufRdData : std_logic_vector(511 downto 0);
    signal firstInFrame : std_logic := '0';
    
    signal wdataFIFO_wrEnAdv3, wdataFIFO_wrEnAdv2, wdataFIFO_wrEnAdv1, wdataFIFO_wrEn : std_logic;
    signal wdataFIFO_empty : std_logic;
    signal wdataFIFO_rdEn : std_logic;
    signal wdataFIFO_dout : std_logic_vector(512 downto 0); -- 512 data bits, plus the axi_wlast bit.
    signal packetBuf0Used, packetBuf1Used, packetBuf2Used, packetBuf3Used : std_logic := '0';
    signal endOfPacket : std_logic;
    signal wdataFIFO_din : std_logic_vector(512 downto 0);
    signal bufRdwlastDel2, bufRdwlastDel1, bufRdwlast : std_logic;
    signal AWFIFO_dout, awFIFO_din : std_logic_vector(29 downto 0);
    signal awFIFO_empty : std_logic;
    signal awFIFO_rdEn : std_logic := '0';
    signal awFIFO_wrEn : std_logic := '0';
    signal awFIFO_wrDataCount : std_logic_vector(5 downto 0);
    signal fifoIn_m02_axi_awaddr : std_logic_vector(29 downto 0);
    signal HBMBuf0PacketCount, HBMBuf1PacketCount : std_logic_vector(36 downto 0);
    -- HBMBuf2PacketCount, HBMBuf3PacketCount : std_logic_vector(31 downto 0);
    signal AWFrameCount : std_logic_vector(36 downto 0);
    signal AWFirstInFrame : std_logic := '0';
    signal AWLastInFrame : std_logic := '0';
    signal maxVirtualChannel : std_logic_vector(10 downto 0);
    signal HBMRdBuffer : std_logic := '0';
    signal HBMBufferEnable : std_logic_vector(3 downto 0);
    signal rstDel1 : std_logic := '0';
    signal HBMWrBuffer : std_logic := '0';
    signal rdFrameCount, frameCount37 : std_logic_vector(36 downto 0);
    signal stationsMinusOne : std_logic_vector(10 downto 0);
    signal coarseMinusOne : std_logic_vector(9 downto 0);
    
    signal rdStation : std_logic_vector(9 downto 0);
    signal rdTime : std_logic_vector(8 downto 0);
    signal rdCoarse : std_logic_vector(8 downto 0);
    signal totalStations : std_logic_vector(10 downto 0);
    signal coarse_x_stations : unsigned(19 downto 0);
    signal outputCoarse_x_stations : unsigned(20 downto 0);
    signal ARvirtualChannel : std_logic_vector(9 downto 0);
    signal ARaddr : std_logic_vector(29 downto 0);
    signal packetsPerFrameMinusOne : std_logic_vector(9 downto 0);
    
    type rd_fsm_type is (start, set_start, get_addr, send_addr, wait_ready, wait_calc0, wait_calc1, update_stationTimeCoarse, check_space, done);
    signal rd_fsm : rd_fsm_type := done;
    signal outstandingRequests : std_logic_vector(5 downto 0) := "000000";
    signal outstandingRequests_x32, outstandingRequests_x27 : std_logic_vector(10 downto 0);
    signal outstandingRequests_x4 : std_logic_vector(10 downto 0);
    signal outstandingRequests_x1 : std_logic_vector(10 downto 0);
    signal fifoSpaceUsed : std_logic_vector(10 downto 0);
    signal rdataFIFO_wrDataCount : std_logic_vector(9 downto 0);
    signal newRequest, requestDone : std_logic;
    
    signal axi2bf_src_rcv, axi2bf_src_send : std_logic := '0';
    signal axi2BF_dataIn : std_logic_vector(68 downto 0);
    signal axi2bf_dest_out : std_logic_vector(68 downto 0);
    signal BFpacketsPerFrameMinusOne : std_logic_vector(9 downto 0);
    signal BFstations, BFstationsMinusOne : std_logic_vector(10 downto 0);
    signal BFcoarse, BFcoarseMinusOne : std_logic_vector(9 downto 0);
    signal axi2bf_dest_req : std_logic;
    type readout_fsm_type is (wait_start_fine, wait_poly_fifo, start_fine, wait_fifo, p0, p1, p2, p3, p4, p5, p6, p7, done);
    signal readout_fsm, readout_fsm_del1, readout_fsm_del2, readout_fsm_del3, readout_fsm_del4 : readout_fsm_type := done;
    signal rdataFIFO_rdEn : std_logic;
    signal rdataFIFO_dout : std_logic_vector(511 downto 0);
    signal rdataFIFO_empty : std_logic;
    signal rdataFIFO_rdDataCount : std_logic_vector(9 downto 0);
    signal outputBuffer : std_logic_vector(639 downto 0);
    
    signal outputFine, outputFineDel1, outputFineDel2, outputFineDel3, outputFineDel4 : std_logic_vector(7 downto 0) := "00000000";
    signal outputStation, outputStationDel1, outputStationDel2, outputStationDel3, outputStationDel4 : std_logic_vector(9 downto 0) := "0000000000";
    signal outputCoarse, outputCoarseDel1, outputCoarseDel2, outputCoarseDel3, outputCoarseDel4 : std_logic_vector(9 downto 0) := "0000000000";
    signal BFFrameCount : std_logic_vector(36 downto 0);
    signal packetCountDel4 : std_logic_vector(39 downto 0);
    signal outputTime : std_logic_vector(9 downto 0);
    signal packetCountDel1, packetCountDel2, packetCountDel3 : std_logic_vector(2 downto 0);
    signal lastStation, lastCoarse, lastTimeIn32, lastTime : std_logic := '0';
    signal BFlastStation, BFlastTimeIn32, BFlastCoarse, BFlastTime : std_logic := '0';
    signal rstDel2, rstDel3 : std_logic;
    signal FBbufferOverflowError : std_logic;
    signal rdataFIFO_rst, awFIFO_rst, wdataFIFO_rst : std_logic := '0';
    signal AXIbufferOverflowError : std_logic;
    signal FB_virtualChannels : std_logic_vector(10 downto 0) := "00000000000";
    
    signal BFlastStationDel2, BFlastStationDel3, BFlastStationDel4 : std_logic;
    signal BFfirstStationDel1, BFfirstStationDel2, BFfirstStationDel3, BFfirstStationDel4 : std_logic;
    signal outputTimeDel1, outputTimeDel2, outputTimeDel3, outputTimeDel4 : std_logic_vector(9 downto 0);
    signal outputVirtualChannelDel2, outputVirtualChannelDel3, outputVirtualChannelDel4 : std_logic_vector(9 downto 0);
    
    signal AWChannelValid : std_logic_vector(2 downto 0) := "000";
    signal copyValid, PacketBuf0Valid, PacketBuf1Valid, PacketBuf2Valid, PacketBuf3Valid, dataChannelValid : std_logic_vector(2 downto 0) := "000";
    
    signal axi2bf_dest_reqDel1 : std_logic;
    
    signal outputBufferDel1, outputBufferDel2, outputBufferDel3, outputBufferDel4, outputBufferDel5, outputBufferDel6 : std_logic_vector(191 downto 0);

    signal m02_arlen_read6 : std_logic;
    signal m02_axi_araddrInt : std_logic_vector(29 downto 0);
    signal gapCount : std_logic_vector(23 downto 0);
    signal gapCount_eq0 : std_logic := '0';
    signal minGapDataIn : std_logic_vector(23 downto 0);
    signal readoutClocks, clocksPerCornerTurnOut : std_logic_vector(31 downto 0);
        
    signal BF2axi_dataIn : std_logic_vector(31 downto 0);
    signal BF2axi_transferCount : std_logic_vector(3 downto 0) := "0000";
    signal BF2axi_src_send : std_logic := '0';
    signal bf2axi_src_rcv : std_logic;
    signal BF2axi_dest_out : std_logic_vector(31 downto 0);
    signal BF2axi_dest_req : std_logic;
    signal readInCount, readInAllClocks : std_logic_vector(31 downto 0);
    signal cdc_dest_req_del1 : std_logic;
    signal fineCount : std_logic_vector(7 downto 0);
    signal outputFineDel4x2 : std_logic_vector(7 downto 0);
    signal metaRecon : std_logic_vector(31 downto 0);
    signal dataMisMatchInt : std_logic := '0';
    signal sample_flagged : std_logic_vector(11 downto 0);
    
    -- number of clocks required for a single beamformer to output 9 packets (=1 coarse channels worth)
    -- Each packet takes 775 clocks to output. The generics in the beamformer are configured 
    -- to allow 900 clocks per packet. 
    constant c_minGapData  : std_logic_vector(15 downto 0)  := x"2000";  -- x2000 = 8192 allows 8192/9 = 910 clocks per packet.
    signal beams_enabled    : std_logic_vector(7 downto 0);
    
    signal valid_int : std_logic := '0';
    
    signal sof_del : std_logic;
    signal sof_count, fb_packet_count, cur_frameCount, recent_vcs, dbg_packetInFrame : std_logic_vector(31 downto 0) := (others => '0');
    signal max_packetInFrame, recent_packetInFrame : std_logic_vector(9 downto 0) := "0000000000";
    signal fb_dbg_reg, axi_dbg_reg : std_logic_vector(159 downto 0);
    signal readout_start_int : std_logic;
    signal wait_poly_fifo_count : std_logic_vector(3 downto 0) := "1111";
    signal outputPktOdd : std_logic;
    signal outputPktOddDel1, outputPktOddDel2, outputPktOddDel3, outputPktOddDel4 : std_logic;
    signal bad_polynomials : std_logic;
    signal bad_polynomials_BF_clk : std_logic;
    
    signal sof_axi_clk, sof_fb_clk : std_logic;
    signal ro_stations : std_logic_vector(10 downto 0); -- up to 1024 stations
    signal ro_coarse   : std_logic_vector(9 downto 0);
    signal ro_stations_coarse_bf_clk, ro_stations_coarse_axi_clk : std_logic_vector(20 downto 0);
    
begin
    
    rdataFIFO_rst <= rstDel2; -- i_axi_clk domain
    awFIFO_rst <= rstDel3;  -- i_axi_clk domain
    
    -- Copy reset across to the wdata FIFO
    xpm_cdc_pulse_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    )
    port map (
        dest_pulse => wdataFIFO_rst, -- 1-bit output: Outputs a pulse the size of one dest_clk period when a pulse transfer is correctly initiated on src_pulse input.
        dest_clk => i_FB_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',     -- 1-bit input: optional; required when RST_USED = 1
        src_clk => i_axi_clk,       -- 1-bit input: Source clock.
        src_pulse => rstDel3,   -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'        -- 1-bit input: optional; required when RST_USED = 1
    );
    
    -- Copy buffer overflow error signal to the i_axi_clk domain
    xpm_cdc_BFoverflow_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    )
    port map (
        dest_pulse => AXIbufferOverflowError, -- 1-bit output: Outputs a pulse the size of one dest_clk period when a pulse transfer is correctly initiated on src_pulse input.
        dest_clk => i_axi_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',     -- 1-bit input: optional; required when RST_USED = 1
        src_clk => i_FB_clk,       -- 1-bit input: Source clock.
        src_pulse => FBbufferOverflowError,   -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'        -- 1-bit input: optional; required when RST_USED = 1
    );    
    
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
            
            -- Hold bufferoverflow error
            if rstDel3 = '1' then
                o_bufferOverflowError <= '0';
            elsif AXIbufferOverflowError = '1' then
                o_bufferOverflowError <= '1';
            end if;
        end if;
    end process;
    
    
    -- Get the number of virtual channels into the i_FB_clk domain
    xpm_cdc_array_single_inst : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF => 3,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
        WIDTH => 11          -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => FB_virtualChannels, -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
        dest_clk => i_FB_clk, -- 1-bit input: Clock signal for the destination clock domain.
        src_clk => i_axi_clk,   -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in => i_virtualChannels  -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain.
    );
    
    xpm_cdc_array_single_dbginst : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF => 3,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
        WIDTH => 160         -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => axi_dbg_reg, -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
        dest_clk => i_axi_clk,   -- 1-bit input: Clock signal for the destination clock domain.
        src_clk => i_FB_clk,     -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in => fb_dbg_reg     -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain.
    );
    
    fb_dbg_reg(31 downto 0) <= sof_count;
    fb_dbg_reg(63 downto 32) <= fb_packet_count;
    fb_dbg_reg(95 downto 64) <= cur_frameCount;
    fb_dbg_reg(127 downto 96) <= recent_vcs;
    fb_dbg_reg(159 downto 128) <= dbg_packetInFrame;
    
    o_sof_count  <= axi_dbg_reg(31 downto 0);       -- out std_logic_vector(31 downto 0);
    o_fb_packet_count <= axi_dbg_reg(63 downto 32); -- out std_logic_vector(31 downto 0);
    o_cur_frameCount <= axi_dbg_reg(95 downto 64);  -- out std_logic_vector(31 downto 0);
    o_recent_vcs <= axi_dbg_reg(127 downto 96);     -- out std_logic_vector(31 downto 0);
    o_dbg_packetInFrame <= axi_dbg_reg(159 downto 128);
    
    process(i_FB_clk)
    begin
        if rising_edge(i_FB_clk) then
            
            -- get input data into 64 bit registers.
            -- Delay data0, data1 and data2 by different amounts so that the part of the buffer they are being written to doesn't clash
            if (g_USE_META) then
                -- Each 32 bits has:
                --   (7:0) = fine channel, runs 0 to 215.
                --   (19:8) = time sample, runs from 0 to 287 (for a 27 LFAA block corner turn).
                --   (31:20) = virtual channel.
                
                if unsigned(packetInFrame) >= (unsigned(g_PACKETS_PER_FRAME) - 1) then  -- e.g. 256 packet per frame, packetInFrame counts 0 to 255
                    nextPacketInFrame <= (others => '0');
                else
                    nextPacketInFrame <= std_logic_vector(unsigned(packetInFrame) + 1);
                end if;
                
                if i_dataValid = '1' and dataValid = '0' then
                    fineCount <= x"01";
                    data0(7 downto 0) <= x"00";
                    data1(7 downto 0) <= x"00";
                    data2(7 downto 0) <= x"00";
                    data0(19 downto 8) <= "00" & nextPacketInFrame;
                    data1(19 downto 8) <= "00" & nextPacketInFrame;
                    data2(19 downto 8) <= "00" & nextPacketInFrame;
                elsif i_dataValid = '1' then
                    fineCount <= std_logic_vector(unsigned(fineCount) + 1);
                    data0(7 downto 0) <= fineCount;
                    data1(7 downto 0) <= fineCount;
                    data2(7 downto 0) <= fineCount;
                    data0(19 downto 8) <= "00" & packetInFrame;
                    data1(19 downto 8) <= "00" & packetInFrame;
                    data2(19 downto 8) <= "00" & packetInFrame;
                end if;
                data0(31 downto 20) <= i_virtualChannel(0)(11 downto 0);
                data1(31 downto 20) <= i_virtualChannel(1)(11 downto 0);
                data2(31 downto 20) <= i_virtualChannel(2)(11 downto 0);
                data0(63 downto 32) <= (others => '0');
                data1(63 downto 32) <= (others => '0');
                data2(63 downto 32) <= (others => '0');
            else
                data0 <= i_data(0);
                data1 <= i_data(1);
                data2 <= i_data(2);
            end if;
            
            data1Del1 <= data1;
            
            data2Del1 <= data2;
            data2Del2 <= data2Del1;
            
            dataValid <= i_dataValid;
            dataValidDel1 <= dataValid;
            dataValidDel2 <= dataValidDel1;
            
            if i_headerValid(0) = '1' or i_headerValid(1) = '1' or i_headerValid(2) = '1' then
                frameCount <= i_frameCount; -- 37 bit corner turn frames since SKA epoch; frame count is the same for all simultaneous streams from the filterbanks.
                virtualChannel <= i_virtualChannel;
            end if;
            
            if i_sof = '1' then  -- start of a 53 ms frame
                -- 4 packet buffers for each stream within the main buffer. Each packet buffer holds one packet (=216 fine channels for one virtual channel = 27 x 256 bit words in the buffer)
                curWrPacketBuffer <= "11";  -- so it will tick over to "00" for the first frame
                packetInFrame <= (others => '1');  -- count of the packet within the frame. For 53ms frames, this will count from 0 to 255 for every group of 3 packets in the frame.
            elsif i_dataValid = '1' and dataValid = '0' then
                dataCount <= "00000000";
                curWrPacketBuffer <= std_logic_vector(unsigned(curWrPacketBuffer) + 1);
                if unsigned(packetInFrame) >= (unsigned(g_PACKETS_PER_FRAME) - 1) then  -- e.g. 256 time samples per frame, then FB_packetsPerFrame = 256, and packetInFrame counts 0 to 255.
                    packetInFrame <= (others => '0');
                else
                    packetInFrame <= std_logic_vector(unsigned(packetInFrame) + 1);
                end if;
            elsif dataValid = '1' then
                dataCount <= std_logic_vector(unsigned(dataCount) + 1);  -- The value in dataCount aligns with the sample in data0, data1, data2.
            end if;
            dataCountDel1 <= dataCount;
            dataCountDel2 <= dataCountDel1;
            
            bufWrAddr(0)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(1)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(2)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(3)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(4)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(5)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(6)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(7)(6 downto 5) <= curWrPacketBuffer;
            
            -- Write data to the buffer.
            -- There are 3 different streams coming in, each of which goes to a different part of the buffer, selected by the high order buffer address (bits 8:7).
            -- The full buffer is 512 bits wide, but data coming in is only 64 bits wide. So each 64 bit piece has its own write enable.
            -- The three input buses are synchronised, so the three buses need to write to the same 64-bit piece in any given clock cycle.
            -- To avoid this, the second stream is delayed on a clock, and the third stream is delayed by 2 clocks. Thus they all write
            -- to different 64-bit pieces.
            for i in 0 to 7 loop
                if (unsigned(dataCount(2 downto 0)) = i) and dataValid = '1' then
                    -- Write data from first input data stream (i_data(0))
                    bufWe(i) <= '1';
                    bufWrData(i) <= data0;
                    bufWrAddr(i)(4 downto 0) <= dataCount(7 downto 3);
                    bufWrAddr(i)(8 downto 7) <= "00";  -- top two bits selects the buffer. "00" selects buffer for i_data(0)
                elsif (unsigned(dataCountDel1(2 downto 0)) = i) and dataValidDel1 = '1' then
                    bufWe(i) <= '1';
                    bufWrData(i) <= data1Del1;
                    bufWrAddr(i)(4 downto 0) <= dataCountDel1(7 downto 3);
                    bufWrAddr(i)(8 downto 7) <= "01"; -- "01" = buffer used for i_data(1)
                elsif (unsigned(dataCountDel2(2 downto 0)) = i) and dataValidDel2 = '1' then
                    bufWe(i) <= '1';
                    bufWrData(i) <= data2Del2;
                    bufWrAddr(i)(4 downto 0) <= dataCountDel2(7 downto 3);
                    bufWrAddr(i)(8 downto 7) <= "10"; -- "10" = buffer used for i_data(2)   
                else
                    bufWe(i) <= '0';  -- No write to this 64 bit segment at the moment.
                    bufWrData(i) <= (others => '0');
                    bufWrAddr(i)(4 downto 0) <= "00000";
                    bufWrAddr(i)(8 downto 7) <= "00";
                end if;
            end loop;
            
            ----------------------------------------------------------------------------
            -- Gather some info on the incoming data stream
            sof_del <= i_sof;
            if i_sof = '1' and sof_del = '0' then
                sof_count <= std_logic_vector(unsigned(sof_count) + 1);
            end if;
            
            if i_dataValid = '0' and dataValid = '1' then
                fb_packet_count <= std_logic_vector(unsigned(fb_packet_count) + 1);
            end if;
            
            if packetInFrame(9) = '0' and unsigned(packetInFrame) > unsigned(max_packetInFrame) then
                max_packetInFrame <= packetInFrame;
            end if;
            
            if i_dataValid = '0' and dataValid = '1' then
                recent_packetInFrame <= packetInFrame;
            end if;
            
            dbg_packetInFrame(9 downto 0) <= max_packetInFrame;
            dbg_packetInFrame(15 downto 10) <= "000000";
            dbg_packetInFrame(25 downto 16) <= recent_packetInFrame;
            dbg_packetInFrame(31 downto 26) <= "000000";
            cur_frameCount <= i_frameCount(31 downto 0);
            recent_vcs(7 downto 0) <= virtualChannel(0)(7 downto 0);
            recent_vcs(15 downto 8) <= virtualChannel(1)(7 downto 0);
            recent_vcs(23 downto 16) <= virtualChannel(2)(7 downto 0);
            recent_vcs(31 downto 24) <= "00000000";
                
            ----------------------------------------------------------------------------
            
            -- At the end of every packet, send a message to the axi clock domain with the virtual channels and timestamps.
            -- This is used to generate the addresses on the aw bus for the 3 packets just received.
            if i_sof = '1' then
                firstInFrame <= '1';
            elsif i_dataValid = '0' and dataValid = '1' then
                firstInFrame <= '0';
            end if;
            
            -- Capture the total number of virtual channels at the start of the full corner turn frame
            if (i_dataValid = '1' and dataValid = '0' and firstInFrame = '1' and (unsigned(virtualChannel(0)) = 0)) then
                maxVirtualChannel <= std_logic_vector(unsigned(FB_virtualChannels) - 1);
                sof_fb_clk <= '1';  -- notify the readout side to register i_stations, i_coarse, for use later in the readout.
            else
                sof_fb_clk <= '0';
            end if;
            
            
            if i_dataValid = '0' and dataValid = '1' then
                cdc_src_send <= '1';
                cdc_src_in(9 downto 0) <= virtualChannel(0)(9 downto 0);
                cdc_src_in(19 downto 10) <= virtualChannel(1)(9 downto 0);
                cdc_src_in(29 downto 20) <= virtualChannel(2)(9 downto 0);
                cdc_src_in(66 downto 30) <= frameCount;
                cdc_src_in(76 downto 67) <= packetInFrame;
                cdc_src_in(77) <= firstInFrame;
                -- set last in frame, if this is the final packet.
                if ((unsigned(packetInFrame) = (unsigned(g_PACKETS_PER_FRAME) - 1)) and 
                    ((virtualChannel(0)(9 downto 0) = maxVirtualChannel(9 downto 0)) or
                     (virtualChannel(1)(9 downto 0) = maxVirtualChannel(9 downto 0)) or
                     (virtualChannel(2)(9 downto 0) = maxVirtualChannel(9 downto 0)))) then
                    cdc_src_in(78) <= '1';  -- last packet in the frame. 
                else
                    cdc_src_in(78) <= '0';
                end if;
                if (unsigned(virtualChannel(0)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    cdc_src_in(79) <= '1';
                else
                    cdc_src_in(79) <= '0';
                end if;
                if (unsigned(virtualChannel(1)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    cdc_src_in(80) <= '1';
                else
                    cdc_src_in(80) <= '0';
                end if;
                if (unsigned(virtualChannel(2)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    cdc_src_in(81) <= '1';
                else
                    cdc_src_in(81) <= '0';
                end if;
            elsif cdc_src_rcv = '1' then
                cdc_src_send <= '0';
            end if;
            
            if i_dataValid = '0' and dataValid = '1' then
                endOfPacket <= '1';
                if (unsigned(virtualChannel(0)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    dataChannelValid(0) <= '1';
                else
                    dataChannelValid(0) <= '0';
                end if;
                if (unsigned(virtualChannel(1)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    dataChannelValid(1) <= '1';
                else
                    dataChannelValid(1) <= '0';
                end if;
                if (unsigned(virtualChannel(2)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    dataChannelValid(2) <= '1';
                else
                    dataChannelValid(2) <= '0';
                end if;
            else
                endOfPacket <= '0';
            end if;

            if endOfPacket = '1' and curWrPacketBuffer = "00" then
                PacketBuf0Used <= '1';
                PacketBuf0Valid <= dataChannelValid;
            elsif copy_fsm = copyDone and copyBuffer = "00" then
                PacketBuf0Used <= '0';
            end if;

            if endOfPacket = '1' and curWrPacketBuffer = "01" then
                PacketBuf1Used <= '1';
                PacketBuf1Valid <= dataChannelValid;
            elsif copy_fsm = copyDone and copyBuffer = "01" then
                PacketBuf1Used <= '0';
            end if; 
             
            if endOfPacket = '1' and curWrPacketBuffer = "10" then
                PacketBuf2Used <= '1';
                PacketBuf2Valid <= dataChannelValid;
            elsif copy_fsm = copyDone and copyBuffer = "10" then
                PacketBuf2Used <= '0';
            end if;    
            
            if endOfPacket = '1' and curWrPacketBuffer = "11" then
                PacketBuf3Used <= '1';
                PacketBuf3Valid <= dataChannelValid;
            elsif copy_fsm = copyDone and copyBuffer = "11" then
                PacketBuf3Used <= '0';
            end if;            
            
            if (endOfPacket = '1' and 
                ((curWrPacketBuffer = "00" and packetBuf0Used = '1') or
                 (curWrPacketBuffer = "01" and packetBuf1Used = '1') or
                 (curWrPacketBuffer = "10" and packetBuf2Used = '1') or
                 (curWrPacketBuffer = "11" and packetBuf3Used = '1'))) then
                FBbufferOverflowError <= '1'; -- we just overwrote one of the packet buffers.
            else
                FBbufferOverflowError <= '0';
            end if;
            
            -- Copy data to the FIFO from the correct buffer.
            -- Note the write requests on the aw bus send the complete packet for the first stream, then the complete packet for the second stream, then the complete packet for the third stream.
            case copy_fsm is
                when idle =>
                    -- wait until a buffer is used, then start copying data
                    if (PacketBuf0Used = '1') then -- buf0, buf1, buf2, buf3 refer to the 4 buffers within each stream (i.e. a block of 27 words in the buffer memory).
                        copyBuffer <= "00";
                        copyValid <= PacketBuf0Valid;
                    elsif packetBuf1Used = '1' then
                        copyBuffer <= "01";
                        copyValid <= PacketBuf1Valid;
                    elsif packetBuf2Used = '1' then
                        copyBuffer <= "10";
                        copyValid <= PacketBuf2Valid;
                    else
                        copyBuffer <= "11";
                        copyValid <= PacketBuf3Valid;
                    end if;
                    if PacketBuf0Used = '1' or PacketBuf1Used = '1' or PacketBuf2Used = '1' or PacketBuf3Used = '1' then
                        copy_fsm <= copyToFIFO;
                    end if;
                    stream <= "00";   -- which of the 3 input streams we are currently working on.
                    bufRdPointer <= "00000";
                    
                when copyToFIFO =>
                    if (unsigned(bufRdPointer) = 26) then
                        bufRdPointer <= "00000";
                        if stream = "10" then
                            copy_fsm <= copyDone;
                            stream <= "00";
                        else
                            stream <= std_logic_vector(unsigned(stream) + 1);
                            copyValid <= '0' & copyValid(2 downto 1);
                        end if;
                    else
                        bufRdPointer <= std_logic_vector(unsigned(bufRdPointer) + 1); -- Go through the 216 fine channels (=27 words in the buffer)
                        if (unsigned(wdataFIFO_wrDataCount) > 16) then
                            copy_fsm <= copyToFIFOWait;
                        end if;
                    end if;
                    
                when copyToFIFOWait =>
                    if (unsigned(wdataFIFO_wrDataCount) < 16) then
                        copy_fsm <= copyToFIFO;
                    end if;
                    
                when copyDone =>
                    if ((copyBuffer = "00" and PacketBuf1Used = '1') or 
                        (copyBuffer = "01" and packetBuf2Used = '1') or 
                        (copyBuffer = "10" and packetBuf3Used = '1') or 
                        (copyBuffer = "11" and packetBuf0Used = '1')) then
                        copy_fsm <= copyToFIFO;
                        copyBuffer <= std_logic_vector(unsigned(copyBuffer) + 1);
                        if copyBuffer = "00" then  -- next buffer will be "01", choose valid for that buffer.
                            copyValid <= PacketBuf1Valid;
                        elsif copyBuffer = "01" then
                            copyValid <= PacketBuf2Valid;
                        elsif copyBuffer = "10" then
                            copyValid <= PacketBuf3Valid;
                        else
                            copyValid <= PacketBuf0Valid;
                        end if;
                    end if;
                    
                when others =>
                    copy_fsm <= idle;
            end case;
            
            bufRdAddr(8 downto 7) <= stream;
            bufRdAddr(6 downto 5) <= copyBuffer;
            bufRdAddr(4 downto 0) <= bufRdPointer;
            if (copy_fsm = copyToFIFO) then
                wdataFIFO_wrEnAdv3 <= copyValid(0);    -- corresponds to bufRdAddr
            else
                wdataFIFO_wrEnAdv3 <= '0';
            end if;
            
            if (bufRdAddr(4 downto 0) = "11010") then
                bufRdwlast <= '1'; -- Writes to the HBM are in bursts of 27 words (27 * 64 = 1728 bytes = 216 channels * 8 bytes/channel)
            else
                bufRdwlast <= '0';
            end if;
            bufRdwlastDel1 <= bufRdwlast;
            bufRdwlastDel2 <= bufRdwlastDel1;  -- bufRdwlast is found based on bufRdAddr, so bufRdwlastDel2 has a 3 cycle latency compared with bufRdAddr, and thus aligns with the data output from the buffer.
            
            wdataFIFO_wrEnAdv2 <= wdataFIFO_wrEnAdv3;
            wdataFIFO_wrEnAdv1 <= wdataFIFO_wrEnAdv2;
            wdataFIFO_wrEn <= wdataFIFO_wrEnAdv1;   -- three cycle latency on the buffer memory.
            
        end if;
    end process;
    
    -- BRAM buffer to store data as it comes in;
    -- Overall size is 512x512 bits
    --  Split into 4 buffers of 128 x 512 bits, with 3 of the buffers used (one per input stream)
    --  Each 128 is further split into 4 smaller buffers of 27 x 512 bits, each sufficient for one filterbank frame (of 216 samples)
    -- Total width is 512 bits, which is split into 8x64 bits, with separate write ports for each 64 bit piece, so we can deliver the data directly to 
    -- the correct place from all 3 input streams simultaneously.
    bufi : entity ct_lib.buffer512x512_wrapper
    Port map(
        i_clk    => i_FB_clk,  -- in std_logic;
        i_we     => bufWe,     -- in std_logic_vector(7 downto 0);  -- Separate write enable for each 32 bit piece.
        i_wrAddr => bufWrAddr, -- in t_slv_9_arr(7 downto 0);       -- Address for each piece of the memory
        i_wrData => bufWrData, -- in t_slv_64_arr(7 downto 0);      -- write data for each 32 bit wide piece of the memory
        i_rdAddr => bufRdAddr, -- in std_logic_vector(8 downto 0);  -- Read address
        o_rdData => bufRdData  -- out std_logic_vector(511 downto 0)  -- read data, 3 cycle latency from the read address.
    );
    
    wdataFIFO_din(511 downto 0) <= bufRdData;
    wdataFIFO_din(512) <= bufRdwlastDel2;
    
    -- Small FWFT FIFO to cross the clock domain to the AXI clock, and to interface to the ready/valid on the m02_axi_wdata bus
    xpm_fifo_async_inst : xpm_fifo_async
    generic map (
        CDC_SYNC_STAGES => 2,       -- DECIMAL
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "distributed", -- String
        FIFO_READ_LATENCY => 0,     -- DECIMAL
        FIFO_WRITE_DEPTH => 32,     -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 6,   -- DECIMAL
        READ_DATA_WIDTH => 513,     -- DECIMAL  -- 256 data bits, plus one extra bit for last in a transaction. Burst are 12, 12, and 3 words, for the 27 words in a single packet.
        READ_MODE => "fwft",        -- String
        RELATED_CLOCKS => 0,        -- DECIMAL
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "0404", -- String
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 513,    -- DECIMAL
        WR_DATA_COUNT_WIDTH => 6    -- DECIMAL
    ) port map (
        almost_empty => open,   -- 1-bit output: Almost Empty : When asserted, this signal indicates that only one more read can be performed before the FIFO goes to empty.
        almost_full => open,    -- 1-bit output: Almost Full: When asserted, this signal indicates that only one more write can be performed before the FIFO is full.
        data_valid => open,     -- 1-bit output: Read Data Valid: When asserted, this signal indicates that valid data is available on the output bus (dout).
        dbiterr => open,        -- 1-bit output: Double Bit Error: Indicates that the ECC decoder detected a double-bit error and data in the FIFO core is corrupted.
        dout => wdataFIFO_dout,   -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven when reading the FIFO.
        empty => wdataFIFO_empty, -- 1-bit output: Empty Flag: When asserted, this signal indicates that the FIFO is empty. Read requests are ignored when the FIFO is empty, initiating a read while empty is not destructive to the FIFO.
        full => open,             -- 1-bit output: Full Flag: When asserted, this signal indicates that the FIFO is full. 
        overflow => open,         -- 1-bit output: Overflow: 
        prog_empty => open,       -- 1-bit output: Programmable Empty.
        prog_full => open,        -- 1-bit output: Programmable Full: 
        rd_data_count => open,    -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates the number of words read from the FIFO.
        rd_rst_busy => open,      -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO read domain is currently in a reset state.
        sbiterr => open,          -- 1-bit output: Single Bit Error: Indicates that the ECC decoder detected and fixed a single-bit error.
        underflow => open,        -- 1-bit output: Underflow: Indicates that the read request (rd_en) during the previous clock cycle was rejected because the FIFO is empty.
        wr_ack => open,           -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => wdataFIFO_wrDataCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,      -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO write domain is currently in a reset state.
        din => wdataFIFO_din,     -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',     -- 1-bit input: Double Bit Error Injection: Injects a double bit error the ECC feature is used on block RAMs or UltraRAM macros.
        injectsbiterr => '0',     -- 1-bit input: Single Bit Error Injection: Injects a single bit error if the ECC feature is used on block RAMs or UltraRAM macros.
        rd_clk => i_axi_clk,      -- 1-bit input: Read clock: Used for read operation. rd_clk must be a free running clock.
        rd_en => wdataFIFO_rdEn,  -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO.
        rst => wdataFIFO_rst,     -- 1-bit input: Reset: Must be synchronous to wr_clk. 
        sleep => '0',             -- 1-bit input: Dynamic power saving: If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_FB_clk,       -- 1-bit input: Write clock: Used for write operation. wr_clk must be a free running clock.
        wr_en => wdataFIFO_wrEn   -- 1-bit input: Write Enable: If the FIFO is not full, asserting this signal causes data (on din) to be written to the FIFO. 
    );
    
    m02_axi_wvalid <= not wdataFIFO_empty;
    
    wdataFIFO_rdEn <= (not wdataFIFO_empty) and m02_axi_wready;
    m02_axi_wdata <= wdataFIFO_dout(511 downto 0);
    m02_axi_wlast <= wdataFIFO_dout(512);
    
    -- Notify the axi bus clock domain that a new packet is available.
    -- We need the virtual channels, and the packet count for the packet.
    xpm_cdc_handshake_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 4,    -- DECIMAL; range: 2-10
        WIDTH => 82          -- DECIMAL; range: 1-1024
    )
    port map (
        dest_out => cdc_dest_out, -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => cdc_dest_req, -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => cdc_src_rcv,   -- 1-bit output: Acknowledgement from destination logic that src_in has been received.
        dest_ack => '1',          -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_axi_clk,    -- 1-bit input: Destination clock.
        src_clk => i_FB_clk,      -- 1-bit input: Source clock.
        src_in => cdc_src_in,     -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => cdc_src_send  -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );
    
    o_readInAllClocks <= readInAllClocks;
    
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
            HBMBufferEnable <= i_HBMBufferEnable;
            rstDel1 <= i_rst;
            rstDel2 <= rstDel1;
            rstDel3 <= rstDel2;
            
            cdc_dest_req_del1 <= cdc_dest_req;
            if cdc_dest_req_del1 = '1' and AWFirstInFrame = '1' then
                readInCount <= (others => '0');
                readInAllClocks <= readInCount;
            else
                readInCount <= std_logic_vector(unsigned(readInCount) + 1);
            end if;
            if cdc_dest_req_del1 = '1' and AWLastInFrame = '1' then
                o_readInClocks <= readInCount;
            end if;
            
            if rstDel1 = '1' then
                HBMWrBuffer <= '0';
                awFIFO_wrEn <= '0';
            elsif cdc_dest_req = '1' then
                -- This occurs at the end of each packet from the filterbanks,
                -- where a packet is a burst of 216 fine channels for 3 consecutive simultaneous virtual channels.
                -- So each packet needs 3 HBM aw requests, one for each virtual channels, where each of the requests is 
                -- for a 27x512bit = 1728 byte write to HBM.
                AWvirtualChannel(0)(9 downto 0) <= cdc_dest_out(9 downto 0);    -- "V" in the address equation
                AWvirtualChannel(1)(9 downto 0) <= cdc_dest_out(19 downto 10);  
                AWvirtualChannel(2)(9 downto 0) <= cdc_dest_out(29 downto 20); 
                AWframeCount <= cdc_dest_out(66 downto 30);
                AWpacketInFrame <= cdc_dest_out(76 downto 67);   -- "T" in the equation below for the address
                AWFirstInFrame <= cdc_dest_out(77);
                AWLastInFrame <= cdc_dest_out(78);
                AWchannelValid <= cdc_dest_out(81 downto 79);

                if cdc_dest_out(77) = '1' then -- this is the first packet in a new frame (i.e. 53.08416 ms)
                    frameCount37 <= cdc_dest_out(66 downto 30); -- frame count used in the meta data at the output to the beamformer.
                    -- Update the HBM buffer to use.
                    HBMWrBuffer <= not HBMWrBuffer;
                end if;
                aw_fsm <= start;
            else
                case aw_fsm is
                    when start =>
                        -- Generate write requests; Single burst of 27 beats, for each of the three channels being processed.
                        --  (total 27 beats = all 216 fine channels (8 fine channels per beat).
                        -- The write address is 
                        --    Byte Address =  T*1024*2048 + V*2048 + F*8
                        -- Fine channel (F) = 0, so 
                        --    Address(20 downto 11) = virtual channel (10 bit value, 0 to 1023)
                        --    Address(28 downto 21) = Time sample (8 bit value, 0 to 255)
                        --    Address(29) = buffer select (each buffer is 0.5GBytes, total 1 GByte)
                        aw_fsm <= set_aw;
                        awFIFO_wrEn <= '0';
                        AWchan <= "00";
                        fifoIn_m02_axi_awaddr(29) <= HBMWrBuffer;
                        if (AWFirstInFrame = '1' and HBMWrBuffer = '0') then
                            HBMBuf0PacketCount <= AWFrameCount(36 downto 0);
                        end if;
                        if (AWFirstInFrame = '1' and HBMWrBuffer = '1') then
                            HBMBuf1PacketCount <= AWFrameCount(36 downto 0);
                        end if;
                        
                    when set_aw => --
                        fifoIn_m02_axi_awaddr(28 downto 21) <= AWpacketInFrame(7 downto 0);
                        if AWchan = "00" then
                            fifoIn_m02_axi_awaddr(20 downto 11) <= AWvirtualChannel(0)(9 downto 0);
                            awFIFO_wrEn <= AWChannelValid(0);
                        elsif AWchan = "01" then
                            fifoIn_m02_axi_awaddr(20 downto 11) <= AWvirtualChannel(1)(9 downto 0);
                            awFIFO_wrEn <= AWChannelValid(1);
                        else
                            fifoIn_m02_axi_awaddr(20 downto 11) <= AWvirtualChannel(2)(9 downto 0);
                            awFIFO_wrEn <= AWChannelValid(2);
                        end if;
                        aw_fsm <= wait_fifo;
                        
                    when wait_fifo =>
                        awFIFO_wrEn <= '0';
                        if (unsigned(awFIFO_wrDataCount) < 24) then
                            if AWchan = "10" then
                                aw_fsm <= done;
                            else
                                aw_fsm <= set_aw;
                                AWchan <= std_logic_vector(unsigned(AWchan) + 1);
                            end if;
                        end if;
                        
                    when done =>
                        awFIFO_wrEn <= '0';
                        aw_fsm <= done;
                    
                    when others =>
                        awFIFO_wrEn <= '0';
                        aw_fsm <= done;
                end case;
            end if;
            
        end if;
    end process;
    
    o_HBMBuf0PacketCount <= HBMBuf0PacketCount;
    o_HBMBuf1PacketCount <= HBMBuf1PacketCount;
    
    awFIFO_din(29 downto 11) <= fifoIn_m02_axi_awaddr(29 downto 11);
    awFIFO_din(10 downto 0) <= "00000000000";  -- The write address is always aligned to 2K.
    
    -- aw transactions go into a FIFO
    xpm_awfifo_sync_inst : xpm_fifo_sync
    generic map (
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "distributed", -- String
        FIFO_READ_LATENCY => 0,     -- DECIMAL
        FIFO_WRITE_DEPTH => 32,     -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 6,   -- DECIMAL
        READ_DATA_WIDTH => 30,      -- DECIMAL
        READ_MODE => "fwft",        -- String
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "0404", -- String
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 30,     -- DECIMAL
        WR_DATA_COUNT_WIDTH => 6    -- DECIMAL
    )
    port map (
        almost_empty => open,
        almost_full => open,
        data_valid => open,      -- 1-bit output: Read Data Valid
        dbiterr => open,         -- 1-bit output: Double Bit Error: 
        dout => awFIFO_dout,     -- READ_DATA_WIDTH-bit output: Read Data.
        empty => awFIFO_empty,   -- 1-bit output: Empty Flag:
        full => open,            -- 1-bit output: Full Flag: 
        overflow => open,        -- 1-bit output: Overflow: 
        prog_empty => open,      -- 1-bit output: Programmable Empty.
        prog_full => open,       -- 1-bit output: Programmable Full.
        rd_data_count => open,   -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count.
        rd_rst_busy => open,     -- 1-bit output: Read Reset Busy.
        sbiterr => open,         -- 1-bit output: Single Bit Error.
        underflow => open,       -- 1-bit output: Underflow.
        wr_ack => open,          -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => awFIFO_wrDataCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,     -- 1-bit output: Write Reset Busy.
        din => awFIFO_din,       -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',    -- 1-bit input: Double Bit Error Injection: Injects a double bit error if ecc is enabled.
        injectsbiterr => '0',    -- 1-bit input: Single Bit Error Injection.
        rd_en => awFIFO_rdEn,    -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO. 
        rst => awFIFO_rst,       -- 1-bit input: Reset: Must be synchronous to wr_clk. 
        sleep => '0',            -- 1-bit input: Dynamic power saving- If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_axi_clk,     -- 1-bit input: Write clock: Used for write operation.
        wr_en => awFIFO_wrEn     -- 1-bit input: Write Enable
    );
    
    m02_axi_awvalid  <= not awFIFO_empty;
    awFIFO_rdEn <= (not awFIFO_empty) and m02_axi_awready;
    m02_axi_awaddr <= awFIFO_dout(29 downto 0);
    m02_axi_awlen <= "00011010";  -- All writes are 27 beats.
    
    ---------------------------------------------------------------------------------------------------
    -- Read side
    -- Read data in the correct order and send on 192 bit words to the beamformer
    -- 
    -- Tasks :
    --   - Generate the m02_axi_ar* bus (m02_axi_arvalid, m02_axi_arready, m02_axi_araddr, m02_axi_arlen)
    --   - Buffer data as it comes back from the HBM
    --   - put it out to the beamformer on a 192 bit bus.
    --
    --  Data coming back from the HBM goes to a 512 deep by 512 bit wide FIFO.
    --
    -- Read Order
    -- ----------
    -- Reminder : 
    --    - 216 fine channels per packet -> 27 x 512 bit words in the HBM, in consecutive memory locations.
    --    - HBM byte Address = (T*1024*2048 + V*2048 + F*8)
    --                   where F = Fine Channel  (0 to 215)
    --                         V = virtual channel (0 to up to 1023)
    --                         T = Time within the block (0 to 255, assuming the first stage corner turn is 24 LFAA blocks) 
    --
    -- Processing for the beamformer needs data for all stations for each virtual channel.
    -- Reads are in blocks of 1728 bytes, corresponding to a full coarse channel for a single station.
    --
    -- The data output order is:
    --
    --    For timeGroup = 0:(<time samples per corner turn>/32 - 1)    -- For 53 ms corner turn, this is 0:(255/32-1) = 0:7
    --       For Coarse = 0:(i_coarse-1)                               -- For 512 stations, 2 coarse, this is 0:1
    --             For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
    --                For Station = 0:(i_stations-1)
    --                   For fineChannel = 0:215
    --                   * Read 1728 bytes = 27 HBM words
    --  
    --  So first few reads for the 512 stations, two channels case is : 
    --    TimeGroup 0, Time = 0, station 0, coarse 0, 27 HBM words (=216 fine channels)
    --    TimeGroup 0, Time = 0, station 1, coarse 0, " 
    --    ...
    --    TimeGroup 0, Time = 0, station 511, coarse 0, "
    --    TimeGroup 0, Time = 1, station 0, coarse 0, words 0-5
    --    ...
    --    TimeGroup 0, Time = 1, station 511, coarse 0, words 0-5
    --    ...
    ---------------------------------------------------------------------------------------------------
    
    
    xpm_cdc_pulse_sof_i : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 0,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    ) port map (
        dest_pulse => sof_axi_clk, -- 1-bit output: Outputs a pulse the size of one dest_clk period when a pulse transfer is correctly initiated on src_pulse input.
        dest_clk => i_axi_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',     -- 1-bit input: optional; required when RST_USED = 1
        src_clk => i_FB_clk,       -- 1-bit input: Source clock.
        src_pulse => sof_fb_clk,   -- 1-bit input: Rising edge of this signal initiates a pulse transfer; mMnimum gap between pulses > 2*(larger(src_clk period, dest_clk period)).
        src_rst => '0'        -- 1-bit input: optional; required when RST_USED = 1
    );
    
    ARvirtualChannel <= std_logic_vector(unsigned(rdStation) + unsigned(coarse_x_stations(9 downto 0)));
    
    -----------------------------------------------------------
    -- 
    xpm_cdc_array_sc_i : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
        WIDTH => 21          -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => ro_stations_coarse_bf_clk, -- WIDTH-bit output: src_in synchronized to the destination clock domain. Registered.
        dest_clk => i_bf_clk, -- 1-bit input: Clock signal for the destination clock domain.
        src_clk => i_axi_clk, -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in => ro_stations_coarse_axi_clk -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain.
    );
    
    ro_stations_coarse_axi_clk(10 downto 0) <= ro_stations;
    ro_stations_coarse_axi_clk(20 downto 11) <= ro_coarse;
    
    o_stations_bf_clk <= ro_stations_coarse_bf_clk(10 downto 0);
    o_coarse_bf_clk <= ro_stations_coarse_bf_clk(20 downto 11);
    -------------------------------------------------------------
    
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
        
            -- Register these values once at the start of every readout.
            -- i_stations, i_coarse can change part way through the input stream if the configuration changes,
            -- but we need a stable value for the duration of reading 53ms of data from the HBM
            if sof_axi_clk = '1' then
                -- sof_axi_clk pulses at the start of a new 53 ms frame coming in from the filterbanks.
                ro_stations <= i_stations;
                ro_coarse <= i_coarse;
            end if;
            
            if cdc_dest_req = '1' and cdc_dest_out(78) = '1' then -- this is the last packet in the frame
                stationsMinusOne <= std_logic_vector(unsigned(ro_stations) - 1);
                coarseMinusOne <= std_logic_vector(unsigned(ro_coarse) - 1);
                totalStations <= ro_stations;
            end if;
            
            coarse_x_stations <= unsigned(rdCoarse) * unsigned(totalStations);  -- 9 bit value x 11 bit value = 20 bit result.
            
            packetsPerFrameMinusOne <= std_logic_vector(unsigned(g_PACKETS_PER_FRAME) - 1);
            -- some tests for the loops registered here to ensure easy timing:
            if (rdStation = stationsMinusOne(9 downto 0)) then
                lastStation <= '1';
            else
                lastStation <= '0';
            end if;
            if rdCoarse = coarseMinusOne(8 downto 0) then
                lastCoarse <= '1';
            else
                lastCoarse <= '0';
            end if;
            if rdTime(4 downto 0) = "11111" then
                lastTimeIn32 <= '1';
            else
                lastTimeIn32 <= '0';
            end if;
            if (rdTime = packetsPerFrameMinusOne(8 downto 0)) then
                lastTime <= '1';
            else
                lastTime <= '0';
            end if;
        
            if cdc_dest_req = '1' and cdc_dest_out(78) = '1' then -- this is the last packet in the frame
                bad_polynomials <= i_bad_polynomials;
            end if;
        
            if cdc_dest_req = '1' and cdc_dest_out(78) = '1' then -- this is the last packet in the frame
                HBMRdBuffer <= HBMWrBuffer;   -- HBMRdBuffer is the top bit of the HBM address (bit 29)
                rdFrameCount <= frameCount37;
                rd_fsm <= start;
                
                rdTime <= (others => '0');     --outermost loop, run from 0 to (i_packetsPerFrame-1)
                rdCoarse <= (others => '0');   --    next level loop, runs from 0 to (i_coarse-1)
                rdStation <= (others => '0');  --            innermost loop, runs from 0 to (i_stations-1)
            else
                case rd_fsm is
                    when start => 
                        m02_axi_arvalid <= '0';
                        rd_fsm <= set_start;
                    
                    when set_start =>
                        -- tell the next level up that we are about to start reading out a new corner turn frame
                        m02_axi_arvalid <= '0';
                        rd_fsm <= get_addr;
                        
                    when get_addr =>
                        -- HBM Byte Address =  T*1024*2048 + V*2048 + F*8
                        -- Fine channel (F) = 0, so 
                        --    Address(20 downto 11) = V = virtual channel (10 bit value, 0 to 1023)
                        --    Address(28 downto 21) = T = Time sample (8 bit value, 0 to 255)
                        --    Address(29) = buffer select (each buffer is 0.5GBytes, total 1 GByte)
                        m02_axi_ARaddrInt(29) <= HBMRdBuffer;
                        m02_axi_ARaddrInt(28 downto 21) <= rdTime(7 downto 0); 
                        m02_axi_ARaddrInt(20 downto 11) <= ARvirtualChannel(9 downto 0); -- virtual channel = rdStation + i_stations*rdCoarse;
                        m02_axi_ARaddrInt(10 downto 0) <= "00000000000";  -- Reads always start on a 2kByte boundary.
                        rd_fsm <= send_addr;
                    
                    when send_addr =>
                        rd_fsm <= wait_ready;
                        m02_axi_arvalid <= '1';
                    
                    when wait_ready => 
                        if m02_axi_arready = '1' then
                            rd_fsm <= wait_calc0;
                            m02_axi_arvalid <= '0';
                        end if;
                    
                    when wait_calc0 => -- few clocks to wait for the fifoSpacedUsed calculation to complete before it is used in "check_space"
                        rd_fsm <= wait_calc1;
                        m02_axi_arvalid <= '0';
                        
                    when wait_calc1 => 
                        rd_fsm <= update_stationTimeCoarse;
                        m02_axi_arvalid <= '0';
                        
                    when update_stationTimeCoarse =>
                        m02_axi_arvalid <= '0';
                        if lastStation = '1' then
                            rdStation <= (others => '0');
                            if lastTimeIn32 = '1' then
                                if lastCoarse = '1' then
                                    rdCoarse <= "000000000";
                                    if lastTime = '1' then
                                        rdTime <= "000000000";
                                        rd_fsm <= done;
                                    else
                                        rdTime <= std_logic_vector(unsigned(rdTime) + 1);
                                        rd_fsm <= check_space;
                                    end if;
                                else
                                    rdCoarse <= std_logic_vector(unsigned(rdCoarse) + 1);
                                    rdTime(4 downto 0) <= "00000";
                                    rd_fsm <= check_space;
                                end if;
                            else
                                rdTime <= std_logic_vector(unsigned(rdTime) + 1);
                                rd_fsm <= check_space;
                            end if;
                        else
                            rdStation <= std_logic_vector(unsigned(rdStation) + 1);
                            rd_fsm <= check_space;
                        end if;
                    
                    when check_space => 
                        m02_axi_arvalid <= '0';
                        -- Make sure we have space in the output buffer for any packets that come back.
                        if ((unsigned(fifoSpaceUsed) < 480) and (unsigned(outstandingRequests) < 60)) then
                            rd_fsm <= get_addr;  -- Note: 480 and 60 are arbitrarily chosen to be safe values; should still work with 506 and 63.
                        end if;
                        
                    when done =>
                        m02_axi_arvalid <= '0';
                        rd_fsm <= done;
                    
                    when others =>
                        rd_fsm <= done;
                end case;
            end if;
            
            -- 27 beats per request
            outstandingRequests_x27 <= std_logic_vector(unsigned(outstandingRequests_x32) - unsigned(outstandingRequests_x4) - unsigned(outstandingRequests_x1));
            fifoSpaceUsed <= std_logic_vector(unsigned(outstandingRequests_x27) + unsigned(rdataFIFO_wrDataCount));
            
            if rd_fsm = start then
                outstandingRequests <= (others => '0');
            elsif newRequest = '1' and requestDone = '0' then
                outstandingRequests <= std_logic_vector(unsigned(outstandingRequests) + 1);
            elsif newRequest = '0' and requestDone = '1' then
                outstandingRequests <= std_logic_vector(unsigned(outstandingRequests) - 1);
            end if;
            
            if rd_fsm = send_addr then
                newRequest <= '1';
            else
                newRequest <= '0';
            end if;
            if m02_axi_rvalid = '1' and m02_axi_rlast = '1' then
                requestDone <= '1';
            else
                requestDone <= '0';
            end if;
            
            if (cdc_dest_req = '1' and cdc_dest_out(78) = '1' and (rd_fsm /= done or unsigned(outstandingRequests) /= 0)) then
                o_readoutError <= '1';
            else
                o_readoutError <= '0';
            end if;
            
            if rd_fsm = set_start then
                readout_start_int <= '1';
                o_readout_frame <= rdFrameCount;
            else
                readout_start_int <= '0';
            end if;
            o_readout_start <= readout_start_int;
            
        end if;
    end process;
    
    m02_axi_ARlen <= "00011010"; -- always read 27 beats = 1728 bytes
    m02_axi_ARaddr <= m02_axi_ARaddrInt;
    
    outstandingRequests_x32 <= outstandingRequests & "00000";
    outstandingRequests_x4 <= "000" & outstandingRequests &  "00";
    outstandingRequests_x1 <= "00000" & outstandingRequests;
    
    xpm_fifo_rdata_inst : xpm_fifo_async
    generic map (
        CDC_SYNC_STAGES => 2,       -- DECIMAL
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "auto", -- String
        FIFO_READ_LATENCY => 2,     -- DECIMAL
        FIFO_WRITE_DEPTH => 512,    -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 10,  -- DECIMAL
        READ_DATA_WIDTH => 512,     -- DECIMAL  -- 512 data bits.
        READ_MODE => "std",         -- String
        RELATED_CLOCKS => 0,        -- DECIMAL
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "0404", -- String
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 512,    -- DECIMAL
        WR_DATA_COUNT_WIDTH => 10   -- DECIMAL
    ) port map (
        almost_empty => open,   -- 1-bit output: Almost Empty : When asserted, this signal indicates that only one more read can be performed before the FIFO goes to empty.
        almost_full => open,    -- 1-bit output: Almost Full: When asserted, this signal indicates that only one more write can be performed before the FIFO is full.
        data_valid => open,     -- 1-bit output: Read Data Valid: When asserted, this signal indicates that valid data is available on the output bus (dout).
        dbiterr => open,        -- 1-bit output: Double Bit Error: Indicates that the ECC decoder detected a double-bit error and data in the FIFO core is corrupted.
        dout => rdataFIFO_dout,   -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven when reading the FIFO.
        empty => rdataFIFO_empty, -- 1-bit output: Empty Flag: When asserted, this signal indicates that the FIFO is empty. Read requests are ignored when the FIFO is empty, initiating a read while empty is not destructive to the FIFO.
        full => open,             -- 1-bit output: Full Flag: When asserted, this signal indicates that the FIFO is full. 
        overflow => open,         -- 1-bit output: Overflow: 
        prog_empty => open,       -- 1-bit output: Programmable Empty.
        prog_full => open,        -- 1-bit output: Programmable Full: 
        rd_data_count => rdataFIFO_rdDataCount,    -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates the number of words read from the FIFO.
        rd_rst_busy => open,      -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO read domain is currently in a reset state.
        sbiterr => open,          -- 1-bit output: Single Bit Error: Indicates that the ECC decoder detected and fixed a single-bit error.
        underflow => open,        -- 1-bit output: Underflow: Indicates that the read request (rd_en) during the previous clock cycle was rejected because the FIFO is empty.
        wr_ack => open,           -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => rdataFIFO_wrDataCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,      -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO write domain is currently in a reset state.
        din => m02_axi_rdata,     -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',     -- 1-bit input: Double Bit Error Injection: Injects a double bit error the ECC feature is used on block RAMs or UltraRAM macros.
        injectsbiterr => '0',     -- 1-bit input: Single Bit Error Injection: Injects a single bit error if the ECC feature is used on block RAMs or UltraRAM macros.
        rd_clk => i_BF_clk,      -- 1-bit input: Read clock: Used for read operation. rd_clk must be a free running clock.
        rd_en => rdataFIFO_rdEn,  -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO.
        rst => rdataFIFO_rst,     -- 1-bit input: Reset: Must be synchronous to wr_clk. 
        sleep => '0',             -- 1-bit input: Dynamic power saving: If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_axi_clk,       -- 1-bit input: Write clock: Used for write operation. wr_clk must be a free running clock.
        wr_en => m02_axi_rvalid   -- 1-bit input: Write Enable: If the FIFO is not full, asserting this signal causes data (on din) to be written to the FIFO. 
    );
    
    m02_axi_rready <= '1'; -- we never make more requests than we can fit in the FIFO.
    
    --

    -- Get i_packetsPerFrame(9:0), i_stations(10:0) and i_coarse(9:0) into the i_BF_clk domain
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
            if (rd_fsm = start) then
                axi2bf_src_send <= '1';
            elsif axi2bf_src_rcv = '1' then
                axi2bf_src_send <= '0';
            end if;
            axi2BF_dataIn(9 downto 0) <= (others => '0');
            axi2BF_dataIn(20 downto 10) <= i_stations;
            axi2BF_dataIn(30 downto 21) <= i_coarse;
            axi2BF_dataIn(31) <= '0';
            axi2BF_dataIn(68 downto 32) <= rdFrameCount;  -- 37 bit count of corner turn frames since the SKA epoch
            
            if BF2axi_dest_req = '1' then
                o_readoutClocks <= BF2axi_dest_out;
            end if;
            
        end if;
    end process;
    
    xpm_cdc_handshake_psc_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        WIDTH => 69          -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => axi2bf_dest_out, -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => axi2bf_dest_req, -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => axi2bf_src_rcv,   -- 1-bit output: Acknowledgement from destination logic that src_in has been received. This signal will be deasserted once destination handshake has fully completed,
        dest_ack => '1', -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_BF_clk, -- 1-bit input: Destination clock.
        src_clk => i_axi_clk,   -- 1-bit input: Source clock.
        src_in => axi2BF_dataIn,  -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => axi2BF_src_send   -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );

    -- Get stats from the Beamform clock domain back into the axi clock domain for viewing in the register interface
    xpm_cdc_handshake_BF2axi_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        WIDTH => 32          -- DECIMAL; range: 1-1024
    )
    port map (
        dest_out => bf2axi_dest_out,  -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => bf2axi_dest_req,  -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => bf2axi_src_rcv,    -- 1-bit output: Acknowledgement from destination logic that src_in has been
                                      -- received. This signal will be deasserted once destination handshake has fully completed,
        dest_ack => '1',              -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_axi_clk,        -- 1-bit input: Destination clock.
        src_clk => i_BF_clk,          -- 1-bit input: Source clock.
        src_in => BF2axi_dataIn,       -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => BF2axi_src_send   -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );

    BF2axi_dataIn <= readoutClocks;
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
        
            BF2axi_transferCount <= std_logic_vector(unsigned(BF2axi_transferCount) + 1);
        
            if BF2axi_transferCount = "1111" then
                BF2axi_src_send <= '1';
            elsif bf2axi_src_rcv = '1' then
                BF2axi_src_send <= '0';
            end if;
            
            -- Get the number of stations, number of coarse channels and number of packets per frame from the clock domain crossing.
            -- Also, the clock crossing occurs once at the start of each corner turn frame (i.e. every 53ms),
            -- so is used to control the readout for a frame.
            if axi2bf_dest_req = '1' then
                BFstations <= axi2bf_dest_out(20 downto 10);
                BFcoarse <= axi2bf_dest_out(30 downto 21);
                BFFrameCount <= axi2bf_dest_out(68 downto 32);  -- Corner turn frame count since the SKA epoch
            end if;
            
            axi2bf_dest_reqDel1 <= axi2bf_dest_req;
            
            BFstationsMinusOne <= std_logic_vector(unsigned(BFstations) - 1);
            BFCoarseMinusOne <= std_logic_vector(unsigned(BFCoarse) - 1);
            BFpacketsPerFrameMinusOne <= std_logic_vector(unsigned(g_PACKETS_PER_FRAME) - 1);
            
            -- Get data out of the FIFO and pass it on.
            if axi2bf_dest_req = '1' then
                readout_fsm <= wait_poly_fifo;
                wait_poly_fifo_count <= "1111";
                outputFine <= "00000000";  -- This value is the current fine channel we are outputting divided by 3, so it runs 0-71
                outputStation <= "0000000000";
                outputCoarse <= "0000000000";
                outputTime <= "0000000000";  -- counts through packets in time; maximum is 256, assuming a 53 ms corner turn (24 SPS frames).
                outputPktOdd <= '0'; -- alternates between 0 and 1 as data goes to different output packets. Used downstream in the beamformers.
            else
                case readout_fsm is
                    when wait_start_fine =>
                        if gapCount_eq0 = '1' then
                            readout_fsm <= start_fine;
                        end if;
                    
                    when wait_poly_fifo =>
                        -- wait until there is polynomial data available in the FIFO.
                        if i_poly_fifo_empty = '0' and wait_poly_fifo_count = "0000" then
                            readout_fsm <= start_fine;
                        end if;
                        if wait_poly_fifo_count /= "0000" then
                            wait_poly_fifo_count <= std_logic_vector(unsigned(wait_poly_fifo_count) - 1);
                        end if;
                    
                    when start_fine =>  -- start of the read of a group of 216 fine channels (=1 coarse channel).
                        readout_fsm <= wait_fifo;
                    
                    when wait_fifo =>
                        if (unsigned(rdataFIFO_rdDataCount) > 2) then
                            readout_fsm <= p0;
                        end if;
                    
                    -- The 8 states p0, p1, p2, p3, p4, p5, p6, p7 read 3 x 512 bit words from the FIFO and output 8 x 192 bit words to the beamformers.
                    when p0 =>
                        readout_fsm <= p1;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1); -- "outputFine" is the fine channel/3. 3 fine channels are sent every clock, so it goes up by 1.
                    
                    when p1 =>
                        readout_fsm <= p2;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p2 =>
                        readout_fsm <= p3;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p3 =>
                        readout_fsm <= p4;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p4 =>
                        readout_fsm <= p5;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p5 =>
                        readout_fsm <= p6;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p6 =>
                        readout_fsm <= p7;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p7 =>
                        if ((unsigned(outputFine) = 71) and (BFlastStation = '1') and 
                            (BFlastCoarse = '1') and (BFlastTime = '1')) then
                            readout_fsm <= done;
                        elsif ((unsigned(outputFine) = 71) and
                               (BFlastStation = '1') and
                               (BFlastTimeIn32 = '1')) then
                            readout_fsm <= wait_start_fine;
                        elsif (unsigned(rdataFIFO_rdDataCount) > 3) then
                            -- The read data count on the FIFO has to be > 3 here, since the FIFO read in the p6 state has not yet propagated 
                            -- through to the rdataFIFO_rdDataCount output. In the wait_fifo state, the correct condition is >2.
                            readout_fsm <= p0;
                        else
                            readout_fsm <= wait_fifo;
                        end if;
                        
                        -- counters keeping track of the loops are:
                        --   outputFine
                        --   outputStation
                        --   outputTime
                        --   outputCoarse
                        --
                        -- The data output order is:
                        --
                        --    For timeGroup = 0:(<time samples per corner turn>/32 - 1)    -- For 60 ms corner turn, this is 0:(288/32-1) = 0:8
                        --       For Coarse = 0:(i_coarse-1)                               -- For 512 stations, 2 coarse, this is 0:1
                        --          For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
                        --             For Station = 0:(i_stations-1)
                        --                 For fine = 0:3:213 
                        --                     Send a 192 bit word (i.e. 3 fine channels).
                        --
                        if unsigned(outputFine) = 71 then   -- counts in groups of 3, so 71 = fine channels 213, 214, 215
                            if BFlastStation = '1' then
                                outputStation <= "0000000000";
                                if BFlastTimeIn32 = '1' then
                                    outputFine <= "00000000";
                                    outputPktOdd <= not outputPktOdd;
                                    if BFlastCoarse = '1' then
                                        outputCoarse <= (others => '0');
                                        if BFlastTime = '1' then
                                            outputTime <= "0000000000";
                                        else
                                            outputTime <= std_logic_vector(unsigned(outputTime) + 1);
                                        end if;
                                    else
                                        outputCoarse <= std_logic_vector(unsigned(outputCoarse) + 1);
                                        outputTime <= std_logic_vector(unsigned(outputTime) - 31);
                                    end if;
                                else
                                    outputFine <= "00000000"; -- back to the first fine channel for the next time sample.
                                    outputTime <= std_logic_vector(unsigned(outputTime) + 1);
                                end if;
                            else
                                outputFine <= "00000000"; -- back to the first fine channel for the same time sample, next station.
                                outputStation <= std_logic_Vector(unsigned(outputStation) + 1);
                            end if;
                        else
                            outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                        end if;
                        
                    when done =>
                        readout_fsm <= done;
                        
                    when others =>
                        readout_fsm <= done;
                end case;
            end if;
            
            if readout_fsm_del1 = start_fine then  -- need to use del1 because in the first run through minGapDataIn is not valid until one clock after we are in the state start_fine.
                gapCount <= minGapDataIn; -- std_logic_vector(to_unsigned(minGap,20));
            elsif unsigned(gapCount) /= 0 then
                gapCount <= std_logic_vector(unsigned(gapCount) - 1);
            end if;
            
            if axi2bf_dest_req = '1' then
                clocksPerCornerTurnOut <= (others => '0');
            elsif readout_fsm /= done then
                clocksPerCornerTurnOut <= std_logic_vector(unsigned(clocksPerCornerTurnOut) + 1);
            end if;
            
            if readout_fsm = done and readout_fsm_del1 /= done then
                readoutClocks <= clocksPerCornerTurnOut;
            end if;
            
            if (unsigned(gapCount) = 0) then
                gapCount_eq0 <= '1';
            else
                gapCount_eq0 <= '0';
            end if;
            
            if outputStation = BFstationsMinusOne(9 downto 0) then
                BFlastStation <= '1';
            else
                BFlastStation <= '0';
            end if;
            
            if unsigned(outputStation) = 0 then
                BFfirstStationDel1 <= '1';
            else
                BFfirstStationDel1 <= '0';
            end if; 
            
            if outputTime(4 downto 0) = "11111" then
                BFlastTimeIn32 <= '1';
            else
                BFlastTimeIn32 <= '0';
            end if;
            
            if (outputCoarse = BFcoarseMinusOne) then
                BFlastCoarse <= '1';
            else
                BFlastCoarse <= '0';
            end if;
            
            if (outputTime = BFpacketsPerFrameMinusOne) then
                BFlastTime <= '1';
            else
                BFlastTime <= '0';
            end if;
            
            
            readout_fsm_del1 <= readout_fsm;
            readout_fsm_del2 <= readout_fsm_del1;
            readout_fsm_del3 <= readout_fsm_del2;
            readout_fsm_del4 <= readout_fsm_del3;
            
            outputFineDel1 <= outputFine;
            outputFineDel2 <= outputFineDel1;
            outputFineDel3 <= outputFineDel2;
            outputFineDel4 <= outputFineDel3;
            
            outputStationDel1 <= outputStation;
            outputStationDel2 <= outputStationDel1;
            outputStationDel3 <= outputStationDel2;
            outputStationDel4 <= outputStationDel3;
            
            BFlastStationDel2 <= BFlastStation;  -- BFLastStation is already one clock behind outputStation
            BFlastStationDel3 <= BFlastStationDel2;
            BFlastStationDel4 <= BFlastStationDel3;
            
            BFfirstStationDel2 <= BFfirstStationDel1;
            BFfirstStationDel3 <= BFfirstStationDel2;
            BFfirstStationDel4 <= BFfirstStationDel3;
            
            outputCoarseDel1 <= outputCoarse;
            outputCoarseDel2 <= outputCoarseDel1;
            outputCoarseDel3 <= outputCoarseDel2;
            outputCoarseDel4 <= outputCoarseDel3;
            
            outputPktOddDel1 <= outputPktOdd;
            outputPktOddDel2 <= outputPktOddDel1;
            outputPktOddDel3 <= outputPktOddDel2;
            outputPktOddDel4 <= outputPktOddDel3;
            
            -- The output packet count is a count of the PST packets since the SKA epoch
            -- There are 32 time samples in each output PST packet, and 256 time samples in each corner turn frame.
            -- 
            packetCountDel1 <= outputTime(7 downto 5);
            packetCountDel2 <= packetCountDel1;
            packetCountDel3 <= packetCountDel2;
            packetCountDel4 <= BFFrameCount & packetCountDel3; -- 3 bits of PST packet within the frame, high 37 bits are the corner turn count
            
            outputTimeDel1 <= outputTime(9 downto 0);
            outputTimeDel2 <= outputTimeDel1;
            outputTimeDel3 <= outputTimeDel2;
            outputTimeDel4 <= outputTimeDel3;
            
            if readout_fsm = p0 or readout_fsm = p2 or readout_fsm = p5 then
                rdataFIFO_rdEn <= '1';
            else
                rdataFIFO_rdEn <= '0';
            end if;
            
            -- output register; converts 512 bit words from the FIFO to 192 bit words on the output bus
            -- Number of valid bits in the output register repeats every 3 reads from the FIFO :
            -- fsm state   Action
            --  p0  read FIFO ->                   0
            --  p1                (0+512 =)      512       --> 192 bits out
            --  p2  read FIFO ->  (512-192=)     320       --> 192 bits out
            --  p3                (320+512-192=) 640       --> 192 bits out
            --  p4                (640-192=)     448       --> 192 bits out
            --  p5  read FIFO ->  (448-192=)     256       --> 192 bits out
            --  p6                (256+512-192=) 576       --> 192 bits out
            --  p7                (576-192=)     384       --> 192 bits out
            --  p0  read FIFO ->  (384-192 =)    192       --> 192 bits out
            --                    (192+512-192=) 512
            --                    etc.
            
            if readout_fsm_del3 = p0 then  -- del3 to account for the fifo read latency.
                outputBuffer(511 downto 0) <= rdataFIFO_dout;
            elsif readout_fsm_del3 = p1 then
                outputBuffer(319 downto 0) <= outputBuffer(511 downto 192);
            elsif readout_fsm_del3 = p2 then
                outputBuffer(127 downto 0) <= outputBuffer(319 downto 192);
                outputBuffer(639 downto 128) <= rdataFIFO_dout;
            elsif readout_fsm_del3 = p3 then
                outputBuffer(447 downto 0) <= outputBuffer(639 downto 192);
            elsif readout_fsm_del3 = p4 then
                outputBuffer(255 downto 0) <= outputBuffer(447 downto 192);
            elsif readout_fsm_del3 = p5 then
                outputBuffer(575 downto 64) <= rdataFIFO_dout;
                outputBuffer(63 downto 0) <= outputBuffer(255 downto 192);
            elsif readout_fsm_del3 = p6 then
                outputBuffer(383 downto 0) <= outputBuffer(575 downto 192);
            elsif readout_fsm_del3 = p7 then
                outputBuffer(191 downto 0) <= outputBuffer(383 downto 192);
            end if;
            
            outputCoarse_x_stations <= unsigned(outputCoarse) * unsigned(BFstations);  -- 9 bit value x 11 bit value = 20 bit result.
            outputVirtualChannelDel2 <= std_logic_vector(unsigned(outputStationDel1) + unsigned(outputCoarse_x_stations(9 downto 0)));
            outputVirtualChannelDel3 <= outputVirtualChannelDel2;
            outputVirtualChannelDel4 <= outputVirtualChannelDel3;
            
            -- Actual outputs
            outputBufferDel1 <= outputBuffer(191 downto 0);
            outputBufferDel2 <= outputBufferDel1;
            outputBufferDel3 <= outputBufferDel2;
            outputBufferDel4 <= outputBufferDel3;
            outputBufferDel5 <= outputBufferDel4;
            outputBufferDel6 <= outputBufferDel5;
            o_data <= outputBufferDel6;  -- o_data has a 6 clock latency relative to the other beamformer outputs (o_valid, o_fine etc.). This is required by the beamformer. 
            
            for i in 0 to 11 loop
                if outputBufferDel5(i*16+15 downto i*16) = "1000000000000000" then
                    sample_flagged(i) <= '1';
                else
                    sample_flagged(i) <= '0';
                end if;
            end loop;
            
            if sample_flagged(3 downto 0) = "0000" then
                o_flagged(0) <= '0';  -- "o_flagged" aligns with o_data
            else
                o_flagged(0) <= '1';
            end if;
            if sample_flagged(7 downto 4) = "0000" then
                o_flagged(1) <= '0';
            else
                o_flagged(1) <= '1';
            end if;
            if sample_flagged(11 downto 8) = "0000" then
                o_flagged(2) <= '0';
            else
                o_flagged(2) <= '1';
            end if;
            
            
            if (readout_fsm_del4 = p0 or readout_fsm_del4 = p1 or readout_fsm_del4 = p2 or readout_fsm_del4 = p3 or
                readout_fsm_del4 = p4 or readout_fsm_del4 = p5 or readout_fsm_del4 = p6 or readout_fsm_del4 = p7) then
                valid_int <= '1';
            else
                valid_int <= '0';
            end if;
            o_fine <= outputFineDel4;        -- 8 bits
            o_coarse <= outputCoarseDel4;
            o_firstStation <= BFfirstStationDel4;
            o_lastStation  <= BFlastStationDel4;
            o_timeStep <= outputTimeDel4(4 downto 0);
            o_virtualChannel <= outputVirtualChannelDel4;  -- 10 bits
            o_packetCount <= packetCountDel4;
            o_outputPktOdd <= outputPktOddDel4;
            
            beams_enabled <= i_beams_enabled;
            -- Each beam requires c_minGapData clocks to output its packets. 
            minGapDataIn <= std_logic_vector(unsigned(c_minGapData) * unsigned(beams_enabled));
            
            -- Reconstruct the data we expect to see if in the test mode, where the data was replaced with meta data.
            outputFineDel4x2 <= outputFineDel3(6 downto 0) & '0';
            
            metaRecon(7 downto 0) <= std_logic_vector(unsigned(outputFineDel4) + unsigned(outputFineDel4x2));  -- outputFineDel4 should align with the data in outputBuffer(31:0)
            metaRecon(19 downto 8) <= "00" & outputTimeDel4;
            metaRecon(31 downto 20) <= "00" & outputVirtualChannelDel4;
            
            if ((metaRecon /= outputBufferDel1(31 downto 0)) and valid_int = '1') then
                dataMismatchInt <= '1';
            else
                dataMisMatchInt <= '0';
            end if;
            o_dataMismatchBFclk <= dataMisMatchInt;
        end if;
    end process;
    
    o_valid <= valid_int;
    
    -- convert dataMisMatch to the i_axi_clk clock domain.
    xpm_cdc_pulsedm_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 0,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    ) port map (
        dest_pulse => o_dataMisMatch, -- 1-bit output: Outputs a pulse the size of one dest_clk period when a pulse transfer is correctly initiated on src_pulse input.
        dest_clk => i_axi_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',     -- 1-bit input: optional; required when RST_USED = 1
        src_clk => i_BF_clk,       -- 1-bit input: Source clock.
        src_pulse => dataMismatchInt,   -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'        -- 1-bit input: optional; required when RST_USED = 1
    );
    
    
    xpm_cdc_single_bp_i : xpm_cdc_single
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1   -- DECIMAL; 0=do not register input, 1=register input
    ) port map (
        dest_out => bad_polynomials_BF_clk, -- 1-bit output: src_in synchronized to the destination clock domain.
        dest_clk => i_BF_clk,          -- 1-bit input: Clock signal for the destination clock domain.
        src_clk  => i_axi_clk,         -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in   => bad_polynomials    -- 1-bit input: Input signal to be synchronized to dest_clk domain.
    );
    
    o_good_polynomials <= not bad_polynomials_BF_clk;
    
end Behavioral;
