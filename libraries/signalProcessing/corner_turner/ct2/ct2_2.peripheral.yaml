schema_name   : args
schema_version: 1.0
schema_type   : peripheral

hdl_library_name       : ct2_2
hdl_library_description: "PST output corner turn and beamformer"

peripherals:
  - peripheral_name        : ct2_2
    peripheral_description : "Control the PST output corner turn"
    slave_ports:
      - slave_name        : StatCtrl
        slave_type        : reg
        number_of_slaves  : 1
        slave_description : "PST output corner turn"
        fields:
          #################################
          - - field_name        : bufferOverflowError
              width             : 1
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Write buffer used to transfer filterbank data into the HBM overflowed. Cleared by a reset in the first stage corner turn. Should never happen."
          #################################
          - - field_name        : readoutError
              width             : 1
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Readout to the beamformer did not finish before the readout of the next HBM buffer started. Cleared by a reset passed in from the first stage corner turn. Should never happen."
          #################################
          - - field_name        : bufferEnable
              width             : 4
              access_mode       : RW
              reset_value       : 0xf
              field_description : "1 bit to enable each of the 4 blocks of 256 MBytes of HBM. Each 256 MBytes of HBM is a single output buffer."
          #################################
          - - field_name        : HBMBuf0CornerTurnCount_low
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "low 32 bits of the corner turn frame count for HBM buffer 0. Each corner turn frame is 53084160 ns. \
                                   The frame currently being written into is the larger of HBMBuf0CornerTurnCount_low/high and HBMBuf1CornerTurnCount_low/high \
                                   The frame being read out is the smaller of the two values. "
          - - field_name        : HBMBuf0CornerTurnCount_high
              width             : 8
              access_mode       : RO
              reset_value       : 0x0
              field_description : "High 8 bits of the corner turn frame count for HBM buffer 0"
          #################################
          - - field_name        : HBMBuf1CornerTurnCount_low
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "low 32 bits of the corner turn frame count for HBM buffer 1"
          - - field_name        : HBMBuf1CornerTurnCount_high
              width             : 8
              access_mode       : RO
              reset_value       : 0x0
              field_description : "high 16 bits of the corner turn frame count for HBM buffer 1"
          #################################
          - - field_name        : jones_buffer0_valid_frame_low
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "bits 31:0 of the corner turn frame count at which jones buffer 0 (which is located in the beamformer) becomes valid.\
                                   In units of corner turn frames since the epoch, where a frame is 53.084160 ms."
          - - field_name        : jones_buffer0_valid_frame_high
              width             : 16
              access_mode       : RW
              reset_value       : 0x0
              field_description : "bits 47:32 of the corner turn frame count at which jones buffer 0 becomes valid."
          - - field_name        : jones_buffer0_valid_duration
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Number of corner turn frames for which jones buffer 0 is valid. \
                                   Each corner turn frame is 53.084160 ms, so this allows for up to 7 years \
                                   If this value is xffffffff then the Jones buffer is assumed to hold default values (i.e. identity matrix) \
                                   This case is flagged in the output packet to PST. \
                                   Alternatively, if no Jones buffer is valid, that is flagged separately in the PST output packet \
                                   "
          - - field_name        : jones_buffer1_valid_frame_low
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "bits 31:0 of the corner turn frame count at which jones buffer 1 (which is located in the beamformer) becomes valid.\
                                   In units of corner turn frames since the epoch, where a frame is 53.084160 ms."
          - - field_name        : jones_buffer1_valid_frame_high
              width             : 16
              access_mode       : RW
              reset_value       : 0x0
              field_description : "bits 47:32 of the corner turn frame count at which jones buffer 1 becomes valid."
          - - field_name        : jones_buffer1_valid_duration
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Number of corner turn frames for which jones buffer 1 is valid."
          #################################
          - - field_name        : poly_buffer0_info_valid
              width             : 1
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Set 1 to indicate that all the registers (including the polynomials themselves) associated with poly buffer 0 are valid. \
                                   This is only checked at the start of a corner turn frame, which lasts 53 ms. So after setting to 0, software should wait at \
                                   least 53 ms before changing the rest of the configuration. " 
          - - field_name        : poly_buffer0_valid_frame_low
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "bits 31:0 of the corner turn frame count at which polynomial buffer 0 (which is located in this module) becomes valid. In units of corner turn frames since the epoch, where a frame is 53.084160 ms."
          - - field_name        : poly_buffer0_valid_frame_high
              width             : 16
              access_mode       : RW
              reset_value       : 0x0
              field_description : "bits 47:32 of the corner turn frame count at which polynomial buffer 0 becomes valid."
          - - field_name        : poly_buffer0_valid_duration
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Number of corner turn frames for which polynomial buffer 0 is valid."
          - - field_name        : poly_buffer0_offset_ns
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Nanoseconds from the polynomial epoch to the start of poly_buffer0_valid_frame_high/low, as an integer."
          - - field_name        : poly_buffer1_info_valid
              width             : 1
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Set 1 to indicate that all the registers (including the polynomials themselves) associated with poly buffer 0 are valid. \
                                   This is only checked at the start of a corner turn frame, which lasts 53 ms. So after setting to 0, software should wait at \
                                   least 53 ms before changing the rest of the configuration. " 
          - - field_name        : poly_buffer1_valid_frame_low
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "bits 31:0 of the corner turn frame count at which polynomial buffer 1 (which is located in this module) becomes valid. In units of corner turn frames since the epoch, where a frame is 53.084160 ms."
          - - field_name        : poly_buffer1_valid_frame_high
              width             : 16
              access_mode       : RW
              reset_value       : 0x0
              field_description : "bits 47:32 of the corner turn frame count at which polynomial buffer 1 becomes valid."
          - - field_name        : poly_buffer1_valid_duration
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Number of corner turn frames for which polynomial buffer 1 is valid."
          - - field_name        : poly_buffer1_offset_ns
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Nanoseconds from the polynomial epoch to the start of poly_buffer0_valid_frame_high/low, as an integer."
          #################################
          - - field_name        : scaleFactor
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Single precision float scaling factor applied in the beamformer to form output packets. Use 0 for firmware to calculate the scaling factor."                  
          #################################
          - - field_name        : BeamsEnabled
              width             : 8
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Number of PST beams currently enabled."              
          #################################
          - - field_name        : NumberOfBeams
              width             : 8
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Number of PST beams supported by this firmware build"
          #################################
          - - field_name        : readInClocks
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Length of the most recent frame in units of 300 MHz clocks at the input to the corner turn."
          #################################
          - - field_name        : readInAllClocks
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Interval between the start of one frame and the start of the next frame in units of 300 MHz clocks at the input to the corner turn."
          #################################
          - - field_name        : readOutClocks
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Length of the most recent frame in units of 400 MHz clocks at the output of the corner turn"
          #################################
          - - field_name        : sof_count
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "debug word"
          - - field_name        : fb_packet_count
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "debug word"
          - - field_name        : cur_frameCount
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "debug word"
          - - field_name        : recent_vcs
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "debug word"
          - - field_name        : dbg_packetInFrame
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "debug word"
          #################################
          - - field_name        : HBM_reset
              width             : 1
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Set this to 1 and then monitor HBM_reset status."
          #################################
          - - field_name        : HBM_reset_status
              width             : 8
              access_mode       : RO
              reset_value       : 0x0
              field_description : "When the lower nibble reads 1 then both HBM read and WR queues have been zeroed out, ready to reconfigure logic.
                                   upper nibble indicates the state of the HBM reset SM.
                                   0 - IDLE
                                   1 - Waiting for WR queue to balance
                                   2 - WRs diabled
                                   3 - Wait for Reads to complete
                                   4 - HBM transfers now complete and resets can occur, at this point the lower nibble should show 1."
       
  - peripheral_name        : PSTbeamPolynomials2
    peripheral_description : "Program the beamformer polynomials"
    slave_ports:
      - slave_name        : PSTbeamPolynomials2
        slave_type        : RAM
        number_of_slaves  : 1
        slave_description : "PST beamformer polynomials \
  Memory space to specify parameters for up to 512 stations x 16 beams, i.e. 8192 polynomials. \
  Each polynomial is 6 single precision floating point values, i.e. 24 bytes \
  Total space allocated : (512 stations) * (16 beams) * (32 bytes) * (2 buffers) = 524288 bytes \
  Byte address = (buffer * 196608) + (beam * 12288) + (station * 24)  \
  where buffer = 0 or 1, beam = 0:15, station = 0:511 \
  The total space allocated is 131072*4 = 524288 bytes, but only 393216 bytes are implemented \
   \
  second block of data at byte address 393216: \
   8192 bytes : \
   for each of 1024 virtual channels, we need \
    sky frequency - 4 byte single precision \
    station number - 9 bits, used to look up the polynomial \
   
  "
        fields            :
          - - field_name        : data
              width             : 32
              user_width        : 32
              access_mode       : RW
              number_of_fields  : 131072
              interface         : simple
              reset_value       : 0
              field_description : "Args ignores this for RAMs"
                           