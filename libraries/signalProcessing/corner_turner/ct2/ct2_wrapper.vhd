----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 06.11.2020 23:01:54
-- Module Name: ct_atomic_pst_wrapper - Behavioral
-- Description: 
--  Wrapper for the PST corner turn module.
--  Two submodules -
--   - Register interface,
--   - Actual corner turn.
----------------------------------------------------------------------------------

library IEEE, axi4_lib, common_lib, ct_lib;
use IEEE.STD_LOGIC_1164.ALL;
use axi4_lib.axi4_lite_pkg.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
USE common_lib.common_pkg.ALL;
--library DSP_top_lib;
--use DSP_top_lib.DSP_top_pkg.all;
use ct_lib.ct2_reg_pkg.all;
use IEEE.NUMERIC_STD.ALL;
Library xpm;
use xpm.vcomponents.all;

entity ct2_wrapper is
    generic (
        g_PST_BEAMS : integer;
        g_USE_META : boolean := FALSE  -- Put meta data into the memory in place of the actual data, to make it easier to find bugs in the corner turn. 
    );
    Port(
        -- Parameters, in the i_axi_clk domain.
        i_stations : in std_logic_vector(10 downto 0); -- up to 1024 stations
        i_coarse   : in std_logic_vector(9 downto 0);  -- Number of coarse channels.
        i_virtualChannels : in std_logic_vector(10 downto 0); -- total virtual channels (= i_stations * i_coarse)
        i_bad_polynomials : in std_logic;
        -- Registers AXI Lite Interface (uses i_axi_clk)
        i_axi_mosi     : in t_axi4_lite_mosi;
        o_axi_miso     : out t_axi4_lite_miso;
        i_axi_rst      : in std_logic;
        -- Polynomial memory axi full interface
        i_axi_full_mosi : in t_axi4_full_mosi;
        o_axi_full_miso : out t_axi4_full_miso;
        -- reset should be passed in from the reset for the first corner turn, in the i_axi_clk domain.
        i_rst          : in std_logic;
        -- Data in from the PST filterbanks; bursts of 216 clocks for each channel.
        -- 
        i_sof          : in std_logic; -- pulse high at the start of every frame. (1 frame is typically 60ms of data).
        i_FB_clk       : in std_logic; -- filterbank clock, expected to be 450 MHz
        i_frameCount     : in std_logic_vector(36 downto 0); -- frame count is the same for all simultaneous output streams.
        i_virtualChannel : in t_slv_16_arr(2 downto 0); -- 3 virtual channels, one for each of the PST data streams.
        i_HeaderValid : in std_logic_vector(2 downto 0);
        i_data        : in t_slv_64_arr(2 downto 0); -- (2 pol)x(16+16 bit complex) = 64 bits, for each of 3 virtual channels.
        i_dataValid   : in std_logic;
        
        -- Data out to the beamformers
        i_BF_clk  : in std_logic; -- beamformer clock, expected to be 400 MHz
        o_data    : out std_logic_vector(191 downto 0); -- (3 consecutive fine channels) * (2 pol) * (2+2 complex) = 24 bytes = 192 bits
        o_flagged : out std_logic_vector(2 downto 0);  -- "o_flagged" is aligned with "o_data"
        o_fine    : out std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        o_coarse  : out std_logic_vector(9 downto 0);  -- index of the coarse (LFAA) frequency channel.
        o_firstStation : out std_logic;
        o_lastStation : out std_logic;
        o_timeStep : out std_logic_vector(4 downto 0);
        o_virtualChannel  : out std_logic_vector(9 downto 0);  -- coarse channel count.
        o_packetCount : out std_logic_vector(39 downto 0); -- PST output packet count, units of 6.63552 ms since the SKA epoch.
        o_outputPktOdd : out std_logic;    -- alternates 0/1 for bursts of data that correspond to a burst of output packets (i.e. 32 time samples, all stations, 1 coarse channel)
        o_valid   : out std_logic;
        -- polynomial data 
        o_phase_virtualChannel : out std_logic_vector(9 downto 0);
        o_phase_timeStep : out std_logic_vector(7 downto 0);
        o_phase_beam : out std_logic_vector(3 downto 0);
        o_phase : out std_logic_vector(23 downto 0);      -- Phase at the start of the coarse channel.
        o_phase_step : out std_logic_vector(23 downto 0); -- Phase step per fine channel.
        o_phase_valid : out std_logic;
        o_phase_clear : out std_logic;
        -- Configuration to the beamformers. These will be stable for the duration of the corner turn frame.
        o_jonesBuffer  : out std_logic;                    -- Which jones buffer to use.
        o_jones_status : out std_logic_vector(1 downto 0); -- bit 0 = used default, bit 1 = jones valid
        o_poly_ok      : out std_logic_vector(1 downto 0); -- The polynomials used are in range. 
        o_beamsEnabled : out std_logic_vector(7 downto 0);
        o_scale_exp_frac : out std_logic_vector(7 downto 0); -- bit 3 = 0 indicates firmware should calculate the scale factor.
        -----------------------------------------------------------
        -- AXI interfaces to the HBM.
        -- Corner turn between filterbanks and beamformer
        i_axi_clk : in std_logic;
        -----------------------------------------------------------
        -- aw bus = write address
        m0_axi_awvalid  : out std_logic;
        m0_axi_awready  : in std_logic;
        m0_axi_awaddr   : out std_logic_vector(29 downto 0);
        m0_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m0_axi_wvalid    : out std_logic;
        m0_axi_wready    : in std_logic;
        m0_axi_wdata     : out std_logic_vector(511 downto 0);
        m0_axi_wlast     : out std_logic;
        -- b bus - write response
        m0_axi_bvalid    : in std_logic;
        m0_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m0_axi_arvalid   : out std_logic;
        m0_axi_arready   : in std_logic;
        m0_axi_araddr    : out std_logic_vector(29 downto 0);
        m0_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m0_axi_rvalid    : in std_logic;
        m0_axi_rready    : out std_logic;
        m0_axi_rdata     : in std_logic_vector(511 downto 0);
        m0_axi_rlast     : in std_logic;
        m0_axi_rresp     : in std_logic_vector(1 downto 0);
        -- debug
        o_dataMismatch      : out std_logic;
        o_dataMismatchBFclk : out std_logic;
        
        -- hbm reset
        o_hbm_reset        : out std_logic;
        i_hbm_status       : in std_logic_vector(7 downto 0)
    );
end ct2_wrapper;

architecture Behavioral of ct2_wrapper is

    -- Each LFAA block is 2048 samples, the PST filterbank uses a 256 point FFT, and is 4/3 oversampled,
    -- so the number of output packets per frame is 2048/256 * 4/3 * g_LFAA_BLOCKS_PER_FRAME
    -- Used to be a generic, but only allowed (i.e. only tested) value is 8, for 24 LFAA packets per frame = 256 output packets per frame. 
    constant  g_LFAA_BLOCKS_PER_FRAME_DIV3 : integer := 8;  -- e.g. value of 1 means there are 3 LFAA blocks per frame for the corner turn, which corresponds to 32 filterbank output packets.

    signal reg_rw : t_statctrl_rw;
    signal reg_ro : t_statctrl_ro;
    signal packetsPerFrame : std_logic_vector(9 downto 0);
    
    signal clkCrossSendCount : std_logic_vector(5 downto 0) := "000000";
    signal triggerSend, triggerSendDel1 : std_logic := '0';
    signal axi2bf_src_send, axi2bf_src_rcv, axi2bf_dest_req : std_logic := '0';
    signal axi2bf_in, axi2bf_dest_out : std_logic_vector(241 downto 0);
    
    signal ct_axi_awvalid : std_logic_vector(0 downto 0);
    signal ct_axi_awready : std_logic_vector(0 downto 0);
    signal ct_axi_awaddr  : std_logic_vector(31 downto 0);
    signal ct_axi_awlen   : std_logic_vector(7 downto 0);
    -- w bus - write data
    signal ct_axi_wvalid : std_logic_vector(0 downto 0);
    signal ct_axi_wready : std_logic_vector(0 downto 0);
    signal ct_axi_wdata  : std_logic_vector(255 downto 0);
    signal ct_axi_wlast  : std_logic_vector(0 downto 0);
    -- b bus - write response
    signal ct_axi_bvalid : std_logic_vector(0 downto 0);
    signal ct_axi_bresp  : std_logic_vector(1 downto 0);
    -- ar bus - read address
    signal ct_axi_arvalid : std_logic_vector(0 downto 0);
    signal ct_axi_arready : std_logic_vector(0 downto 0);
    signal ct_axi_araddr  : std_logic_vector(31 downto 0);
    signal ct_axi_arlen   : std_logic_vector(7 downto 0);
    -- r bus - read data
    signal ct_axi_rvalid : std_logic_vector(0 downto 0);
    signal ct_axi_rready : std_logic_vector(0 downto 0);
    signal ct_axi_rdata  : std_logic_vector(255 downto 0);
    signal ct_axi_rlast  : std_logic_vector(0 downto 0);
    signal ct_axi_rresp  : std_logic_vector(1 downto 0);
    signal ct_axi_bready : std_logic_vector(0 downto 0);
    
    -- 32 bit versions of the address
    signal m0_axi_araddr32bit, m0_axi_awaddr32bit : std_logic_vector(31 downto 0);
    signal m1_axi_araddr32bit, m1_axi_awaddr32bit : std_logic_vector(31 downto 0);
    signal m2_axi_araddr32bit, m2_axi_awaddr32bit : std_logic_vector(31 downto 0);
    signal m3_axi_araddr32bit, m3_axi_awaddr32bit : std_logic_vector(31 downto 0);
    signal axi_rstn : std_logic;
    
    signal beamsEnabled : std_logic_vector(7 downto 0);
    signal axi_scale_exp, axi_shift  : std_logic_vector(7 downto 0);
    signal scale_exp, scale_frac, axi_shift_final : std_logic_vector(3 downto 0);
    
    signal vcmap_rdAddr : std_logic_Vector(9 downto 0);
    signal vcmap_dout : std_logic_Vector(63 downto 0);
    signal polymem_rdAddr : std_logic_Vector(15 downto 0);
    signal polymem_dout : std_logic_Vector(63 downto 0);
    
    signal bfclk_poly_buffer0_valid_frame, bfclk_poly_buffer1_valid_frame : std_logic_vector(47 downto 0);
    signal bfclk_poly_buffer0_valid_duration, bfclk_poly_buffer1_valid_duration : std_logic_vector(31 downto 0);
    
    signal bfclk_stations : std_logic_vector(10 downto 0);
    signal bfclk_coarse : std_logic_vector(9 downto 0);
        
    signal bfclk_poly_buffer0_offset_ns, bfclk_poly_buffer1_offset_ns : std_logic_vector(31 downto 0);
    signal bfclk_poly_buffer0_info_valid, bfclk_poly_buffer1_info_valid : std_logic;
    
    signal poly_phase_virtualChannel : std_logic_vector(9 downto 0);
    signal poly_phase_timestep : std_logic_vector(7 downto 0);
    signal poly_phase_beam : std_logic_vector(3 downto 0);  -- 0 to 15
    signal poly_phase : std_logic_vector(23 downto 0);      -- Phase at the start of the coarse channel.
    signal poly_phase_step : std_logic_vector(23 downto 0); -- Phase step per fine channel.
    signal poly_phase_valid : std_logic;
    signal poly_stop : std_logic;
    signal poly_fifo_din : std_logic_vector(69 downto 0);
    signal poly_fifo_wr_data_count : std_logic_vector(9 downto 0);
    signal poly_fifo_dout : std_logic_vector(69 downto 0);
    signal HBMBuf0_ct_count : std_logic_vector(36 downto 0);
    signal HBMBuf1_ct_count : std_logic_vector(36 downto 0);
    signal poly_fifo_empty : std_logic;
    signal readout_start_axi, readout_start_BFclk : std_logic;
    signal readout_ctframe_BFclk, readout_ctframe_axi, readout_ctframe_axi_hold : std_logic_vector(36 downto 0);
    signal jones_buf0_valid_frame, jones_buf1_valid_frame, jones_buf0_valid_duration, jones_buf1_valid_duration : std_logic_vector(36 downto 0);
    signal jones_buf0_end_frame, jones_buf1_end_frame : std_logic_vector(36 downto 0);
    signal jones_buf0_gt_cur_time, jones_buf1_gt_cur_time, jones_buf0_valid_forever, jones_buf1_valid_forever, jones_buf0_more_recent : std_logic;
    signal jones_buf0_valid, jones_buf1_valid, jones_buf0_more_recent_del1, jones_buf0_valid_forever_del1, jones_buf1_valid_forever_del1 : std_logic;
    signal readout_start_axi_del3, readout_start_axi_del2, readout_start_axi_del1 : std_logic;
    signal jones_sel_bits, jones_sel_bits_BF_clk : std_logic_vector(2 downto 0);
    signal sel_jones_buf, jones_used_default, jones_valid : std_logic := '0';
    signal fine_int : std_logic_vector(7 downto 0);
    signal valid_int : std_logic;
    signal poly_fifo_rst, poly_fifo_rdEn, read_16_delays : std_logic;
    signal readout_start_del : std_logic_vector(15 downto 0);
    signal delays_to_send : std_logic_vector(7 downto 0) := x"00";
    signal poly_fifo_rdEn_del1 : std_logic;
    
begin
    
    packetsPerFrame <= std_logic_vector(to_unsigned(g_LFAA_BLOCKS_PER_FRAME_DIV3*32,10));
    
    ct_inst : entity ct_lib.ct2_out
    generic map (
        g_PST_BEAMS => g_PST_BEAMS,
        g_USE_META => g_USE_META,
        g_PACKETS_PER_FRAME => "0100000000"
    ) port map(
        -- Parameters, in the i_axi_clk domain.
        i_stations => i_stations(10 downto 0),       -- in (10:0); -- up to 1024 stations
        i_coarse   => i_coarse(9 downto 0),          -- in (9:0);  -- Number of coarse channels.
        i_virtualChannels => i_virtualChannels(10 downto 0), -- in (10:0); -- total virtual channels (= i_stations * i_coarse)
        i_HBMBufferEnable    => reg_rw.bufferEnable, -- in (3:0);
        o_HBMBuf0PacketCount => HBMBuf0_ct_count,    -- out (36:0);
        o_HBMBuf1PacketCount => HBMBuf1_ct_count,    -- out (36:0);
        i_beams_enabled  => beamsEnabled,            -- in (7:0);
        i_bad_polynomials => i_bad_polynomials,      -- in std_logic;
        -- Indicate start of readout of a corner turn frame
        -- Outputs on i_axi_clk
        o_readout_start => readout_start_axi,   -- out std_logic;
        o_readout_frame => readout_ctframe_axi, -- out (36:0); Corner turn frame that we are about to start reading out, relative to the SKA epoch
        -- Polynomial FIFO is empty, on i_BF_clk 
        i_poly_fifo_empty => poly_fifo_empty, -- in std_logic;
        -- Number of stations and number of coarse channels to use for readout, on bf_clk
        o_stations_bf_clk => bfclk_stations, -- out (10:0);
        o_coarse_bf_clk => bfclk_coarse,     -- out (9:0);
        --
        i_rst => i_rst, -- in std_logic;
        -- Data in from the PST filterbanks; bursts of 216 clocks for each channel.
        -- 
        i_sof          => i_sof, --  in std_logic; Pulse high at the start of every frame. (1 frame is 53 ms of data).
        i_FB_clk       => i_FB_clk, -- in std_logic; Filterbank clock, expected to be 450 MHz
        i_frameCount     => i_frameCount, -- in (36:0); Frame count is the same for all simultaneous output streams.
        i_virtualChannel => i_virtualChannel, -- in t_slv_16_arr(2 downto 0); 3 virtual channels, one for each of the PST data streams.
        i_HeaderValid => i_HeaderValid, -- in std_logic_vector(2 downto 0);
        i_data        => i_data, -- in t_ctc_output_payload_arr(2 downto 0); -- 8 bit data; fields are Hpol.re, .Hpol.im, .Vpol.re, .Vpol.im, for each of i_data(0), i_data(1), i_data(2)
        i_dataValid   => i_dataValid, -- in std_logic;
        -- Data out to the beamformer
        i_BF_clk  => i_BF_clk,  -- in std_logic; -- beamformer clock, expected to be 400 MHz
        o_data    => o_data,    -- out (191:0); -- 3 consecutive fine channels delivered every clock.
        o_flagged => o_flagged, -- out (2:0);
        o_fine    => fine_int,  -- out (7:0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        o_coarse  => o_coarse,  -- out (9:0);
        o_firstStation => o_firstStation, -- out std_logic;
        o_lastStation => o_lastStation,   -- out std_logic;
        o_timeStep => o_timeStep,         -- out (4:0)
        o_virtualChannel  => o_virtualChannel, -- out (9:0);
        o_packetCount => o_packetCount,        -- out (39:0);
        o_outputPktOdd => o_outputPktOdd,      -- out std_logic;
        o_good_polynomials => o_poly_ok(0),     -- bad station polynomials.
        o_valid   => valid_int,                -- out std_logic;
        -----------------------------------------------------------
        -- AXI interface to the HBM
        -- Corner turn between filterbanks and beamformer
        -- aw bus = write address
        i_axi_clk => i_axi_clk, -- in std_logic;
        -- 
        m02_axi_awvalid  => m0_axi_awvalid, -- out std_logic;
        m02_axi_awready  => m0_axi_awready, -- in std_logic;
        m02_axi_awaddr   => m0_axi_awaddr(29 downto 0),  -- out (29:0);
        m02_axi_awlen    => m0_axi_awlen,   -- out (7:0);
        -- w bus - write data
        m02_axi_wvalid   => m0_axi_wvalid, -- out std_logic;
        m02_axi_wready   => m0_axi_wready, -- in std_logic;
        m02_axi_wdata    => m0_axi_wdata,  -- out (511:0);
        m02_axi_wlast    => m0_axi_wlast,  -- out std_logic;
        -- b bus - write response
        m02_axi_bvalid   => m0_axi_bvalid, -- in std_logic;
        m02_axi_bresp    => m0_axi_bresp,  -- in (1:0);
        -- ar bus - read address
        m02_axi_arvalid  => m0_axi_arvalid, -- out std_logic;
        m02_axi_arready  => m0_axi_arready, -- in std_logic;
        m02_axi_araddr   => m0_axi_araddr(29 downto 0),  -- out (29:0);
        m02_axi_arlen    => m0_axi_arlen,   -- out (7:0);
        -- r bus - read data
        m02_axi_rvalid   => m0_axi_rvalid,  -- in std_logic;
        m02_axi_rready   => m0_axi_rready,  -- out std_logic;
        m02_axi_rdata    => m0_axi_rdata,   -- in (255:0);
        m02_axi_rlast    => m0_axi_rlast,   -- in std_logic;
        m02_axi_rresp    => m0_axi_rresp,   -- in (1:0);
        ---------------------------------------------------
        -- Error signals on the i_axi_clk domain.
        o_bufferOverflowError => reg_ro.bufferoverflowerror, -- out std_logic; buffer between data input from the filterbanks and the HBM has overflowed; this should never happen.
        o_readoutError => reg_ro.readoutError,  -- out std_logic; readout to the beamformer didn't finish before the next readout started. This should never happen.
        o_readoutClocks => reg_ro.readoutClocks, -- out (31:0), Number of beamformer (400MHz) clocks required to read the most recent frame out from HBM
        o_readInClocks  => reg_ro.readInClocks,   -- out (31:0), Number of axi (300MHz) clocks required to write the most recent frame into HBM
        o_readInAllClocks => reg_ro.readInAllClocks, -- out (31:0), Number of axi (300MHz) clocks between the start of one frame and the start of the next.
        o_dataMismatch => o_dataMismatch,
        --
        o_sof_count         => reg_ro.sof_count, -- : out std_logic_vector(31 downto 0);
        o_fb_packet_count   => reg_ro.fb_packet_count, -- : out std_logic_vector(31 downto 0);
        o_cur_frameCount    => reg_ro.cur_frameCount, -- : out std_logic_vector(31 downto 0);
        o_recent_vcs        => reg_ro.recent_vcs,   -- : out std_logic_vector(31 downto 0);
        o_dbg_packetInFrame => reg_ro.dbg_packetInFrame, -- : out std_logic_vector(31 downto 0);
        --
        o_dataMismatchBFclk => o_dataMismatchBFclk  -- out std_logic
    );
    
    o_fine <= fine_int;
    o_valid <= valid_int;
    
    o_hbm_reset                         <= reg_rw.hbm_reset;
    reg_ro.hbm_reset_status             <= i_hbm_status;
    
    reg_ro.HBMBuf0CornerTurnCount_low <= HBMBuf0_ct_count(31 downto 0);
    reg_ro.HBMBuf0CornerTurnCount_high <= "000" & HBMBuf0_ct_count(36 downto 32);
    reg_ro.HBMBuf1CornerTurnCount_low <= HBMBuf1_ct_count(31 downto 0);
    reg_ro.HBMBuf1CornerTurnCount_high <= "000" & HBMBuf1_ct_count(36 downto 32);
    
    ct_axi_araddr(31 downto 30) <= "00";
    ct_axi_awaddr(31 downto 30) <= "00";
    -- axi signals that are not used by the corner turn.
    ct_axi_bready(0) <= '1';
    -------------------------------------------------------------------------------
    -- Convert single AXI bus to from the corner turn core module above into 
    -- 4 separate AXI busses, one for each 256 MByte piece of the HBM.
    -- This is to maximize the bandwidth to the HBM
    axi_rstn <= not i_axi_rst;
    
    reginst : entity ct_lib.ct2_reg
    port map (
        MM_CLK              => i_axi_clk, -- IN    STD_LOGIC;
        MM_RST              => i_axi_rst, -- IN    STD_LOGIC;
        SLA_IN              => i_axi_mosi, -- IN    t_axi4_lite_mosi;
        SLA_OUT             => o_axi_miso, -- OUT   t_axi4_lite_miso;
        STATCTRL_FIELDS_RW	=> reg_rw, --  OUT t_statctrl_rw;
        STATCTRL_FIELDS_RO	=> reg_ro  -- IN  t_statctrl_ro
    );
    
    reg_ro.numberofbeams <= std_logic_vector(to_unsigned(g_PST_BEAMS,8));
    
    -- Get reg_rw.beamsEnabled into the i_BF_clk domain
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
            clkCrossSendCount <= std_logic_vector(unsigned(clkCrossSendCount) + 1);
            triggerSend <= clkCrossSendCount(5);
            triggerSendDel1 <= triggerSend;
        
            if (triggerSend = '1' and triggerSendDel1 = '0') then
                axi2bf_src_send <= '1';
            elsif axi2bf_src_rcv = '1' then
                axi2bf_src_send <= '0';
            end if;
            
            axi2bf_in(7 downto 0) <= reg_rw.beamsEnabled;
            axi2bf_in(15 downto 12) <= axi_shift_final;
            if (unsigned(reg_rw.scaleFactor) = 0) then
                axi2bf_in(11 downto 8) <= "0000";
            else
                axi2bf_in(11 downto 8) <= '1' & reg_rw.scaleFactor(22 downto 20);
            end if;
            --
            axi2bf_in(47 downto 16) <= reg_rw.poly_buffer0_valid_frame_low(31 downto 0);
            axi2bf_in(63 downto 48) <= reg_rw.poly_buffer0_valid_frame_high(15 downto 0);
            axi2bf_in(95 downto 64) <= reg_rw.poly_buffer0_valid_duration(31 downto 0);
            axi2bf_in(127 downto 96) <= reg_rw.poly_buffer1_valid_frame_low(31 downto 0);
            axi2bf_in(143 downto 128) <= reg_rw.poly_buffer1_valid_frame_high(15 downto 0);
            axi2bf_in(175 downto 144) <= reg_rw.poly_buffer1_valid_duration(31 downto 0);
            axi2bf_in(207 downto 176) <= reg_rw.poly_buffer0_offset_ns(31 downto 0); -- single precision value
            axi2bf_in(239 downto 208) <= reg_rw.poly_buffer1_offset_ns(31 downto 0);
            axi2bf_in(240) <= reg_rw.poly_buffer0_info_valid;
            axi2bf_in(241) <= reg_rw.poly_buffer1_info_valid;
            
            -- scale factor in the register is a floating point value, but the beamformer 
            -- only supports scale factors in a restricted range. Find the best allowed value.
            -- single precision exponent of 128 = floating point values between 2 and 4 
            -- x4 is the maximum scale up that the beamformer allows.
            axi_shift <= std_logic_vector(to_unsigned(128,8) - unsigned(axi_scale_exp));
            if unsigned(axi_shift) > 15 then
                axi_shift_final <= "1111";
            elsif unsigned(axi_scale_exp) > 128 then
                axi_shift_final <= "0000";
            else 
                axi_shift_final <= axi_shift(3 downto 0);
            end if;
            
            -- readout_start_axi, -- out std_logic;
            -- o_readout_frame => readout_ctframe_axi, -- out std_logic_vector(36 downto 0);
            if readout_start_axi = '1' then
                -- Determine which Jones buffer to use
                readout_ctframe_axi_hold <= readout_ctframe_axi;
            end if;
            readout_start_axi_del1 <= readout_start_axi;
            ----------------------------------------------------------------------
            -- Pipelined calculation of which Jones buffer to use:
            -- 1st pipeline stage
            if (unsigned(readout_ctframe_axi_hold) >= unsigned(jones_buf0_valid_frame)) then
                jones_buf0_gt_cur_time <= '1';
            else
                jones_buf0_gt_cur_time <= '0';
            end if;
            jones_buf0_end_frame <= std_logic_vector(unsigned(jones_buf0_valid_frame) + unsigned(jones_buf0_valid_duration));
            
            if (unsigned(readout_ctframe_axi_hold) >= unsigned(jones_buf1_valid_frame)) then
                jones_buf1_gt_cur_time <= '1';
            else
                jones_buf1_gt_cur_time <= '0';
            end if;
            jones_buf1_end_frame <= std_logic_vector(unsigned(jones_buf1_valid_frame) + unsigned(jones_buf1_valid_duration));
            
            if reg_rw.jones_buffer0_valid_duration = x"ffffffff" then
                jones_buf0_valid_forever <= '1';
            else
                jones_buf0_valid_forever <= '0';
            end if;
            
            if reg_rw.jones_buffer1_valid_duration = x"ffffffff" then
                jones_buf1_valid_forever <= '1';
            else
                jones_buf1_valid_forever <= '0';
            end if;
            
            if unsigned(jones_buf0_valid_frame) > unsigned(jones_buf1_valid_frame) then
                jones_buf0_more_recent <= '1';
            else
                jones_buf0_more_recent <= '0';
            end if;
            readout_start_axi_del2 <= readout_start_axi_del1;
            
            -- 2nd pipeline stage
            if (jones_buf0_gt_cur_time = '1') and (unsigned(readout_ctframe_axi_hold) < unsigned(jones_buf0_end_frame)) then
                jones_buf0_valid <= '1';
            else
                jones_buf0_valid <= '0';
            end if;
            if (jones_buf1_gt_cur_time = '1') and (unsigned(readout_ctframe_axi_hold) < unsigned(jones_buf1_end_frame)) then
                jones_buf1_valid <= '1';
            else
                jones_buf1_valid <= '0';
            end if;
            jones_buf0_more_recent_del1 <= jones_buf0_more_recent;
            jones_buf0_valid_forever_del1 <= jones_buf0_valid_forever;
            jones_buf1_valid_forever_del1 <= jones_buf1_valid_forever;
            readout_start_axi_del3 <= readout_start_axi_del2;
            -- 3rd pipeline stage
            -- calculates which buffer to use ("sel_jones_buf") 
            -- and also flags that go in the output PST packets 
            -- "jones_valid" : selected a valid jones buffer with validity time < xffffffff
            -- "jones_used_default" : 
            if readout_start_axi_del3 = '1' then
                if ((jones_buf0_valid = '1' and (jones_buf0_more_recent_del1 = '1' or jones_buf1_valid = '0')) or
                    (jones_buf0_valid_forever_del1 = '1' and jones_buf1_valid = '0')) then
                    sel_jones_buf <= '0'; -- select Jones buffer 0
                    jones_used_default <= jones_buf0_valid_forever_del1;
                    jones_valid <= jones_buf0_valid and (not jones_buf0_valid_forever_del1);
                else
                    sel_jones_buf <= '1'; -- select Jones buffer 1
                    jones_used_default <= jones_buf1_valid_forever_del1;
                    jones_valid <= jones_buf1_valid and (not jones_buf1_valid_forever_del1);
                end if;
            end if;
            
            -- Pass to the BF clock domain to output to the beamformer.
            -- This should occur well in advance of the first data going to the beamformer, 
            -- because both are triggered by the same thing, and this only has a few clocks latency,
            -- while the beamformer data output has to calculate polynomials and fetch data from the HBM. 
            jones_sel_bits(0) <= sel_jones_buf;
            jones_sel_bits(1) <= jones_used_default;
            jones_sel_bits(2) <= jones_valid;
            ------------------------------------------------------------------------------------------
            
        end if;
    end process;
    
    jones_buf0_valid_frame <= reg_rw.jones_buffer0_valid_frame_high(4 downto 0) & reg_rw.jones_buffer0_valid_frame_low(31 downto 0);
    jones_buf1_valid_frame <= reg_rw.jones_buffer1_valid_frame_high(4 downto 0) & reg_rw.jones_buffer1_valid_frame_low(31 downto 0);
    
    jones_buf0_valid_duration <= "00000" & reg_rw.jones_buffer0_valid_duration;
    jones_buf1_valid_duration <= "00000" & reg_rw.jones_buffer1_valid_duration;
    
    axi_scale_exp <= reg_rw.scaleFactor(30 downto 23);
    
    xpm_cdc_handshake_psc_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        WIDTH => 242         -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => axi2bf_dest_out,   -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => axi2bf_dest_req,   -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => axi2bf_src_rcv,     -- 1-bit output: Acknowledgement from destination logic that src_in has been received. This signal will be deasserted once destination handshake has fully completed,
        dest_ack => '1',               -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_BF_clk,          -- 1-bit input: Destination clock.
        src_clk => i_axi_clk,          -- 1-bit input: Source clock.
        src_in => axi2bf_in,           -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => axi2BF_src_send    -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );
    
    
    -- Start of frame pulse
    -- 8 destination ff to ensure that the frame count is stable before the pulse happens in the i_BF_clk domain. 
    xpm_cdc_pulse_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 8,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    ) port map (
        dest_pulse => readout_start_BFclk, -- 1-bit output: Outputs a pulse the size of one dest_clk period
        dest_clk   => i_BF_clk,   -- 1-bit input: Destination clock.
        dest_rst   => '0',        -- 1-bit input: optional; required when RST_USED = 1
        src_clk    => i_axi_clk,  -- 1-bit input: Source clock.
        src_pulse  => readout_start_axi,  -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst    => '0'         -- 1-bit input: optional; required when RST_USED = 1
    );

    -- DEST_SYNC_FF must be a few less than for the crc_pulse above, since the output is sampled on the output of the pulse.
    xpm_cdc_array_single_inst : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
        WIDTH => 37          -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => readout_ctframe_BFclk,   -- WIDTH-bit output: src_in synchronized to the destination clock domain.
        dest_clk => i_BF_clk,  -- 1-bit input: Clock signal for the destination clock domain.
        src_clk  => i_axi_clk, -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in   => readout_ctframe_axi_hold -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain. 
    );
    
    xpm_cdc_array_single2 : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF => 3,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1,  -- DECIMAL; 0=do not register input, 1=register input
        WIDTH => 3           -- DECIMAL; range: 1-1024
    ) port map (
        dest_out => jones_sel_bits_BF_clk,   -- WIDTH-bit output: src_in synchronized to the destination clock domain.
        dest_clk => i_BF_clk,  -- 1-bit input: Clock signal for the destination clock domain.
        src_clk  => i_axi_clk, -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in   => jones_sel_bits -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain. 
    );
        
    
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            if axi2bf_dest_req = '1' then
                beamsEnabled <= axi2bf_dest_out(7 downto 0);
                scale_exp <= axi2bf_dest_out(15 downto 12);
                scale_frac <= axi2bf_dest_out(11 downto 8);
                
                bfclk_poly_buffer0_valid_frame(47 downto 0) <= axi2bf_dest_out(63 downto 16);
                bfclk_poly_buffer0_valid_duration(31 downto 0) <= axi2bf_dest_out(95 downto 64);
                bfclk_poly_buffer1_valid_frame(47 downto 0) <= axi2bf_dest_out(143 downto 96);
                bfclk_poly_buffer1_valid_duration(31 downto 0) <= axi2bf_dest_out(175 downto 144);
                
                bfclk_poly_buffer0_offset_ns <= axi2bf_dest_out(207 downto 176); -- integer nanoseconds offset
                bfclk_poly_buffer1_offset_ns <= axi2bf_dest_out(239 downto 208);
                
                bfclk_poly_buffer0_info_valid <= axi2bf_dest_out(240);
                bfclk_poly_buffer1_info_valid <= axi2bf_dest_out(241);
                
            end if;
        end if;
    end process;
    
    o_beamsEnabled <= beamsEnabled;
    o_scale_exp_frac <= scale_exp & scale_frac;  -- scale_frac = 0 indicates firmware should calculate the scale factor.
    
    ---------------------------------------------------------------------------------
    -- Polynomial evaluation
    
    polymemi : entity ct_lib.ct2_poly_mem
    port map(
        -- Registers axi full interface
        i_MACE_clk => i_axi_clk,   -- in std_logic;
        i_MACE_rst => i_axi_rst,   -- in std_logic;
        i_axi_mosi => i_axi_full_mosi, -- in  t_axi4_full_mosi;
        o_axi_miso => o_axi_full_miso, -- out t_axi4_full_miso;
        -- Memory interface
        i_BF_clk  => i_BF_clk, --  in std_logic;
        -- 4 clock read latency
        i_vcmap_RdAddr => vcmap_rdAddr, -- in (9:0);
        o_vcmap_dout   => vcmap_dout,   -- out(63:0);
        -- 15 clock read latency (chained ultraRAMs, 12 deep to get 49152 = 12 * 4096) 
        i_polymem_RdAddr => polymem_rdAddr, -- in (15:0);
        o_polymem_dout => polymem_dout     -- out (63:0) 
    );
    
    
    polyEvali : entity ct_lib.ct2_poly_eval
    port map(
        i_BF_clk => i_BF_clk, -- in std_logic;
        -------------------------------------------------------------------------------
        -- Control registers
        i_poly_buffer0_valid_frame    => bfclk_poly_buffer0_valid_frame,    -- in (47:0);
        i_poly_buffer0_valid_duration => bfclk_poly_buffer0_valid_duration, -- in (31:0);
        i_poly_buffer0_offset_ns      => bfclk_poly_buffer0_offset_ns,      -- in (31:0);
        i_poly_buffer0_valid          => bfclk_poly_buffer0_info_valid,     -- in std_logic;
        i_poly_buffer1_valid_frame    => bfclk_poly_buffer1_valid_frame,    -- in (47:0);
        i_poly_buffer1_valid_duration => bfclk_poly_buffer1_valid_duration, -- in (31:0);
        i_poly_buffer1_offset_ns      => bfclk_poly_buffer1_offset_ns,      -- in (31:0);
        i_poly_buffer1_valid          => bfclk_poly_buffer1_info_valid,     -- in std_logic;
        i_beams => beamsEnabled,                    -- in (7:0); Number of beams to evaluate
        -- i_stations, i_coarse need to be valid on when i_poly_eval = '1'
        i_stations => bfclk_stations(9 downto 0),   -- in (9:0); Number of stations
        i_coarse   => bfclk_coarse,                 -- in (9:0); Number of coarse channels
        -------------------------------------------------------------------------------
        -- polynomial configuration memory interface
        -- 4 clock read latency
        o_vcmap_RdAddr => vcmap_rdAddr, -- out (9:0);
        i_vcmap_rdData => vcmap_dout,   -- in (63:0);
        -- 15 clock read latency (chained ultraRAMs, 12 deep to get 49152 = 12 * 4096) 
        o_polymem_RdAddr => polymem_rdAddr, -- out (15:0);
        i_polymem_rdData => polymem_dout,   -- in (63:0); 
        --------------------------------------------------------------------------------
        -- Polynomial results output
        i_poly_eval => readout_start_BFclk,   -- in std_logic; Initiate polynomial evaluation for a corner turn frame.
        i_ct_frame  => readout_ctframe_BFclk, -- in (36:0);  Which corner turn frame to use 
        o_phase_virtualChannel => poly_phase_virtualChannel, -- out (9:0);
        o_phase_timeStep => poly_phase_timestep,  -- out (7:0);
        o_phase_beam     => poly_phase_beam,  -- out (3:0);   0 to 15
        o_phase          => poly_phase,       -- out (23:0);  Phase at the start of the coarse channel.
        o_phase_step     => poly_phase_step,  -- out (23:0);  Phase step per fine channel.
        o_phase_valid    => poly_phase_valid, -- out std_logic;
        o_poly_ok        => o_poly_ok(1),     -- out std_logic; Current time is between start and end validity times for the polynomial buffer in use
        i_stop           => poly_stop         -- in std_logic; Data delivered to a FIFO, FIFO near full so stop sending stuff.
    );
    
    
    -- FIFO for the polynomial info  
    poly_fifo_din(9 downto 0) <= poly_phase_virtualChannel;
    poly_fifo_din(17 downto 10) <= poly_phase_timestep;
    poly_fifo_din(21 downto 18) <= poly_phase_beam;
    poly_fifo_din(45 downto 22) <= poly_phase;
    poly_fifo_din(69 downto 46) <= poly_phase_step;
    
    poly_fifoi : xpm_fifo_sync
    generic map (
        CASCADE_HEIGHT => 0,        -- DECIMAL
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "auto", -- String
        FIFO_READ_LATENCY => 1,     -- DECIMAL
        FIFO_WRITE_DEPTH => 512,    -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 10,  -- DECIMAL
        READ_DATA_WIDTH => 70,      -- DECIMAL
        READ_MODE => "std",         -- String
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "1404", -- String; x1404 enables read and write data counts, and the data_valid flag.
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 70,     -- DECIMAL
        WR_DATA_COUNT_WIDTH => 10   -- DECIMAL
    ) port map (
        almost_empty => open,     -- 1-bit output: Almost Empty 
        almost_full => open,      -- 1-bit output: Almost Full
        data_valid => open,       -- 1-bit output: Read Data Valid
        dbiterr => open,          -- 1-bit output: Double Bit Error
        dout => poly_fifo_dout,   -- READ_DATA_WIDTH-bit output: Read Data
        empty => poly_fifo_empty, -- 1-bit output: Empty Flag
        full => open,             -- 1-bit output: Full Flag
        overflow => open,         -- 1-bit output: Overflow
        prog_empty => open,       -- 1-bit output: Programmable Empty
        prog_full => open,        -- 1-bit output: Programmable Full
        rd_data_count => open,    -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count
        rd_rst_busy => open,      -- 1-bit output: Read Reset Busy
        sbiterr => open,          -- 1-bit output: Single Bit Error
        underflow => open,        -- 1-bit output: Underflow
        wr_ack => open,           -- 1-bit output: Write Acknowledge
        wr_data_count => poly_fifo_wr_data_count, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count
        wr_rst_busy => open,      -- 1-bit output: Write Reset Busy
        din => poly_fifo_din,     -- WRITE_DATA_WIDTH-bit input: Write Data
        injectdbiterr => '0',     -- 1-bit input: Double Bit Error Injection
        injectsbiterr => '0',     -- 1-bit input: Single Bit Error Injection
        rd_en => poly_fifo_rdEn,  -- 1-bit input: Read Enable
        rst   => poly_fifo_rst,   -- 1-bit input: Reset: Must be synchronous to wr_clk
        sleep => '0',             -- 1-bit input: Dynamic power saving
        wr_clk => i_BF_clk,       -- 1-bit input: Write clock
        wr_en => poly_phase_valid -- 1-bit input: Write Enable
    );
    
    poly_fifo_rdEn <= '1' when (unsigned(delays_to_send) > 0) and poly_fifo_empty = '0' else '0';
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            if unsigned(poly_fifo_wr_data_count) > 255 then
                poly_stop <= '1';
            else
                poly_stop <= '0';
            end if;
            
            poly_fifo_rst <= readout_start_BFclk;
            o_phase_clear <= readout_start_BFclk;
            
            -- delay line to ensure FIFO reset is complete before we try to read from it.
            readout_start_del(0) <= readout_start_BFclk;
            readout_start_del(15 downto 1) <= readout_start_del(14 downto 0);
            
            -- The pattern for data going to the beamformers is :
            --
            --  For timeGroup = 0:(<time samples per corner turn>/32 - 1) -- 53 ms corner turn, So 0:(256/32-1) = 0:7
            --     For Coarse = 0:(i_coarse-1)                            -- For 512 stations, 2 coarse, this is 0:1
            --        For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
            --           For Station = 0:(i_stations-1)                   -- Up to 512 stations
            --              For beam = 0:(i_beams-1)                      -- Up to 16 beams
            --
            -- The polynomial evaluation always evaluates for the 16 beam case, 
            -- so every time we move to a new station, we have to read out 16 entries from the FIFO.
            -- The total number of delays per corner turn is up to : 
            --  (16 beams) * (512 stations) * (256 times) * (2 coarse) = 4194304
            --
            
            if readout_start_del(15) = '1' then
                -- start of a new corner turn frame, we need to deliver 16 delays ( = 1 per beam)
                delays_to_send <= std_logic_vector(to_unsigned(16,8));
            else
                if read_16_delays = '1' and poly_fifo_rdEn = '0' then
                    -- start of data for a block of 216 fine channels, send delays for the subsequent 216 fine channels.
                    -- The beamformers have a two deep fifo so we can send future data.
                    delays_to_send <= std_logic_vector(unsigned(delays_to_send) + 16);
                elsif read_16_delays = '1' and poly_fifo_rdEn = '1' then
                    delays_to_send <= std_logic_vector(unsigned(delays_to_send) + 15);
                elsif poly_fifo_rdEn = '1' then
                    delays_to_send <= std_logic_vector(unsigned(delays_to_send) - 1); 
                end if;
            end if;
            
            if unsigned(fine_int) = 0 and valid_int = '1' then
                read_16_delays <= '1';
            else
                read_16_delays <= '0';
            end if; 
            
            -- Readout of the fifo to the beamformers
            o_phase_virtualChannel <= poly_fifo_dout(9 downto 0);
            o_phase_timeStep <= poly_fifo_dout(17 downto 10); -- out (7:0);
            o_phase_beam <= poly_fifo_dout(21 downto 18); -- out (3:0);
            o_phase <= poly_fifo_dout(45 downto 22);      -- out (23:0); Phase at the start of the coarse channel.
            o_phase_step <= poly_fifo_dout(69 downto 46); -- out (23:0); Phase step per fine channel.
            poly_fifo_rdEn_del1 <= poly_fifo_rdEn;
            o_phase_valid <= poly_fifo_rdEn_del1;         -- out std_logic;
            
            o_jonesBuffer <= jones_sel_bits_BF_clk(0);     -- Which jones buffer to use.
            o_jones_status(0) <= jones_sel_bits_BF_clk(1); -- Used the defaults; i.e. valid_duration register was xffffffff
            o_jones_status(1) <= jones_sel_bits_BF_clk(2); -- Jones matrices are valid
            
        end if;
    end process;
    
end Behavioral;
