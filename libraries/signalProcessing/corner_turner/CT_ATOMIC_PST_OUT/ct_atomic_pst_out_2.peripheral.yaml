schema_name   : args
schema_version: 1.0
schema_type   : peripheral

hdl_library_name       : ct_atomic_pst_out_2
hdl_library_description: "PST output corner turn and beamformer"

peripherals:
  - peripheral_name        : ct_atomic_pst_out_2
    peripheral_description : "Control the PST output corner turn"
    slave_ports:
      - slave_name        : StatCtrl
        slave_type        : reg
        number_of_slaves  : 1
        slave_description : "PST output corner turn"
        fields:
          #################################
          - - field_name        : bufferOverflowError
              width             : 1
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Write buffer used to transfer filterbank data into the HBM overflowed. Cleared by a reset in the first stage corner turn. Should never happen."
          #################################
          - - field_name        : readoutError
              width             : 1
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Readout to the beamformer did not finish before the readout of the next HBM buffer started. Cleared by a reset passed in from the first stage corner turn. Should never happen."
          #################################
          - - field_name        : bufferEnable
              width             : 4
              access_mode       : RW
              reset_value       : 0xf
              field_description : "1 bit to enable each of the 4 blocks of 256 MBytes of HBM. Each 256 MBytes of HBM is a single output buffer."
          #################################
          - - field_name        : HBMBuf0PacketCount
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "LFAA packet count for the first packet in HBM buffer 0"
          #################################
          - - field_name        : HBMBuf1PacketCount
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "LFAA packet count for the first packet in HBM buffer 1"
          #################################
          - - field_name        : HBMBuf2PacketCount
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "LFAA packet count for the first packet in HBM buffer 2"
          #################################
          - - field_name        : HBMBuf3PacketCount
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "LFAA packet count for the first packet in HBM buffer 3"
          #################################
          - - field_name        : BFJonesSelect
              width             : 1
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Selects which of the two Jones Matrices buffers to use in the beamformers. This is applied when the packet count reaches the value in the register packetCountBFJonesSwitch."
          #################################
          - - field_name        : PacketCountBFJonesSwitch
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Packet count at which to switch the buffer to use for the beamformer Jones Matrices. Use zero to switch at the next frame."
          #################################
          - - field_name        : BFPhaseSelect
              width             : 1
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Selects which of the two Jones Matrices buffers to use in the beamformers. This is applied when the packet count reaches the value in the register packetCountBFPhaseSwitch."
          #################################
          - - field_name        : PacketCountBFPhaseSwitch
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Packet count at which to switch the buffer to use for the beamformer phase tracking. Use zero to switch at the next frame."
           #################################
          - - field_name        : scaleFactor
              width             : 32
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Single precision float scaling factor applied in the beamformer to form output packets. Use 0 for firmware to calculate the scaling factor."
          #################################
          - - field_name        : BeamsEnabled
              width             : 8
              access_mode       : RW
              reset_value       : 0x0
              field_description : "Number of PST beams currently enabled."              
          #################################
          - - field_name        : NumberOfBeams
              width             : 8
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Number of PST beams supported by this firmware build"
          #################################
          - - field_name        : readInClocks
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Length of the most recent frame in units of 300 MHz clocks at the input to the corner turn."
          #################################
          - - field_name        : readInAllClocks
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Interval between the start of one frame and the start of the next frame in units of 300 MHz clocks at the input to the corner turn."
          #################################
          - - field_name        : readOutClocks
              width             : 32
              access_mode       : RO
              reset_value       : 0x0
              field_description : "Length of the most recent frame in units of 400 MHz clocks at the output of the corner turn"                     
                 