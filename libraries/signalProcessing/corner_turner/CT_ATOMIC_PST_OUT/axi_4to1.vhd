----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 10.03.2021 15:10:19
-- Module Name: axi_4to1 - Behavioral
-- Description: 
--   Converts 1 axi bus into 4 axi busses, in order to maximize the bandwidth to the HBM by using different AXI channels for different 256MByte pieces of HBM.
--   Note:
--    This could be done automatically by just having a single interface and letting Vitis direct 
--    it to the selected HBM interface. However, when consecutive reads go to different interfaces, 
--    the interconnect created by Vitis waits until the first read transaction is complete before 
--    starting the next read transaction, presumably in order to avoid out of order responses.
--    This defeats the purpose of sharing the bandwidth between multiple interfaces.
--    
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
Library xpm;
use xpm.vcomponents.all;
library ct_lib;
use ct_lib.all;
use IEEE.NUMERIC_STD.ALL;
library common_lib;
use common_lib.common_pkg.all;

entity axi_4to1 is
    Port(
        i_clk      : in std_logic;
        i_rst      : in STD_LOGIC;
        -------------------------------------------------------------
        -- First master
        m0_araddr  : out STD_LOGIC_VECTOR ( 31 downto 0 );
        m0_arlen   : out STD_LOGIC_VECTOR ( 7 downto 0 );
        m0_arready : in STD_LOGIC;
        m0_arvalid : out STD_LOGIC;
        --
        m0_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
        m0_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
        m0_awready : in STD_LOGIC;
        m0_awvalid : out STD_LOGIC;
        --
        m0_bready : out STD_LOGIC;
        m0_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
        m0_bvalid : in STD_LOGIC;
        --
        m0_rdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
        m0_rlast : in STD_LOGIC;
        m0_rready : out STD_LOGIC;
        m0_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
        m0_rvalid : in STD_LOGIC;
        --
        m0_wdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
        m0_wlast : out STD_LOGIC;
        m0_wready : in STD_LOGIC;
        m0_wvalid : out STD_LOGIC;
        -----------------------------------------------------------
        -- Second master
        m1_araddr  : out STD_LOGIC_VECTOR ( 31 downto 0 );
        m1_arlen   : out STD_LOGIC_VECTOR ( 7 downto 0 );
        m1_arready : in STD_LOGIC;
        m1_arvalid : out STD_LOGIC;
        --
        m1_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
        m1_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
        m1_awready : in STD_LOGIC;
        m1_awvalid : out STD_LOGIC;
        --
        m1_bready : out STD_LOGIC;
        m1_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
        m1_bvalid : in STD_LOGIC;
        --
        m1_rdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
        m1_rlast : in STD_LOGIC;
        m1_rready : out STD_LOGIC;
        m1_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
        m1_rvalid : in STD_LOGIC;
        --
        m1_wdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
        m1_wlast : out STD_LOGIC;
        m1_wready : in STD_LOGIC;
        m1_wvalid : out STD_LOGIC;
        ----------------------------------------------------------
        -- Third master
        m2_araddr  : out STD_LOGIC_VECTOR ( 31 downto 0 );
        m2_arlen   : out STD_LOGIC_VECTOR ( 7 downto 0 );
        m2_arready : in STD_LOGIC;
        m2_arvalid : out STD_LOGIC;
        --
        m2_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
        m2_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
        m2_awready : in STD_LOGIC;
        m2_awvalid : out STD_LOGIC;
        --
        m2_bready : out STD_LOGIC;
        m2_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
        m2_bvalid : in STD_LOGIC;
        --
        m2_rdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
        m2_rlast : in STD_LOGIC;
        m2_rready : out STD_LOGIC;
        m2_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
        m2_rvalid : in STD_LOGIC;
        --
        m2_wdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
        m2_wlast : out STD_LOGIC;
        m2_wready : in STD_LOGIC;
        m2_wvalid : out STD_LOGIC;
        ------------------------------------------------------------
        -- Fourth master
        m3_araddr  : out STD_LOGIC_VECTOR ( 31 downto 0 );
        m3_arlen   : out STD_LOGIC_VECTOR ( 7 downto 0 );
        m3_arready : in STD_LOGIC;
        m3_arvalid : out STD_LOGIC;
        --
        m3_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
        m3_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
        m3_awready : in STD_LOGIC;
        m3_awvalid : out STD_LOGIC;
        --
        m3_bready : out STD_LOGIC;
        m3_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
        m3_bvalid : in STD_LOGIC;
        --
        m3_rdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
        m3_rlast : in STD_LOGIC;
        m3_rready : out STD_LOGIC;
        m3_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
        m3_rvalid : in STD_LOGIC;
        --
        m3_wdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
        m3_wlast : out STD_LOGIC;
        m3_wready : in STD_LOGIC;
        m3_wvalid : out STD_LOGIC;
        ------------------------------------------------------------
        -- Slave interface
        s0_araddr  : in STD_LOGIC_VECTOR ( 31 downto 0 );
        s0_arlen   : in STD_LOGIC_VECTOR ( 7 downto 0 );
        s0_arready : out STD_LOGIC;
        s0_arvalid : in STD_LOGIC;
        --
        s0_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
        s0_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
        s0_awready : out STD_LOGIC;
        s0_awvalid : in STD_LOGIC;
        --
        s0_bready : in STD_LOGIC;
        s0_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
        s0_bvalid : out STD_LOGIC;
        --
        s0_rdata : out STD_LOGIC_VECTOR ( 255 downto 0 );
        s0_rlast : out STD_LOGIC;
        s0_rready : in STD_LOGIC;
        s0_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
        s0_rvalid : out STD_LOGIC;
        --
        s0_wdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
        s0_wlast : in STD_LOGIC;
        s0_wready : out STD_LOGIC;
        s0_wvalid : in STD_LOGIC
        --
    );
end axi_4to1;

architecture Behavioral of axi_4to1 is

    type rdwr_fsm_type is (idle, send, checkFIFO);
    signal wr_fsm : rdwr_fsm_type := idle;
    signal rd_fsm : rdwr_fsm_type := idle;
    signal s0_awaddrDel1 : std_logic_vector(31 downto 0);
    signal s0_awlenDel1 : std_logic_vector(7 downto 0);
    signal awFIFOwr : std_logic;
    signal m0123_awvalid : std_logic_vector(3 downto 0);
    signal wSelectFIFOdin : std_logic_vector(1 downto 0);

    signal awFIFOFull : std_logic_vector(3 downto 0);
    signal wSelectFIFOFull : std_logic;
    signal wSelectFIFOwrCount : std_logic_vector(5 downto 0);
    signal wSelectFIFOempty : std_logic;

    signal wdataFIFODin : std_logic_vector(256 downto 0);  -- wlast & wdata.
    signal wdataFIFOWrEn : std_logic_vector(3 downto 0);
    signal wdataFIFOFull : std_logic_vector(3 downto 0);
    signal wSelectFIFOdout : std_logic_vector(1 downto 0);
    signal wSelectFIFOrdCount : std_logic_vector(5 downto 0);
    signal wSelectFIFOrdEn : std_logic;
    signal awFIFOdin : std_logic_vector(39 downto 0);
    signal awFIFOwrCount : t_slv_6_arr(3 downto 0); -- out std_logic_vector(5 downto 0);
    signal awFIFOdout : t_slv_40_arr(3 downto 0);
    signal awFIFOempty : std_logic_vector(3 downto 0);
    signal awFIFOrdCount : t_slv_6_arr(3 downto 0);
    signal awFIFOrdEn : std_logic_vector(3 downto 0);
    
    signal wDataFIFOdout : t_slv_257_arr(3 downto 0);
    signal wDataFIFOEmpty : std_logic_vector(3 downto 0);
    signal wDataFIFOrdEn : std_logic_vector(3 downto 0);
    signal wDataFIFOrdCount, wDataFIFOwrCount : t_slv_6_arr(3 downto 0);
    
    signal m0123_arvalid : std_logic_vector(3 downto 0);
    signal s0_araddrDel1 : std_logic_vector(31 downto 0);
    signal s0_arlenDel1 : std_logic_vector(7 downto 0);
    
    signal arFIFOwr : std_logic;
    signal rSelectFIFOFull : std_logic;
    signal rSelectFIFOdin : std_logic_vector(1 downto 0);
    signal rSelectFIFOwrCount, rSelectFIFOrdCount : std_logic_vector(6 downto 0);
    signal arFIFOdin : std_logic_vector(39 downto 0);
    
    signal arFIFOwrCount, arFIFOrdCount : t_slv_6_arr(3 downto 0);
    signal arFIFOdout : t_slv_40_arr(3 downto 0);
    signal arFIFOempty : std_logic_vector(3 downto 0);
    signal arFIFOFull : std_logic_vector(3 downto 0);
    signal arFIFOrdEn : std_logic_vector(3 downto 0);
    
    signal rDataFIFOdout : t_slv_257_arr(3 downto 0);
    signal rDataFIFOempty : std_logic_vector(3 downto 0);
    signal rSelectFIFOdout : std_logic_vector(1 downto 0);
    signal rSelectFIFOrdEn : std_logic;
    signal rSelectFIFOempty : std_logic;
    signal rdataFIFOFull : std_logic_vector(3 downto 0);
    signal rDataFIFOwrCount, rDataFIFOrdCount : t_slv_6_arr(3 downto 0);
    signal rdataFIFOWrEn : std_logic_vector(3 downto 0);
    signal rdataFIFODin : t_slv_257_arr(3 downto 0);
    signal rdataFIFOrdEn : std_logic_vector(3 downto 0);
    
    component ila_beamData
    port (
        clk : in std_logic;
        probe0 : in std_logic_vector(119 downto 0)); 
    end component;
    
    signal wSelectFIFOwrCount_dbg : std_logic_vector(5 downto 0);  -- 6 bits
    signal rSelectFIFOwrCount_dbg : std_logic_vector(6 downto 0); -- <= rSelectFIFOwrCount;  --7 bits
    signal awFIFOfull_dbg, awFIFOempty_dbg, arFIFOfull_dbg, arFIFOempty_dbg, rdataFIFOFull_dbg, rdataFIFOempty_dbg, wdataFIFOFull_dbg, wdataFIFOEmpty_dbg : std_logic_vector(3 downto 0);
    signal s0_araddr_dbg : std_logic_vector(31 downto 0); -- 32
    signal s0_arlen_dbg : std_logic_vector(7 downto 0); -- in STD_LOGIC_VECTOR ( 7 downto 0 );
    signal s0_arready_dbg, s0_arvalid_dbg : std_logic;
    signal s0_awaddr_dbg : std_logic_vector(31 downto 0); --  : in STD_LOGIC_VECTOR ( 31 downto 0 );
    signal s0_awlen_dbg : std_logic_vector(7 downto 0); -- in STD_LOGIC_VECTOR ( 7 downto 0 );
    signal s0_awready_dbg, s0_awvalid_dbg, s0_rlast_dbg, s0_rready_dbg, s0_rvalid_dbg : STD_LOGIC;
    signal s0_wlast_dbg, s0_wready_dbg, s0_wvalid_dbg : STD_LOGIC;
    
    signal ErrData : std_logic_vector(9 downto 0);
    
begin
    
    -- debug 
    ErrData(0) <= '1' when s0_wdata(31 downto 0) = x"F712EFF7" else '0';
    ErrData(1) <= '1' when s0_rdata(31 downto 0) = x"F712EFF7" else '0';
    ErrData(2) <= '1' when m0_wdata(31 downto 0) = x"F712EFF7" else '0';
    ErrData(3) <= '1' when m0_rdata(31 downto 0) = x"F712EFF7" else '0';
    ErrData(4) <= '1' when m1_wdata(31 downto 0) = x"F712EFF7" else '0';
    ErrData(5) <= '1' when m1_rdata(31 downto 0) = x"F712EFF7" else '0';
    ErrData(6) <= '1' when m2_wdata(31 downto 0) = x"F712EFF7" else '0';
    ErrData(7) <= '1' when m2_rdata(31 downto 0) = x"F712EFF7" else '0';
    ErrData(8) <= '1' when m3_wdata(31 downto 0) = x"F712EFF7" else '0';
    ErrData(9) <= '1' when m3_rdata(31 downto 0) = x"F712EFF7" else '0';
    
    ----------------------------------------------------------------------
    -- Write side.
    -- wraddr bus is forwarded to the correct output interface based on the high address bits,
    -- and the interface is recorded in a FIFO. The output of the FIFO is used to direct the wdata 
    -- bus to the correct output bus.
    m0_awaddr <= awFIFOdout(0)(31 downto 0);
    m0_awlen <= awFIFOdout(0)(39 downto 32);
    m1_awaddr <= awFIFOdout(1)(31 downto 0);
    m1_awlen <= awFIFOdout(1)(39 downto 32);
    m2_awaddr <= awFIFOdout(2)(31 downto 0);
    m2_awlen <= awFIFOdout(2)(39 downto 32);
    m3_awaddr <= awFIFOdout(3)(31 downto 0);
    m3_awlen <= awFIFOdout(3)(39 downto 32);
    
    m0_awvalid <= not awFIFOempty(0);
    m1_awvalid <= not awFIFOempty(1);
    m2_awvalid <= not awFIFOempty(2);
    m3_awvalid <= not awFIFOempty(3);
    
    m0_wdata <= wDataFIFOdout(0)(255 downto 0);
    m0_wlast <= wDataFIFOdout(0)(256);
    m0_wvalid <= not wDataFIFOEmpty(0);    
    m1_wdata <= wDataFIFOdout(1)(255 downto 0);
    m1_wlast <= wDataFIFOdout(1)(256);
    m1_wvalid <= not wDataFIFOEmpty(1);
    m2_wdata <= wDataFIFOdout(2)(255 downto 0);
    m2_wlast <= wDataFIFOdout(2)(256);
    m2_wvalid <= not wDataFIFOEmpty(2);
    m3_wdata <= wDataFIFOdout(3)(255 downto 0);
    m3_wlast <= wDataFIFOdout(3)(256);
    m3_wvalid <= not wDataFIFOEmpty(3);
    
    -- b channel is ignored for both master and slave busses.
    m0_bready <= '1';
    m1_bready <= '1';
    m2_bready <= '1';
    m3_bready <= '1';
    
    s0_bresp <= "00";
    s0_bvalid <= '0'; 
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            if i_rst = '1' then
                wr_fsm <= idle;
                m0123_awvalid <= "0000";
                awFIFOwr <= '0';
                s0_awready <= '0';
            else
                case wr_fsm is
                    when idle =>
                        if s0_awvalid = '1' then
                            wr_fsm <= send;
                            s0_awaddrDel1 <= s0_awaddr;
                            s0_awlenDel1 <= s0_awlen;
                            if s0_awaddr(29 downto 28) = "00" then
                                m0123_awvalid <= "0001";
                            elsif s0_awaddr(29 downto 28) = "01" then
                                m0123_awvalid <= "0010";
                            elsif s0_awaddr(29 downto 28) = "10" then
                                m0123_awvalid <= "0100";
                            else
                                m0123_awvalid <= "1000";
                            end if;
                            s0_awready <= '0';
                            awFIFOwr <= '1';
                        else
                            s0_awready <= '1';
                            m0123_awvalid <= "0000";
                            awFIFOwr <= '0';
                        end if;
                        
                    when send =>
                        -- We check that all the FIFOs have space after every transaction is accepted 
                        -- on the aw bus, so no need to check here. 
                        m0123_awvalid <= "0000";
                        wr_fsm <= checkFIFO;
                        s0_awready <= '0';
                        awFIFOwr <= '0';
                        
                    when checkFIFO =>
                        if wSelectFIFOFull = '0' and awFIFOFull = "0000" then
                            wr_fsm <= idle;
                            s0_awready <= '1';
                        else
                            s0_awready <= '0';
                        end if;
                        awFIFOwr <= '0';
                        m0123_awvalid <= "0000";
                    
                    when others =>
                        wr_fsm <= idle;
                    
                end case;
            end if;
            
        end if;
    end process;
    
    wSelectFIFOdin <= s0_awaddrDel1(29 downto 28);  -- Selects the destination for the data on the wdata bus associated with this address.
    wdataFIFODin <= s0_wlast & s0_wdata;
    
    s0_wready <= '1' when wSelectFIFOempty = '0' and wdataFIFOFull(to_integer(unsigned(wSelectFIFODout))) = '0' else '0';
    wSelectFIFOrdEn <= s0_wready and s0_wlast and s0_wvalid;
    
    awFIFOdin <= s0_awlenDel1 & s0_awaddrDel1;
    
    -- w_select_FIFO holds the output port for the wdata bus.
    w_Select_FIFO_inst : entity ct_lib.dsfifo32
    generic map (g_WIDTH => 2)
    port map(
        i_clk => i_clk, --  in std_logic;
        i_rst => i_rst, --  in std_logic;
        -- data input side
        i_wren => awFIFOwr,           -- in std_logic;
        i_data => wSelectFIFOdin,       -- in std_logic_vector(g_WIDTH-1 downto 0);
        o_wrDataCount => wSelectFIFOwrCount, -- out std_logic_vector(5 downto 0);
        o_full => wSelectFIFOFull,           -- out std_logic;
        -- data output side
        o_data => wSelectFIFOdout,           -- out std_logic_vector(g_WIDTH-1 downto 0);
        o_empty => wSelectFIFOempty,         -- out std_logic;
        o_rdDataCount => wSelectFIFOrdCount, -- out std_logic_vector(5 downto 0);
        i_rdEn => wSelectFIFOrdEn            -- in std_logic
    );
    
    awFIFOrdEn(0) <= (not awFIFOempty(0)) and m0_awready;
    awFIFOrdEn(1) <= (not awFIFOempty(1)) and m1_awready;
    awFIFOrdEn(2) <= (not awFIFOempty(2)) and m2_awready;
    awFIFOrdEn(3) <= (not awFIFOempty(3)) and m3_awready;
    
    wDataFIFOrdEn(0) <= (not wDataFIFOEmpty(0)) and m0_wready;
    wDataFIFOrdEn(1) <= (not wDataFIFOEmpty(1)) and m1_wready;
    wDataFIFOrdEn(2) <= (not wDataFIFOEmpty(2)) and m2_wready;
    wDataFIFOrdEn(3) <= (not wDataFIFOEmpty(3)) and m3_wready;
    
    wfifo_gen : for i in 0 to 3 generate
        -- FIFOs for the aw bus and wdata bus for each output AXI port.
        
        awFIFO_inst : entity ct_lib.dsfifo32
        generic map ( g_WIDTH => 40)  -- 32 bits for the awaddr, 8 bits for the awlen
        Port map(
            i_clk => i_clk, --  in std_logic;
            i_rst => i_rst, --  in std_logic;
            -- data input side
            i_wren => m0123_awvalid(i),        -- in std_logic;
            i_data => awFIFOdin,               -- in std_logic_vector(g_WIDTH-1 downto 0);
            o_wrDataCount => awFIFOwrCount(i), -- out std_logic_vector(5 downto 0);
            o_full => awFIFOFull(i),           -- out std_logic;
            -- data output side
            o_data => awFIFOdout(i),           -- out std_logic_vector(g_WIDTH-1 downto 0);
            o_empty => awFIFOempty(i),         -- out std_logic;
            o_rdDataCount => awFIFOrdCount(i), -- out std_logic_vector(5 downto 0);
            i_rdEn => awFIFOrdEn(i)            -- in std_logic
        );
        
        wdataFIFOWrEn(i) <= '1' when (s0_wvalid = '1' and wSelectFIFOempty = '0' and unsigned(wSelectFIFOdout) = i and wdataFIFOFull(i) = '0') else '0';
        
        wdataFIFO_inst : entity ct_lib.dsfifo32
        generic map ( g_WIDTH => 257)  -- 256 bits for wdata, 1 bit for wlast.
        Port map(
            i_clk => i_clk, --  in std_logic;
            i_rst => i_rst, --  in std_logic;
            -- Data input side
            i_wren => wdataFIFOWrEn(i),           -- in std_logic;
            i_data => wdataFIFODin,               -- in std_logic_vector(g_WIDTH-1 downto 0);
            o_wrDataCount => wDataFIFOwrCount(i), -- out std_logic_vector(5 downto 0);
            o_full => wdataFIFOFull(i),           -- out std_logic;
            -- Data output side
            o_data => wDataFIFOdout(i),           -- out std_logic_vector(g_WIDTH-1 downto 0);
            o_empty => wDataFIFOempty(i),         -- out std_logic;
            o_rdDataCount => wDataFIFOrdCount(i), -- out std_logic_vector(5 downto 0);
            i_rdEn => wDataFIFOrdEn(i)            -- in std_logic
        );
        
    end generate;
    
    -------------------------------------------------------------------------------------------------------
    -- Read side
    -- This is very similar to the write side - almost identical state machine, with corresponding FIFOs.
    --
    
    m0_araddr <= arFIFOdout(0)(31 downto 0);
    m0_arlen <= arFIFOdout(0)(39 downto 32);
    m1_araddr <= arFIFOdout(1)(31 downto 0);
    m1_arlen <= arFIFOdout(1)(39 downto 32);
    m2_araddr <= arFIFOdout(2)(31 downto 0);
    m2_arlen <= arFIFOdout(2)(39 downto 32);
    m3_araddr <= arFIFOdout(3)(31 downto 0);
    m3_arlen <= arFIFOdout(3)(39 downto 32);
    
    m0_arvalid <= not arFIFOempty(0);
    m1_arvalid <= not arFIFOempty(1);
    m2_arvalid <= not arFIFOempty(2);
    m3_arvalid <= not arFIFOempty(3);
    
    arFIFOrdEn(0) <= (not arFIFOempty(0)) and m0_arready;
    arFIFOrdEn(1) <= (not arFIFOempty(1)) and m1_arready;
    arFIFOrdEn(2) <= (not arFIFOempty(2)) and m2_arready;
    arFIFOrdEn(3) <= (not arFIFOempty(3)) and m3_arready;
    
    m0_rready <= not rdataFIFOFull(0);
    m1_rready <= not rdataFIFOFull(1);
    m2_rready <= not rdataFIFOFull(2);
    m3_rready <= not rdataFIFOFull(3);
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then

            if s0_rready = '1' or s0_rvalid = '0' then
                s0_rdata <= rDataFIFOdout(to_integer(unsigned(rSelectFIFOdout)))(255 downto 0);
                s0_rlast <= rDataFIFOdout(to_integer(unsigned(rSelectFIFOdout)))(256);
                s0_rvalid <= (not rSelectFIFOempty) and (not rDataFIFOEmpty(to_integer(unsigned(rSelectFIFOdout)))); 
            end if;
            
            
            if i_rst = '1' then
                rd_fsm <= idle;
                m0123_arvalid <= "0000";
                arFIFOwr <= '0';
                s0_arready <= '0';
            else
                case rd_fsm is
                    when idle =>
                        if s0_arvalid = '1' then
                            rd_fsm <= send;
                            s0_araddrDel1 <= s0_araddr;
                            s0_arlenDel1 <= s0_arlen;
                            if s0_araddr(29 downto 28) = "00" then
                                m0123_arvalid <= "0001";
                            elsif s0_araddr(29 downto 28) = "01" then
                                m0123_arvalid <= "0010";
                            elsif s0_araddr(29 downto 28) = "10" then
                                m0123_arvalid <= "0100";
                            else
                                m0123_arvalid <= "1000";
                            end if;
                            s0_arready <= '0';
                            arFIFOwr <= '1';
                        else
                            s0_arready <= '1';
                            m0123_arvalid <= "0000";
                            arFIFOwr <= '0';
                        end if;
                        
                    when send =>
                        m0123_arvalid <= "0000";
                        rd_fsm <= checkFIFO;
                        s0_arready <= '0';
                        arFIFOwr <= '0';
                        
                    when checkFIFO =>
                        if rSelectFIFOFull = '0' and arFIFOFull = "0000" then
                            rd_fsm <= idle;
                            s0_arready <= '1';
                        else
                            s0_arready <= '0';
                        end if;
                        arFIFOwr <= '0';
                        m0123_arvalid <= "0000";
                    
                    when others =>
                        rd_fsm <= idle;
                    
                end case;
            end if;

            
        end if;
    end process;
    
    rSelectFIFOdin <= s0_araddrDel1(29 downto 28);  -- Selects the source port for the data on the rdata bus associated with this address.
    -- Read from the select fifo when there is data available and we are reading the last data word in a burst from one of the rdata FIFOs. 
    rSelectFIFOrdEn <= (not rSelectFIFOempty) and rDataFIFOdout(to_integer(unsigned(rSelectFIFODout)))(256) and ((s0_rvalid and s0_rready) or (not s0_rvalid)) 
                       and (not rDataFIFOempty(to_integer(unsigned(rSelectFIFODout))));
    
    arFIFOdin <= s0_arlenDel1 & s0_araddrDel1;
    
    -- r_select_FIFO holds the input port to take data for the s0_rdata bus from.
    -- This ensures that data is delivered from the rdata fifos in the order it was requested.
    r_Select_FIFO_inst : entity ct_lib.dsfifo64
    generic map (g_WIDTH => 2)
    port map(
        i_clk => i_clk, --  in std_logic;
        i_rst => i_rst, --  in std_logic;
        -- data input side
        i_wren => arFIFOwr,           -- in std_logic;
        i_data => rSelectFIFOdin,       -- in std_logic_vector(g_WIDTH-1 downto 0);
        o_wrDataCount => rSelectFIFOwrCount, -- out std_logic_vector(6 downto 0);
        o_full => rSelectFIFOFull,           -- out std_logic;
        -- data output side
        o_data => rSelectFIFOdout,           -- out std_logic_vector(g_WIDTH-1 downto 0);
        o_empty => rSelectFIFOempty,         -- out std_logic;
        o_rdDataCount => rSelectFIFOrdCount, -- out std_logic_vector(6 downto 0);
        i_rdEn => rSelectFIFOrdEn            -- in std_logic
    );
    
    rdataFIFOWrEn(0) <= '1' when (m0_rvalid = '1' and rdataFIFOFull(0) = '0') else '0';
    rdataFIFOWrEn(1) <= '1' when (m1_rvalid = '1' and rdataFIFOFull(1) = '0') else '0';
    rdataFIFOWrEn(2) <= '1' when (m2_rvalid = '1' and rdataFIFOFull(2) = '0') else '0';
    rdataFIFOWrEn(3) <= '1' when (m3_rvalid = '1' and rdataFIFOFull(3) = '0') else '0';
    
    rdataFIFODin(0) <= m0_rlast & m0_rdata;
    rdataFIFODin(1) <= m1_rlast & m1_rdata;
    rdataFIFODin(2) <= m2_rlast & m2_rdata;
    rdataFIFODin(3) <= m3_rlast & m3_rdata;
    
    rfifo_gen : for i in 0 to 3 generate
        -- FIFOs for the ar bus and rdata bus for each output AXI port.
        
        arFIFO_inst : entity ct_lib.dsfifo32
        generic map ( g_WIDTH => 40)  -- 32 bits for araddr, 8 bits for arlen
        Port map(
            i_clk => i_clk, --  in std_logic;
            i_rst => i_rst, --  in std_logic;
            -- data input side
            i_wren => m0123_arvalid(i),        -- in std_logic;
            i_data => arFIFOdin,               -- in std_logic_vector(g_WIDTH-1 downto 0);
            o_wrDataCount => arFIFOwrCount(i), -- out std_logic_vector(5 downto 0);
            o_full => arFIFOFull(i),           -- out std_logic;
            -- data output side
            o_data => arFIFOdout(i),           -- out std_logic_vector(g_WIDTH-1 downto 0);
            o_empty => arFIFOempty(i),         -- out std_logic;
            o_rdDataCount => arFIFOrdCount(i), -- out std_logic_vector(5 downto 0);
            i_rdEn => arFIFOrdEn(i)            -- in std_logic
        );
        
        rdataFIFOrdEn(i) <= '1' when (unsigned(rSelectFIFODout) = i) and ((s0_rvalid = '1' and s0_rready = '1') or (s0_rvalid = '0')) else '0';
        
        rdataFIFO_inst : entity ct_lib.dsfifo32
        generic map (g_WIDTH => 257)  -- 256 bits for rdata, 1 bit for rlast.
        Port map(
            i_clk => i_clk, --  in std_logic;
            i_rst => i_rst, --  in std_logic;
            -- Data input side
            i_wren => rdataFIFOWrEn(i),           -- in std_logic;
            i_data => rdataFIFODin(i),            -- in std_logic_vector(g_WIDTH-1 downto 0);
            o_wrDataCount => rDataFIFOwrCount(i), -- out std_logic_vector(5 downto 0);
            o_full => rdataFIFOFull(i),           -- out std_logic;
            -- Data output side
            o_data => rDataFIFOdout(i),           -- out std_logic_vector(g_WIDTH-1 downto 0);
            o_empty => rDataFIFOempty(i),         -- out std_logic;
            o_rdDataCount => rDataFIFOrdCount(i), -- out std_logic_vector(5 downto 0);
            i_rdEn => rDataFIFOrdEn(i)            -- in std_logic
        );
        
    end generate;



    process(i_clk)
    begin
        if rising_edge(i_clk) then
            wSelectFIFOwrCount_dbg <= wSelectFIFOwrCount;  -- 6 bits
            rSelectFIFOwrCount_dbg <= rSelectFIFOwrCount;  --7 bits
            awFIFOfull_dbg <= awFIFOfull;   -- 4 
            awFIFOempty_dbg <= awFIFOempty; -- 4
            arFIFOfull_dbg <= arFIFOfull;  -- 4 
            arFIFOempty_dbg <= arFIFOempty;  -- 4
            rdataFIFOFull_dbg <= rdataFIFOFull; -- 4
            rdataFIFOempty_dbg <= rdataFIFOempty; -- 4
            wdataFIFOFull_dbg <= wdataFIFOFull; -- 4
            wdataFIFOEmpty_dbg <= wdataFIFOEmpty;  -- 4
            
            s0_araddr_dbg <= s0_araddr; -- 32
            s0_arlen_dbg <= s0_arlen;     -- in STD_LOGIC_VECTOR ( 7 downto 0 );
            s0_arready_dbg <= s0_arready; -- out STD_LOGIC;
            s0_arvalid_dbg <= s0_arvalid; -- in STD_LOGIC;
            --
            s0_awaddr_dbg <= s0_awaddr; --  : in STD_LOGIC_VECTOR ( 31 downto 0 );
            s0_awlen_dbg <= s0_awlen; -- in STD_LOGIC_VECTOR ( 7 downto 0 );
            s0_awready_dbg <= s0_awready; --  : out STD_LOGIC;
            s0_awvalid_dbg <= s0_awvalid; -- : in STD_LOGIC;
            --
            s0_rlast_dbg <= s0_rlast; --  : out STD_LOGIC;
            s0_rready_dbg <= s0_rready; --  : in STD_LOGIC;
            s0_rvalid_dbg <= s0_rvalid; -- : out STD_LOGIC;
            --
            s0_wlast_dbg <= s0_wlast; -- : in STD_LOGIC;
            s0_wready_dbg <= s0_wready; --  : out STD_LOGIC;
            s0_wvalid_dbg <= s0_wvalid; -- : in STD_LOGIC
            
        end if;
    end process;

    -- Debug outputs
--    u_s01_ila : ila_beamData
--    port map (
--        clk => i_clk,
--        probe0(5 downto 0) => wSelectFIFOwrCount_dbg, -- 6 bits
--        probe0(12 downto 6) => rSelectFIFOwrCount_dbg,  --7 bits
--        probe0(16 downto 13) => awFIFOfull_dbg, -- <= awFIFOfull;   -- 4 
--        probe0(20 downto 17) => awFIFOempty_dbg, -- <= awFIFOempty; -- 4
--        probe0(24 downto 21) => arFIFOfull_dbg, -- <= arFIFOfull;  -- 4 
--        probe0(28 downto 25) => arFIFOempty_dbg, -- <= arFIFOempty;  -- 4
--        probe0(32 downto 29) => rdataFIFOFull_dbg, -- <= rdataFIFOFull; -- 4
--        probe0(36 downto 33) => rdataFIFOempty_dbg, -- <= rdataFIFOempty; -- 4
--        probe0(40 downto 37) => wdataFIFOFull_dbg, -- <= wdataFIFOFull; -- 4
--        probe0(44 downto 41) => wdataFIFOEmpty_dbg, -- <= wdataFIFOEmpty;  -- 4
--        --    
--        probe0(72 downto 45) => s0_araddr_dbg(31 downto 4), -- <= s0_araddr; -- 32
--        probe0(76 downto 73) => s0_arlen_dbg(3 downto 0), -- <= s0_arlen;     -- in STD_LOGIC_VECTOR ( 7 downto 0 );
--        probe0(77) => s0_arready_dbg, -- <= s0_arready; -- out STD_LOGIC;
--        probe0(78) => s0_arvalid_dbg, -- <= s0_arvalid; -- in STD_LOGIC;
--        --
--        probe0(106 downto 79) => s0_awaddr_dbg(31 downto 4), -- <= s0_awaddr; --  : in STD_LOGIC_VECTOR ( 31 downto 0 );
--        probe0(110 downto 107) => s0_awlen_dbg(3 downto 0), -- <= s0_awlen; -- in STD_LOGIC_VECTOR ( 7 downto 0 );
--        probe0(111) => s0_awready_dbg, -- <= s0_awready, --  : out STD_LOGIC;
--        probe0(112) => s0_awvalid_dbg, -- <= s0_awvalid, -- : in STD_LOGIC;
--        --
--        probe0(113) => s0_rlast_dbg, -- <= s0_rlast; --  : out STD_LOGIC;
--        probe0(114) => s0_rready_dbg, -- <= s0_rready; --  : in STD_LOGIC;
--        probe0(115) => s0_rvalid_dbg, -- <= s0_rvalid; -- : out STD_LOGIC;
--        --
--        probe0(116) => s0_wlast_dbg, -- <= s0_wlast; -- : in STD_LOGIC;
--        probe0(117) => s0_wready_dbg, -- <= s0_wready; --  : out STD_LOGIC;
--        probe0(118) => s0_wvalid_dbg, -- <= s0_wvalid; -- : in STD_LOGIC
--        probe0(119) => '0'
--    );
    
end Behavioral;
