----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 06.11.2020 23:01:54
-- Module Name: ct_atomic_pst_wrapper - Behavioral
-- Description: 
--  Wrapper for the PST corner turn module.
--  Two submodules -
--   - Register interface,
--   - Actual corner turn.
----------------------------------------------------------------------------------

library IEEE, axi4_lib, common_lib, DSP_top_lib, ct_lib;
use IEEE.STD_LOGIC_1164.ALL;
use axi4_lib.axi4_lite_pkg.ALL;
USE common_lib.common_pkg.ALL;
use DSP_top_lib.DSP_top_pkg.all;
use ct_lib.ct_atomic_pst_out_reg_pkg.all;
use IEEE.NUMERIC_STD.ALL;
Library xpm;
use xpm.vcomponents.all;

entity ct_atomic_pst_wrapper is
    generic (
        -- Each LFAA block is 2048 samples, thePST filterbank uses a 256 point FFT, and is 4/3 oversampled,
        -- so the number of output packets per frame is 2048/256 * 4/3 * g_LFAA_BLOCKS_PER_FRAME
        g_LFAA_BLOCKS_PER_FRAME_DIV3 : integer;  -- e.g. value of 1 means there are 3 LFAA blocks per frame for the corner turn, which corresponds to 32 filterbank output packets.
        g_PST_BEAMS : integer;
        g_USE_META : boolean := FALSE  -- Put meta data into the memory in place of the actual data, to make it easier to find bugs in the corner turn. 
    );
    Port(
        -- Parameters, in the i_axi_clk domain.
        i_stations : in std_logic_vector(10 downto 0); -- up to 1024 stations
        i_coarse   : in std_logic_vector(9 downto 0);  -- Number of coarse channels.
        i_virtualChannels : in std_logic_vector(10 downto 0); -- total virtual channels (= i_stations * i_coarse)
        -- Registers AXI Lite Interface (uses i_axi_clk)
        i_axi_mosi     : in t_axi4_lite_mosi;
        o_axi_miso     : out t_axi4_lite_miso;
        i_axi_rst      : in std_logic;
        -- reset should be passed in from the reset for the first corner turn, in the i_axi_clk domain.
        i_rst          : in std_logic;
        -- Data in from the PST filterbanks; bursts of 216 clocks for each channel.
        -- 
        i_sof          : in std_logic; -- pulse high at the start of every frame. (1 frame is typically 60ms of data).
        i_FB_clk       : in std_logic; -- filterbank clock, expected to be 450 MHz
        i_frameCount     : in std_logic_vector(36 downto 0); -- frame count is the same for all simultaneous output streams.
        i_virtualChannel : in t_slv_16_arr(2 downto 0); -- 3 virtual channels, one for each of the PST data streams.
        i_HeaderValid : in std_logic_vector(2 downto 0);
        i_data        : in t_ctc_output_payload_arr(2 downto 0); -- 8 bit data; fields are Hpol.re, .Hpol.im, .Vpol.re, .Vpol.im, for each of i_data(0), i_data(1), i_data(2)
        i_dataValid   : in std_logic;
        
        -- Data out to the beamformers
        i_BF_clk  : in std_logic; -- beamformer clock, expected to be 400 MHz
        o_data    : out std_logic_vector(95 downto 0); -- 3 consecutive fine channels delivered every clock.
        o_flagged : out std_logic_vector(2 downto 0);  -- "o_flagged" is aligned with "o_data"
        o_fine    : out std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        o_coarse  : out std_logic_vector(9 downto 0);  -- index of the coarse (LFAA) frequency channel.
        o_firstStation : out std_logic;
        o_lastStation : out std_logic;
        o_timeStep : out std_logic_vector(4 downto 0);
        o_virtualChannel  : out std_logic_vector(9 downto 0);  -- coarse channel count.
        o_packetCount : out std_logic_vector(36 downto 0); -- The packet count for this packet, based on the original packet count from LFAA.
        o_valid   : out std_logic;
        -- Configuration to the beamformers
        o_jonesBuffer : out std_logic;
        o_phaseBuffer : out std_logic;
        o_beamsEnabled : out std_logic_vector(7 downto 0);
        o_scale_exp_frac : out std_logic_vector(7 downto 0); -- bit 3 = 0 indicates firmware should calculate the scale factor.
        -----------------------------------------------------------
        -- AXI interfaces to the HBM.
        -- Corner turn between filterbanks and beamformer
        -- There are four separate busses to increase the memory bandwidth, since the read side needs to do short length bursts that are not very efficient.
        i_axi_clk : in std_logic;
        -----------------------------------------------------------
        -- aw bus = write address
        m0_axi_awvalid  : out std_logic;
        m0_axi_awready  : in std_logic;
        m0_axi_awaddr   : out std_logic_vector(27 downto 0);
        m0_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m0_axi_wvalid    : out std_logic;
        m0_axi_wready    : in std_logic;
        m0_axi_wdata     : out std_logic_vector(255 downto 0);
        m0_axi_wlast     : out std_logic;
        -- b bus - write response
        m0_axi_bvalid    : in std_logic;
        m0_axi_bready    : out std_logic;
        m0_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m0_axi_arvalid   : out std_logic;
        m0_axi_arready   : in std_logic;
        m0_axi_araddr    : out std_logic_vector(27 downto 0);
        m0_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m0_axi_rvalid    : in std_logic;
        m0_axi_rready    : out std_logic;
        m0_axi_rdata     : in std_logic_vector(255 downto 0);
        m0_axi_rlast     : in std_logic;
        m0_axi_rresp     : in std_logic_vector(1 downto 0);
        --------------------------------------------------------------
        -- aw bus = write address
        m1_axi_awvalid  : out std_logic;
        m1_axi_awready  : in std_logic;
        m1_axi_awaddr   : out std_logic_vector(27 downto 0);
        m1_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m1_axi_wvalid    : out std_logic;
        m1_axi_wready    : in std_logic;
        m1_axi_wdata     : out std_logic_vector(255 downto 0);
        m1_axi_wlast     : out std_logic;
        -- b bus - write response
        m1_axi_bvalid    : in std_logic;
        m1_axi_bready    : out std_logic;
        m1_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m1_axi_arvalid   : out std_logic;
        m1_axi_arready   : in std_logic;
        m1_axi_araddr    : out std_logic_vector(27 downto 0);
        m1_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m1_axi_rvalid    : in std_logic;
        m1_axi_rready    : out std_logic;
        m1_axi_rdata     : in std_logic_vector(255 downto 0);
        m1_axi_rlast     : in std_logic;
        m1_axi_rresp     : in std_logic_vector(1 downto 0);
        ---------------------------------------------------------------
        -- aw bus = write address
        m2_axi_awvalid  : out std_logic;
        m2_axi_awready  : in std_logic;
        m2_axi_awaddr   : out std_logic_vector(27 downto 0);
        m2_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m2_axi_wvalid    : out std_logic;
        m2_axi_wready    : in std_logic;
        m2_axi_wdata     : out std_logic_vector(255 downto 0);
        m2_axi_wlast     : out std_logic;
        -- b bus - write response
        m2_axi_bvalid    : in std_logic;
        m2_axi_bready    : out std_logic;
        m2_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m2_axi_arvalid   : out std_logic;
        m2_axi_arready   : in std_logic;
        m2_axi_araddr    : out std_logic_vector(27 downto 0);
        m2_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m2_axi_rvalid    : in std_logic;
        m2_axi_rready    : out std_logic;
        m2_axi_rdata     : in std_logic_vector(255 downto 0);
        m2_axi_rlast     : in std_logic;
        m2_axi_rresp     : in std_logic_vector(1 downto 0);
        -----------------------------------------------------------------
        -- aw bus = write address
        m3_axi_awvalid  : out std_logic;
        m3_axi_awready  : in std_logic;
        m3_axi_awaddr   : out std_logic_vector(27 downto 0);
        m3_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m3_axi_wvalid    : out std_logic;
        m3_axi_wready    : in std_logic;
        m3_axi_wdata     : out std_logic_vector(255 downto 0);
        m3_axi_wlast     : out std_logic;
        -- b bus - write response
        m3_axi_bvalid    : in std_logic;
        m3_axi_bready    : out std_logic;
        m3_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m3_axi_arvalid   : out std_logic;
        m3_axi_arready   : in std_logic;
        m3_axi_araddr    : out std_logic_vector(27 downto 0);
        m3_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m3_axi_rvalid    : in std_logic;
        m3_axi_rready    : out std_logic;
        m3_axi_rdata     : in std_logic_vector(255 downto 0);
        m3_axi_rlast     : in std_logic;
        m3_axi_rresp     : in std_logic_vector(1 downto 0);
        -- debug
        o_dataMismatch : out std_logic;
        o_dataMismatchBFclk : out std_logic
    );
end ct_atomic_pst_wrapper;

architecture Behavioral of ct_atomic_pst_wrapper is

    signal reg_rw : t_statctrl_rw;
    signal reg_ro : t_statctrl_ro;
    signal packetsPerFrame : std_logic_vector(9 downto 0);
    
    signal clkCrossSendCount : std_logic_vector(5 downto 0) := "000000";
    signal triggerSend, triggerSendDel1 : std_logic := '0';
    signal axi2bf_src_send, axi2bf_src_rcv, axi2bf_dest_req : std_logic := '0';
    signal axi2bf_in, axi2bf_dest_out : std_logic_vector(15 downto 0);
    
    signal ct_axi_awvalid : std_logic_vector(0 downto 0);
    signal ct_axi_awready : std_logic_vector(0 downto 0);
    signal ct_axi_awaddr  : std_logic_vector(31 downto 0);
    signal ct_axi_awlen   : std_logic_vector(7 downto 0);
    -- w bus - write data
    signal ct_axi_wvalid : std_logic_vector(0 downto 0);
    signal ct_axi_wready : std_logic_vector(0 downto 0);
    signal ct_axi_wdata  : std_logic_vector(255 downto 0);
    signal ct_axi_wlast  : std_logic_vector(0 downto 0);
    -- b bus - write response
    signal ct_axi_bvalid : std_logic_vector(0 downto 0);
    signal ct_axi_bresp  : std_logic_vector(1 downto 0);
    -- ar bus - read address
    signal ct_axi_arvalid : std_logic_vector(0 downto 0);
    signal ct_axi_arready : std_logic_vector(0 downto 0);
    signal ct_axi_araddr  : std_logic_vector(31 downto 0);
    signal ct_axi_arlen   : std_logic_vector(7 downto 0);
    -- r bus - read data
    signal ct_axi_rvalid : std_logic_vector(0 downto 0);
    signal ct_axi_rready : std_logic_vector(0 downto 0);
    signal ct_axi_rdata  : std_logic_vector(255 downto 0);
    signal ct_axi_rlast  : std_logic_vector(0 downto 0);
    signal ct_axi_rresp  : std_logic_vector(1 downto 0);    
    -- unused ct axi bus signals
    --signal ct_axi_arburst : std_logic_vector(1 downto 0);
    --signal ct_axi_arcache : std_logic_vector(3 downto 0);
    --signal ct_axi_arlock : std_logic_vector(0 downto 0);
    --signal ct_axi_arprot : std_logic_vector(2 downto 0);
    --signal ct_axi_arqos : std_logic_vector(3 downto 0);
    --signal ct_axi_awlock : std_logic_vector(0 downto 0);
    --signal ct_axi_awprot : std_logic_vector(2 downto 0);
    --signal ct_axi_awqos  : std_logic_vector(3 downto 0);
    --signal ct_axi_awburst : std_logic_vector(1 downto 0);
    --signal ct_axi_awcache : STD_LOGIC_VECTOR(3 downto 0);
    signal ct_axi_bready : std_logic_vector(0 downto 0);
    
    -- unused axi signals
    --signal m0_axi_wstrb, m1_axi_wstrb, m2_axi_wstrb, m3_axi_wstrb : STD_LOGIC_VECTOR(31 downto 0);
    --signal m0_axi_arsize, m1_axi_arsize, m2_axi_arsize, m3_axi_arsize : STD_LOGIC_VECTOR(2 downto 0 );
    --signal ct_axi_arsize, ct_axi_awsize : std_logic_vector(3 downto 0);
    --signal ct_axi_wstrb : std_logic_vector(31 downto 0);
    
    -- 32 bit versions of the address
    signal m0_axi_araddr32bit, m0_axi_awaddr32bit : std_logic_vector(31 downto 0);
    signal m1_axi_araddr32bit, m1_axi_awaddr32bit : std_logic_vector(31 downto 0);
    signal m2_axi_araddr32bit, m2_axi_awaddr32bit : std_logic_vector(31 downto 0);
    signal m3_axi_araddr32bit, m3_axi_awaddr32bit : std_logic_vector(31 downto 0);
    signal axi_rstn : std_logic;
    
    signal beamsEnabled : std_logic_vector(7 downto 0);
    signal axi_scale_exp, axi_shift  : std_logic_vector(7 downto 0);
    signal scale_exp, scale_frac, axi_shift_final : std_logic_vector(3 downto 0);
    
begin
    
    packetsPerFrame <= std_logic_vector(to_unsigned(g_LFAA_BLOCKS_PER_FRAME_DIV3*32,10));
    
    ct_inst : entity ct_lib.ct_atomic_pst_out
    generic map (
        g_PST_BEAMS => g_PST_BEAMS,
        g_USE_META => g_USE_META
    )
    port map(
        -- Parameters, in the i_axi_clk domain.
        i_stations => i_stations(10 downto 0), -- in std_logic_vector(10 downto 0); -- up to 1024 stations
        i_coarse   => i_coarse(9 downto 0), -- in std_logic_vector(9 downto 0);  -- Number of coarse channels.
        i_virtualChannels => i_virtualChannels(10 downto 0), -- in std_logic_vector(10 downto 0); -- total virtual channels (= i_stations * i_coarse)
        i_packetsPerFrame => packetsPerFrame, -- in std_logic_vector(9 downto 0); -- Packets per frame; e.g. if the first corner turn is 27 LFAA packets, then this will be 288.
        i_HBMBufferEnable    => reg_rw.bufferEnable, --  in std_logic_vector(3 downto 0);
        o_HBMBuf0PacketCount => reg_ro.HBMBuf0packetCount, -- out std_logic_vector(31 downto 0);
        o_HBMBuf1PacketCount => reg_ro.HBMBuf1packetCount, -- out std_logic_vector(31 downto 0);
        o_HBMBuf2PacketCount => reg_ro.HBMBuf2PacketCount, -- out std_logic_vector(31 downto 0);
        o_HBMBuf3PacketCount => reg_ro.HBMBuf3PacketCount, -- out std_logic_vector(31 downto 0);
        
        i_beams_enabled  => beamsEnabled,
        
        i_BFJonesSelect => reg_rw.bfjonesselect,   -- std_logic;
        i_BFPhaseSelect => reg_rw.bfphaseselect,   -- std_logic;
        i_JonesSwitch   => reg_rw.packetcountbfjonesswitch, -- 32 bits.
        i_phaseSwitch   => reg_rw.packetcountbfphaseswitch, -- 32 bits.
        
        i_rst => i_rst, -- in std_logic;
        -- Data in from the PST filterbanks; bursts of 216 clocks for each channel.
        -- 
        i_sof          => i_sof, --  in std_logic; -- pulse high at the start of every frame. (1 frame is typically 60ms of data).
        i_FB_clk       => i_FB_clk, -- in std_logic; -- filterbank clock, expected to be 450 MHz
        i_frameCount     => i_frameCount, -- in std_logic_vector(36 downto 0); -- frame count is the same for all simultaneous output streams.
        i_virtualChannel => i_virtualChannel, --  in t_slv_16_arr(2 downto 0); -- 3 virtual channels, one for each of the PST data streams.
        i_HeaderValid => i_HeaderValid, -- in std_logic_vector(2 downto 0);
        i_data        => i_data, -- in t_ctc_output_payload_arr(2 downto 0); -- 8 bit data; fields are Hpol.re, .Hpol.im, .Vpol.re, .Vpol.im, for each of i_data(0), i_data(1), i_data(2)
        i_dataValid   => i_dataValid, -- in std_logic;
        -- Data out to the beamformer
        i_BF_clk  => i_BF_clk, -- in std_logic; -- beamformer clock, expected to be 400 MHz
        o_data    => o_data,   -- out std_logic_vector(95 downto 0); -- 3 consecutive fine channels delivered every clock.
        o_flagged => o_flagged, -- out std_logic_vector(2 downto 0);
        o_fine    => o_fine,   -- out std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        o_coarse  => o_coarse, -- out (9:0);
        o_firstStation => o_firstStation, -- out std_logic;
        o_lastStation => o_lastStation,   -- out std_logic;
        o_timeStep => o_timeStep,         -- out (4:0)
        o_virtualChannel  => o_virtualChannel,  -- out std_logic_vector(9 downto 0);  -- coarse channel count.
        o_packetCount => o_packetCount, -- out std_logic_vector(36 downto 0); -- The packet count for this packet, based on the original packet count from LFAA.
        o_valid   => o_valid,   -- out std_logic;
        -- Configuration out to the beamformer. These select which buffer to use for the double buffered jones matrix and phase tracking data.  
        o_jonesBuffer => o_jonesBuffer,
        o_phaseBuffer => o_phaseBuffer,
        -- AXI interface to the HBM
        -- Corner turn between filterbanks and beamformer
        -- aw bus = write address
        i_axi_clk => i_axi_clk, -- in std_logic;
        -- 
        m02_axi_awvalid  => ct_axi_awvalid(0), -- out std_logic;
        m02_axi_awready  => ct_axi_awready(0), -- in std_logic;
        m02_axi_awaddr   => ct_axi_awaddr(29 downto 0),  -- out std_logic_vector(29 downto 0);
        m02_axi_awlen    => ct_axi_awlen,   -- out std_logic_vector(7 downto 0);
        -- w bus - write data
        m02_axi_wvalid   => ct_axi_wvalid(0), -- out std_logic;
        m02_axi_wready   => ct_axi_wready(0), -- in std_logic;
        m02_axi_wdata    => ct_axi_wdata,  -- out std_logic_vector(255 downto 0);
        m02_axi_wlast    => ct_axi_wlast(0),  -- out std_logic;
        -- b bus - write response
        m02_axi_bvalid   => ct_axi_bvalid(0), -- in std_logic;
        m02_axi_bresp    => ct_axi_bresp,  -- in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m02_axi_arvalid  => ct_axi_arvalid(0), -- out std_logic;
        m02_axi_arready  => ct_axi_arready(0), -- in std_logic;
        m02_axi_araddr   => ct_axi_araddr(29 downto 0),  -- out std_logic_vector(29 downto 0);
        m02_axi_arlen    => ct_axi_arlen,   -- out std_logic_vector(7 downto 0);
        -- r bus - read data
        m02_axi_rvalid   => ct_axi_rvalid(0),  -- in std_logic;
        m02_axi_rready   => ct_axi_rready(0),  -- out std_logic;
        m02_axi_rdata    => ct_axi_rdata,   -- in std_logic_vector(255 downto 0);
        m02_axi_rlast    => ct_axi_rlast(0),   -- in std_logic;
        m02_axi_rresp    => ct_axi_rresp,   -- in std_logic_vector(1 downto 0);
        ---------------------------------------------------
        -- Error signals on the i_axi_clk domain.
        o_bufferOverflowError => reg_ro.bufferoverflowerror, -- out std_logic; buffer between data input from the filterbanks and the HBM has overflowed; this should never happen.
        o_readoutError => reg_ro.readoutError,  -- out std_logic  -- readout to the beamformer didn't finish before the next readout started. This should never happen.
        o_readoutClocks => reg_ro.readoutClocks, -- out (31:0), Number of beamformer (400MHz) clocks required to read the most recent frame out from HBM
        o_readInClocks  => reg_ro.readInClocks,   -- out (31:0), Number of axi (300MHz) clocks required to write the most recent frame into HBM
        o_readInAllClocks => reg_ro.readInAllClocks, -- out (31:0) -- Number of axi (300MHz) clocks between the start of one frame and the start of the next.
        o_dataMismatch => o_dataMismatch,
        o_dataMismatchBFclk => o_dataMismatchBFclk  -- out std_logic
    );
    ct_axi_araddr(31 downto 30) <= "00";
    ct_axi_awaddr(31 downto 30) <= "00";
    -- axi signals that are not used by the corner turn.
    --ct_axi_arburst <= "01";
    --ct_axi_arcache <= "0011";
    --ct_axi_arlock(0) <= '0';
    --ct_axi_arprot <= "000";
    --ct_axi_arqos <= "0000";
    --ct_axi_arsize <= "0101";  -- 2^5 = 32 bytes per beat.
    --ct_axi_awlock(0) <= '0';
    --ct_axi_awprot <= "000";
    --ct_axi_awqos <= "0000";
    --ct_axi_awsize <= "0101";
    --ct_axi_awburst <= "01";  -- in STD_LOGIC_VECTOR ( 1 downto 0 );
    --ct_axi_awcache <= "0011";   -- in STD_LOGIC_VECTOR ( 3 downto 0 );
    ct_axi_bready(0) <= '1';
    --ct_axi_wstrb <= (others => '1');
    -------------------------------------------------------------------------------
    -- Convert single AXI bus to from the corner turn core module above into 
    -- 4 separate AXI busses, one for each 256 MByte piece of the HBM.
    -- This is to maximize the bandwidth to the HBM
    axi_rstn <= not i_axi_rst;
    
    
    aximux : entity ct_lib.axi_4to1
    Port map(
        i_clk      => i_axi_clk,   -- in std_logic;
        i_rst      => i_axi_rst,   -- in STD_LOGIC;
        -------------------------------------------------------------
        -- First master
        m0_araddr  => m0_axi_araddr32bit, -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        m0_arlen   => m0_axi_arlen,       -- out STD_LOGIC_VECTOR ( 7 downto 0 );
        m0_arready => m0_axi_arready,     -- in STD_LOGIC;
        m0_arvalid => m0_axi_arvalid,     -- out STD_LOGIC;
        --
        m0_awaddr  => m0_axi_awaddr32bit, -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        m0_awlen   => m0_axi_awlen,       -- out STD_LOGIC_VECTOR ( 7 downto 0 );
        m0_awready => m0_axi_awready,    -- in STD_LOGIC;
        m0_awvalid => m0_axi_awvalid,    -- out STD_LOGIC;
        -- The write response bus exists here but is ignored.
        m0_bready => m0_axi_bready,      -- out STD_LOGIC;
        m0_bresp  => m0_axi_bresp,       -- in STD_LOGIC_VECTOR ( 1 downto 0 );
        m0_bvalid => m0_axi_bvalid,      -- in STD_LOGIC;
        --
        m0_rdata  => m0_axi_rdata,       -- in STD_LOGIC_VECTOR ( 255 downto 0 );
        m0_rlast  => m0_axi_rlast,       -- in STD_LOGIC;
        m0_rready => m0_axi_rready,      -- out STD_LOGIC;
        m0_rresp  => m0_axi_rresp,       -- in STD_LOGIC_VECTOR ( 1 downto 0 );  NOTE : rresp is ignored.
        m0_rvalid => m0_axi_rvalid,      -- in STD_LOGIC;
        --
        m0_wdata  => m0_axi_wdata,       -- out STD_LOGIC_VECTOR ( 255 downto 0 );
        m0_wlast  => m0_axi_wlast,       -- out STD_LOGIC;
        m0_wready => m0_axi_wready,      -- in STD_LOGIC;
        m0_wvalid => m0_axi_wvalid,      -- out STD_LOGIC;
        -----------------------------------------------------------
        -- Second master
        m1_araddr  => m1_axi_araddr32bit, -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        m1_arlen   => m1_axi_arlen,       -- out STD_LOGIC_VECTOR ( 7 downto 0 );
        m1_arready => m1_axi_arready,     -- in STD_LOGIC;
        m1_arvalid => m1_axi_arvalid,     -- out STD_LOGIC;
        --
        m1_awaddr  => m1_axi_awaddr32bit, -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        m1_awlen   => m1_axi_awlen,       -- out STD_LOGIC_VECTOR ( 7 downto 0 );
        m1_awready => m1_axi_awready,    -- in STD_LOGIC;
        m1_awvalid => m1_axi_awvalid,    -- out STD_LOGIC;
        -- The write response bus exists here but is ignored.
        m1_bready => m1_axi_bready,      -- out STD_LOGIC;
        m1_bresp  => m1_axi_bresp,       -- in STD_LOGIC_VECTOR ( 1 downto 0 );
        m1_bvalid => m1_axi_bvalid,      -- in STD_LOGIC;
        --
        m1_rdata  => m1_axi_rdata,       -- in STD_LOGIC_VECTOR ( 255 downto 0 );
        m1_rlast  => m1_axi_rlast,       -- in STD_LOGIC;
        m1_rready => m1_axi_rready,      -- out STD_LOGIC;
        m1_rresp  => m1_axi_rresp,       -- in STD_LOGIC_VECTOR ( 1 downto 0 );  NOTE : rresp is ignored.
        m1_rvalid => m1_axi_rvalid,      -- in STD_LOGIC;
        --
        m1_wdata  => m1_axi_wdata,       -- out STD_LOGIC_VECTOR ( 255 downto 0 );
        m1_wlast  => m1_axi_wlast,       -- out STD_LOGIC;
        m1_wready => m1_axi_wready,      -- in STD_LOGIC;
        m1_wvalid => m1_axi_wvalid,      -- out STD_LOGIC;        
        
        ----------------------------------------------------------
        -- Third master

        m2_araddr  => m2_axi_araddr32bit, -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        m2_arlen   => m2_axi_arlen,       -- out STD_LOGIC_VECTOR ( 7 downto 0 );
        m2_arready => m2_axi_arready,     -- in STD_LOGIC;
        m2_arvalid => m2_axi_arvalid,     -- out STD_LOGIC;
        --
        m2_awaddr  => m2_axi_awaddr32bit, -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        m2_awlen   => m2_axi_awlen,       -- out STD_LOGIC_VECTOR ( 7 downto 0 );
        m2_awready => m2_axi_awready,    -- in STD_LOGIC;
        m2_awvalid => m2_axi_awvalid,    -- out STD_LOGIC;
        -- The write response bus exists here but is ignored.
        m2_bready => m2_axi_bready,      -- out STD_LOGIC;
        m2_bresp  => m2_axi_bresp,       -- in STD_LOGIC_VECTOR ( 1 downto 0 );
        m2_bvalid => m2_axi_bvalid,      -- in STD_LOGIC;
        --
        m2_rdata  => m2_axi_rdata,       -- in STD_LOGIC_VECTOR ( 255 downto 0 );
        m2_rlast  => m2_axi_rlast,       -- in STD_LOGIC;
        m2_rready => m2_axi_rready,      -- out STD_LOGIC;
        m2_rresp  => m2_axi_rresp,       -- in STD_LOGIC_VECTOR ( 1 downto 0 );  NOTE : rresp is ignored.
        m2_rvalid => m2_axi_rvalid,      -- in STD_LOGIC;
        --
        m2_wdata  => m2_axi_wdata,       -- out STD_LOGIC_VECTOR ( 255 downto 0 );
        m2_wlast  => m2_axi_wlast,       -- out STD_LOGIC;
        m2_wready => m2_axi_wready,      -- in STD_LOGIC;
        m2_wvalid => m2_axi_wvalid,      -- out STD_LOGIC;        
        
        ------------------------------------------------------------
        -- Fourth master

        m3_araddr  => m3_axi_araddr32bit, -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        m3_arlen   => m3_axi_arlen,       -- out STD_LOGIC_VECTOR ( 7 downto 0 );
        m3_arready => m3_axi_arready,     -- in STD_LOGIC;
        m3_arvalid => m3_axi_arvalid,     -- out STD_LOGIC;
        --
        m3_awaddr  => m3_axi_awaddr32bit, -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        m3_awlen   => m3_axi_awlen,       -- out STD_LOGIC_VECTOR ( 7 downto 0 );
        m3_awready => m3_axi_awready,    -- in STD_LOGIC;
        m3_awvalid => m3_axi_awvalid,    -- out STD_LOGIC;
        -- The write response bus exists here but is ignored.
        m3_bready => m3_axi_bready,      -- out STD_LOGIC;
        m3_bresp  => m3_axi_bresp,       -- in STD_LOGIC_VECTOR ( 1 downto 0 );
        m3_bvalid => m3_axi_bvalid,      -- in STD_LOGIC;
        --
        m3_rdata  => m3_axi_rdata,       -- in STD_LOGIC_VECTOR ( 255 downto 0 );
        m3_rlast  => m3_axi_rlast,       -- in STD_LOGIC;
        m3_rready => m3_axi_rready,      -- out STD_LOGIC;
        m3_rresp  => m3_axi_rresp,       -- in STD_LOGIC_VECTOR ( 1 downto 0 );  NOTE : rresp is ignored.
        m3_rvalid => m3_axi_rvalid,      -- in STD_LOGIC;
        --
        m3_wdata  => m3_axi_wdata,       -- out STD_LOGIC_VECTOR ( 255 downto 0 );
        m3_wlast  => m3_axi_wlast,       -- out STD_LOGIC;
        m3_wready => m3_axi_wready,      -- in STD_LOGIC;
        m3_wvalid => m3_axi_wvalid,      -- out STD_LOGIC;        
        
        ------------------------------------------------------------
        -- Slave interface
        s0_araddr  => ct_axi_araddr,   -- in STD_LOGIC_VECTOR ( 31 downto 0 );
        s0_arlen   => ct_axi_arlen,    -- in STD_LOGIC_VECTOR ( 7 downto 0 );
        s0_arready => ct_axi_arready(0),  -- out STD_LOGIC;
        s0_arvalid => ct_axi_arvalid(0),  -- in STD_LOGIC;
        --
        s0_awaddr => ct_axi_awaddr,    -- in STD_LOGIC_VECTOR ( 31 downto 0 );
        s0_awlen  => ct_axi_awlen,     -- in STD_LOGIC_VECTOR ( 7 downto 0 );
        s0_awready => ct_axi_awready(0),  -- out STD_LOGIC;
        s0_awvalid => ct_axi_awvalid(0),  -- in STD_LOGIC;
        --
        s0_bready => ct_axi_bready(0),  -- in STD_LOGIC;
        s0_bresp  => ct_axi_bresp,   -- out STD_LOGIC_VECTOR ( 1 downto 0 );
        s0_bvalid => ct_axi_bvalid(0),  -- out STD_LOGIC;
        --
        s0_rdata => ct_axi_rdata,    -- out STD_LOGIC_VECTOR ( 255 downto 0 );
        s0_rlast => ct_axi_rlast(0),    -- out STD_LOGIC;
        s0_rready => ct_axi_rready(0),  -- in STD_LOGIC;
        s0_rresp  => ct_axi_rresp,   -- out STD_LOGIC_VECTOR ( 1 downto 0 );
        s0_rvalid => ct_axi_rvalid(0),  -- out STD_LOGIC;
        --
        s0_wdata  => ct_axi_wdata,   -- in STD_LOGIC_VECTOR ( 255 downto 0 );
        s0_wlast  => ct_axi_wlast(0),   -- in STD_LOGIC;
        s0_wready => ct_axi_wready(0),  -- out STD_LOGIC;
        s0_wvalid => ct_axi_wvalid(0)   -- in STD_LOGIC
    );
    
    
    m0_axi_araddr <= m0_axi_araddr32bit(27 downto 0);
    m0_axi_awaddr <= m0_axi_awaddr32bit(27 downto 0);

    m1_axi_araddr <= m1_axi_araddr32bit(27 downto 0);
    m1_axi_awaddr <= m1_axi_awaddr32bit(27 downto 0);
    
    m2_axi_araddr <= m2_axi_araddr32bit(27 downto 0);
    m2_axi_awaddr <= m2_axi_awaddr32bit(27 downto 0);
    
    m3_axi_araddr <= m3_axi_araddr32bit(27 downto 0);
    m3_axi_awaddr <= m3_axi_awaddr32bit(27 downto 0);

    reginst : entity ct_lib.ct_atomic_pst_out_reg
    port map (
        MM_CLK              => i_axi_clk, -- IN    STD_LOGIC;
        MM_RST              => i_axi_rst, -- IN    STD_LOGIC;
        SLA_IN              => i_axi_mosi, -- IN    t_axi4_lite_mosi;
        SLA_OUT             => o_axi_miso, -- OUT   t_axi4_lite_miso;
        STATCTRL_FIELDS_RW	=> reg_rw, --  OUT t_statctrl_rw;
        STATCTRL_FIELDS_RO	=> reg_ro  -- IN  t_statctrl_ro
    );
    
    reg_ro.numberofbeams <= std_logic_vector(to_unsigned(g_PST_BEAMS,8));


    
    -- Get reg_rw.beamsEnabled into the i_BF_clk domain
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
            clkCrossSendCount <= std_logic_vector(unsigned(clkCrossSendCount) + 1);
            triggerSend <= clkCrossSendCount(5);
            triggerSendDel1 <= triggerSend;
        
            if (triggerSend = '1' and triggerSendDel1 = '0') then
                axi2bf_src_send <= '1';
            elsif axi2bf_src_rcv = '1' then
                axi2bf_src_send <= '0';
            end if;
            
            axi2bf_in(7 downto 0) <= reg_rw.beamsEnabled;
            axi2bf_in(15 downto 12) <= axi_shift_final;
            if (unsigned(reg_rw.scaleFactor) = 0) then
                axi2bf_in(11 downto 8) <= "0000";
            else
                axi2bf_in(11 downto 8) <= '1' & reg_rw.scaleFactor(22 downto 20);
            end if;
            
            -- scale factor in the register is a floating point value, but the beamformer 
            -- only supports scale factors in a restricted range. Find the best allowed value.
            -- beamformer code for explanation of the +9, related to total scaling in the beamformer.
            axi_shift <= std_logic_vector(to_unsigned(127+9,8) - unsigned(axi_scale_exp));
            if unsigned(axi_shift) > 15 then
                axi_shift_final <= "1111";
            elsif unsigned(axi_scale_exp) > (127+9) then
                axi_shift_final <= "0000";
            else 
                axi_shift_final <= axi_shift(3 downto 0);
            end if;
            
        end if;
    end process;
    
    axi_scale_exp <= reg_rw.scaleFactor(30 downto 23);
    
    xpm_cdc_handshake_psc_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        WIDTH => 16          -- DECIMAL; range: 1-1024
    )
    port map (
        dest_out => axi2bf_dest_out,   -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => axi2bf_dest_req,   -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => axi2bf_src_rcv,     -- 1-bit output: Acknowledgement from destination logic that src_in has been received. This signal will be deasserted once destination handshake has fully completed,
        dest_ack => '1',               -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_BF_clk,          -- 1-bit input: Destination clock.
        src_clk => i_axi_clk,          -- 1-bit input: Source clock.
        src_in => axi2bf_in, -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => axi2BF_src_send    -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
            if axi2bf_dest_req = '1' then
                beamsEnabled <= axi2bf_dest_out(7 downto 0);
                scale_exp <= axi2bf_dest_out(15 downto 12);
                scale_frac <= axi2bf_dest_out(11 downto 8);
            end if;
        end if;
    end process;
    
    o_beamsEnabled <= beamsEnabled;
    o_scale_exp_frac <= scale_exp & scale_frac;  -- scale_frac = 0 indicates firmware should calculate the scale factor.
    
end Behavioral;
