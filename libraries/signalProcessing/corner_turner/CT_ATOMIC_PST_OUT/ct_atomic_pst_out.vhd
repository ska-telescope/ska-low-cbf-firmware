----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 30.10.2020 22:21:03
-- Module Name: ct_atomic_pst_out - Behavioral
-- Description: 
--    Corner turn between the filterbanks and the beamformer for PST processing. 
-- 
-- Data coming in from the filterbanks :
--   3 dual-pol channels, with burst of 216 fine channels at a time.
--   Total number of bytes per clock coming in is  (3 channels)*(2 pol)*(2 complex) = 12 bytes.
--   with roughly 216 out of every 256 clocks active.
--   Total data rate in is thus roughly (12 bytes * 8 bits)*216/256 * 450 MHz = 36 Gb/sec (this is the average data rate while data is flowing)
--   Actual total data rate in is (4/3 oversampling) * (216/256 fine channels used) * (1/1080ns sampling period) * (32 bits/sample) * (1024 channels) = 34.1 Gb/sec 
--
-- Storing to HBM
--   We get about 60 ms of data for 3 channels, before getting 60ms for the next 3 channels, up to 1024 channels.
--   The beamformers need this to be rearranged so the data for all stations can be processed before moving to the next time.
--   The number of PST filterbank outputs per frame is related to the number of LFAA packets per frame by
--     (4/3) * (LFAA packets * (2048/256)); so for 27 LFAA packets per frame = 288 filterbank frames
--   Each virtual channel is stored as a contiguous block of memory, so the address in the memory is calculated as:
--     Given F = Fine Channel  (0 to 215)
--           V = virtual channel (0 to up to 1023)
--           T = Time within the block (0 to 288, assuming the first stage corner turn is 27 LFAA blocks)
--     Then the sample address =  V*2^16*2 + T*216*2 + F
--     Byte adddress is the sample address x4 since samples are 4 bytes.
--     Because of the extra factor of 2 in the equation, only the first half of each block of 1728 bytes is used. This
--     means that there is enough space for the bytes per sample to be doubled to 8, so we could store 16 bit data from 
--     the filterbanks instead of 8 bit data.
--     The 10 bit virtual channels is permuted so that the LSB of the virtual channel is the MSB of the sample address. This is
--     done so that stations alternate between different pieces of the HBM, which improves the effective bandwidth when reading
--     the data out.
--  
--   There is a buffer in this module to hold data from the three streams while it is being written to the HBM
--   The buffer is 512 deep x 256 bits wide.
--      - Split into 4 buffers, of which 3 are used.
--        - Each of the three smaller buffers is 128 deep x 256 bits wide
--          - Each of the smaller buffers is split into 4 regions, each of which is 32 deep x 256 wide.
--            32 x 256 bits = 1024 bytes = sufficient space for 216 fine channels (=1 PST filterbank output frame).
--            So as soon as a frame is written in, all the write requests are generated to write it to HBM
--   Writes to the HBM for each packet consist of 2 writes of 12 words, and 1 write of 3 words, for 27 x 256 bit words total.  
--
-- Data out to the beamformer
--   Use a 400 MHz clock, 96 bits/clock, for a data rate of  38.4 Gbits/sec. 
--     (Note : other options : 64 bits/clock needs at least a minimum 540 MHz clock (too fast for comfort)
--                       while 128 bits/clock only needs a 300 MHz clock (too slow; too many DSPs required))
--   96 bits = 3 * 32 bits = 3 dual-pol samples
--   
--   Data is processed by the beamformer in groups of either 48 fine channels or 24 fine channels.
--   The order of data going to the beamformer :
--
--    For timeGroup = 0:(<time samples per corner turn>/32 - 1)    -- For 60 ms corner turn, this is 0:(288/32-1) = 0:8
--       For Coarse = 0:(i_coarse-1)                               -- For 512 stations, 2 coarse, this is 0:1
--          For FineGroup = 0:4                                    -- Process 4 groups of fine channels (0:47, 48:95, 96:143, 144:191, 191:215)
--             For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
--                For Station = 0:(i_stations-1)
--                   * Read either 6 HBM words or 3, depending on FineGroup.
--                   For fine_offset = 0:3:45  (or 0:3:21 for the last group of fine channels, where fine_channel_start = 192)
--                      Send a 96 bit word (i.e. 3 fine channels).
--
----------------------------------------------------------------------------------
library IEEE, ct_lib, DSP_top_lib, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use DSP_top_lib.DSP_top_pkg.all;
USE common_lib.common_pkg.ALL;
Library xpm;
use xpm.vcomponents.all;

entity ct_atomic_pst_out is
    generic (
        g_PST_BEAMS : integer := 16;
        g_USE_META : boolean := FALSE  -- Put meta data into the memory in place of the actual data, to make it easier to find bugs in the corner turn. 
    );
    port(
        -- Parameters, in the i_axi_clk domain.
        i_stations : in std_logic_vector(10 downto 0); -- up to 1024 stations
        i_coarse   : in std_logic_vector(9 downto 0);  -- Number of coarse channels.
        i_virtualChannels : in std_logic_vector(10 downto 0); -- total virtual channels (= i_stations * i_coarse)
        i_packetsPerFrame : in std_logic_vector(9 downto 0); -- Packets per frame; e.g. if the first corner turn is 27 LFAA packets, then this will be 288.
        i_HBMBufferEnable : in std_logic_vector(3 downto 0);
        o_HBMBuf0PacketCount : out std_logic_vector(31 downto 0);
        o_HBMBuf1PacketCount : out std_logic_vector(31 downto 0);
        o_HBMBuf2PacketCount : out std_logic_vector(31 downto 0);
        o_HBMBuf3PacketCount : out std_logic_vector(31 downto 0);
        
        i_beams_enabled : in std_logic_vector(7 downto 0);
        
        i_BFJonesSelect : in std_logic;
        i_BFPhaseSelect : in std_logic;
        i_JonesSwitch   : in std_logic_vector(31 downto 0); -- Packet count to switch to the new value of BFJonesSelect
        i_phaseSwitch   : in std_logic_vector(31 downto 0); -- Packet count to switch to the new value of BFPhaseSelect
        
        i_rst : in std_logic;
        -- Data in from the PST filterbanks; bursts of 216 clocks for each channel.
        -- 
        i_sof          : in std_logic; -- pulse high at the start of every frame. (1 frame is typically 60ms of data).
        i_FB_clk       : in std_logic; -- filterbank clock, expected to be 450 MHz
        i_frameCount     : in std_logic_vector(36 downto 0); -- frame count is the same for all simultaneous output streams.
        i_virtualChannel : in t_slv_16_arr(2 downto 0); -- 3 virtual channels, one for each of the PST data streams.
        i_HeaderValid : in std_logic_vector(2 downto 0);
        i_data        : in t_ctc_output_payload_arr(2 downto 0); -- 8 bit data; fields are Hpol.re, .Hpol.im, .Vpol.re, .Vpol.im, for each of i_data(0), i_data(1), i_data(2)
        i_dataValid   : in std_logic;
        
        -- Data out to the beamformer
        i_BF_clk  : in std_logic; -- beamformer clock, expected to be 400 MHz
        o_data    : out std_logic_vector(95 downto 0); -- 3 consecutive fine channels delivered every clock. 6 clock latency relative to the other beamformer outputs, to allow time to get the phase ready in the beamformers.
        o_flagged : out std_logic_vector(2 downto 0);  -- o_flagged is aligned with o_data.
        o_fine    : out std_logic_vector(7 downto 0);  -- fine channel / 3, so the actual fine channel for the first of the 3 fine channels in o_data is (o_fine*3)
        o_coarse  : out std_logic_vector(9 downto 0);  -- Coarse channel, indexed from 0 to however many distinct coarse channels there are.
        o_firstStation : out std_logic;
        o_lastStation  : out std_logic;
        o_timeStep     : out std_logic_vector(4 downto 0);
        o_virtualChannel  : out std_logic_vector(9 downto 0);  -- coarse channel count.
        o_packetCount : out std_logic_vector(36 downto 0); -- The packet count for this packet, based on the original packet count from LFAA.
        o_valid   : out std_logic;
        -- Configuration out to the beamformer. These select which buffer to use for the double buffered jones matrix and phase tracking data.  
        o_jonesBuffer : out std_logic;
        o_phaseBuffer : out std_logic;
        -- AXI interface to the HBM
        -- Corner turn between filterbanks and beamformer
        -- aw bus = write address
        i_axi_clk : in std_logic;
        -- 
        m02_axi_awvalid  : out std_logic;
        m02_axi_awready  : in std_logic;
        m02_axi_awaddr   : out std_logic_vector(29 downto 0);
        m02_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m02_axi_wvalid    : out std_logic;
        m02_axi_wready    : in std_logic;
        m02_axi_wdata     : out std_logic_vector(255 downto 0);
        m02_axi_wlast     : out std_logic;
        -- b bus - write response
        m02_axi_bvalid    : in std_logic;
        m02_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m02_axi_arvalid   : out std_logic;
        m02_axi_arready   : in std_logic;
        m02_axi_araddr    : out std_logic_vector(29 downto 0);
        m02_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m02_axi_rvalid    : in std_logic;
        m02_axi_rready    : out std_logic;
        m02_axi_rdata     : in std_logic_vector(255 downto 0);
        m02_axi_rlast     : in std_logic;
        m02_axi_rresp     : in std_logic_vector(1 downto 0);
        
        ---------------------------------------------------
        -- Monitoring and Error signals on the i_axi_clk domain.
        o_bufferOverflowError : out std_logic; -- buffer between data input from the filterbanks and the HBM has overflowed; this should never happen.
        o_readoutError : out std_logic;  -- readout to the beamformer didn't finish before the next readout started. This should never happen.
        o_readoutClocks : out std_logic_vector(31 downto 0); -- Number of beamformer (400MHz) clocks required to read the most recent frame out from HBM
        o_readInClocks : out std_logic_vector(31 downto 0);   -- Number of axi (300MHz) clocks required to write the most recent frame into HBM
        o_readInAllClocks : out std_logic_vector(31 downto 0); -- Number of axi (300MHz) clocks between the start of one frame and the start of the next.
        o_dataMismatch : out std_logic;
        -- on the 400MHz clock.
        o_dataMismatchBFclk : out std_logic
    );
end ct_atomic_pst_out;

architecture Behavioral of ct_atomic_pst_out is

    signal data0, data1, data1Del1, data2, data2Del1, data2Del2 : std_logic_vector(31 downto 0);
    signal dataValid, dataValidDel1, dataValidDel2 : std_logic := '0';
    signal dataCount, dataCountDel1, dataCountDel2 : std_logic_vector(7 downto 0) := "00000000";  -- Counts through the 216 fine channels in an incoming packet.
    signal bufWe : std_logic_vector(7 downto 0);
    signal bufWrData : t_slv_32_arr(7 downto 0);
    signal bufWrAddr : t_slv_9_arr(7 downto 0); 
    signal curWrPacketBuffer : std_logic_vector(1 downto 0);
    signal frameCount : std_logic_vector(36 downto 0);
    signal virtualChannel : t_slv_16_arr(2 downto 0);
    signal packetInFrame, nextPacketInFrame : std_logic_vector(9 downto 0);
    
    signal cdc_src_send : std_logic := '0';
    signal cdc_src_rcv : std_logic;
    signal cdc_src_in : std_logic_vector(81 downto 0);
    signal cdc_dest_out : std_logic_vector(81 downto 0);
    signal cdc_dest_req : std_logic;
    
    signal Tx216, Tx216_fbclk : unsigned(19 downto 0);
    signal Tx216_fbclk_del1 : std_logic_vector(19 downto 0);
    signal Tx216Del, Tx216plus384Del, Tx216plus768Del : std_logic_vector(18 downto 0);
    type aw_fsm_type is (Fine0, Fine0SecondTransaction, Fine0Wait, Fine12, Fine12SecondTransaction, Fine12Wait, Fine24, Fine24SecondTransaction, Fine24Wait, done, wait_Tx216, start);
    signal aw_fsm : aw_fsm_type := done;
    signal AWchan : std_logic_vector(1 downto 0);
    signal AWvirtualChannel : t_slv_16_arr(2 downto 0);
    signal AWpacketInFrame : std_logic_vector(9 downto 0);
    signal bufRdAddr : std_logic_vector(8 downto 0);
    type copy_fsm_type is (idle, copyToFIFO, copyToFIFOWait, copyDone);
    signal copy_fsm : copy_fsm_type := idle;
    signal bufRdPointer : std_logic_vector(4 downto 0);
    signal copyBuffer : std_logic_vector(1 downto 0);
    signal stream : std_logic_vector(1 downto 0);
    signal wdataFIFO_wrDataCount : std_logic_vector(5 downto 0);
    signal bufRdData : std_logic_vector(255 downto 0);
    signal firstInFrame : std_logic := '0';
    
    signal wdataFIFO_wrEnAdv3, wdataFIFO_wrEnAdv2, wdataFIFO_wrEnAdv1, wdataFIFO_wrEn : std_logic;
    signal wdataFIFO_empty : std_logic;
    signal wdataFIFO_rdEn : std_logic;
    
    signal axi2fb_dest_req : std_logic;
    signal FB_packetsPerFrame : std_logic_vector(9 downto 0);
    signal axi2fb_src_rcv : std_logic;
    signal axi2fb_dest_out : std_logic_vector(20 downto 0);
    signal axi2FB_src_send : std_logic := '0';
    signal wdataFIFO_dout : std_logic_vector(256 downto 0); -- 255 data bits, plus the axi_wlast bit.
    signal packetBuf0Used, packetBuf1Used, packetBuf2Used, packetBuf3Used : std_logic := '0';
    signal endOfPacket : std_logic;
    signal wdataFIFO_din : std_logic_vector(256 downto 0);
    signal bufRdwlastDel2, bufRdwlastDel1, bufRdwlast : std_logic;
    signal AWFIFO_dout, awFIFO_din : std_logic_vector(37 downto 0);
    signal awFIFO_empty : std_logic;
    signal awFIFO_rdEn : std_logic;
    signal awFIFO_wrEn : std_logic;
    signal awFIFO_wrDataCount : std_logic_vector(5 downto 0);
    signal fifoIn_m02_axi_awaddr : std_logic_vector(29 downto 0);
    signal fifoIn_m02_axi_awlen : std_logic_vector(7 downto 0);
    signal HBMBuf0PacketCount, HBMBuf1PacketCount : std_logic_vector(31 downto 0);
    -- HBMBuf2PacketCount, HBMBuf3PacketCount : std_logic_vector(31 downto 0);
    signal AWFrameCount : std_logic_vector(36 downto 0);
    signal AWFirstInFrame : std_logic := '0';
    signal AWLastInFrame : std_logic := '0';
    signal maxVirtualChannel : std_logic_vector(10 downto 0);
    signal HBMRdBuffer : std_logic := '0';
    signal HBMBufferEnable : std_logic_vector(3 downto 0);
    signal rstDel1 : std_logic := '0';
    signal HBMWrBuffer : std_logic := '0';
    signal rdFrameCount, frameCount32 : std_logic_vector(31 downto 0);
    signal stationsMinusOne : std_logic_vector(10 downto 0);
    signal coarseMinusOne : std_logic_vector(9 downto 0);
    
    signal rdStation : std_logic_vector(9 downto 0);
    signal rdTime : std_logic_vector(8 downto 0);
    signal rdCoarse : std_logic_vector(8 downto 0);
    signal rdFine : std_logic_vector(4 downto 0);
    signal rdTime_x_27 : std_logic_vector(17 downto 0);
    signal rdFineExt : std_logic_vector(17 downto 0);
    signal finePlusTime : std_logic_vector(17 downto 0);
    signal totalStations : std_logic_vector(10 downto 0);
    signal coarse_x_stations : unsigned(19 downto 0);
    signal outputCoarse_x_stations : unsigned(20 downto 0);
    signal ARvirtualChannel : std_logic_vector(9 downto 0);
    signal ARaddr : std_logic_vector(29 downto 0);
    signal packetsPerFrameMinusOne : std_logic_vector(9 downto 0);
    
    type rd_fsm_type is (start, get_addr, send_addr, wait_ready_extra, update_stationFineCoarse, check_space, wait_ready, done);
    signal rd_fsm : rd_fsm_type := done;
    signal outstandingRequests : std_logic_vector(6 downto 0) := "0000000";
    signal outstandingRequests_x4 : std_logic_Vector(9 downto 0);
    signal outstandingRequests_x2 : std_logic_vector(9 downto 0);
    signal outstandingRequests_x6 : std_logic_vector(9 downto 0);
    signal fifoSpaceUsed : std_logic_vector(9 downto 0);
    signal rdataFIFO_wrDataCount : std_logic_vector(9 downto 0);
    signal newRequest, requestDone : std_logic;
    
    signal axi2bf_src_rcv, axi2bf_src_send : std_logic := '0';
    signal axi2BF_dataIn : std_logic_vector(129 downto 0);
    signal axi2bf_dest_out : std_logic_vector(129 downto 0);
    signal BFpacketsPerFrame, BFpacketsPerFrameMinusOne : std_logic_vector(9 downto 0);
    signal BFstations, BFstationsMinusOne : std_logic_vector(10 downto 0);
    signal BFcoarse, BFcoarseMinusOne : std_logic_vector(9 downto 0);
    signal axi2bf_dest_req : std_logic;
    type readout_fsm_type is (wait_start48, start48, wait_start24, start24, wait_fifo, p0, p1, p2, p3, p4, p5, p6, p7, done);
    signal readout_fsm, readout_fsm_del1, readout_fsm_del2, readout_fsm_del3, readout_fsm_del4 : readout_fsm_type := done;
    signal rdataFIFO_rdEn : std_logic;
    signal rdataFIFO_dout : std_logic_vector(255 downto 0);
    signal rdataFIFO_empty : std_logic;
    signal rdataFIFO_rdDataCount : std_logic_vector(9 downto 0);
    signal outputBuffer : std_logic_vector(319 downto 0);
    
    signal outputFine, outputFineDel1, outputFineDel2, outputFineDel3, outputFineDel4 : std_logic_vector(7 downto 0) := "00000000";
    signal outputStation, outputStationDel1, outputStationDel2, outputStationDel3, outputStationDel4 : std_logic_vector(9 downto 0) := "0000000000";
    signal outputCoarse, outputCoarseDel1, outputCoarseDel2, outputCoarseDel3, outputCoarseDel4 : std_logic_vector(9 downto 0) := "0000000000";
    
    signal BFFrameCountx32 : std_logic_vector(36 downto 0);
    signal BFFrameCount : std_logic_vector(31 downto 0);
    
    signal packetCountDel2_37bit : std_logic_Vector(36 downto 0);
    signal packetCOuntDel2_37bit_x2 : std_logic_vector(36 downto 0);
    signal packetCountDel3, packetCountDel4 : std_logic_vector(36 downto 0);
    signal outputTime : std_logic_vector(9 downto 0);
    signal packetCountDel1, packetCountDel2 : std_logic_vector(9 downto 0);
    signal lastStation, lastCoarse, lastTimeIn32, lastTime, lastFine : std_logic := '0';
    signal BFlastStation, BFlastTimeIn32, BFlastCoarse, BFlastTime : std_logic := '0';
    signal rstDel2, rstDel3 : std_logic;
    signal FBbufferOverflowError : std_logic;
    signal rdataFIFO_rst, awFIFO_rst, wdataFIFO_rst : std_logic := '0';
    signal AXIbufferOverflowError : std_logic;
    signal axi2fbSendCount : std_logic_vector(5 downto 0) := "000000";
    signal axi2FB_src_in : std_logic_vector(20 downto 0);
    signal FB_virtualChannels : std_logic_vector(10 downto 0) := "00000000000";
    
    signal BFlastStationDel2, BFlastStationDel3, BFlastStationDel4 : std_logic;
    signal BFfirstStationDel1, BFfirstStationDel2, BFfirstStationDel3, BFfirstStationDel4 : std_logic;
    signal outputTimeDel1, outputTimeDel2, outputTimeDel3, outputTimeDel4 : std_logic_vector(9 downto 0);
    signal outputVirtualChannelDel2, outputVirtualChannelDel3, outputVirtualChannelDel4 : std_logic_vector(9 downto 0);
    
    signal AWChannelValid : std_logic_vector(2 downto 0) := "000";
    signal copyValid, PacketBuf0Valid, PacketBuf1Valid, PacketBuf2Valid, PacketBuf3Valid, dataChannelValid : std_logic_vector(2 downto 0) := "000";
    
    signal BFJonesSelect : std_logic;
    signal BFPhaseSelect : std_logic;
    signal BFJonesSwitch : std_logic_vector(31 downto 0);
    signal BFPhaseSwitch : std_logic_vector(31 downto 0);
    
    signal curJonesBuffer, curPhaseBuffer,  axi2bf_dest_reqDel1 : std_logic;
    
    signal outputBufferDel1, outputBufferDel2, outputBufferDel3, outputBufferDel4, outputBufferDel5, outputBufferDel6 : std_logic_vector(95 downto 0);
    signal Tx216_fbclk_mod1024 : std_logic_vector(9 downto 0);
    signal PacketBuf0Tx216_mod1024, PacketBuf1Tx216_mod1024, PacketBuf2Tx216_mod1024, PacketBuf3Tx216_mod1024 : std_logic_vector(9 downto 0);
    
    signal copyTx216mod1024 : std_logic_vector(9 downto 0);
    signal bufRdLast : std_logic;
    signal DidSecondTransaction, Non4Kread : std_logic;  -- just used to identify occurences of this in the simulation.
    signal araddr4K : std_logic_vector(6 downto 0);
    signal m02_arlen_read6 : std_logic;
    signal m02_axi_araddrInt : std_logic_vector(29 downto 0);
    signal m02_axi_arlenInt  : std_logic_vector(7 downto 0);
    signal gapCount : std_logic_vector(19 downto 0);
    --signal minGap : integer;
    signal gapCount_eq0 : std_logic := '0';
    signal minGapDataIn, BFstations_x512, BFstations_x32, BFstations_x16 : std_logic_vector(19 downto 0);
    signal readoutClocks, clocksPerCornerTurnOut : std_logic_vector(31 downto 0);
        
    signal BF2axi_dataIn : std_logic_vector(31 downto 0);
    signal BF2axi_transferCount : std_logic_vector(3 downto 0) := "0000";
    signal BF2axi_src_send : std_logic := '0';
    signal bf2axi_src_rcv : std_logic;
    signal BF2axi_dest_out : std_logic_vector(31 downto 0);
    signal BF2axi_dest_req : std_logic;
    signal readInCount, readInAllClocks : std_logic_vector(31 downto 0);
    signal cdc_dest_req_del1 : std_logic;
    signal fineCount : std_logic_vector(7 downto 0);
    signal outputFineDel4x2 : std_logic_vector(7 downto 0);
    signal metaRecon : std_logic_vector(31 downto 0);
    signal dataMisMatchInt : std_logic := '0';
    
    constant c_minGapData  : std_logic_vector(11 downto 0)  := x"7D0";  -- 2000
    
    signal beams_enabled    : std_logic_vector(7 downto 0);
    signal minGap_a         : std_logic_vector(19 downto 0);
    signal minGap_b         : std_logic_vector(19 downto 0);
    
begin
    
    rdataFIFO_rst <= rstDel2; -- i_axi_clk domain
    awFIFO_rst <= rstDel3;  -- i_axi_clk domain
    
    -- Copy reset across to the wdata FIFO
    xpm_cdc_pulse_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    )
    port map (
        dest_pulse => wdataFIFO_rst, -- 1-bit output: Outputs a pulse the size of one dest_clk period when a pulse transfer is correctly initiated on src_pulse input.
        dest_clk => i_FB_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',     -- 1-bit input: optional; required when RST_USED = 1
        src_clk => i_axi_clk,       -- 1-bit input: Source clock.
        src_pulse => rstDel3,   -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'        -- 1-bit input: optional; required when RST_USED = 1
    );
    
    -- Copy buffer overflow error signal to the i_axi_clk domain
    xpm_cdc_BFoverflow_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 1,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    )
    port map (
        dest_pulse => AXIbufferOverflowError, -- 1-bit output: Outputs a pulse the size of one dest_clk period when a pulse transfer is correctly initiated on src_pulse input.
        dest_clk => i_axi_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',     -- 1-bit input: optional; required when RST_USED = 1
        src_clk => i_FB_clk,       -- 1-bit input: Source clock.
        src_pulse => FBbufferOverflowError,   -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'        -- 1-bit input: optional; required when RST_USED = 1
    );    
    
    
    -- Get packetsPerFrame and other info into the i_FB_clk domain
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
            axi2fbSendCount <= std_logic_vector(unsigned(axi2fbSendCount) + 1);
            if axi2fbSendCount = "111111" then -- periodically send it across the clock domain.
                axi2FB_src_send <= '1';
            elsif axi2fb_src_rcv = '1' then
                axi2FB_src_send <= '0';
            end if;
            axi2FB_src_in(9 downto 0) <= i_packetsPerFrame;
            axi2FB_src_in(20 downto 10) <= i_virtualChannels;
            
            
            -- Hold bufferoverflow error
            if rstDel3 = '1' then
                o_bufferOverflowError <= '0';
            elsif AXIbufferOverflowError = '1' then
                o_bufferOverflowError <= '1';
            end if;
        end if;
    end process;
    
    xpm_cdc_handshake_ppf_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        WIDTH => 21          -- DECIMAL; range: 1-1024
    )
    port map (
        dest_out => axi2fb_dest_out, -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => axi2fb_dest_req, -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => axi2fb_src_rcv,   -- 1-bit output: Acknowledgement from destination logic that src_in has been
                                     -- received. This signal will be deasserted once destination handshake has fully completed,
        dest_ack => '1',             -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_FB_clk,        -- 1-bit input: Destination clock.
        src_clk  => i_axi_clk,       -- 1-bit input: Source clock.
        src_in   => axi2FB_src_in,   -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => axi2FB_src_send  -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );
    
    
    process(i_FB_clk)
    begin
        if rising_edge(i_FB_clk) then
            
            if axi2fb_dest_req = '1' then
                FB_packetsPerFrame <= axi2fb_dest_out(9 downto 0);
                FB_virtualChannels <= axi2fb_dest_out(20 downto 10);
            end if;
            
            -- get input data into 32 bit registers.
            -- Delay data0, data1 and data2 by different amounts so that the part of the buffer they are being written to doesn't clash
            if (g_USE_META) then
                -- each 32 bits has:
                --   (7:0) = fine channel, runs 0 to 215.
                --   (19:8) = time sample, runs from 0 to 287 (for a 27 LFAA block corner turn).
                --   (31:20) = virtual channel.
                
                if unsigned(packetInFrame) >= (unsigned(FB_packetsPerFrame) - 1) then  -- e.g. 288 time samples per frame, then FB_packetsPerFrame = 288, and packetInFrame counts 0 to 287.
                    nextPacketInFrame <= (others => '0');
                else
                    nextPacketInFrame <= std_logic_vector(unsigned(packetInFrame) + 1);
                end if;
                
                if i_dataValid = '1' and dataValid = '0' then
                    fineCount <= x"01";
                    data0(7 downto 0) <= x"00";
                    data1(7 downto 0) <= x"00";
                    data2(7 downto 0) <= x"00";
                    data0(19 downto 8) <= "00" & nextPacketInFrame;
                    data1(19 downto 8) <= "00" & nextPacketInFrame;
                    data2(19 downto 8) <= "00" & nextPacketInFrame;
                elsif i_dataValid = '1' then
                    fineCount <= std_logic_vector(unsigned(fineCount) + 1);
                    data0(7 downto 0) <= fineCount;
                    data1(7 downto 0) <= fineCount;
                    data2(7 downto 0) <= fineCount;
                    data0(19 downto 8) <= "00" & packetInFrame;
                    data1(19 downto 8) <= "00" & packetInFrame;
                    data2(19 downto 8) <= "00" & packetInFrame;
                end if;
                data0(31 downto 20) <= i_virtualChannel(0)(11 downto 0);
                data1(31 downto 20) <= i_virtualChannel(1)(11 downto 0);
                data2(31 downto 20) <= i_virtualChannel(2)(11 downto 0);
            else
                data0(7 downto 0) <= i_data(0).Hpol.re;
                data0(15 downto 8) <= i_data(0).Hpol.im;
                data0(23 downto 16) <= i_data(0).Vpol.re;
                data0(31 downto 24) <= i_data(0).Vpol.im;
                
                data1(7 downto 0) <= i_data(1).Hpol.re;
                data1(15 downto 8) <= i_data(1).Hpol.im;
                data1(23 downto 16) <= i_data(1).Vpol.re;
                data1(31 downto 24) <= i_data(1).Vpol.im;
                
                data2(7 downto 0) <= i_data(2).Hpol.re;
                data2(15 downto 8) <= i_data(2).Hpol.im;
                data2(23 downto 16) <= i_data(2).Vpol.re;
                data2(31 downto 24) <= i_data(2).Vpol.im;
            end if;
            
            data1Del1 <= data1;
            
            data2Del1 <= data2;
            data2Del2 <= data2Del1;
            
            dataValid <= i_dataValid;
            dataValidDel1 <= dataValid;
            dataValidDel2 <= dataValidDel1;
            
            
            if i_headerValid(0) = '1' or i_headerValid(1) = '1' or i_headerValid(2) = '1' then
                frameCount <= i_frameCount; -- in std_logic_vector(36 downto 0); -- frame count is the same for all simultaneous output streams.
                virtualChannel <= i_virtualChannel;
            end if;
            
            if i_sof = '1' then  -- start of a (typically) 60ms frame
                -- 4 packet buffers for each stream within the main buffer. Each packet buffer holds one packet (=216 fine channels for one virtual channel = 27 x 256 bit words in the buffer)
                curWrPacketBuffer <= "11";  -- so it will tick over to "00" for the first frame
                packetInFrame <= (others => '1');  -- count of the packet within the frame. For 60ms frames, this will count from 0 to 287 for every group of 3 packets in the frame.
            elsif i_dataValid = '1' and dataValid = '0' then
                dataCount <= "00000000";
                curWrPacketBuffer <= std_logic_vector(unsigned(curWrPacketBuffer) + 1);
                if unsigned(packetInFrame) >= (unsigned(FB_packetsPerFrame) - 1) then  -- e.g. 288 time samples per frame, then FB_packetsPerFrame = 288, and packetInFrame counts 0 to 287.
                    packetInFrame <= (others => '0');
                else
                    packetInFrame <= std_logic_vector(unsigned(packetInFrame) + 1);
                end if;
            elsif dataValid = '1' then
                dataCount <= std_logic_vector(unsigned(dataCount) + 1);  -- The value in dataCount aligns with the sample in data0, data1, data2.
            end if;
            dataCountDel1 <= dataCount;
            dataCountDel2 <= dataCountDel1;
            
            bufWrAddr(0)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(1)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(2)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(3)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(4)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(5)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(6)(6 downto 5) <= curWrPacketBuffer;
            bufWrAddr(7)(6 downto 5) <= curWrPacketBuffer;
            
            -- Write data to the buffer.
            -- There are 3 different streams coming in, each of which goes to a different part of the buffer, selected by the high order buffer address (bits 8:7).
            -- The full buffer is 256 bits wide, but data coming in is only 32 bits wide. So each 32 bit piece has its own write enable.
            -- The three input buses are synchronised, so the three buses need to write to the same 32-bit piece in any given clock cycle.
            -- To avoid this, the second stream is delayed on a clock, and the third stream is delayed by 2 clocks. Thus they all write
            -- to different 32-bit pieces.
            for i in 0 to 7 loop
                if (unsigned(dataCount(2 downto 0)) = i) and dataValid = '1' then
                    -- Write data from first input data stream (i_data(0))
                    bufWe(i) <= '1';
                    bufWrData(i) <= data0;
                    bufWrAddr(i)(4 downto 0) <= dataCount(7 downto 3);
                    bufWrAddr(i)(8 downto 7) <= "00";  -- top two bits selects the buffer. "00" selects buffer for i_data(0)
                elsif (unsigned(dataCountDel1(2 downto 0)) = i) and dataValidDel1 = '1' then
                    bufWe(i) <= '1';
                    bufWrData(i) <= data1Del1;
                    bufWrAddr(i)(4 downto 0) <= dataCountDel1(7 downto 3);
                    bufWrAddr(i)(8 downto 7) <= "01"; -- "01" = buffer used for i_data(1)
                elsif (unsigned(dataCountDel2(2 downto 0)) = i) and dataValidDel2 = '1' then
                    bufWe(i) <= '1';
                    bufWrData(i) <= data2Del2;
                    bufWrAddr(i)(4 downto 0) <= dataCountDel2(7 downto 3);
                    bufWrAddr(i)(8 downto 7) <= "10"; -- "10" = buffer used for i_data(2)   
                else
                    bufWe(i) <= '0';  -- No write to this 32 bit segment at the moment.
                    bufWrData(i) <= (others => '0');
                    bufWrAddr(i)(4 downto 0) <= "00000";
                    bufWrAddr(i)(8 downto 7) <= "00";
                end if;
            end loop;
            
            -- At the end of every packet, send a message to the axi clock domain with the virtual channels and timestamps.
            -- This is used to generate the addresses on the aw bus for the 3 packets just received.
            if i_sof = '1' then
                firstInFrame <= '1';
            elsif i_dataValid = '0' and dataValid = '1' then
                firstInFrame <= '0';
            end if;
            
            maxVirtualChannel <= std_logic_vector(unsigned(FB_virtualChannels) - 1);
            
            if i_dataValid = '0' and dataValid = '1' then
                cdc_src_send <= '1';
                cdc_src_in(9 downto 0) <= virtualChannel(0)(9 downto 0);
                cdc_src_in(19 downto 10) <= virtualChannel(1)(9 downto 0);
                cdc_src_in(29 downto 20) <= virtualChannel(2)(9 downto 0);
                cdc_src_in(66 downto 30) <= frameCount;
                cdc_src_in(76 downto 67) <= packetInFrame;
                cdc_src_in(77) <= firstInFrame;
                -- set last in frame, if this is the final packet.
                if ((unsigned(packetInFrame) = (unsigned(FB_packetsPerFrame) - 1)) and 
                    ((virtualChannel(0)(9 downto 0) = maxVirtualChannel(9 downto 0)) or
                     (virtualChannel(1)(9 downto 0) = maxVirtualChannel(9 downto 0)) or
                     (virtualChannel(2)(9 downto 0) = maxVirtualChannel(9 downto 0)))) then
                    cdc_src_in(78) <= '1';  -- last packet in the frame. 
                else
                    cdc_src_in(78) <= '0';
                end if;
                if (unsigned(virtualChannel(0)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    cdc_src_in(79) <= '1';
                else
                    cdc_src_in(79) <= '0';
                end if;
                if (unsigned(virtualChannel(1)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    cdc_src_in(80) <= '1';
                else
                    cdc_src_in(80) <= '0';
                end if;
                if (unsigned(virtualChannel(2)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    cdc_src_in(81) <= '1';
                else
                    cdc_src_in(81) <= '0';
                end if;
                
                
            elsif cdc_src_rcv = '1' then
                cdc_src_send <= '0';
            end if;

            
            if i_dataValid = '0' and dataValid = '1' then
                endOfPacket <= '1';
                if (unsigned(virtualChannel(0)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    dataChannelValid(0) <= '1';
                else
                    dataChannelValid(0) <= '0';
                end if;
                if (unsigned(virtualChannel(1)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    dataChannelValid(1) <= '1';
                else
                    dataChannelValid(1) <= '0';
                end if;
                if (unsigned(virtualChannel(2)(9 downto 0)) <= unsigned(maxVirtualChannel(9 downto 0))) then
                    dataChannelValid(2) <= '1';
                else
                    dataChannelValid(2) <= '0';
                end if;
            else
                endOfPacket <= '0';
            end if;

            Tx216_fbclk <= unsigned(packetInFrame) * 432; -- 10 bit, becomes 20 bits. Address is 4x this, for since we have 4 bytes per frequency channel (2 pol, 2 (re+im)). x432 not x216 since we interleave between different 256Mbyte blocks for increased HBM bandwidth, and allow space for possible future expansion to 16 bit values.
            Tx216_fbclk_del1 <= std_logic_vector(Tx216_fbclk);  -- packetInFrame is captured at the start of the packet, but tx216 is used at the end of the packet, so plenty of time for pipeline stages
            Tx216_fbclk_mod1024 <= Tx216_fbclk_del1(9 downto 0);

            if endOfPacket = '1' and curWrPacketBuffer = "00" then
                PacketBuf0Used <= '1';
                PacketBuf0Valid <= dataChannelValid;
                -- This is needed to calculate when we cross a 4096 byte boundary, which is used to set the axi_wlast bit when delivering the data, 
                -- since axi doesn't allow transactions to cross a 4096 byte boundary.
                PacketBuf0Tx216_mod1024 <= Tx216_fbclk_mod1024;   -- base address in the HBM is 4x this, so using mod 1024 means we see the offset from a 4096 byte boundary.
            elsif copy_fsm = copyDone and copyBuffer = "00" then
                PacketBuf0Used <= '0';
            end if;

            if endOfPacket = '1' and curWrPacketBuffer = "01" then
                PacketBuf1Used <= '1';
                PacketBuf1Valid <= dataChannelValid;
                PacketBuf1Tx216_mod1024 <= Tx216_fbclk_mod1024;
            elsif copy_fsm = copyDone and copyBuffer = "01" then
                PacketBuf1Used <= '0';
            end if; 
             
            if endOfPacket = '1' and curWrPacketBuffer = "10" then
                PacketBuf2Used <= '1';
                PacketBuf2Valid <= dataChannelValid;
                PacketBuf2Tx216_mod1024 <= Tx216_fbclk_mod1024;
            elsif copy_fsm = copyDone and copyBuffer = "10" then
                PacketBuf2Used <= '0';
            end if;    
            
            if endOfPacket = '1' and curWrPacketBuffer = "11" then
                PacketBuf3Used <= '1';
                PacketBuf3Valid <= dataChannelValid;
                PacketBuf3Tx216_mod1024 <= Tx216_fbclk_mod1024;
            elsif copy_fsm = copyDone and copyBuffer = "11" then
                PacketBuf3Used <= '0';
            end if;            
            
            if (endOfPacket = '1' and 
                ((curWrPacketBuffer = "00" and packetBuf0Used = '1') or
                 (curWrPacketBuffer = "01" and packetBuf1Used = '1') or
                 (curWrPacketBuffer = "10" and packetBuf2Used = '1') or
                 (curWrPacketBuffer = "11" and packetBuf3Used = '1'))) then
                FBbufferOverflowError <= '1'; -- we just overwrote one of the packet buffers.
            else
                FBbufferOverflowError <= '0';
            end if;
            
            -- Copy data to the FIFO from the correct buffer.
            -- Note the write requests on the aw bus send the complete packet for the first stream, then the complete packet for the second stream, then the complete packet for the third stream.
            case copy_fsm is
                when idle =>
                    -- wait until a buffer is used, then start copying data
                    if (PacketBuf0Used = '1') then -- buf0, buf1, buf2, buf3 refer to the 4 buffers within each stream (i.e. a block of 27 words in the buffer memory).
                        copyBuffer <= "00";
                        copyValid <= PacketBuf0Valid;
                        copyTx216mod1024 <= PacketBuf0Tx216_mod1024;
                    elsif packetBuf1Used = '1' then
                        copyBuffer <= "01";
                        copyValid <= PacketBuf1Valid;
                        copyTx216mod1024 <= PacketBuf1Tx216_mod1024;
                    elsif packetBuf2Used = '1' then
                        copyBuffer <= "10";
                        copyValid <= PacketBuf2Valid;
                        copyTx216mod1024 <= PacketBuf2Tx216_mod1024;
                    else
                        copyBuffer <= "11";
                        copyValid <= PacketBuf3Valid;
                        copyTx216mod1024 <= PacketBuf3Tx216_mod1024;
                    end if;
                    if PacketBuf0Used = '1' or PacketBuf1Used = '1' or PacketBuf2Used = '1' or PacketBuf3Used = '1' then
                        copy_fsm <= copyToFIFO;
                    end if;
                    stream <= "00";   -- which of the 3 input streams we are currently working on.
                    bufRdPointer <= "00000";
                    
                when copyToFIFO =>
                    if (unsigned(bufRdPointer) = 26) then
                        bufRdPointer <= "00000";
                        copyTx216mod1024 <= std_logic_vector(unsigned(copyTx216mod1024) - 208);
                        if stream = "10" then
                            copy_fsm <= copyDone;
                            stream <= "00";
                        else
                            stream <= std_logic_vector(unsigned(stream) + 1);
                            copyValid <= '0' & copyValid(2 downto 1);
                        end if;
                    else
                        bufRdPointer <= std_logic_vector(unsigned(bufRdPointer) + 1); -- Go through the 216 fine channels (=27 words in the buffer)
                        copyTx216mod1024 <= std_logic_vector(unsigned(copyTx216mod1024) + 8);  -- 32 bytes per transfer = 8x4 bytes.
                        if (unsigned(wdataFIFO_wrDataCount) > 16) then
                            copy_fsm <= copyToFIFOWait;
                        end if;
                    end if;
                    
                when copyToFIFOWait =>
                    if (unsigned(wdataFIFO_wrDataCount) < 16) then
                        copy_fsm <= copyToFIFO;
                    end if;
                    
                when copyDone =>
                    if ((copyBuffer = "00" and PacketBuf1Used = '1') or 
                        (copyBuffer = "01" and packetBuf2Used = '1') or 
                        (copyBuffer = "10" and packetBuf3Used = '1') or 
                        (copyBuffer = "11" and packetBuf0Used = '1')) then
                        copy_fsm <= copyToFIFO;
                        copyBuffer <= std_logic_vector(unsigned(copyBuffer) + 1);
                        if copyBuffer = "00" then  -- next buffer will be "01", choose valid for that buffer.
                            copyValid <= PacketBuf1Valid;
                            copyTx216mod1024 <= PacketBuf1Tx216_mod1024;
                        elsif copyBuffer = "01" then
                            copyValid <= PacketBuf2Valid;
                            copyTx216mod1024 <= PacketBuf2Tx216_mod1024;
                        elsif copyBuffer = "10" then
                            copyValid <= PacketBuf3Valid;
                            copyTx216mod1024 <= PacketBuf3Tx216_mod1024;
                        else
                            copyValid <= PacketBuf0Valid;
                            copyTx216mod1024 <= PacketBuf0Tx216_mod1024;
                        end if;
                    end if;
                    
                when others =>
                    copy_fsm <= idle;
            end case;
            
            bufRdAddr(8 downto 7) <= stream;
            bufRdAddr(6 downto 5) <= copyBuffer;
            bufRdAddr(4 downto 0) <= bufRdPointer;
            if (copy_fsm = copyToFIFO) then
                wdataFIFO_wrEnAdv3 <= copyValid(0);    -- corresponds to bufRdAddr
            else
                wdataFIFO_wrEnAdv3 <= '0';
            end if;
            if copyTx216mod1024 = "1111111000" then
                bufRdLast <= '1';  -- This will be the last in an AXI burst because it is being written to the last word before a 4096 byte boundary.
            else
                bufRdLast <= '0';
            end if; 
            
            
            if (bufRdAddr(4 downto 0) = "01011" or bufRdAddr(4 downto 0) = "10111" or bufRdAddr(4 downto 0) = "11010" or bufRdLast = '1') then
                bufRdwlast <= '1'; -- Writes to the HBM have bursts of 12, 12 and 3 to pass the full 27 words
            else
                bufRdwlast <= '0';
            end if;
            bufRdwlastDel1 <= bufRdwlast;
            bufRdwlastDel2 <= bufRdwlastDel1;  -- bufRdwlast is found based on bufRdAddr, so bufRdwlastDel2 has a 3 cycle latency compared with bufRdAddr, and thus aligns with the data output from the buffer.
            
            wdataFIFO_wrEnAdv2 <= wdataFIFO_wrEnAdv3;
            wdataFIFO_wrEnAdv1 <= wdataFIFO_wrEnAdv2;
            wdataFIFO_wrEn <= wdataFIFO_wrEnAdv1;   -- three cycle latency on the buffer memory.
            
        end if;
    end process;
    
    -- BRAM buffer to store data as it comes in;
    -- Overall size is 512x256 bits
    --  Split into 4 buffers of 128 x 256 bits, with 3 of the buffers used (one per input stream)
    --  Each 128 is further split into 4 smaller buffers of 32 x 256 bits, each sufficient for one filterbank frame (of 216 samples)
    -- Total width is 256 bits, which is split into 8x32 bits, with separate write ports for each 32 bit piece, so we can deliver the data directly to 
    -- the correct place from all 3 input streams simultaneously.
    bufi : entity ct_lib.buffer512x256_wrapper
    Port map(
        i_clk    => i_FB_clk,  --  in std_logic;
        i_we     => bufWe,     -- in std_logic_vector(7 downto 0);  -- Separate write enable for each 32 bit piece.
        i_wrAddr => bufWrAddr, -- in t_slv_9_arr(7 downto 0);       -- Address for each piece of the memory
        i_wrData => bufWrData, -- in t_slv_32_arr(7 downto 0);      -- write data for each 32 bit wide piece of the memory
        i_rdAddr => bufRdAddr, -- in std_logic_vector(8 downto 0);  -- Read address
        o_rdData => bufRdData  -- out std_logic_vector(255 downto 0)  -- read data, 3 cycle latency from the read address.
    );
    
    wdataFIFO_din(255 downto 0) <= bufRdData;
    wdataFIFO_din(256) <= bufRdwlastDel2;
    
    -- Small FWFT FIFO to cross the clock domain to the AXI clock, and to interface to the ready/valid on the m02_axi_wdata bus
    xpm_fifo_async_inst : xpm_fifo_async
    generic map (
        CDC_SYNC_STAGES => 2,       -- DECIMAL
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "distributed", -- String
        FIFO_READ_LATENCY => 0,     -- DECIMAL
        FIFO_WRITE_DEPTH => 32,     -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 6,   -- DECIMAL
        READ_DATA_WIDTH => 257,     -- DECIMAL  -- 256 data bits, plus one extra bit for last in a transaction. Burst are 12, 12, and 3 words, for the 27 words in a single packet.
        READ_MODE => "fwft",        -- String
        RELATED_CLOCKS => 0,        -- DECIMAL
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "0404", -- String
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 257,    -- DECIMAL
        WR_DATA_COUNT_WIDTH => 6    -- DECIMAL
    )
    port map (
        almost_empty => open,   -- 1-bit output: Almost Empty : When asserted, this signal indicates that only one more read can be performed before the FIFO goes to empty.
        almost_full => open,    -- 1-bit output: Almost Full: When asserted, this signal indicates that only one more write can be performed before the FIFO is full.
        data_valid => open,     -- 1-bit output: Read Data Valid: When asserted, this signal indicates that valid data is available on the output bus (dout).
        dbiterr => open,        -- 1-bit output: Double Bit Error: Indicates that the ECC decoder detected a double-bit error and data in the FIFO core is corrupted.
        dout => wdataFIFO_dout,   -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven when reading the FIFO.
        empty => wdataFIFO_empty, -- 1-bit output: Empty Flag: When asserted, this signal indicates that the FIFO is empty. Read requests are ignored when the FIFO is empty, initiating a read while empty is not destructive to the FIFO.
        full => open,             -- 1-bit output: Full Flag: When asserted, this signal indicates that the FIFO is full. 
        overflow => open,         -- 1-bit output: Overflow: 
        prog_empty => open,       -- 1-bit output: Programmable Empty.
        prog_full => open,        -- 1-bit output: Programmable Full: 
        rd_data_count => open,    -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates the number of words read from the FIFO.
        rd_rst_busy => open,      -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO read domain is currently in a reset state.
        sbiterr => open,          -- 1-bit output: Single Bit Error: Indicates that the ECC decoder detected and fixed a single-bit error.
        underflow => open,        -- 1-bit output: Underflow: Indicates that the read request (rd_en) during the previous clock cycle was rejected because the FIFO is empty.
        wr_ack => open,           -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => wdataFIFO_wrDataCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,      -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO write domain is currently in a reset state.
        din => wdataFIFO_din,     -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',     -- 1-bit input: Double Bit Error Injection: Injects a double bit error the ECC feature is used on block RAMs or UltraRAM macros.
        injectsbiterr => '0',     -- 1-bit input: Single Bit Error Injection: Injects a single bit error if the ECC feature is used on block RAMs or UltraRAM macros.
        rd_clk => i_axi_clk,      -- 1-bit input: Read clock: Used for read operation. rd_clk must be a free running clock.
        rd_en => wdataFIFO_rdEn,  -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO.
        rst => wdataFIFO_rst,     -- 1-bit input: Reset: Must be synchronous to wr_clk. 
        sleep => '0',             -- 1-bit input: Dynamic power saving: If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_FB_clk,       -- 1-bit input: Write clock: Used for write operation. wr_clk must be a free running clock.
        wr_en => wdataFIFO_wrEn   -- 1-bit input: Write Enable: If the FIFO is not full, asserting this signal causes data (on din) to be written to the FIFO. 
    );
    
    m02_axi_wvalid <= not wdataFIFO_empty;
    
    wdataFIFO_rdEn <= (not wdataFIFO_empty) and m02_axi_wready;
    m02_axi_wdata <= wdataFIFO_dout(255 downto 0);
    m02_axi_wlast <= wdataFIFO_dout(256);
    
    -- Notify the axi bus clock domain that a new packet is available.
    -- We need the virtual channels, and the packet count for the packet.
    xpm_cdc_handshake_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 4,    -- DECIMAL; range: 2-10
        WIDTH => 82          -- DECIMAL; range: 1-1024
    )
    port map (
        dest_out => cdc_dest_out, -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => cdc_dest_req, -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => cdc_src_rcv,   -- 1-bit output: Acknowledgement from destination logic that src_in has been received.
        dest_ack => '1',          -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_axi_clk,    -- 1-bit input: Destination clock.
        src_clk => i_FB_clk,      -- 1-bit input: Source clock.
        src_in => cdc_src_in,     -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => cdc_src_send  -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );
    
    o_readInAllClocks <= readInAllClocks;
    
    process(i_axi_clk)
        variable fifoIn_m02_axi_awlen_temp : unsigned(6 downto 0);
    begin
        if rising_edge(i_axi_clk) then
            HBMBufferEnable <= i_HBMBufferEnable;
            rstDel1 <= i_rst;
            rstDel2 <= rstDel1;
            rstDel3 <= rstDel2;
            
            cdc_dest_req_del1 <= cdc_dest_req;
            if cdc_dest_req_del1 = '1' and AWFirstInFrame = '1' then
                readInCount <= (others => '0');
                readInAllClocks <= readInCount;
            else
                readInCount <= std_logic_vector(unsigned(readInCount) + 1);
            end if;
            if cdc_dest_req_del1 = '1' and AWLastInFrame = '1' then
                o_readInClocks <= readInCount;
            end if;
            
            if rstDel1 = '1' then
                HBMWrBuffer <= '0';
--                if HBMBufferEnable(0) = '1' then
--                    HBMWrBuffer <= "00";
--                elsif HBMBufferEnable(1) = '1' then
--                    HBMWrBuffer <= "01";
--                elsif HBMBufferEnable(2) = '1' then
--                    HBMWrBuffer <= "10";
--                else
--                    HBMWrBuffer <= "11";
--                end if;
            elsif cdc_dest_req = '1' then
                AWvirtualChannel(0)(9 downto 0) <= cdc_dest_out(9 downto 0);    -- "V" in the address equation
                AWvirtualChannel(1)(9 downto 0) <= cdc_dest_out(19 downto 10);  
                AWvirtualChannel(2)(9 downto 0) <= cdc_dest_out(29 downto 20); 
                AWframeCount <= cdc_dest_out(66 downto 30);
                AWpacketInFrame <= cdc_dest_out(76 downto 67);   -- "T" in the equation below for the address
                AWFirstInFrame <= cdc_dest_out(77);
                AWLastInFrame <= cdc_dest_out(78);
                AWchannelValid <= cdc_dest_out(81 downto 79);

                if cdc_dest_out(77) = '1' then -- this is the first packet in a new frame (typically 60 ms)
                    frameCount32 <= cdc_dest_out(66 downto 35); -- frame count used in the meta data at the output to the beamformer.
                    -- Update the HBM buffer to use.
                    HBMWrBuffer <= not HBMWrBuffer;
                end if;
                aw_fsm <= start;
            else
                case aw_fsm is
                    when start =>
                        -- Generate write requests; 2 bursts of 12, 1 burst of 3, for each of the three channels being processed.
                        --  (total 27 beats = all 216 fine channels (8 fine channels per beat).
                        -- The write address is 
                        --    Address = 4 * (V*2^16 + T*216 + F)
                        -- where F = Fine Channel  (0 to 215)
                        --       V = virtual channel (0 to up to 1023)
                        --       T = Time within the block (0 to 288, assuming the first stage corner turn is 27 LFAA blocks)
                        -- 
                        aw_fsm <= wait_Tx216;
                        awFIFO_wrEn <= '0';
                        
                        fifoIn_m02_axi_awaddr(29) <= HBMWrBuffer;
                        if (AWFirstInFrame = '1' and HBMWrBuffer = '0') then
                            HBMBuf0PacketCount <= AWFrameCount(36 downto 5);
                        end if;
                        if (AWFirstInFrame = '1' and HBMWrBuffer = '1') then
                            HBMBuf1PacketCount <= AWFrameCount(36 downto 5);
                        end if;
                        
                    when wait_Tx216 =>  -- just wait for the pipeline stage for the multiplication.
                        aw_fsm <= Fine0;
                        AWchan <= "00";
                        awFIFO_wrEn <= '0';
                        
                    when Fine0 => -- 12 beat burst
                        fifoIn_m02_axi_awaddr(18 downto 0) <= Tx216Del;  -- Tx216Del is always a multiple of 32, so we never need to worry about non-aligned reads.
                        if AWchan = "00" then
                            fifoIn_m02_axi_awaddr(27 downto 19) <= AWvirtualChannel(0)(9 downto 1);
                            fifoIn_m02_axi_awaddr(28) <= AWvirtualChannel(0)(0);  -- LSB of the virtual channel is in bit position 28, so stations alternate between different 256 MByte pieces of HBM.
                            awFIFO_wrEn <= AWChannelValid(0);
                        elsif AWchan = "01" then
                            fifoIn_m02_axi_awaddr(27 downto 19) <= AWvirtualChannel(1)(9 downto 1);
                            fifoIn_m02_axi_awaddr(28) <= AWvirtualChannel(1)(0);
                            awFIFO_wrEn <= AWChannelValid(1);
                        else
                            fifoIn_m02_axi_awaddr(27 downto 19) <= AWvirtualChannel(2)(9 downto 1);
                            fifoIn_m02_axi_awaddr(28) <= AWvirtualChannel(2)(0);
                            awFIFO_wrEn <= AWChannelValid(2);
                        end if;
                        
                        if (unsigned(Tx216Del(11 downto 0)) > 3712) then  -- 3712 = 4096 - 384, so if the start address of the burst is beyond this, then we need to break the burst into two parts.
                            fifoIn_m02_axi_awlen_temp := (127 - unsigned(Tx216Del(11 downto 5)));  -- total of 128 x 32 byte words per 4096 bytes.
                            fifoIn_m02_axi_awlen <= '0' & std_logic_vector(fifoIn_m02_axi_awlen_temp);
                            aw_fsm <= Fine0SecondTransaction;
                        else
                            fifoIn_m02_axi_awlen <= "00001011";   -- 12 beats.
                            aw_fsm <= Fine0Wait;    
                        end if;
                        
                    when Fine0SecondTransaction =>
                        fifoIn_m02_axi_awaddr(11 downto 0) <= "000000000000";  -- must be on a 4096 byte boundary
                        fifoIn_m02_axi_awaddr(18 downto 12) <= std_logic_vector(unsigned(fifoIn_m02_axi_awaddr(18 downto 12)) + 1);
                        fifoIn_m02_axi_awlen <= std_logic_vector(10 - unsigned(fifoIn_m02_axi_awlen));  -- e.g previous length was 1, so 2 words sent, words remaining = 12-2 = 10 = awlen of 9 = 10-1.
                        -- awFIFO_wrEn remains as it was in the previous state ("fine0")
                        aw_fsm <= Fine0Wait;
                        
                    when Fine0Wait =>
                        awFIFO_wrEn <= '0';
                        if (unsigned(awFIFO_wrDataCount) < 24) then
                            aw_fsm <= Fine12;
                        end if;
                        
                    when Fine12 => -- 12 beat burst starting at an offset of 12 beats (12 beats = 12*32 = 384 bytes)
                        fifoIn_m02_axi_awaddr(18 downto 0) <= std_logic_vector(unsigned(Tx216Del) + 384);
                        if AWchan = "00" then
                            awFIFO_wrEn <= AWChannelValid(0);
                        elsif AWchan = "01" then
                            awFIFO_wrEn <= AWChannelValid(1);
                        else
                            awFIFO_wrEn <= AWChannelValid(2);
                        end if;
                        
                        if (unsigned(Tx216plus384Del(11 downto 0)) > 3712) then  -- 3712 = 4096 - 384, so if the start address of the burst is beyond this, then we need to break the burst into two parts.
                            fifoIn_m02_axi_awlen_temp := (127 - unsigned(Tx216plus384Del(11 downto 5)));  -- total of 128 x 32 byte words per 4096 bytes.
                            fifoIn_m02_axi_awlen <= '0' & std_logic_vector(fifoIn_m02_axi_awlen_temp);
                            aw_fsm <= Fine12SecondTransaction;
                        else
                            fifoIn_m02_axi_awlen <= "00001011";   -- 12 beats.
                            aw_fsm <= Fine12Wait;
                        end if;      
                    
                    when Fine12SecondTransaction =>
                        fifoIn_m02_axi_awaddr(11 downto 0) <= "000000000000";  -- must be on a 4096 byte boundary
                        fifoIn_m02_axi_awaddr(18 downto 12) <= std_logic_vector(unsigned(fifoIn_m02_axi_awaddr(18 downto 12)) + 1);
                        fifoIn_m02_axi_awlen <= std_logic_vector(10 - unsigned(fifoIn_m02_axi_awlen));
                        -- awFIFO_wrEn remains as it was in the previous state ("fine0")
                        aw_fsm <= Fine12Wait;
                    
                    when Fine12Wait =>
                        awFIFO_wrEn <= '0';
                        if (unsigned(awFIFO_wrDataCount) < 24) then
                            aw_fsm <= Fine24;
                        end if;
                    
                    when Fine24 => -- 3 beat burst starting with an offset of 24 beats (24 beats = 24*32 = 768 bytes)
                        fifoIn_m02_axi_awaddr(18 downto 0) <= std_logic_vector(unsigned(Tx216Del) + 768);
                        
                        if AWchan = "00" then
                            awFIFO_wrEn <= AWChannelValid(0);
                        elsif AWchan = "01" then
                            awFIFO_wrEn <= AWChannelValid(1);
                        else
                            awFIFO_wrEn <= AWChannelValid(2);
                        end if;
                   
                        if (unsigned(Tx216plus768Del(11 downto 0)) > 4000) then  -- 4000 = 4096 - 96, so if the start address of the burst is beyond this, then we need to break the burst into two parts.
                            fifoIn_m02_axi_awlen_temp := (127 - unsigned(Tx216plus768Del(11 downto 5)));  -- total of 128 x 32 byte words per 4096 bytes.
                            fifoIn_m02_axi_awlen <= '0' & std_logic_vector(fifoIn_m02_axi_awlen_temp);
                            aw_fsm <= Fine24SecondTransaction;
                        else
                            fifoIn_m02_axi_awlen <= "00000010";   -- 3 beats.
                            aw_fsm <= Fine24Wait;
                        end if; 
                        
                    when Fine24SecondTransaction =>
                        fifoIn_m02_axi_awaddr(11 downto 0) <= "000000000000";  -- must be on a 4096 byte boundary
                        fifoIn_m02_axi_awaddr(18 downto 12) <= std_logic_vector(unsigned(fifoIn_m02_axi_awaddr(18 downto 12)) + 1);
                        fifoIn_m02_axi_awlen <= std_logic_vector(1 - unsigned(fifoIn_m02_axi_awlen));
                        -- awFIFO_wrEn remains as it was in the previous state ("fine0")
                        aw_fsm <= Fine24Wait;
                        
                    when Fine24Wait =>
                        awFIFO_wrEn <= '0';
                        if (unsigned(awFIFO_wrDataCount) < 24) then
                            if AWchan = "10" then
                                aw_fsm <= done;
                            else
                                aw_fsm <= Fine0;
                                AWchan <= std_logic_vector(unsigned(AWchan) + 1);
                            end if;
                        end if;
                        
                    when done =>
                        aw_fsm <= done;
                    
                    when others =>
                        aw_fsm <= done;
                end case;
            end if;
            
            Tx216 <= unsigned(AWpacketInFrame) * 432;  -- 432 = 2*216;  Result is in units of 4 bytes, see comments in the introduction. Maximum possible value is 287*216*2 = bit less than 2^17.
            Tx216Del(18 downto 2) <= std_logic_vector(Tx216(16 downto 0));
            Tx216Del(1 downto 0) <= "00";  -- Tx216Del is in units of bytes.
            
            Tx216Plus384Del <= std_logic_vector(unsigned(Tx216Del) + 384);
            Tx216plus768Del <= std_logic_vector(unsigned(Tx216Del) + 768);
            
            if (aw_fsm = Fine24SecondTransaction or aw_fsm = Fine12SecondTransaction or aw_fsm = Fine0SecondTransaction) then
                DidSecondTransaction <= '1';
            else
                DidSecondTransaction <= '0';
            end if;
            
        end if;
    end process;
    
    o_HBMBuf0PacketCount <= HBMBuf0PacketCount;
    o_HBMBuf1PacketCount <= HBMBuf1PacketCount;
    o_HBMBuf2PacketCount <= (others => '0'); -- HBMBuf2PacketCount;
    o_HBMBuf3PacketCount <= (others => '0'); -- HBMBuf3PacketCount;
    
    awFIFO_din(29 downto 0) <= fifoIn_m02_axi_awaddr;
    awFIFO_din(37 downto 30) <= fifoIn_m02_axi_awlen;
    
    -- aw transactions go into a FIFO
    xpm_awfifo_sync_inst : xpm_fifo_sync
    generic map (
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "distributed", -- String
        FIFO_READ_LATENCY => 0,     -- DECIMAL
        FIFO_WRITE_DEPTH => 32,     -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 6,   -- DECIMAL
        READ_DATA_WIDTH => 38,      -- DECIMAL
        READ_MODE => "fwft",        -- String
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "0404", -- String
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 38,     -- DECIMAL
        WR_DATA_COUNT_WIDTH => 6    -- DECIMAL
    )
    port map (
        almost_empty => open,
        almost_full => open,
        data_valid => open,      -- 1-bit output: Read Data Valid
        dbiterr => open,         -- 1-bit output: Double Bit Error: 
        dout => awFIFO_dout,     -- READ_DATA_WIDTH-bit output: Read Data.
        empty => awFIFO_empty,   -- 1-bit output: Empty Flag:
        full => open,            -- 1-bit output: Full Flag: 
        overflow => open,        -- 1-bit output: Overflow: 
        prog_empty => open,      -- 1-bit output: Programmable Empty.
        prog_full => open,       -- 1-bit output: Programmable Full.
        rd_data_count => open,   -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count.
        rd_rst_busy => open,     -- 1-bit output: Read Reset Busy.
        sbiterr => open,         -- 1-bit output: Single Bit Error.
        underflow => open,       -- 1-bit output: Underflow.
        wr_ack => open,          -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => awFIFO_wrDataCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,     -- 1-bit output: Write Reset Busy.
        din => awFIFO_din,       -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',    -- 1-bit input: Double Bit Error Injection: Injects a double bit error if ecc is enabled.
        injectsbiterr => '0',    -- 1-bit input: Single Bit Error Injection.
        rd_en => awFIFO_rdEn,    -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO. 
        rst => awFIFO_rst,       -- 1-bit input: Reset: Must be synchronous to wr_clk. 
        sleep => '0',            -- 1-bit input: Dynamic power saving- If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_axi_clk,     -- 1-bit input: Write clock: Used for write operation.
        wr_en => awFIFO_wrEn     -- 1-bit input: Write Enable
    );
    
    m02_axi_awvalid  <= not awFIFO_empty;
    awFIFO_rdEn <= (not awFIFO_empty) and m02_axi_awready;
    m02_axi_awaddr <= awFIFO_dout(29 downto 0);
    m02_axi_awlen <= awFIFO_dout(37 downto 30);
    
    ---------------------------------------------------------------------------------------------------
    -- Read side
    -- Read data in the correct order and send on 96 bit words to the beamformer
    -- 
    -- Tasks :
    --   - Generate the m02_axi_ar* bus (m02_axi_arvalid, m02_axi_arready, m02_axi_araddr, m02_axi_arlen)
    --   - Buffer data as it comes back from the HBM
    --   - put it out to the beamformer on a 96 bit bus.
    --
    --  Data coming back from the HBM goes to a 512 deep by 256 bit wide FIFO.
    --
    -- Read Order
    -- ----------
    -- Reminder : 
    --    - 216 fine channels per packet -> 27 x 256 bit words in the HBM, in consecutive memory locations.
    --    - HBM byte Address = 4 * (V*2^16 + T*216 + F)
    --                   where F = Fine Channel  (0 to 215)
    --                         V = virtual channel (0 to up to 1023)
    --                         T = Time within the block (0 to 288, assuming the first stage corner turn is 27 LFAA blocks) 
    --      HBM word address (32 byte words) = V * 2^13 + T*27 + F/8 
    --                   With F being 0, 48, 96, 144, 196; i.e. F/8 = 0, 6, 12, 18, 24
    -- Processing for the beamformer needs data for all stations for a small number of fine channels.
    -- In order to get sufficient bandwidth (need about 34 Gb/sec minimum) from the HBM, bursts of at least 3 words are needed.
    -- Actual read bursts are mostly 6; 27 = 4*6 + 3, so 4 reads of 6 words and 1 read of 3 words per 216 fine channels.
    -- The output packets to PST consist of 32 times x 24 fine channels
    --   - Note 3 LFAA packets => 32 times = (3 LFAA packets) * (2048 samples/LFAA packet) / (256 samples/PST FFT) * (4/3 oversampling)
    -- So the output order is:
    --
    --    For timeGroup = 0:(<time samples per corner turn>/32 - 1)    -- For 60 ms corner turn, this is 0:(288/32-1) = 0:8
    --       For Coarse = 0:(i_coarse-1)                               -- For 512 stations, 2 coarse, this is 0:1
    --          For FineGroup = 0:4                                    -- Process 4 groups of fine channels (0:47, 48:95, 96:143, 144:191, 191:215)
    --             For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
    --                For Station = 0:(i_stations-1)
    --                   * Read either 6 HBM words or 3, depending on FineGroup.
    --  
    --  So first few reads for the 512 stations, two channels case is : 
    --    TimeGroup 0, Time = 0, station 0, coarse 0, words 0-5
    --    TimeGroup 0, Time = 0, station 1, coarse 0, words 0-5
    --    ...
    --    TimeGroup 0, Time = 0, station 511, coarse 0, words 0-5
    --    TimeGroup 0, Time = 1, station 0, coarse 0, words 0-5
    --    ...
    --    TimeGroup 0, Time = 1, station 511, coarse 0, words 0-5
    --    ...
    ---------------------------------------------------------------------------------------------------
    
    rdFineExt <= "0000000000000" & rdFine;
    finePlusTime <= std_logic_vector(unsigned(rdFineExt) + unsigned(rdTime_x_27));
    
    ARvirtualChannel <= std_logic_vector(unsigned(rdStation) + unsigned(coarse_x_stations(9 downto 0)));
    
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
        
            stationsMinusOne <= std_logic_vector(unsigned(i_stations) - 1);
            coarseMinusOne <= std_logic_vector(unsigned(i_coarse) - 1);
            packetsPerFrameMinusOne <= std_logic_vector(unsigned(i_packetsPerFrame) - 1);
            rdTime_x_27 <= std_logic_vector(unsigned(rdTime) * 54);  -- 54 = 27*2; rdTime is 9 bit; vhdl makes the answer 18 bits.
            totalStations <= i_stations;
            coarse_x_stations <= unsigned(rdCoarse) * unsigned(totalStations);  -- 9 bit value x 11 bit value = 20 bit result.
        
            -- some tests for the loops registered here to ensure easy timing:
            if (rdStation = stationsMinusOne(9 downto 0)) then
                lastStation <= '1';
            else
                lastStation <= '0';
            end if;
            if rdCoarse = coarseMinusOne(8 downto 0) then
                lastCoarse <= '1';
            else
                lastCoarse <= '0';
            end if;
            if rdTime(4 downto 0) = "11111" then
                lastTimeIn32 <= '1';
            else
                lastTimeIn32 <= '0';
            end if;
            if (rdTime = packetsPerFrameMinusOne(8 downto 0)) then
                lastTime <= '1';
            else
                lastTime <= '0';
            end if;
            if unsigned(rdFine) = 24 then
                lastFine <= '1';
            else
                lastFine <= '0';
            end if;
       
        
            if cdc_dest_req = '1' and cdc_dest_out(78) = '1' then -- this is the last packet in the frame
                HBMRdBuffer <= HBMWrBuffer;   -- HBMRdBuffer is the top bit of the HBM address (bit 29)
                rdFrameCount <= frameCount32;
                rd_fsm <= start;
                
                rdTime <= (others => '0');     --outermost loop, run from 0 to (i_packetsPerFrame-1)
                rdCoarse <= (others => '0');   --    next level loop, runs from 0 to (i_coarse-1)
                rdFine <= (others => '0');     --        next level loop, steps through 0, 6, 12, 18, 24 (measured in 32-byte words in the HBM)
                rdStation <= (others => '0');  --            innermost loop, runs from 0 to (i_stations-1)
           
            else
                case rd_fsm is
                    when start => 
                        m02_axi_arvalid <= '0';
                        rd_fsm <= get_addr;
                        
                    when get_addr =>
                        -- 
                        m02_axi_ARaddrInt(29) <= HBMRdBuffer;
                        m02_axi_ARaddrInt(28) <= ARvirtualChannel(0);  -- Low bit of the virtual channel is bit 28 of the HBM address, so that successive reads go to different pieces of the HBM.
                        m02_axi_ARaddrInt(27 downto 19) <= ARvirtualChannel(9 downto 1);  -- virtual channel = rdStation + i_stations*rdCoarse;
                        m02_axi_ARaddrInt(18 downto 5) <= finePlusTime(13 downto 0);     --  = rdFine + 27 * rdTime * 2;
                        
                        --m02_axi_ARaddrInt(29 downto 28) <= HBMRdBuffer;
                        --m02_axi_ARaddrInt(27 downto 18) <= ARvirtualChannel;  -- virtual channel = rdStation + i_stations*rdCoarse;
                        --m02_axi_ARaddrInt(17 downto 5) <= finePlusTime(12 downto 0);     --  = rdFine + 27 * rdTime;
                        
                        araddr4K <= finePlusTime(6 downto 0); -- there are 128 different possible start addresses in a 4k block of memory.
                        m02_axi_ARaddrInt(4 downto 0) <= "00000";
                        if rdFine = "11000" then
                            m02_arlen_read6 <= '0';
                            m02_axi_arlenInt(6 downto 0) <= "0000010"; -- when rdFine = 24, get the last 3 beats in the channel = last 24 fine channels
                        else
                            m02_arlen_read6 <= '1';
                            m02_axi_arlenInt(6 downto 0) <= "0000101"; -- Otherwise 6 beats for each read = 48 fine channels
                        end if;
                        rd_fsm <= send_addr;
                    
                    when send_addr =>
                        -- 6 words from word address 123 or more gets us to address 128, and thus crosses a 4k boundary.
                        -- 3 words from word address 126 or 127 gets us to address 128, and thus crosses a 4k boundary. 
                        if ((unsigned(araddr4K) > 122) and (m02_arlen_read6 = '1')) or ((unsigned(araddr4K) > 125) and (m02_arlen_read6 = '0')) then
                            m02_axi_arlenInt(6 downto 0) <= std_logic_vector(127 - unsigned(araddr4K));
                            rd_fsm <= wait_ready_extra;
                        else
                            rd_fsm <= wait_ready;
                        end if;   
                        m02_axi_arvalid <= '1';
                    
                    when wait_ready_extra =>
                        -- This state only occurs if a read address had to be split into two pieces to avoid a burst that crosses a 4k address boundary.
                        if m02_axi_arready = '1' then
                            m02_axi_ARaddrInt(11 downto 0) <= "000000000000"; -- The next read must be on a 4k boundary (that's why we came to this state)
                            m02_axi_ARaddrInt(18 downto 12) <= std_logic_vector(unsigned(m02_axi_ARaddrInt(18 downto 12)) + 1);
                            
                            if m02_arlen_read6 = '1' then
                                m02_axi_arlenInt(3 downto 0) <= std_logic_vector(4 - unsigned(m02_axi_arlenInt(3 downto 0))); -- so, e.g. previous read was len 0 (1 word), next read will be len 4 (5 words), for 6 words total.
                            else
                                m02_axi_arlenInt(0) <= not m02_axi_arlenInt(0); -- previous value could be 0 or 1, and next value is 1 or 0, for 3 word to read total (0=1 word, 1 = 2 words)
                            end if;
                            rd_fsm <= wait_ready;
                        end if;
                    
                    when wait_ready => 
                        if m02_axi_arready = '1' then
                            rd_fsm <= update_stationFineCoarse;
                            m02_axi_arvalid <= '0';
                        end if;
                    
                    when update_stationFineCoarse =>
                        m02_axi_arvalid <= '0';
                        if lastStation = '1' then
                            rdStation <= (others => '0');
                            if lastTimeIn32 = '1' then
                                if lastFine = '1' then
                                    rdFine <= "00000";
                                    if lastCoarse = '1' then
                                        rdCoarse <= "000000000";
                                        if lastTime = '1' then
                                            rdTime <= "000000000";
                                            rd_fsm <= done;
                                        else
                                            rdTime <= std_logic_vector(unsigned(rdTime) + 1);
                                            rd_fsm <= check_space;
                                        end if;
                                    else
                                        rdCoarse <= std_logic_vector(unsigned(rdCoarse) + 1);
                                        rdTime(4 downto 0) <= "00000";
                                        rd_fsm <= check_space;
                                    end if;
                                else
                                    rdFine <= std_logic_vector(unsigned(rdFine) + 6);
                                    rdTime(4 downto 0) <= "00000";
                                    rd_fsm <= check_space;
                                end if;
                            else
                                rdTime <= std_logic_vector(unsigned(rdTime) + 1);
                                rd_fsm <= check_space;
                            end if;
                        else
                            rdStation <= std_logic_vector(unsigned(rdStation) + 1);
                            rd_fsm <= check_space;
                        end if;
                    
                    when check_space => 
                        m02_axi_arvalid <= '0';
                        -- Make sure we have space in the output buffer for any packets that come back.
                        if ((unsigned(fifoSpaceUsed) < 480) and (unsigned(outstandingRequests) < 60)) then
                            rd_fsm <= get_addr;  -- Note: 480 and 60 are arbitrarily chosen to be safe values; should still work with 506 and 63.
                        end if;
                        
                    when done =>
                        m02_axi_arvalid <= '0';
                        rd_fsm <= done;
                    
                    when others =>
                        rd_fsm <= done;
                end case;
            end if;
            
            -- x6 is an overestimate, since some of those outstanding requests are for only 3 words.
            outstandingRequests_x6 <= std_logic_vector(unsigned(outstandingRequests_x4) + unsigned(outstandingRequests_x2));
            fifoSpaceUsed <= std_logic_vector(unsigned(outstandingRequests_x6) + unsigned(rdataFIFO_wrDataCount));
            
            if rd_fsm = start then
                outstandingRequests <= "0000000";
            elsif newRequest = '1' and requestDone = '0' then
                outstandingRequests <= std_logic_vector(unsigned(outstandingRequests) + 1);
            elsif newRequest = '0' and requestDone = '1' then
                outstandingRequests <= std_logic_vector(unsigned(outstandingRequests) - 1);
            end if;
            
            if (rd_fsm = send_addr or (rd_fsm = wait_ready_extra and m02_axi_arready = '1')) then
                newRequest <= '1';
            else
                newRequest <= '0';
            end if;
            if m02_axi_rvalid = '1' and m02_axi_rlast = '1' then
                requestDone <= '1';
            else
                requestDone <= '0';
            end if;
            
            if (cdc_dest_req = '1' and cdc_dest_out(78) = '1' and (rd_fsm /= done or outstandingRequests /= "0000000")) then
                o_readoutError <= '1';
            else
                o_readoutError <= '0';
            end if;
            
            if (rd_fsm = wait_ready_extra) then
                Non4Kread <= '1';
            else
                Non4Kread <= '0';
            end if;
            
        end if;
    end process;
    
    m02_axi_arlenInt(7) <= '0';
    
    m02_axi_ARlen <= m02_axi_ARlenInt;
    m02_axi_ARaddr <= m02_axi_ARaddrInt;
    
    outstandingRequests_x4 <= '0' & outstandingRequests & "00";
    outstandingRequests_x2 <= "00" & outstandingRequests & '0';
    
    xpm_fifo_rdata_inst : xpm_fifo_async
    generic map (
        CDC_SYNC_STAGES => 2,       -- DECIMAL
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "auto", -- String
        FIFO_READ_LATENCY => 2,     -- DECIMAL
        FIFO_WRITE_DEPTH => 512,    -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 10,  -- DECIMAL
        READ_DATA_WIDTH => 256,     -- DECIMAL  -- 256 data bits, plus one extra bit for last in a transaction. Burst are 12, 12, and 3 words, for the 27 words in a single packet.
        READ_MODE => "std",         -- String
        RELATED_CLOCKS => 0,        -- DECIMAL
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "0404", -- String
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 256,    -- DECIMAL
        WR_DATA_COUNT_WIDTH => 10   -- DECIMAL
    )
    port map (
        almost_empty => open,   -- 1-bit output: Almost Empty : When asserted, this signal indicates that only one more read can be performed before the FIFO goes to empty.
        almost_full => open,    -- 1-bit output: Almost Full: When asserted, this signal indicates that only one more write can be performed before the FIFO is full.
        data_valid => open,     -- 1-bit output: Read Data Valid: When asserted, this signal indicates that valid data is available on the output bus (dout).
        dbiterr => open,        -- 1-bit output: Double Bit Error: Indicates that the ECC decoder detected a double-bit error and data in the FIFO core is corrupted.
        dout => rdataFIFO_dout,   -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven when reading the FIFO.
        empty => rdataFIFO_empty, -- 1-bit output: Empty Flag: When asserted, this signal indicates that the FIFO is empty. Read requests are ignored when the FIFO is empty, initiating a read while empty is not destructive to the FIFO.
        full => open,             -- 1-bit output: Full Flag: When asserted, this signal indicates that the FIFO is full. 
        overflow => open,         -- 1-bit output: Overflow: 
        prog_empty => open,       -- 1-bit output: Programmable Empty.
        prog_full => open,        -- 1-bit output: Programmable Full: 
        rd_data_count => rdataFIFO_rdDataCount,    -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates the number of words read from the FIFO.
        rd_rst_busy => open,      -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO read domain is currently in a reset state.
        sbiterr => open,          -- 1-bit output: Single Bit Error: Indicates that the ECC decoder detected and fixed a single-bit error.
        underflow => open,        -- 1-bit output: Underflow: Indicates that the read request (rd_en) during the previous clock cycle was rejected because the FIFO is empty.
        wr_ack => open,           -- 1-bit output: Write Acknowledge: This signal indicates that a write request (wr_en) during the prior clock cycle is succeeded.
        wr_data_count => rdataFIFO_wrDataCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
        wr_rst_busy => open,      -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO write domain is currently in a reset state.
        din => m02_axi_rdata,     -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
        injectdbiterr => '0',     -- 1-bit input: Double Bit Error Injection: Injects a double bit error the ECC feature is used on block RAMs or UltraRAM macros.
        injectsbiterr => '0',     -- 1-bit input: Single Bit Error Injection: Injects a single bit error if the ECC feature is used on block RAMs or UltraRAM macros.
        rd_clk => i_BF_clk,      -- 1-bit input: Read clock: Used for read operation. rd_clk must be a free running clock.
        rd_en => rdataFIFO_rdEn,  -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO.
        rst => rdataFIFO_rst,     -- 1-bit input: Reset: Must be synchronous to wr_clk. 
        sleep => '0',             -- 1-bit input: Dynamic power saving: If sleep is High, the memory/fifo block is in power saving mode.
        wr_clk => i_axi_clk,       -- 1-bit input: Write clock: Used for write operation. wr_clk must be a free running clock.
        wr_en => m02_axi_rvalid   -- 1-bit input: Write Enable: If the FIFO is not full, asserting this signal causes data (on din) to be written to the FIFO. 
    );
    
    m02_axi_rready <= '1'; -- we never make more requests than we can fit in the FIFO.
    
    --

    -- Get i_packetsPerFrame(9:0), i_stations(10:0) and i_coarse(9:0) into the i_BF_clk domain
    process(i_axi_clk)
    begin
        if rising_edge(i_axi_clk) then
            if (rd_fsm = start) then
                axi2bf_src_send <= '1';
            elsif axi2bf_src_rcv = '1' then
                axi2bf_src_send <= '0';
            end if;
            axi2BF_dataIn(9 downto 0) <= i_packetsPerFrame;
            axi2BF_dataIn(20 downto 10) <= i_stations;
            axi2BF_dataIn(30 downto 21) <= i_coarse;
            axi2BF_dataIn(31) <= '0';
            axi2BF_dataIn(63 downto 32) <= rdFrameCount;
            
            axi2BF_dataIn(64) <= i_BFJonesSelect;
            axi2BF_dataIn(65) <= i_BFPhaseSelect;
            axi2BF_dataIn(97 downto 66) <= i_JonesSwitch;
            axi2BF_dataIn(129 downto 98) <= i_phaseSwitch;
            
            if BF2axi_dest_req = '1' then
                o_readoutClocks <= BF2axi_dest_out;
            end if;
            
        end if;
    end process;
    
    xpm_cdc_handshake_psc_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        WIDTH => 130          -- DECIMAL; range: 1-1024
    )
    port map (
        dest_out => axi2bf_dest_out, -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => axi2bf_dest_req, -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => axi2bf_src_rcv,   -- 1-bit output: Acknowledgement from destination logic that src_in has been
                                    -- received. This signal will be deasserted once destination handshake has fully completed,
        dest_ack => '1', -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_BF_clk, -- 1-bit input: Destination clock.
        src_clk => i_axi_clk,   -- 1-bit input: Source clock.
        src_in => axi2BF_dataIn,  -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => axi2BF_src_send   -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );

    -- Get stats from the Beamform clock domain back into the axi clock domain for viewing in the register interface
    xpm_cdc_handshake_BF2axi_inst : xpm_cdc_handshake
    generic map (
        DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
        DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_SYNC_FF => 2,    -- DECIMAL; range: 2-10
        WIDTH => 32          -- DECIMAL; range: 1-1024
    )
    port map (
        dest_out => bf2axi_dest_out,  -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
        dest_req => bf2axi_dest_req,  -- 1-bit output: Goes high for 1 clock to indicate that dest_out is valid.
        src_rcv => bf2axi_src_rcv,    -- 1-bit output: Acknowledgement from destination logic that src_in has been
                                      -- received. This signal will be deasserted once destination handshake has fully completed,
        dest_ack => '1',              -- 1-bit input: optional; required when DEST_EXT_HSK = 1
        dest_clk => i_axi_clk,        -- 1-bit input: Destination clock.
        src_clk => i_BF_clk,          -- 1-bit input: Source clock.
        src_in => BF2axi_dataIn,       -- WIDTH-bit input: Input bus that will be synchronized to the destination clock domain.
        src_send => BF2axi_src_send   -- 1-bit input: assert when src_rcv is deasserted, deasserted once src_rcv is asserted.
    );

    BF2axi_dataIn <= readoutClocks;
    
    BFFrameCountx32 <= BFFrameCount & "00000";
    
    process(i_BF_clk)
    begin
        if rising_edge(i_BF_clk) then
        
            BF2axi_transferCount <= std_logic_vector(unsigned(BF2axi_transferCount) + 1);
        
            if BF2axi_transferCount = "1111" then
                BF2axi_src_send <= '1';
            elsif bf2axi_src_rcv = '1' then
                BF2axi_src_send <= '0';
            end if;
            
            
            -- Get the number of stations, number of coarse channels and number of packets per frame from the clock domain crossing.
            -- Also, the clock crossing occurs once at the start of each frame, so is used to control the readout for a frame.
            if axi2bf_dest_req = '1' then
                BFpacketsPerFrame <= axi2bf_dest_out(9 downto 0);  -- Number of times per frame.
                BFstations <= axi2bf_dest_out(20 downto 10);
                BFcoarse <= axi2bf_dest_out(30 downto 21);
                BFFrameCount <= axi2bf_dest_out(63 downto 32);
                
                BFJonesSelect <= axi2bf_dest_out(64);
                BFPhaseSelect <= axi2bf_dest_out(65);
                BFJonesSwitch <= axi2bf_dest_out(97 downto 66);
                BFPhaseSwitch <= axi2bf_dest_out(129 downto 98);
            end if;
            
            axi2bf_dest_reqDel1 <= axi2bf_dest_req;
            
            if axi2bf_dest_reqDel1 = '1' then
                if (unsigned(BFFrameCount) >= unsigned(BFJonesSwitch)) then
                    curJonesBuffer <= BFJonesSelect;
                end if;
                if (unsigned(BFFrameCount) >= unsigned(BFPhaseSwitch)) then
                    curPhaseBuffer <= BFPhaseSelect;
                end if;
            end if;
            
            BFstationsMinusOne <= std_logic_vector(unsigned(BFstations) - 1);
            BFCoarseMinusOne <= std_logic_vector(unsigned(BFCoarse) - 1);
            BFpacketsPerFrameMinusOne <= std_logic_vector(unsigned(BFpacketsPerFrame) - 1);
            
            -- Get data out of the FIFO and pass it on.
            if axi2bf_dest_req = '1' then
                readout_fsm <= start48;
                
                outputFine <= "00000000";  -- This value is the current fine channel we are outputting divided by 3, so it runs from 0-15, 16-31, 32-47, 48-63, 64-71
                outputStation <= "0000000000";
                outputCoarse <= "0000000000";
                outputTime <= "0000000000";  -- counts through packets in time; maximum is 288, assuming a 60 ms corner turn.
                
            else
                case readout_fsm is
                    
                    when wait_start48 =>
                        if gapCount_eq0 = '1' then
                            readout_fsm <= start48;
                        end if; 
                    
                    when start48 =>  -- start of the read of a group of 48 fine channels.
                        readout_fsm <= wait_fifo; 
                
                    when wait_start24 =>
                        if gapCount_eq0 = '1' then
                            readout_fsm <= start24;
                        end if;
                
                    when start24 =>
                        readout_fsm <= wait_fifo;  -- start of a read of the last 24 fine channels.
                
                    when wait_fifo =>
                        if (unsigned(rdataFIFO_rdDataCount) > 2) then
                            readout_fsm <= p0;
                        end if;
                    
                    -- The 8 states p0, p1, p2, p3, p4, p5, p6, p7 read 3 x 256 bit words from the FIFO and output 8 x 96 bit words to the beamformers.
                    when p0 =>
                        readout_fsm <= p1;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1); -- "outputFine" is the fine channel/3. 3 fine channels are sent every clock, so it goes up by 1.
                    
                    when p1 =>
                        readout_fsm <= p2;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p2 =>
                        readout_fsm <= p3;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p3 =>
                        readout_fsm <= p4;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p4 =>
                        readout_fsm <= p5;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p5 =>
                        readout_fsm <= p6;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p6 =>
                        readout_fsm <= p7;
                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                    
                    when p7 =>
                        if ((unsigned(outputFine) = 71) and
                            (BFlastStation = '1') and 
                            (BFlastCoarse = '1') and
                            (BFlastTime = '1')) then
                            readout_fsm <= done;
                        elsif ((unsigned(outputFine) = 15 or unsigned(outputFine) = 31 or unsigned(outputFine) = 47) and
                               (BFlastStation = '1') and
                               (BFlastTimeIn32 = '1')) then
                            readout_fsm <= wait_start48;
                        elsif ((unsigned(outputFine) = 63) and
                               (BFlastStation = '1') and
                               (BFlastTimeIn32 = '1')) then
                            readout_fsm <= wait_start24;
                        elsif (unsigned(rdataFIFO_rdDataCount) > 3) then
                            -- The read data count on the FIFO has to be > 3 here, since the FIFO read in the p6 state has not yet propagated 
                            -- through to the rdataFIFO_rdDataCount output. In the wait_fifo state, the correct condition is >2.
                            readout_fsm <= p0;
                        else
                            readout_fsm <= wait_fifo;
                        end if;
                        
                        -- counters keeping track of the loops are:
                        --   outputFine
                        --   outputStation
                        --   outputTime
                        --   outputCoarse
                        --
                        -- The data output order is:
                        --
                        --    For timeGroup = 0:(<time samples per corner turn>/32 - 1)    -- For 60 ms corner turn, this is 0:(288/32-1) = 0:8
                        --       For Coarse = 0:(i_coarse-1)                               -- For 512 stations, 2 coarse, this is 0:1
                        --          For FineGroup = 0:4                                    -- Process 4 groups of fine channels (0:47, 48:95, 96:143, 144:191, 191:215)
                        --             For Time = timeGroup * 32 + (0:31)                  -- 32 times needed for an output packet
                        --                For Station = 0:(i_stations-1)
                        --                   * Read either 6 HBM words or 3, depending on FineGroup.
                        --                   For fine_offset = 0:3:45  (or 0:3:21 for the last group of fine channels, where fine_channel_start = 192)
                        --                      Send a 96 bit word (i.e. 3 fine channels).
                        if unsigned(outputFine) = 15 or unsigned(outputFine) = 31 or unsigned(outputFine) = 47 or unsigned(outputFine) = 63 or unsigned(outputFine) = 71 then
                            if BFlastStation = '1' then
                                outputStation <= "0000000000";
                                if BFlastTimeIn32 = '1' then
                                    if (unsigned(outputFine) = 71) then
                                        outputFine <= "00000000";
                                        if BFlastCoarse = '1' then
                                            outputCoarse <= (others => '0');
                                            if BFlastTime = '1' then
                                                outputTime <= "0000000000";
                                            else
                                                outputTime <= std_logic_vector(unsigned(outputTime) + 1);
                                            end if;
                                        else
                                            outputCoarse <= std_logic_vector(unsigned(outputCoarse) + 1);
                                            outputTime <= std_logic_vector(unsigned(outputTime) - 31);
                                        end if;
                                    else
                                        outputFine <= std_logic_vector(unsigned(outputFine) + 1);  -- go on to the next "FineGroup"
                                        outputTime <= std_logic_vector(unsigned(outputTime) - 31); 
                                    end if;
                                else
                                    if (unsigned(outputFine) = 71) then
                                        outputFine <= "01000000"; -- = 64; The last group of fine channels sent only has 24 fine channel, so we only rewind by (24/3)-1 = 7.
                                    else
                                        outputFine <= std_logic_vector(unsigned(outputFine) - 15);
                                    end if;
                                    outputTime <= std_logic_vector(unsigned(outputTime) + 1);
                                end if;
                            else
                                if (unsigned(outputFine) = 71) then
                                    outputFine <= "01000000"; -- = 64; The last group of fine channels sent only has 24 fine channels, so we only rewind by (24/3)-1 = 7.
                                else
                                    outputFine <= std_logic_vector(unsigned(outputFine) - 15);
                                end if;
                                outputStation <= std_logic_Vector(unsigned(outputStation) + 1);
                            end if;
                        else
                            outputFine <= std_logic_vector(unsigned(outputFine) + 1);
                        end if;
                        
                    when done =>
                        readout_fsm <= done;
                        
                    when others =>
                        readout_fsm <= done;
                end case;
            end if;
            
            if readout_fsm_del1 = start48 then  -- need to use del1 because in the first run through minGapDataIn is not valid until one clock after we are in the state start48.
                gapCount <= minGapDataIn; -- std_logic_vector(to_unsigned(minGap,20));
            elsif readout_fsm = start24 then
                gapCount <= '0' & minGapDataIn(19 downto 1); -- half the packets, half the time
            elsif unsigned(gapCount) /= 0 then
                gapCount <= std_logic_vector(unsigned(gapCount) - 1);
            end if;
            
            if axi2bf_dest_req = '1' then
                clocksPerCornerTurnOut <= (others => '0');
            elsif readout_fsm /= done then
                clocksPerCornerTurnOut <= std_logic_vector(unsigned(clocksPerCornerTurnOut) + 1);
            end if;
            
            if readout_fsm = done and readout_fsm_del1 /= done then
                readoutClocks <= clocksPerCornerTurnOut;
            end if;
            
            
            
            --!!! When gapCount hits zero we should output a signal to tell the beamformers to start sending the packets from the output buffers.
            --!!! Should ensure that minGap is always the limiting factor, i.e. minGap should be longer than the time taken to read out the data
            --!!! to the beamformers.
            --!!! Time taken to read out 48 fine channels (=two packets worth) of data to the beamformers is 512 * (number of stations)
            --!!! Time required to send those packets on the interface is 2 * g_PST_BEAMS * 800 + 100 (Note : 800 and 100 are defined due to the way the generics are set. These could be slightly smaller.)
            
            
            if (unsigned(gapCount) = 0) then
                gapCount_eq0 <= '1';
            else
                gapCount_eq0 <= '0';
            end if;
             
            
            if outputStation = BFstationsMinusOne(9 downto 0) then
                BFlastStation <= '1';
            else
                BFlastStation <= '0';
            end if;
            
            if unsigned(outputStation) = 0 then
                BFfirstStationDel1 <= '1';
            else
                BFfirstStationDel1 <= '0';
            end if; 
            
            if outputTime(4 downto 0) = "11111" then
                BFlastTimeIn32 <= '1';
            else
                BFlastTimeIn32 <= '0';
            end if;
            
            if (outputCoarse = BFcoarseMinusOne) then
                BFlastCoarse <= '1';
            else
                BFlastCoarse <= '0';
            end if;
            
            if (outputTime = BFpacketsPerFrameMinusOne) then
                BFlastTime <= '1';
            else
                BFlastTime <= '0';
            end if;
            
            
            readout_fsm_del1 <= readout_fsm;
            readout_fsm_del2 <= readout_fsm_del1;
            readout_fsm_del3 <= readout_fsm_del2;
            readout_fsm_del4 <= readout_fsm_del3;
            
            outputFineDel1 <= outputFine;
            outputFineDel2 <= outputFineDel1;
            outputFineDel3 <= outputFineDel2;
            outputFineDel4 <= outputFineDel3;
            
            outputStationDel1 <= outputStation;
            outputStationDel2 <= outputStationDel1;
            outputStationDel3 <= outputStationDel2;
            outputStationDel4 <= outputStationDel3;
            
            BFlastStationDel2 <= BFlastStation;  -- BFLastStation is already one clock behind outputStation
            BFlastStationDel3 <= BFlastStationDel2;
            BFlastStationDel4 <= BFlastStationDel3;
            
            BFfirstStationDel2 <= BFfirstStationDel1;
            BFfirstStationDel3 <= BFfirstStationDel2;
            BFfirstStationDel4 <= BFfirstStationDel3;
            
            outputCoarseDel1 <= outputCoarse;
            outputCoarseDel2 <= outputCoarseDel1;
            outputCoarseDel3 <= outputCoarseDel2;
            outputCoarseDel4 <= outputCoarseDel3;
            
            packetCountDel1 <= outputTime;
            packetCountDel2 <= packetCountDel1;
            packetCountDel3 <= std_logic_vector(unsigned(packetCountDel2_37bit) + unsigned(packetCountDel2_37bit_x2)); -- multiply by 3, since output packet numbers are in units of 64 LFAA samples, not 192.
            packetCountDel4 <= std_logic_vector(unsigned(packetCountDel3) + unsigned(BFFrameCountx32));
            
            outputTimeDel1 <= outputTime(9 downto 0);
            outputTimeDel2 <= outputTimeDel1;
            outputTimeDel3 <= outputTimeDel2;
            outputTimeDel4 <= outputTimeDel3;
            
            if readout_fsm = p0 or readout_fsm = p2 or readout_fsm = p5 then
                rdataFIFO_rdEn <= '1';
            else
                rdataFIFO_rdEn <= '0';
            end if;
            
            -- output register; converts 256 bit words from the FIFO to 96 bit words on the output bus
            -- Number of valid bits in the output register repeats every 3 reads from the FIFO :
            -- fsm state   Action
            --  p0  read FIFO ->                   0
            --  p1                (0+256 =)      256       --> 96 bits out
            --  p2  read FIFO ->  (256-96 =)     160       --> 96 bits out
            --  p3                (160+256-96 =) 320       --> 96 bits out
            --  p4                (320-96 =)     224       --> 96 bits out
            --  p5  read FIFO ->  (224-96 =)     128       --> 96 bits out
            --  p6                (128+256-96 =) 288       --> 96 bits out
            --  p7                (288-96 =)     192       --> 96 bits out
            --  p0  read FIFO ->  (192-96 =)      96       --> 96 bits out
            --                    (96+256-96 =)  256
            --                    etc.
            
            if readout_fsm_del3 = p0 then  -- del3 to account for the fifo read latency.
                outputBuffer(255 downto 0) <= rdataFIFO_dout;
            elsif readout_fsm_del3 = p1 then
                outputBuffer(159 downto 0) <= outputBuffer(255 downto 96);
            elsif readout_fsm_del3 = p2 then
                outputBuffer(63 downto 0) <= outputBuffer(159 downto 96);
                outputBuffer(319 downto 64) <= rdataFIFO_dout;
            elsif readout_fsm_del3 = p3 then
                outputBuffer(223 downto 0) <= outputBuffer(319 downto 96);
            elsif readout_fsm_del3 = p4 then
                outputBuffer(127 downto 0) <= outputBuffer(223 downto 96);
            elsif readout_fsm_del3 = p5 then
                outputBuffer(287 downto 32) <= rdataFIFO_dout;
                outputBuffer(31 downto 0) <= outputBuffer(127 downto 96);
            elsif readout_fsm_del3 = p6 then
                outputBuffer(191 downto 0) <= outputBuffer(287 downto 96);
            elsif readout_fsm_del3 = p7 then
                outputBuffer(95 downto 0) <= outputBuffer(191 downto 96);
            end if;
            
            outputCoarse_x_stations <= unsigned(outputCoarse) * unsigned(BFstations);  -- 9 bit value x 11 bit value = 20 bit result.
            outputVirtualChannelDel2 <= std_logic_vector(unsigned(outputStationDel1) + unsigned(outputCoarse_x_stations(9 downto 0)));
            outputVirtualChannelDel3 <= outputVirtualChannelDel2;
            outputVirtualChannelDel4 <= outputVirtualChannelDel3;
            
            -- Actual outputs
            outputBufferDel1 <= outputBuffer(95 downto 0);
            outputBufferDel2 <= outputBufferDel1;
            outputBufferDel3 <= outputBufferDel2;
            outputBufferDel4 <= outputBufferDel3;
            outputBufferDel5 <= outputBufferDel4;
            outputBufferDel6 <= outputBufferDel5;
            o_data <= outputBufferDel6;  -- o_data has a 6 clock latency relative to the other beamformer outputs (o_valid, o_fine etc.). This is required by the beamformer. 
            
            if outputBufferDel6(7 downto 0) = "10000000" or outputBufferDel6(15 downto 8) = "10000000" or outputBufferDel6(23 downto 16) = "10000000" or outputBufferDel6(31 downto 24) = "10000000" then
                o_flagged(0) <= '1';  -- "o_flagged" aligns with o_data
            else
                o_flagged(0) <= '0';
            end if;
            if outputBufferDel6(39 downto 32) = "10000000" or outputBufferDel6(47 downto 40) = "10000000" or outputBufferDel6(55 downto 48) = "10000000" or outputBufferDel6(63 downto 56) = "10000000" then
                o_flagged(1) <= '1';
            else
                o_flagged(1) <= '0';
            end if;
            if outputBufferDel6(71 downto 64) = "10000000" or outputBufferDel6(79 downto 72) = "10000000" or outputBufferDel6(87 downto 80) = "10000000" or outputBufferDel6(95 downto 88) = "10000000" then
                o_flagged(2) <= '1';
            else
                o_flagged(2) <= '0';
            end if;
            
            
            if (readout_fsm_del4 = p0 or readout_fsm_del4 = p1 or readout_fsm_del4 = p2 or readout_fsm_del4 = p3 or
                readout_fsm_del4 = p4 or readout_fsm_del4 = p5 or readout_fsm_del4 = p6 or readout_fsm_del4 = p7) then
                o_valid <= '1';
            else
                o_valid <= '0';
            end if;
            o_fine <= outputFineDel4;        -- 8 bits
            o_coarse <= outputCoarseDel4;
            o_firstStation <= BFfirstStationDel4;
            o_lastStation  <= BFlastStationDel4;
            o_timeStep <= outputTimeDel4(4 downto 0);
            o_virtualChannel <= outputVirtualChannelDel4;  -- 10 bits
            o_packetCount <= packetCountDel4;
            
            o_jonesBuffer <= curJonesBuffer; -- out std_logic;
            o_phaseBuffer <= curPhaseBuffer; -- out std_logic;
            
            --minGapDataIn <= std_logic_vector(unsigned(BFstations_x512) + unsigned(BFstations_x32) + unsigned(BFstations_x16));
            -- update to deal small number of stations
--            I suggest setting  "minGapDataIn" to  "g_PST_BEAMS * 2 * 800 + 200"
--            Or better, instead of using g_PST_BEAMS, use the actual number of beams enabled in the registers
--            for number of enabled beams us "axi2bf_dest_out" from the level up - ct_atomic_pst_wrapper.vhd
--            c_minGapData
--            i_beams_enabled
            beams_enabled   <= i_beams_enabled;
--            minGap_a        <= x"00" & c_minGapData;
--            minGap_b        <= x"000" & beams_enabled;
            minGapDataIn    <= std_logic_vector(unsigned(c_minGapData) * unsigned(beams_enabled));
            
            -- Reconstruct the data we expect to see if in the test mode, where the data was replaced with meta data.
            outputFineDel4x2 <= outputFineDel3(6 downto 0) & '0';
            
            metaRecon(7 downto 0) <= std_logic_vector(unsigned(outputFineDel4) + unsigned(outputFineDel4x2));  -- outputFineDel4 should align with the data in outputBuffer(31:0)
            metaRecon(19 downto 8) <= "00" & outputTimeDel4;
            metaRecon(31 downto 20) <= "00" & outputVirtualChannelDel4;
            
            if ((metaRecon /= outputBufferDel1(31 downto 0)) and o_valid = '1') then
                dataMismatchInt <= '1';
            else
                dataMisMatchInt <= '0';
            end if;
            o_dataMismatchBFclk <= dataMisMatchInt;
        end if;
    end process;
    
    packetCountDel2_37bit(9 downto 0) <= packetCountDel2;
    packetCountDel2_37bit(36 downto 10) <= (others => '0');
    packetCountDel2_37bit_x2(10 downto 0) <= packetCountDel2 & '0';
    packetCountDel2_37bit_x2(36 downto 11) <= (others => '0');
    
    
    -- convert dataMisMatch to the i_axi_clk clock domain.
    xpm_cdc_pulsedm_inst : xpm_cdc_pulse
    generic map (
        DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        REG_OUTPUT => 0,     -- DECIMAL; 0=disable registered output, 1=enable registered output
        RST_USED => 0,       -- DECIMAL; 0=no reset, 1=implement reset
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    ) port map (
        dest_pulse => o_dataMisMatch, -- 1-bit output: Outputs a pulse the size of one dest_clk period when a pulse transfer is correctly initiated on src_pulse input.
        dest_clk => i_axi_clk,     -- 1-bit input: Destination clock.
        dest_rst => '0',     -- 1-bit input: optional; required when RST_USED = 1
        src_clk => i_BF_clk,       -- 1-bit input: Source clock.
        src_pulse => dataMismatchInt,   -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the destination clock domain.
        src_rst => '0'        -- 1-bit input: optional; required when RST_USED = 1
    );        
        
    -----------------------------------------------------------------------------------------------
    -- Minimum gap between data bursts.
    -- Two approaches :
    --  (1) Based on the time required to deliver the packets on the output interface
    --  (2) Based on the time required to deliver the data to the beamformers.
    -- 
    -- The second approach is used because:
    --   - It means the time taken to deliver data is small if there is a small number of virtual channels used, which is convenient for simulation.
    --   - It is the only way to do it if all virtual channels are used.
    -- However it does assume that the number of beams enabled is small enough that it doesn't saturate the output bus from the beamformers to the packetiser.
    --
    -- The beamformers deliver packets with an interval of 800 clocks
    -- So e.g. if there are 16 beamformers, then for a burst of 48 fine channels (which generates 2 packets)
    --
    -- Beam   (clock offset to deliver first packet)   (clock offset to start second packet)
    --  0       0                                       0+16*800 + 100    = 12900
    --  1       800                                     800+16*800 + 100  = 13700
    --  2       1600                                    1600+16*800 + 100 = 14500
    --  3       2400
    --  ...
    --  15      12000                                   12000 + 16*800 + 100 = 24900
    --
    -- This means the minimum gap from the start of one burst to the start of the next should be g_PST_BEAMS * 800 * 2 + 100
    --
    
    -- Unused version : minGapBeams  <= g_PST_BEAMS * 2 * 800 + 200;  -- minimum gap according to the number of beams we are producing
    
    -- The minimum gap can also be found based on the time required to send the input data to the beamformers.
    -- In this case, the minimum gap is 
    --  (32 times) * (16 fine channels groups) * (number of stations) = 512 * stations
    -- However, this will finish the full 1024 virtual channels faster than necessary. 
    -- The maximum time allowable for this amount of input data is actually (1+1/8) * 512 * stations
    -- Using a larger time relaxes the constraint on the HBM bandwidth.
    -- This uses (1+3/32) * 512 * BFstations = (512 + 32 + 16) * BFstations
    
    BFstations_x512 <= BFstations & "000000000";  -- 11 bits + 9 bits = 20 bits
    BFstations_x32 <= "0000" & BFstations & "00000";
    BFstations_x16 <= "00000" & BFstations & "0000";
    
end Behavioral;
