----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 31.10.2020 19:55:16
-- Module Name: buffer512x256_wrapper - Behavioral
-- Description: 
--  Buffer memory for ct_atomic_pst_out.vhd
--  The total buffer is 512 x 256bits. 
--  The memory is split into 8 pieces, each 32 bits wide.
--  This enables a write side that allows different addresses for each 32 bit piece.
--  
----------------------------------------------------------------------------------

library IEEE, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
Library xpm;
use xpm.vcomponents.all;
USE common_lib.common_pkg.ALL;

entity buffer512x256_wrapper is
    Port(
        i_clk    : in std_logic;
        i_we     : in std_logic_vector(7 downto 0);  -- Separate write enable for each 32 bit piece.
        i_wrAddr : in t_slv_9_arr(7 downto 0);       -- Address for each piece of the memory
        i_wrData : in t_slv_32_arr(7 downto 0);      -- write data for each 32 bit wide piece of the memory
        i_rdAddr : in std_logic_vector(8 downto 0);  -- Read address
        o_rdData : out std_logic_vector(255 downto 0)  -- read data, 3 cycle latency from the read address.
    );
end buffer512x256_wrapper;

architecture Behavioral of buffer512x256_wrapper is

    signal doutb : t_slv_32_arr(7 downto 0);
    signal we : t_slv_1_arr(7 downto 0);

begin


-- <-----Cut code below this line and paste into the architecture body---->

   -- xpm_memory_sdpram: Simple Dual Port RAM
   -- Xilinx Parameterized Macro, version 2020.1
    memblocksInst : for i in 0 to 7 generate
        xpm_memory_sdpram_inst : xpm_memory_sdpram
        generic map (
            ADDR_WIDTH_A => 9,         -- DECIMAL
            ADDR_WIDTH_B => 9,         -- DECIMAL
            AUTO_SLEEP_TIME => 0,            -- DECIMAL
            BYTE_WRITE_WIDTH_A => 32,        -- DECIMAL
            CASCADE_HEIGHT => 0,             -- DECIMAL
            CLOCKING_MODE => "common_clock", -- String
            ECC_MODE => "no_ecc",            -- String
            MEMORY_INIT_FILE => "none",      -- String
            MEMORY_INIT_PARAM => "0",        -- String
            MEMORY_OPTIMIZATION => "true",   -- String
            MEMORY_PRIMITIVE => "block",     -- String
            MEMORY_SIZE => 16384,            -- DECIMAL; Total memory size in bits = 512 * 32
            MESSAGE_CONTROL => 0,            -- DECIMAL
            READ_DATA_WIDTH_B => 32,         -- DECIMAL
            READ_LATENCY_B => 2,             -- DECIMAL
            READ_RESET_VALUE_B => "0",       -- String
            RST_MODE_A => "SYNC",            -- String
            RST_MODE_B => "SYNC",            -- String
            SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
            USE_MEM_INIT => 1,               -- DECIMAL
            WAKEUP_TIME => "disable_sleep",  -- String
            WRITE_DATA_WIDTH_A => 32,        -- DECIMAL
            WRITE_MODE_B => "no_change"      -- String
        )
        port map (
            dbiterrb => open,     -- 1-bit output: Status signal to indicate double bit error occurrence on the data output of port B.
            doutb => doutb(i),    -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
            sbiterrb => open,     -- 1-bit output: Status signal to indicate single bit error occurrence on the data output of port B.
            addra => i_wrAddr(i), -- ADDR_WIDTH_A-bit input: Address for port A write operations.
            addrb => i_rdAddr,    -- ADDR_WIDTH_B-bit input: Address for port B read operations.
            clka => i_clk,        -- 1-bit input: Clock signal for port A. Also clocks port B when parameter CLOCKING_MODE is "common_clock".
            clkb => i_clk,        -- 1-bit input: Clock signal for port B when parameter CLOCKING_MODE is "independent_clock". Unused when parameter CLOCKING_MODE is "common_clock".
            dina => i_wrData(i),  -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
            ena => '1',      -- 1-bit input: Memory enable signal for port A. Must be high on clock cycles when write operations are initiated. Pipelined internally.
            enb => '1',      -- 1-bit input: Memory enable signal for port B. Must be high on clock cycles when read operations are initiated. Pipelined internally.
            injectdbiterra => '0', -- 1-bit input: Controls double bit error injection on input data when ECC enabled.
            injectsbiterra => '0', -- 1-bit input: Controls single bit error injection on input data when ECC enabled 
            regceb => '1',         -- 1-bit input: Clock Enable for the last register stage on the output data path.
            rstb => '0',           -- 1-bit input: Reset signal for the final port B output register stage. Synchronously resets output port doutb to the value specified by parameter READ_RESET_VALUE_B.
            sleep => '0',          -- 1-bit input: sleep signal to enable the dynamic power saving feature.
            wea => we(i)         -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector for port A input data port dina. 1 bit wide when word-wide writes are used.
        );
        we(i)(0) <= i_we(i);
        
    end generate;
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            o_rdData(31 downto 0) <= doutb(0);
            o_rdData(63 downto 32) <= doutb(1);
            o_rdData(95 downto 64) <= doutb(2);
            o_rdData(127 downto 96) <= doutb(3);
            o_rdData(159 downto 128) <= doutb(4);
            o_rdData(191 downto 160) <= doutb(5);
            o_rdData(223 downto 192) <= doutb(6);
            o_rdData(255 downto 224) <= doutb(7);
        end if;
    end process;
    
    
end Behavioral;

