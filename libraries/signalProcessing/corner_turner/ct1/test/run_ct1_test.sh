python3 ct1_test.py -d test1.txt test1.yaml

python3 ct1_test.py -f PST_filtertaps.txt -d test2.txt test2.yaml

# PST_filtertaps.txt = file listing the filtertaps used in the firmware
# test2_ct1_out.txt = output data from corner turn 1 as written out by the simulation
# test2_fb_out.txt = outptu data from the filterbank+fine delay as written out by the simulation
# test2.txt = file written by the python code with the register settings, to be read in by the simulation
# test2.yaml = yaml file defining the test case, the main input to the python model.
python3 ct1_test.py -f PST_filtertaps.txt -t test2_ct1_out.txt -fb test2_fb_out.txt -d test2.txt  test2.yaml
