---------------------------------------------------------------------------------------------------
-- 
-- Corner Turner Coarse (CTC) - Main Package
--
---------------------------------------------------------------------------------------------------
--
-- Author  : Norbert Abel (norbert_abel@gmx.net)
-- Standard: VHDL'08
--
---------------------------------------------------------------------------------------------------
--
-- Provides basic functions and data records for the Coarse Corner Turner
--
---------------------------------------------------------------------------------------------------

library IEEE;
library dsp_top_lib;
use dsp_top_lib.dsp_top_pkg.all;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

package ctc_pkg is
    

    constant pc_CTC_INPUT_NUMBER  : natural := 1;          --how many input ports are there?
    constant pc_STATIONS_PER_PORT : natural := 6;          --how many stations are there per input port?
    constant pc_STATION_NUMBER    : natural := pc_STATIONS_PER_PORT * pc_CTC_INPUT_NUMBER;
    constant pc_CTC_OUTPUT_NUMBER : natural := 2;          --how many output ports are there (hpol+vpol = 1 output)?
    constant pc_CTC_MAX_STATIONS  : natural := 6;          --PISA: 512

    --------------------------------------------------
    -- WALL TIME
    --------------------------------------------------
    -- wall_time definitions moved to DSP_top_pkg.vhd 
    
    --------------------------------------------------
    -- COMMON 16 bit DATA
    --------------------------------------------------
   
        
    --------------------------------------------------
    -- HBM DATA
    --------------------------------------------------
    type t_ctc_hbm_data is record
        data : std_logic_vector(255 downto 0);
        meta : t_ctc_meta_a(15 downto 0);
    end record;    
    constant pc_CTC_HBM_DATA_RFI  : t_ctc_hbm_data := (data=>X"8080808080808080808080808080808080808080808080808080808080808080", meta=>(others=>pc_CTC_META_ZERO));    
    type t_ctc_hbm_data_a is array (integer range <>) of t_ctc_hbm_data;
    
    --------------------------------------------------
    -- COMMON HELPER FUNCTIONS
    --------------------------------------------------
    
    function log2_ceil(i: integer) return integer;
    function maxi (a: integer; b: integer) return integer;
    function THIS_IS_SIMULATION return boolean;
    -- Moved to dsp_top_pkg
    --function sel (s: boolean; a: integer; b: integer) return integer;
    
    function fixed_string(s: string; l:integer) return string;

end package ctc_pkg;



package body ctc_pkg is

    --------------------------------------------------
    -- COMMON HELPER FUNCTIONS
    --------------------------------------------------
    function log2_ceil(i: integer) return integer is
    begin
        return integer(ceil(log2(real(i))));
    end function;
    
    function maxi (a: integer; b: integer) return integer is
    begin
        if (a>b) then
            return a;
        else
            return b;
        end if;        
    end function;

    function THIS_IS_SIMULATION return boolean is
    begin
        -- synthesis translate_off
        return true;
        -- synthesis translate_on
        return false;
    end function;

    function fixed_string(s: string; l:integer) return string is
        variable o: string(1 to l);
    begin
        if s'length>=256 then
            o:=s(1 to l);
        else
            o(1 to s'length):=s;
        end if;    
        return o;
    end function;



end ctc_pkg;