#RESET_ALVEO=$1
#XCLBIN_DIR=trafgen_build_211109_185258

echo -e ""
#pushd ../../../../..
export GITREPO=$(pwd)
echo "GITREPO = $GITREPO"

if ["$XCLBIN_DIR" = ""]; then
    cd $GITREPO/output/
    echo "INFO: Searching for latest output build directory"
    ls -lr
    ##Find latest output directorys
    XCLBIN_DIR=`ls -dr * |head -n1 | tr -d '\n'`
    if [ -z $XCLBIN_DIR ]; then
        echo "FATAL: Could not find the latest fpga output directory"
        exit 1
    fi
    echo -e 
fi

echo -e 
echo "*********************************************************************************************************************************"
echo "INFO: Loading .ltr file from :$GITREPO/output/$XCLBIN_DIR"
echo "*********************************************************************************************************************************"
echo -e

source /tools/Xilinx/Vitis/2021.1/settings64.sh
debug_hw --vivado --host localhost --ltx_file $GITREPO/output/$XCLBIN_DIR/trafgen.ltx 
